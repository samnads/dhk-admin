$("#EmpWorkPrint").click(function () {
    var divContents = $("#employeeWorkPrint").html();
    var maid = $('#maid option:selected').text();
    var month = $('#month').val();
    var year = $('#year').val();
    var printWindow = window.open('', '', 'height=400,width=800');
    printWindow.document.write('<html><head><title></title>');
    printWindow.document.write('</head><body >');
    printWindow.document.write('<h1 align="center">Employee Work Report</h1>');
    printWindow.document.write('<br />');
    printWindow.document.write('&nbsp;&nbsp;<b>Employee</b>&nbsp;: &nbsp;');
    printWindow.document.write(maid);
    printWindow.document.write('&nbsp;&nbsp;<b>Month</b>&nbsp;: &nbsp;');
    printWindow.document.write(month);
    printWindow.document.write('&nbsp;&nbsp;<b>Year</b>&nbsp;&nbsp;: &nbsp;');
    printWindow.document.write(year);
    printWindow.document.write('<br /><br />');
    printWindow.document.write(divContents);
    printWindow.document.write('</body></html>');
    printWindow.document.close();
    printWindow.print();
});
function exportF(elem) {
    var table = document.getElementById("employeeWorkPrint");
    var html = table.outerHTML;
    var url = 'data:application/vnd.ms-excel,' + escape(html); // Set your html table into url 
    elem.setAttribute("href", url);
    elem.setAttribute("download", "EmployeeWorkReport.xls"); // Choose the file name
    return false;
}
(function (a) {
    a(document).ready(function (b) {
        if (a('#da-ex-datatable-numberpaging').length > 0) {
            a("table#da-ex-datatable-numberpaging").dataTable({
                sPaginationType: "full_numbers", "bSort": false, "iDisplayLength": 100, "scrollY": true,
                "scrollX": true
            });
            a("table#da-ex-datatable-default").dataTable({ "iDisplayLength": 100 });
        }
    });
})(jQuery);
// *****************************************************************************

$('[data-action="excel-export"]').click(function () {
    var maidName = $("#maid option:selected").text().trim();
    var selectedMonth = $("#month").val();
    var selectedYear = $("#year").val();

    var fileName = "Employee Work Report";
    
    // Add maid name to the file name if selected
    if (maidName) {
        fileName += "_" + maidName;
    }

    // Add selected month to the file name if selected
    if (selectedMonth) {
        fileName += "_" + selectedMonth;
    }

    // Add selected year to the file name if selected
    if (selectedYear) {
        fileName += "_" + selectedYear;
    }

    var fileType = "xlsx";
    var table = document.getElementById("employeeWorkPrint");
    var wb = XLSX.utils.table_to_book(table, { sheet: "Report", dateNF: 'dd/mm/yyyy;@', cellDates: true, raw: true });
    const ws = wb.Sheets['Report'];
    var wscols = [
        { wch: 15 },
        { wch: 25 },
        { wch: 30 },
        { wch: 30 },
        { wch: 30 },
        { wch: 30 },
        { wch: 30 },
        { wch: 30 },
        { wch: 30 },
        { wch: 20 },
    ];
    ws['!cols'] = wscols;

    return XLSX.writeFile(wb, null || fileName + "." + (fileType || "xlsx"));
});
