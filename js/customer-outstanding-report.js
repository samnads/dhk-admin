var customerlistDataTable;
var customerOutDataTable;
$(document).ready(function () {

    /**
     * Function return table of contents with particular customer's pending invoices
     */
    listCustOutInvoicesTable = function (customer_id) {
        customerOutDataTable = $('#customeroutstandingdetailstable').DataTable({
            'bFilter': false,
            'bLengthChange': false,
            'pageLength': 50,
            'processing': true,
            'serverSide': true,
            "bDestroy": true,
            'bSort': false,
            'serverMethod': 'post',
            'ajax': {
                'url': _base_url + 'invoice/get_customer_outstanding_invoices_datatable',
                'data': function (data) {
                    data.customer_id = customer_id;
                },
                "complete": function (json, type) {
                    var _resp = $.parseJSON(json.responseText);
                }
            },
            'columns': [
                { data: 'slno' },
                { data: 'invoicenum' },
                { data: 'invoicedate' },
                { data: 'duedate' },
                { data: 'total' },
                { data: 'paidamt' },
                { data: 'dueamt' },
            ],
        });
    }
    customerlistDataTable = $('#customeroutstandinglisttable').DataTable({
        'bFilter': false,
        'bLengthChange': false,
        'pageLength': 50,
        'processing': true,
        'serverSide': true,
        'bSort': false,
        'serverMethod': 'get',
        'ajax': {
            'url': _base_url + 'laravel/data/report/customer_outstanding_report',
            'data': function (data) {
                data.filter_date_from = $('#cust_from_date').val() ? moment($('#cust_from_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD') : undefined;
                data.filter_date_to = $('#cust_to_date').val() ? moment($('#cust_to_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD') : undefined;
                data.filter_customer_active = $('#all-customers').val() || undefined;
                data.filter_customer_id = $('#customers_vh_rep_new').val();
            },
            "complete": function (json, type) {
                var _resp = $.parseJSON(json.responseText);
                console.log(_resp);
                console.log(_resp.totalBalance);
                $('#balance-total').text(_resp.totalBalance);
            }
        },
        'columns': [
            { data: 'slno' },
            { data: 'customer_name' },
            { data: 'mobile_number_1' },
            { data: 'area_name' },
            { data: 'invoice_net_amount' },
            { data: 'paid_amount' },
            { data: 'balance' },
            { data: 'last_invoice_date' },
            { data: 'payment_type' },
            { data: 'action' },
        ],
        order: [[1, 'asc']],
        "columnDefs": [{
            "targets": 0,
            "orderable": false
        },
        {
            "targets": 3,
            "orderable": false
        },
        {
            "targets": 7,
            "orderable": false
        }]
    });

    $('#cust_from_date,#cust_to_date').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
    });

    $('#cust_from_date').change(function () {
        customerlistDataTable.draw();
    });

    $('#cust_to_date').change(function () {
        customerlistDataTable.draw();
    });

    $('#customers_vh_rep_new').change(function () {
        customerlistDataTable.draw();
    });

    $('#all-customers').change(function () {
        customerlistDataTable.draw();
    });

    $('#customerexcelbtn').click(function () {
        $("#customerexcelbtn").html('<i class="fa fa-spinner fa-spin"></i>');
        var status = $('#all-customers').val();
        var fromdate = $('#cust_from_date').val();
        var todate = $('#cust_to_date').val();
        var custid = $('#customers_vh_rep_new').val();

        $.ajax({
            type: "POST",
            url: _base_url + 'customerexcel/customeroutstandingexportexcel',
            data: { fromdate: fromdate, todate: todate, status: status, custid: custid },
            cache: false,
            success: function (response) {
                $("#customerexcelbtn").html('<i class="fa fa-file-excel-o"></i>');
                window.location = response;
            }
        });
    });
});


function closeFancy() {
    // $.fancybox.close();
	$("#common-popup-wrapper-id").hide(500);
}
var enable_disable_id = null;
var current_status = null;
var _this = null;
function confirm_disable_enable_modal($this, id, status) {
    _this = $this;
    enable_disable_id = id;
    current_status = status;
    if (status == 0) {
        $.fancybox.open({
            autoCenter: true,
            fitToView: false,
            scrolling: false,
            openEffect: 'none',
            openSpeed: 1,
            autoSize: false,
            width: 450,
            height: 'auto',
            helpers: {
                overlay: {
                    css: {
                        'background': 'rgba(0, 0, 0, 0.3)'
                    },
                    closeClick: false
                }
            },
            padding: 0,
            closeBtn: false,
            content: $('#enable-popup'),
        });
    } else {
        $.fancybox.open({
            autoCenter: true,
            fitToView: false,
            scrolling: false,
            openEffect: 'none',
            openSpeed: 1,
            autoSize: false,
            width: 450,
            height: 'auto',
            helpers: {
                overlay: {
                    css: {
                        'background': 'rgba(0, 0, 0, 0.3)'
                    },
                    closeClick: false
                }
            },
            padding: 0,
            closeBtn: false,
            content: $('#disable-popup'),
        });
    }
}
function confirm_enable_disable() {
    $.ajax({
        type: "POST",
        url: _base_url + "customer/remove_customer",
        data: { customer_id: enable_disable_id, customer_status: current_status },
        dataType: "text",
        cache: false,
        success: function (result) {
            //window.location.assign(_base_url + 'customers');
            if (result == 1) {
                $(_this).attr('class', 'btn btn-success btn-small');
                $(_this).html('<i class="btn-icon-only icon-ok"></i>');
            }
            else {
                if (result == 'exist_bookings') // Edited by Geethu
                {
                    alert('Warning! Can\'t deactivate this customer, have some active bookings.');
                    result = 0;
                }
                else {
                    $(_this).attr('class', 'btn btn-danger btn-small');
                    $(_this).html('<i class="btn-icon-only icon-remove"> </i>');
                }

            }
            $(_this).attr('onclick', 'delete_customer(this, ' + enable_disable_id + ', ' + result + ')');
        }
    });
    closeFancy();
    customerlistDataTable.draw();
}
/*****************************************************
 * Popup for showing outstanding details
 * ***************************************************/
function viewDetailsModal(){
	$("#common-popup-wrapper-id").show(500);
  // $.fancybox.open({
        // autoCenter : true,
        // fitToView : false,
        // scrolling : false,
        // openEffect : 'none',
        // openSpeed : 1,
        // autoSize: false,
        // width:450,
        // height:'auto',
        // helpers : {
            // overlay : {
                // css : {
                    // 'background' : 'rgba(0, 0, 0, 0.3)'
                // },
                // closeClick: false
            // }
        // },
        // padding : 0,
        // closeBtn : false,
        // content: $('#details-div'),
  // });
}