$().ready(function () {
    $('#services_edit_form').validate({
        focusInvalid: false,
        rules: {
            edit_service_type_name: {
                required: true,
            },
            edit_customer_app_service_type_name: {
                required: true,
            },
            'service_type_category[]': {
                required: true,
            },
            // info_html: {
            //     required: false,
            // },
           
        },
        messages: {
            edit_service_type_name: "Enter service name.",
            edit_customer_app_service_type_name: "Enter customer app service name.",
            'service_type_category[]': "select a service Category",
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "service_type_category[]") {
                error.appendTo(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $('#services_edit').html('Please wait...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "services/update_service",
                data: $('#services_edit_form').serialize(),
                success: function (data) {
                    if (data.status == true) {
                        $('#services_edit').html('Saved');
                        alert(data.message);
                        window.location.href = _base_url + "services";
                    }
                    else {
                        alert(data.message);
                        $('#services_edit').html('Save').removeAttr("disabled");
                    }
                },
                error: function (data) {
                    alert("An error occured!");
                    $('#services_sub').html('Save').removeAttr("disabled");
                },
            });
        }
    });

    var quill = new Quill('#quill_editor', {
        theme: 'snow'
    });
    const delta = quill.clipboard.convert($('#info_html').val());
    quill.setContents(delta, 'silent');
    quill.on('text-change', function (delta, oldDelta, source) {
        document.getElementById("info_html").value = quill.root.innerHTML;
    });
});