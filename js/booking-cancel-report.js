$('#OneDayDate,#search_date_to').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
});
$("#OneDayPrint").click(function () {
    var divContents = $("#OneDayReportPrint").html();
    var date = $('#OneDayDate').val();
    var printWindow = window.open('', '', 'height=400,width=800');
    printWindow.document.write('<html><head><title></title>');
    printWindow.document.write('</head><body >');
    printWindow.document.write('<h1 align="center">One Day Cancel Report</h1>');
    printWindow.document.write('<br />');
    printWindow.document.write('&nbsp;&nbsp;<b>Date</b>&nbsp;: &nbsp;');
    printWindow.document.write(date);
    printWindow.document.write('<br /><br />');
    printWindow.document.write(divContents);
    printWindow.document.write('</body></html>');
    printWindow.document.close();
    printWindow.print();
});
// ******************************************************************
// Function to format date as "d-m-y"
function formatDate(dateString) {
    var parts = dateString.split("/");
    if (parts.length === 3) {
        var day = parts[0];
        var month = parts[1];
        var year = parts[2];

        if (day.length === 1) day = "0" + day;
        if (month.length === 1) month = "0" + month;

        return day + '-' + month + '-' + year.substr(2);
    }

    return dateString;
}

var report_file_name = "Booking Cancel Report";

$('[data-action="excel-export"]').click(function () {
    var fromDate = $("#OneDayDate").val();
    var toDate = $("#search_date_to").val();

    var formattedFromDate = formatDate(fromDate);
    var formattedToDate = formatDate(toDate);

    var fileName = report_file_name + " " + formattedFromDate + " - " + formattedToDate;
    var fileType = "xlsx";
    var table = document.getElementById("OneDayReportPrint");
    var wb = XLSX.utils.table_to_book(table, { sheet: "Report" });
    const ws = wb.Sheets['Report'];
    var wscols = [
        { wch: 15 },
        { wch: 25 },
        { wch: 30 },
        { wch: 25 },
        { wch: 20 },
        { wch: 20 },
        { wch: 20 },
        { wch: 20 },
        { wch: 20 },
        { wch: 20 },
    ];
    ws['!cols'] = wscols;
    return XLSX.writeFile(wb, null || fileName + "." + (fileType || "xlsx"));
});


// *********************************************************************************
(function (a) {
    a(document).ready(function (b) {
        if (a('#da-ex-datatable-numberpaging').length > 0) {
            a("table#da-ex-datatable-numberpaging").dataTable({
                sPaginationType: "full_numbers", "bSort": false, "iDisplayLength": 100, "scrollY": true,
                "scrollX": true
            });
            a("table#da-ex-datatable-default").dataTable({ "iDisplayLength": 100 });
        }
    });
})(jQuery);