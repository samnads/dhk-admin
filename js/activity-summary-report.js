$('#ActFromDate,#ActToDate').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
});
$("#ActivityPrint").click(function () {
    var divContents = $("#ActivityReportPrint").html();
    var from_date = $('#ActFromDate').val();
    var to_date = $('#ActToDate').val();
    var printWindow = window.open('', '', 'height=400,width=800');
    printWindow.document.write('<html><head><title></title>');
    printWindow.document.write('</head><body >');
    printWindow.document.write('<h1 align="center">Activity Summary Report</h1>');
    printWindow.document.write('<br />');
    printWindow.document.write('&nbsp;&nbsp;<b>From</b>&nbsp;: &nbsp;');
    printWindow.document.write(from_date);
    printWindow.document.write('&nbsp;&nbsp;<b>To</b>&nbsp;: &nbsp;');
    printWindow.document.write(to_date);
    printWindow.document.write('<br /><br />');
    printWindow.document.write(divContents);
    printWindow.document.write('</body></html>');
    printWindow.document.close();
    printWindow.print();
});
function exportF(elem) {
    var table = document.getElementById("ActivityReportPrint");
    var html = table.outerHTML;
    var url = 'data:application/vnd.ms-excel,' + escape(html); // Set your html table into url 
    elem.setAttribute("href", url);
    elem.setAttribute("download", "ActivitySummaryReport.xls"); // Choose the file name
    return false;
}
$('[data-action="excel-export"]').click(function () {
    var fileName = report_file_name;
    var fileType = "xlsx";
    var table = document.getElementById("ActivityReportPrint");
    var wb = XLSX.utils.table_to_book(table, { sheet: "Report",dateNF:'dd/mm/yyyy;@',cellDates:true, raw: true });
    const ws = wb.Sheets['Report'];
    var wscols = [
        { wch: 15 },
        { wch: 25 },
        { wch: 30 },
        { wch: 25 },
        { wch: 20 },
        { wch: 20 },
        { wch: 20 },
        { wch: 20 },
        { wch: 20 },
        { wch: 20 },
    ];
    ws['!cols'] = wscols;
    return XLSX.writeFile(wb, null || fileName + "." + (fileType || "xlsx"));
});
(function (a) {
    a(document).ready(function (b) {
        if (a('#da-ex-datatable-numberpaging').length > 0) {
            a("table#da-ex-datatable-numberpaging").dataTable({
                sPaginationType: "full_numbers", "bSort": false, "iDisplayLength": 100, "scrollY": true,
                "scrollX": true
            });
            a("table#da-ex-datatable-default").dataTable({ "iDisplayLength": 100 });
        }

    });
})(jQuery);