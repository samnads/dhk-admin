(function (a) {
    a(document).ready(function (b) {
        if (a('#employee-work-report-table').length > 0) {
            a("table#employee-work-report-table").dataTable({
                'sPaginationType': "full_numbers", "bSort": true, "iDisplayLength": 100, "scrollY": true, "orderMulti": false,
                "scrollX": true,
                'columnDefs': [
                    {
                        'targets': [],
                        'orderable': false
                    },
                ]
            });
        }
    });
})(jQuery);
function exportF(elem) {
    var table = document.getElementById("employeeWorkPrint");
    var html = table.outerHTML;
    var url = 'data:application/vnd.ms-excel,' + escape(html); // Set your html table into url 
    elem.setAttribute("href", url);
    elem.setAttribute("download", "EmployeeWorkReport.xls"); // Choose the file name
    return false;
}
$("#EmpWorkPrint").click(function () {
    var divContents = $("#employeeWorkPrint").html();
    var maid = $('#maid option:selected').text();
    var month = $('#month').val();
    var year = $('#year').val();
    var printWindow = window.open('', '', 'height=400,width=800');
    printWindow.document.write('<html><head><title></title>');
    printWindow.document.write('</head><body >');
    printWindow.document.write('<h1 align="center">Employee Work Report</h1>');
    printWindow.document.write('<br />');
    printWindow.document.write('&nbsp;&nbsp;<b>Employee</b>&nbsp;: &nbsp;');
    printWindow.document.write(maid);
    printWindow.document.write('&nbsp;&nbsp;<b>Month</b>&nbsp;: &nbsp;');
    printWindow.document.write(month);
    printWindow.document.write('&nbsp;&nbsp;<b>Year</b>&nbsp;&nbsp;: &nbsp;');
    printWindow.document.write(year);
    printWindow.document.write('<br /><br />');
    printWindow.document.write(divContents);
    printWindow.document.write('</body></html>');
    printWindow.document.close();
    printWindow.print();
});
$('[data-action="excel-export"]').click(function () {
    var fileName = report_file_name;
    var fileType = "xlsx";
    var table = document.getElementById("employeeWorkPrint");
    var wb = XLSX.utils.table_to_book(table, { sheet: "Report" });
    const ws = wb.Sheets['Report'];
    var wscols = [
        { wch: 15 },
        { wch: 25 },
        { wch: 30 },
        { wch: 25 },
        { wch: 20 }
    ];
    ws['!cols'] = wscols;
    return XLSX.writeFile(wb, null || fileName + "." + (fileType || "xlsx"));
});