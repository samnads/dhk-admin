//$('.sel2').select2();
$('.sel2').select2({
    //Your parameters
    formatNoMatches: '<a href="javascript:void(0);" id="add-customer-popup-new" onclick="return add_customer_new()">Add Customer</a>',
    closeOnSelect: true,
});

$('.sel3').select2({
    //Your parameters
    //formatNoMatches: '<a href="javascript:void(0);" id="add-customer-popup-new" onclick="return add_customer_new()">Add Customer</a>',
    closeOnSelect: true,
});

$("#u_customer_id").select2({
	ajax: { 
	 url: _base_url+"customer/report_srch_usr_news",
	 type: "post",
	 dataType: 'json',
	 delay: 150,
	 data: function (params) {
			return {
			  searchTerm: params.term, // search term
			};
		  },
	 processResults: function (response) {
		 if(response.id === "")
		 { 
		
		 } else {
			return {results: response};
		 }
	 },
	 cache: true
	},
});

$('#user_date').datepicker({format: 'dd/mm/yyyy', autoclose: true, startDate: new Date()}).on('changeDate', function(ev){ 
    var _date = new Date(ev.date);
    var _new_date = ('0' + _date.getDate()).slice(-2) + '-' + ('0' + (_date.getMonth()+1)).slice(-2) + '-' + _date.getFullYear();
});

$('.user_end_datepicker').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    startDate: new Date($('#user-repeate-end-start').val())
});

$('#user-repeat-end-date').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
});

$("#customer-popup").validate({
    rules: {
        customer_name: "required",
        area: "required",
        address: "required",
        mobile_number1: {
            required: true,
            number: true,
            remote: {
                url: _base_url + "booking/register_contact_exists",
                type: "post",
                data: {
                    phone: function(){ return $("#mobile_number1").val(); }
                }
            }
        }

    },
    messages: {
        customer_name: "Please enter your name",
        area: "Please select your area",
        address: "Please enter your address",
        mobile_number1: {
            required : "Please enter your contact number",
            number: "Please enter numbers only",
            remote: "Mobile number already used."
        }
    }
});

$("#popup-add-customer").click(function()
{
    if($("#customer-popup").valid())
    {
        $('.mm-loader').css('display','block');
//        //$('body').on('click', '#popup-add-customer', function() {
        var check_mobile = $('#customer-popup #mobile_number1').val();
        $.post(_base_url + 'booking', {action: 'register_contact_exists_new',
            phone: check_mobile,
        },
        function (response) 
        {
            var _json = $.parseJSON(response);
            if(_json.status == 'exist')
            {
                $('.mm-loader').css('display','none');
                $('#u-error').html(_json.message);
                $('#u-error').show();
            } else if(_json.status == 'not'){
                $('#u-error').hide();
                $.post(_page_url+'/add_customer_popup', $("#customer-popup" ).serialize())
                .done(function( data ) {
                    //alert(data);
                    var _json = $.parseJSON(data);
                    $('#customer-popup')[0].reset();
                    $("#u_customer_id").html(_json.html);
                    $("#u_customer_id").select2("val", _json.cust_id);
                    $('#customer-picked-address-u').html('');
                    //$("#customer-add-popup").css("width","50%").hide();
                    $("#customer-add-popup").hide();
                    get_no_of_address(_json.cust_id);
                    //get_pending_amount(_json.cust_id);
                    //$("#customer-detail-popup").css("width","50%").show();
                    $("#customer-detail-popup").show();
                    $('.mm-loader').css('display','none');
                });  
            }
        }).error(function () {
            alert('Unexpected error!')
        });
    }
});

$('body').on('click', '.pick_customer_address', function() {
	var _address_id = $(this).attr('id').replace('cadddress-', '');
	$('#customer-address-id-user').val(_address_id);
	var _addzone = $('#caddzone-' + _address_id).html();
	var _address = $('#cadd-' + _address_id).html();
	$('#customer-address-panel-user').slideUp(function() {
            $('#customer-picked-address-u').html('<div class="address"><strong>' + _addzone + '</strong> - ' + _address + '</div><div class="action"><span id="chg-cust-address">Change</span></div><div class="clear"></div>');
            $('#customer-picked-address-u').show();
            $('#customer-address-panel-user .inner').html('Loading<span class="dots_loader"></span>');

            var _booking_id = $('#booking-id-user').val();
	});
});

$('body').on('change', '#u_customer_id', function() 
{
    var _customer_id = $(this).val();
    $('#customer-picked-address-u').html('');
    get_no_of_address(_customer_id);
});

$('body').on('change', '#user_booking_type', function () 
{
    var custid = $('#u_customer_id').val();
    var _booking_type = $(this).val();
    if (_booking_type == 'WE' || _booking_type == 'BW')
    {
        $('#user-repeat-days').css('display', 'block');
        $('#user-repeat-ends').css('display', 'block');
    } else
    {
        $('#user-repeat-days').hide();
        $('#user-repeat-ends').hide();
    }  
});

$('body').on('change', 'input[name="user_repeat_end"]', function() 
{
    if($('#user-repeat-end-never').is(':checked'))
    {
        $('#user-repeat-end-date').val('');
    }
});

$('body').on('change', 'input[name="user_repeat_end"]', function() {
    if($(this).val() == 'ondate')
    {
        $('#user-repeat-end-date').removeAttr('disabled'); 
    }
    else
    {
        $('#user-repeat-end-date').attr('disabled', 'disabled'); 
    }
});

$('body').on('click', '#add-user-booking-nform', function() {
    $('.field-error').text('');
    var _service_date = $('#user_date').val();
    var _customer_id = $.trim($('#u_customer_id').val());
    var _customer_address_id = $.trim($('#customer-address-id-user').val());
    var _service_type_id = $.trim($('#user_service_type_id').val());
    var _cleaning_material = $('#user_cleaning_materials').is(':checked') ? 'Y' : 'N';
    var _from_time = $.trim($('#b-from-time').val());
    var _to_time = $.trim($('#b-to-time').val());
    var _booking_type = $('#user_booking_type').val();
    var _tot_amt = $.trim($('#user_total_amt').val());
    var _note = $.trim($('#user_notes').val());
    //$('#u-error').text('');
    if($.isNumeric(_customer_id) == false)
    {
        $('#u_customer_id').parent().append('<label class="field-error text-danger">Select a customer !</label>');
        $('#maid_search').hide();
        $('#maid_search').html('');
        return false;
    }
    else if($.isNumeric(_customer_address_id) == false)
    {
        $('#u_customer_id').parent().append('<label class="field-error text-danger">Pick one customer address !</label>');
        $('#maid_search').hide();
        $('#maid_search').html('');
        open_address_panel(_customer_id);
        return false;
    }
    else if($.isNumeric(_service_type_id) == false)
    {
        $('#user_service_type_id').parent().append('<label class="field-error text-danger">Select service type !</label>');
        $('#maid_search').hide();
        $('#maid_search').html('');
        return false;
    }
    else if(_service_date == '')
    {
        $('#user_date').parent().append('<label class="field-error text-danger">Select service date !</label>');
        $('#maid_search').hide();
        $('#maid_search').html('');
        return false;
    }
    else if (_booking_type == '') {
        $('#user_booking_type').parent().append('<label class="field-error text-danger">Select repeat type !</label>');
        $('#maid_search').hide();
        $('#maid_search').html('');
        return false;
    }
    else if(_from_time == '' || _to_time == '')
    {
        $('#b-from-time').parent().append('<label class="field-error text-danger">Select booking time !</label>');
        $('#maid_search').hide();
        $('#maid_search').html('');
        return false;
    }
    var _repeat_days = [];
    var _repeat_end = '';
    var _repeat_end_date = '';
    if(_booking_type == 'WE' || _booking_type == 'BW')
    {
        _repeat_days = $('input[id^="user-repeat-on-"]:checked').map(function() {
            return this.value;
        }).get();
		  
        if(_repeat_days.length == 0)
        {
            $('#user-repeat-days').append('<label class="field-error text-danger">Select repeat days !</label>');
            $('#maid_search').hide();
            $('#maid_search').html('');
            return false;
        }
        _repeat_end = $('input[name="user_repeat_end"]:checked').val();
        if(_repeat_end == 'ondate')
        {
            _repeat_end_date = $('#user-repeat-end-date').val();
            if(_repeat_end_date == '')
            {
                $('#user-repeat-ends').append('<label class="field-error text-danger">Select service end date !</label>');
                $('#maid_search').hide();
                $('#maid_search').html('');
                return false;
            }
        }
    }
    if(_tot_amt == '')
    {
        $('#user_total_amt').parent().append('<label class="field-error text-danger">Enter total amount !</label>');
        $('#maid_search').hide();
        $('#maid_search').html('');
        return false;
    }
    $('#maid_search').hide();
    var location_filter_type = 'all';
    var loc_type , loc_val;
    loc_type = location_filter_type;
    if(location_filter_type == 'all')
    {
        loc_val = 0;
    }
    loc_type = location_filter_type;
    $.ajax({
        type    : "POST",
        url     : _base_url + "booking/maids_booking",
        data    : 
            { 
                book_date    : _service_date,
                from_time   :_from_time,
                to_time     :_to_time,
                book_type   :_booking_type,
                location_type   :loc_type,
                location_value: loc_val,
                cust_address_id: _customer_address_id,
                customer_id :_customer_id,
				repeat_days: _repeat_days,
                repeat_end : _repeat_end,
                repeat_end_date: _repeat_end_date
            },
        dataType: "text",
        cache   : false,
        success : function (result) 
        {
            $('#maid_search').html(result);
            $('#maid_search').show();
            $('#maid_search').addClass('widget-content');
        }
    });
});

$('body').on('click', '.book_maid_list_new', function() 
{
    var id_maid_data = $(this).attr('id');
    var id_maid = id_maid_data.split('maid_');
    //alert(id_maid[1]);
    var _maid_name = $.trim($('#name_'+id_maid[1]).html());

    $.fancybox.open({
            autoCenter : true,
            fitToView : false,
            scrolling : false,
            openEffect : 'none',
            openSpeed : 1,
            helpers : {
                    overlay : {
                            css : {
                                    'background' : 'rgba(0, 0, 0, 0.3)'
                            },
                            closeClick: true
                    }
            },
            padding : 0,
            closeBtn : true,
            content : _alert_html = '<div id="alert-popup"><div class="head">Book<span class="alert-popup-close pop_close"></span></div><div class="content padd20">Are you sure want to continue?</div><div class="bottom"><input type="button" value="Yes" class="pop_close book_yes_new" style="background:#b2d157;border:1px solid" id="'+id_maid[1]+'"  />&nbsp;&nbsp;<input type="button" value="No" class="book_no pop_close"  /></div></div>',
            topRatio : 0.2,
    });                                
});

$('body').on('click', '.book_yes_new', function()
{
    $.fancybox.close();
    $('.mm-loader').css('display','block');											  
    var _maid_id = $(this).attr('id');
    var _action='book-maid-new';

    var _service_date = $('#user_date').val();
    var _customer_id = $.trim($('#u_customer_id').val());
    var _customer_address_id = $.trim($('#customer-address-id-user').val());
    var _from_time = $.trim($('#b-from-time').val());
    var _to_time = $.trim($('#b-to-time').val());
    var _booking_type = $.trim($('#user_booking_type').val());
    var _service_type = $.trim($('#user_service_type_id').val());
    var _cleaning_mat = $.trim($('#user_cleaning_materials').val());
    var _total_amt = $.trim($('#user_total_amt').val());
    var _notes = $.trim($('#user_notes').val());

    var _repeat_days = [];
    var _repeat_end = '';
    var _repeat_end_date = '';
    if(_booking_type == 'WE' || _booking_type == 'BW')
    {
        _repeat_days = $('input[id^="user-repeat-on-"]:checked').map(function() {
            return this.value;
        }).get();
        _repeat_end = $('input[name="user_repeat_end"]:checked').val();
        if(_repeat_end == 'ondate')
        {
            _repeat_end_date = $('#user-repeat-end-date').val();

        }
    }

    $.post( _page_url, { action: _action, customer_id: _customer_id, customer_address_id: _customer_address_id, maid_id: _maid_id, service_date:_service_date, time_from: _from_time, time_to: _to_time, booking_type: _booking_type, repeat_days: _repeat_days, repeat_end: _repeat_end, repeat_end_date: _repeat_end_date, service_type_id: _service_type, cleaning_mat: _cleaning_mat, total_amt: _total_amt, notes: _notes }, function(response) 
    {
        //refresh_grid();
        var _alert_html = '';
			
        if(response == 'refresh')
        {
            $('.mm-loader').css('display','none');	
            _alert_html = '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="alert-popup-close cancel_btn pop_close" style="float:none;"  /></div></div>';
        }
        else
        {
            var _resp = $.parseJSON(response);
            if(typeof _resp.status && _resp.status == 'success')
            {	
                $('.mm-loader').css('display','none');	
                _alert_html = '<div id="alert-popup" class="blue-popup"><div class="head">Success<span class="alert-popup-close pop_close_new"></span></div><div class="content padd20">Booking has been done successfully.</div><div class="bottom"><input type="button" value="OK" class="pop_close_new alert-popup-close" style="float:none;" /></div></div>';
            }
            else if(typeof _resp.status && _resp.status == 'error')
            {
                $('.mm-loader').css('display','none');
                _alert_html = '<div id="alert-popup"><div class="head">Error<span class="alert-popup-close pop_close"></span></div><div class="content padd20">'+ _resp.message+'</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close alert-popup-close" style="float:none;"  /></div></div>';
            }
            else
            {
                $('.mm-loader').css('display','none');
               _alert_html = '<div id="alert-popup"><div class="head">Error<span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close alert-popup-close" style="float:none;"  /></div></div>';
            }
        }		
        if(_alert_html  != '')
        {
            $.fancybox.open({
                    autoCenter : true,
                    fitToView : false,
                    scrolling : false,
                    openEffect : 'fade',
                    openSpeed : 100,
                    helpers : {
                            overlay : {
                                    css : {
                                            'background' : 'rgba(0, 0, 0, 0.3)'
                                    },
                                    closeClick: false
                            }
                    },
                    padding : 0,
                    closeBtn : false,
                    content: _alert_html
            });
        }
    });	
});

$('body').on('click', '.pop_close_new', function() {
    _bpop_open = false;
    parent.$.fancybox.close();
    window.location = _page_url;
});

$('body').on('change', '#b-from-time', function() {
    refresh_to_time();
});



















function refresh_to_time() 
{
    $("#b-to-time").select2("val", "");
    var _selected_index = $("#b-from-time")[0].selectedIndex;
	
    var _time_to_options = '<option></option>';
    var _i = 0;
    var _last_index;
    $('#b-from-time option').each(function(index, option)
    {
        if(index > _selected_index)
        {
            _time_to_options += '<option value="' + $(option).val() + '">' + $(option).text() + '</option>';
            _last_index = index;
            _i++;
        }
    });
    if(_i == 0)
    {
        _time_to_options += '<option value="">No Time</option>';
    }
	
    $('#b-to-time').html(_time_to_options);
}

function add_customer_new()
{
    var typed_cont = $("#s2id_autogen1_search").val();
    $('#customer-popup #customer_name').val(typed_cont);
    $(".sel2").select2("close");
    $("#customer-detail-popup").hide();
    $("#customer-add-popup").show();
    $("#u_customer_id").select2("val", "");
    $('#customer-picked-address-u').html('');

}

function loadLocationField(id)
{
    if(document.getElementById(id).value!="")
    {
        var input = document.getElementById(id);
        var autocomplete = new google.maps.places.Autocomplete(input);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
            // document.getElementById(id).value = place.formatted_address;


            for (var i = 0; i < place.address_components.length; i++) 
            {
                var addressType = place.address_components[i].types[0];

                if (componentForm[addressType]) 
                {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(id).value = val;
                }
            }
        });
    }
}

function get_no_of_address(customer_id)
{
    $.post(_page_url, { action: 'get-no-of-customer-address', customer_id: customer_id }, function(response)
    {
        if(response == 'refresh')
        {
            $.fancybox.open({
                autoCenter : true,
                fitToView : false,
                scrolling : false,
                openEffect : 'fade',
                openSpeed : 100,
                helpers : {
                        overlay : {
                                css : {
                                        'background' : 'rgba(0, 0, 0, 0.3)'
                                },
                                closeClick: false
                        }
                },
                padding : 0,
                closeBtn : false,
                content:  '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>'
            });
        }
        else
        {
            //customer details on right side
            $.post(_page_url, { action: 'get-details-customer', customer_id: customer_id }, function(response)
            {
                var _respval = $.parseJSON(response);
                if(_respval.payment_type == "D")
                {
                    var paytype = "Daily";
                } else if(_respval.payment_type == "W")
                {
                    var paytype = "Weekly";
                } else if(_respval.payment_type == "M")
                {
                    var paytype = "Monthly";
                } else {
                    var paytype = "";
                }
                if(_respval.customer_booktype == '0')
                {
                    var cus_booktype = "Non Regular";
                } else if(_respval.customer_booktype == '1')
                {
                    var cus_booktype = "Regular";
                } else {
                    var cus_booktype = "";
                }
                $('#b-customer-ids-cell').text(_respval.customer_name);
                $('#b-customer-mobile-cell').text(_respval.mobile_number_1);
                $('#b-customer-email-cell').text(_respval.email_address);
                $('#b-customer-paytype-cell').text(paytype);
                $('#b-customer-booktype-cell').text(cus_booktype);
                $('#b-customer-paymode-cell').text(_respval.payment_mode);
                $("#customer-add-popup").hide();
                $("#customer-detail-popup").show();
            });
                                
            //Ends
            var _resp = $.parseJSON(response);				
            var _address_html = '<div class="table">';
                               
            if(_resp.no_of_address == 1)
            {
                $.each(_resp.address, function(key, val)
                {
                    var _address_id = val.customer_address_id;
                    $('#customer-address-id-user').val(_address_id);

                    var _addzone = '<strong>' + val.zone_name + ' - ' + val.area_name + '</strong>';
                    var _address = val.customer_address;
                                         
                    $('#customer-address-panel-user').slideUp(function() 
                    {
                        $('#b-customer-area-cell').text(val.area_name);
                        $('#b-customer-zone-cell').text(val.zone_name);
                        $('#b-customer-address-cell').text(_address);
                        $('#customer-address-panel-user .inner').html('Loading<span class="dots_loader"></span>');
                        var _booking_id = $('#booking-id-user').val();
                        
                    });
                });
            }
            else
            {
                open_address_panel(customer_id)
            }      
        }
    }); 
}

function open_address_panel(customer_id)
{
    $('#customer-address-id-user').val('');
    $('#customer-address-panel-user').hide();
    $('#customer-address-panel-user .inner').html('Loading<span class="dots_loader"></span>');
	
    if($.isNumeric(customer_id) && customer_id > 0)
    {	
        $('#customer-address-panel-user').slideDown();
		
        $.post(_page_url, { action: 'get-customer-address', customer_id: customer_id }, function(response)
        {
            if(response == 'refresh')
            {
                $.fancybox.open({
                        autoCenter : true,
                        fitToView : false,
                        scrolling : false,
                        openEffect : 'fade',
                        openSpeed : 100,
                        helpers : {
                                overlay : {
                                        css : {
                                                'background' : 'rgba(0, 0, 0, 0.3)'
                                        },
                                        closeClick: false
                                }
                        },
                        padding : 0,
                        closeBtn : false,
                        content:  '<div id="alert-popup"><div class="head">Error!!! <span class="alert-popup-close pop_close"></span></div><div class="content padd20">An unexpected error. Please try again.</div><div class="bottom"><input type="button" value="Close" class="cancel_btn pop_close"  /></div></div>'
                });
            }
            else
            {
                var _resp = $.parseJSON(response);				
                var _address_html = '<div class="table">';                                
                                
                $.each(_resp, function(key, val) {
                    _address_html += '<div class="row"><div class="cell1"><span id="caddzone-' + val.customer_address_id + '"><strong>' + val.zone_name + ' - ' + val.area_name + '</strong></span><br /><span id="cadd-' + val.customer_address_id + '">' + val.customer_address + '</span></div><div class="cell2"><input type="button" value="Pick &raquo;" id="cadddress-' + val.customer_address_id + '" class="pick_customer_address"  /></div></div>';
                });				

                _address_html += '</div>';

                $('#customer-address-panel-user .inner').html(_address_html);
            }
        });
    }
}