var servicelistDataTable;
$(document).ready(function () {
	/**
     * Function return table of contents with particular customer's pending invoices
     */
	servicelistDataTable = $('#sevicereportlist').DataTable({
        'bFilter': false,
        'bLengthChange': false,
        'pageLength': 50,
        'processing': true,
        'serverSide': true,
        'bSort': false,
        'serverMethod': 'get',
        'ajax': {
            'url': _base_url + 'laravel/data/report/service_report',
            'data': function (data) {
                data.filter_date_from = $('#service_from_date').val() ? moment($('#service_from_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD') : undefined;
                data.filter_date_to = $('#service_to_date').val() ? moment($('#service_to_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD') : undefined;
                data.filter_service = $('#service_type_id').val() || undefined;
            },
            "complete": function (json, type) {
                var _resp = $.parseJSON(json.responseText);
                console.log(_resp);
                // console.log(_resp.totalBalance);
                $('#hours-total').text(_resp.totalHrs);
                $('#invoice-total').text(_resp.totalInvoice);
            }
        },
        'columns': [
            { data: 'slno' },
            { data: 'service' },
            { data: 'totalhrs' },
            { data: 'inv_value' },
            // { data: 'amt_paid' },
            // { data: 'bal_amt' },
        ],
    });
	
	$('#service_from_date,#service_to_date').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
    });

    $('#service_from_date').change(function () {
        servicelistDataTable.draw();
    });

    $('#service_to_date').change(function () {
        servicelistDataTable.draw();
    });

    $('#service_type_id').change(function () {
        servicelistDataTable.draw();
    });
	
	$('#serviceexcelbtn').click(function () {
        $("#serviceexcelbtn").html('<i class="fa fa-spinner fa-spin"></i>');
        var fromdate = $('#service_from_date').val() ? moment($('#service_from_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD') : undefined;
        var todate = $('#service_to_date').val() ? moment($('#service_to_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD') : undefined;
        var serviceid = $('#service_type_id').val();
		
        $.ajax({
            type: "GET",
            // url: _base_url + 'customerexcel/customeroutstandingexportexcel',
            url: _base_url + 'laravel/data/report/service_report_excel',
            data: { filter_date_from: fromdate, filter_date_to: todate, filter_service_id: serviceid },
            cache: false,
            success: function (response) {
                $("#serviceexcelbtn").html('<i class="fa fa-file-excel-o"></i>');
				$('#servicetablebody').html(response.aaData);
				console.log(response);
                // window.location = response;
				var fileName = "Service Report";
				  var fileType = "xlsx";
				  var table = document.getElementById("table-for-excel-service");
				  var wb = XLSX.utils.table_to_book(table, {sheet: "Report"});
				  const ws = wb.Sheets['Report'];
				  var wscols = [
					{wch:20},
					{wch:30},
					{wch:25},
					{wch:25}
				  ];
				  ws['!cols'] = wscols;
					return XLSX.writeFile(wb, null || fileName + "." + (fileType || "xlsx"));
            }
        });
    });
	
});