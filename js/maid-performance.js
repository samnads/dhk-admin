$(document).ready(function () {
	$('#performanceexcelbtn').click(function () {
        $("#performanceexcelbtn").html('<i class="fa fa-spinner fa-spin"></i>');
        var fromdate = $('#vehicle_date').val();
        var todate = $('#vehicle_date_to').val();
        var zoneid = $('#zones').val();

        $.ajax({
            type: "POST",
            url: _base_url + 'customerexcel/maid_performance_report_excel',
            data: { fromdate: fromdate, todate: todate, zoneid: zoneid },
            cache: false,
            success: function (response) {
				console.log(response);
                $("#performanceexcelbtn").html('<i class="fa fa-file-excel-o"></i>');
                window.location = response;
            }
        });
    });
});