var paymentlisttable;
$(document).ready(function () {
	paymentlisttable = $('#onlinepaymentlisttable').DataTable({
        'bFilter': false,
        'bLengthChange': false,
        'pageLength': 100,
        'processing': true,
        'serverSide': true,
        'bSort': false,
        'serverMethod': 'post',
        'ajax': {
            'url': _base_url + 'customer/list_ajax_online_payments_list',
            'data': function (data) {
                if ($('#payment-date').length) {
                    var regdate = $('#payment-date').val();
                    data.regdate = regdate;
                } else {
                    data.regdate = '';
                }
                if ($('#payment-date-to').length) {
                    var regdateto = $('#payment-date-to').val();
                    data.regdateto = regdateto;
                } else {
                    data.regdateto = '';
                }
                data.custid = $('#customers_vh_rep_new').val();
                data.status = $('#pay_status').val();
            },
            "complete": function (json, type) {
                var _resp = $.parseJSON(json.responseText);
				$("#totamt").text(_resp.totalamount);
				$("#convamt").text(_resp.totalconvamount);
				$("#netamt").text(_resp.totalnetamount);
            }
        },
        'columns': [
            { data: 'slno' },
            { data: 'customer' },
            { data: 'transactionid' },
            { data: 'refno' },
            { data: 'amount' },
            { data: 'convfee' },
            { data: 'netamount' },
            { data: 'description' },
            { data: 'status' },
            { data: 'paiddate' },
            { data: 'action' },
        ],
    });
	
	$('#payment-date, #payment-date-to').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
    });
	
	$('#payment-date, #payment-date-to').change(function () {
        paymentlisttable.draw();
    });
	
	$('#customers_vh_rep_new').change(function () {
        paymentlisttable.draw();
    });
	
	$('#pay_status').change(function () {
        paymentlisttable.draw();
    });
	
	$('#onlinepaymentexceldownload').click(function () {
        $("#onlinepaymentexceldownload").html('<i class="fa fa-spinner fa-spin"></i>');
        var status = $('#pay_status').val();
        var fromdate = $('#payment-date').val();
        var todate = $('#payment-date-to').val();
        var custid = $('#customers_vh_rep_new').val();
        $.ajax({
            type: "POST",
            url: _base_url + 'customerexcel/onlinepaymentexcel',
            data: { fromdate: fromdate, todate: todate, status: status, custid: custid },
            cache: false,
            success: function (response) {
                $("#onlinepaymentexceldownload").html('<i class="fa fa-file-excel-o"></i>');
                window.location = response;
            }
        });
    });

    $("#update-payment-form").submit(function(event){
        event.preventDefault();
        var element = $("#update-payment-form");
        $("button[type=submit]").prop('disabled',true);
        $.ajax({
            url: _base_url + 'customer/update_online_payment',
            type: 'post',
            data: element.serializeArray(),
            dataType: 'json',
            success: function(_resp){
                $("button[type=submit]").prop('disabled',false);
                if(_resp.status == "success")
                {
                    toastr.success(_resp.message);
                    $.fancybox.close();
				    paymentlisttable.draw();
                } else {
                    toastr.error(_resp.message);
                }
            }, error: function(jqXHR, exception){
                console.log('Something went wrong');
            }
        })
    });
});

function updatePaymentStatus(payid)
{
    $('.mm-loader').show();
    $.ajax({
        type: "POST",
        url: _base_url + "customer/get_payment_details",
        data: { payid: payid },
        dataType: "text",
        cache: false,
        success: function (result) {
            var obj = jQuery.parseJSON(result);
            $.each($.parseJSON(result), function (edit, value) {
                $('#edit_customername').val(value.customer_name);
                $('#edit_paymentid').val(value.payment_id);
                $('#edit_customerid').val(value.customer_id);
                $('#edit_paiddatetime').val(value.payment_datetime);
                $('#edit_refno').val(value.reference_id);
                $('#edit_transactionid').val(value.transaction_id);
                $('#edit_amount').val(value.amount);
            });
            $.fancybox.open({
                autoCenter : true,
                fitToView : false,
                scrolling : false,
                openEffect : 'none',
                openSpeed : 1,
                autoSize: false,
                width:450,
                height:'auto',
                helpers : {
                overlay : {
                    css : {
                    'background' : 'rgba(0, 0, 0, 0.3)'
                    },
                    closeClick: false
                }
                },
                padding : 0,
                closeBtn : false,
                content: $('#edit-popup'),
            });
            $('.mm-loader').hide();
        },
        error:function(data){
        $('.mm-loader').hide();
            alert(data.statusText);
            console.log(data);
        }
    });
}

function closeFancy(){
    $.fancybox.close();
}