var quickbookexpensepaymentslisttable;
$(document).ready(function () {
    quickbookexpensepaymentslisttable = $('#quickbookExpensePaymentslisttable').DataTable({
        'bFilter': false,
        'bLengthChange': false,
        'pageLength': 100,
        'processing': true,
        'serverSide': true,
        'bSort': false,
        'serverMethod': 'post',
        'ajax': {
            'url': _base_url + 'quickbook/list_ajax_quickbook_expense_list',
            'data': function (data) {
                if ($('#invoice-date').length) {
                    var regdate = $('#invoice-date').val();
                    data.regdate = regdate;
                } else {
                    data.regdate = '';
                }
                if ($('#invoice-date-to').length) {
                    var regdateto = $('#invoice-date-to').val();
                    data.regdateto = regdateto;
                } else {
                    data.regdateto = '';
                }
                // data.paytype = $('#payment_type').val();
                // data.sourceval = $('#sort_source').val();
                // data.customertype = $('#sort_cust_type').val();
                // data.useractive = $('#all-customers').val();
				data.custid = $('#customers_vh_rep_new').val();
                data.keywordsearch = $('#keyword-search').val();
            },
            "complete": function (json, type) {
                var _resp = $.parseJSON(json.responseText);
            }
        },
        'columns': [
            { data: 'slno' },
            { data: 'expref' },
            { data: 'name' },
            { data: 'mobile' },
            { data: 'expdate' },
            { data: 'expamount' },
            { data: 'payref' },
            { data: 'allocated' },
        ],
        // "columnDefs": [{
            // "targets": 1,
            // "visible": false
        // }]
    });    

    $("#keyword-search").keyup(function () {
        var vall = $('#keyword-search').val().length
        //if (vall >= 3) {
		quickbookexpensepaymentslisttable.draw();
        //}
    });
	
	$('#customers_vh_rep_new').change(function () {
        quickbookexpensepaymentslisttable.draw();
    });
	
	$('#invoice-date, #invoice-date-to').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
    });
	
	$('#invoice-date, #invoice-date-to').change(function () {
        quickbookexpensepaymentslisttable.draw();
    });
});

function closeFancy() {
    $.fancybox.close();
}

function confirm_disable_enable_modal() {
    $.fancybox.open({
		autoCenter: true,
		fitToView: false,
		scrolling: false,
		openEffect: 'none',
		openSpeed: 1,
		autoSize: false,
		width: 450,
		height: 'auto',
		helpers: {
			overlay: {
				css: {
					'background': 'rgba(0, 0, 0, 0.3)'
				},
				closeClick: false
			}
		},
		padding: 0,
		closeBtn: false,
		content: $('#enable-popup'),
	});
}

function sync_expense_payments() {
	$('.mm-loader').css('display','block');
	var custid = $('#customers_vh_rep_new').val();
	var search = $('#keyword-search').val();
	var regdate = $('#invoice-date').val();
	var regdateto = $('#invoice-date-to').val();
    $.ajax({
        type: "POST",
        url: _base_url + "quickbook/sync_all_expense_payments_quickbooks",
        data: { custid: custid, search: search, regdate: regdate, regdateto: regdateto },
        // dataType: "text",
        cache: false,
        success: function (result) {
			closeFancy();
			$('.mm-loader').css('display','none');
			var _resp = JSON.parse(result);
			console.log(_resp);
            if(_resp.status == "error") {
                toastr.error(_resp.message);
				quickbookexpensepaymentslisttable.draw();
            } else if(_resp.status == "success") {
				$("#sync-error-div").html(_resp.message);
				$.fancybox.open({
					autoCenter: true,
					fitToView: false,
					scrolling: false,
					openEffect: 'none',
					openSpeed: 1,
					autoSize: false,
					width: 'auto',
					height: 'auto',
					helpers: {
						overlay: {
							css: {
								'background': 'rgba(0, 0, 0, 0.3)'
							},
							closeClick: false
						}
					},
					padding: 0,
					closeBtn: false,
					content: $('#disable-popup'),
				});
				quickbookexpensepaymentslisttable.draw();
			} else {
               toastr.error("Something went wrong."); 
			   quickbookexpensepaymentslisttable.draw();
            }
        }
    });
    
	// $('.mm-loader').css('display','block');
    // quickbookinvoicepaymentslisttable.draw();
}