$('#vehicle_date').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
});
function exportF(elem) {
    var table = document.getElementById("divToPrint");
    var html = table.outerHTML;
    var url = 'data:application/vnd.ms-excel,' + escape(html); // Set your html table into url 
    elem.setAttribute("href", url);
    elem.setAttribute("download", "BookingReport.xls"); // Choose the file name
    return false;
}
(function (a) {
    a(document).ready(function (b) {
        if (a('#da-ex-datatable-numberpaging').length > 0) {
            a("table#da-ex-datatable-numberpaging").dataTable({
                sPaginationType: "full_numbers", "bSort": true, "iDisplayLength": 100, "scrollY": true,
                "scrollX": true
            });
            a("table#da-ex-datatable-default").dataTable({ "iDisplayLength": 100 });
        }
    });
})(jQuery);
$('[data-action="excel-export"]').click(function () {
    var fileName = report_file_name;
    var fileType = "xlsx";
    var table = document.getElementById("divToPrint");
    var wb = XLSX.utils.table_to_book(table, { sheet: "Report" });
    const ws = wb.Sheets['Report'];
    var wscols = [
        { wch: 15 },
        { wch: 25 },
        { wch: 30 },
        { wch: 25 },
        { wch: 20 }
    ];
    ws['!cols'] = wscols;
    return XLSX.writeFile(wb, null || fileName + "." + (fileType || "xlsx"));
});