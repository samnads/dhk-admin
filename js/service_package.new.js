$.validator.addMethod("lessThanOrEqual",
    function (value, element, param) {
        var $otherElement = $(param);
        return parseFloat(value) <= parseFloat($otherElement.val());
    }
);
$().ready(function () {
    $('#service_package_new_form').validate({
        focusInvalid: false,
        rules: {
            service_type_id: {
                required: true,
            },
            building_type_id: {
                required: true,
            },
            building_type_room_id: {
                required: true,
            },
            description: {
                required: true,
            },
            info_html: {
                required: false,
            },
            actual_total_amount: {
                required: true,
                min: 1,
            },
            cart_limit: {
                required: true,
                min: 1,
            },
            sort_order: {
                required: true,
                min: 1,
            },
            total_amount: {
                required: true,
                min: 1,
                lessThanOrEqual: "#actual_total_amount"
            },
            customer_app_thumbnail: {
                required: true,
            },
            service_time: {
                required: true,
            },
        },
        messages: {
            service_type_id: "Select service type.",
            building_type_id: "Select category.",
            building_type_room_id: "Select sub category.",
            description: "Add some description.",
            info_html: "Add some html info.",
            actual_total_amount: {
                required: "Enter actual amount.",
                min: "Enter a value greater than or equal to 1.",
            },
            cart_limit: {
                required: "Enter Quantity limit.",
                min: "Enter a value greater than or equal to 1.",
            },
            sort_order: {
                required: "Enter sort order.",
                min: "Enter a value greater than or equal to 1.",
            },
            total_amount: {
                required: "Enter sale amount.",
                lessThanOrEqual: "Sale amount must be less than actual amount.",
                min: "Enter a value greater than or equal to 1.",
            },
            customer_app_thumbnail: {
                required: "Upload Image.",
            },
        },
        submitHandler: function (form) {
            $('#package-save-btn').html('Please wait...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "service_package/save_package",
                data: $('#service_package_new_form').serialize(),
                success: function (data) {
                    if (data.status == true) {
                        $('#package-save-btn').html('Saved');
                        alert(data.message);
                        window.location.href = _base_url + "service_package";
                    }
                    else {
                        alert(data.message);
                        $('#package-save-btn').html('Save').removeAttr("disabled");
                    }
                },
                error: function (data) {
                    alert("An error occured!");
                    $('#package-save-btn').html('Save').removeAttr("disabled");
                },
            });
        }
    });
    var quill = new Quill('#quill_editor', {
        theme: 'snow'
    });
    quill.on('text-change', function (delta, oldDelta, source) {
        document.getElementById("info_html").value = quill.root.innerHTML;
    });
});
$('#service_package_new_form select[name="building_type_id"]').change(function () {
    var building_type_id = this.value;
    if(building_type_id){
        var options = `<option value="">-- Loading... --</option>`;
        $('#service_package_new_form select[name="building_type_room_id"]').html(options);
        $.ajax({
            type: 'GET',
            dataType: "json",
            url: _base_url + "service_package/get_building_rooms_by_building_type",
            data: { building_type_id: building_type_id },
            success: function (response) {
                var options = `<option value="">-- Select Sub Category --</option>`;
                $.each(response.data, function (key, element) {
                    options += `<option value="` + element.id + `">` + element.name +`</option>`;
                });
                $('#service_package_new_form select[name="building_type_room_id"]').html(options);
            },
            error: function (response) {
            },
        });
    }
    else{
        $('#service_package_new_form select[name="building_type_room_id"]').html(`<option value="">-- Select Category First --</option>`);
    }
});

$('#service_package_new_form select[name="service_type_id"]').change(function () {
    var service_type_id = this.value;
    $('#service_package_new_form select[name="building_type_room_id"]').html('<option value="">-- Select Sub Category --</option>');
    if(service_type_id){
        var options = `<option value="">-- Loading... --</option>`;
        $('#service_package_new_form select[name="building_type_id"]').html(options);
        $.ajax({
            type: 'GET',
            dataType: "json",
            url: _base_url + "service_package/get_building_types_for_service",
            data: { service_type_id: service_type_id },
            success: function (response) {
                var options = `<option value="">-- Select Category --</option>`;
                $.each(response.data, function (key, element) {
                    options += `<option value="` + element.id + `">` + element.name +`</option>`;
                });
                $('#service_package_new_form select[name="building_type_id"]').html(options);
            },
            error: function (response) {
            },
        });
    }
    else{
        $('#service_package_new_form select[name="building_type_id"]').html(`<option value="">-- Select Category --</option>`);
    }
});