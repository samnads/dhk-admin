var stafflistDataTable;
$(document).ready(function () {
	/**
     * Function return table of contents with particular customer's pending invoices
     */
	stafflistDataTable = $('#staffreportlist').DataTable({
        'bFilter': false,
        'bLengthChange': false,
        'pageLength': 50,
        'processing': true,
        'serverSide': true,
        'bSort': false,
        'serverMethod': 'get',
        'ajax': {
            'url': _base_url + 'laravel/data/report/staff_report',
            'data': function (data) {
                data.filter_date_from = $('#staff_from_date').val() ? moment($('#staff_from_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD') : undefined;
                data.filter_date_to = $('#staff_to_date').val() ? moment($('#staff_to_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD') : undefined;
                data.filter_staff = $('#staff_type_id').val() || undefined;
            },
            "complete": function (json, type) {
				$('.mm-loader').css('display','none');
                var _resp = $.parseJSON(json.responseText);
                console.log(_resp);
                // console.log(_resp.totalBalance);
                $('#hours-booked').text(_resp.totalhoursbooked);
                $('#hours-free').text(_resp.totalhoursfree);
                $('#hours-absent').text(_resp.totalhoursabsent);
                $('#total-amt').text(_resp.totalinvamt);
            }
        },
        'columns': [
            { data: 'slno' },
            { data: 'staffname' },
            { data: 'hoursbooked' },
            { data: 'hoursfree' },
            { data: 'hoursabsent' },
            { data: 'totalamt' },
            // { data: 'amt_paid' },
            // { data: 'bal_amt' },
        ],
    });
	
	$('#staff_from_date,#staff_to_date').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
    });

    $('#staff_from_date').change(function () {
		$('.mm-loader').css('display','block');
        stafflistDataTable.draw();
    });

    $('#staff_to_date').change(function () {
		$('.mm-loader').css('display','block');
        stafflistDataTable.draw();
    });

    $('#staff_type_id').change(function () {
		$('.mm-loader').css('display','block');
        stafflistDataTable.draw();
    });
	
	$('#staffexcelbtn').click(function () {
        $("#staffexcelbtn").html('<i class="fa fa-spinner fa-spin"></i>');
        var fromdate = $('#staff_from_date').val() ? moment($('#staff_from_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD') : undefined;
        var todate = $('#staff_to_date').val() ? moment($('#staff_to_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD') : undefined;
        var staffid = $('#staff_type_id').val();
		
        $.ajax({
            type: "GET",
            // url: _base_url + 'customerexcel/customeroutstandingexportexcel',
            url: _base_url + 'laravel/data/report/staff_report_excel',
            data: { filter_date_from: fromdate, filter_date_to: todate, filter_staff_id: staffid },
            cache: false,
            success: function (response) {
                $("#staffexcelbtn").html('<i class="fa fa-file-excel-o"></i>');
				$('#stafftablebody').html(response.aaData);
				console.log(response);
                // window.location = response;
				var fileName = "Staff Report";
				  var fileType = "xlsx";
				  var table = document.getElementById("table-for-excel-staff");
				  var wb = XLSX.utils.table_to_book(table, {sheet: "Report"});
				  const ws = wb.Sheets['Report'];
				  var wscols = [
					{wch:10},
					{wch:15},
					{wch:15},
					{wch:15},
					{wch:15},
					{wch:30},
				  ];
				  ws['!cols'] = wscols;
					return XLSX.writeFile(wb, null || fileName + "." + (fileType || "xlsx"));
            }
        });
    });
	
});