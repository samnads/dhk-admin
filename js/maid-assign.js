function getTimeToOptionsHtml(from_time) {
    var _time_to_options = '';
    $('#assign_form select[name="time_from"] option').each(function (index, option) {
        if ($(option).val() > from_time) {
            _time_to_options += '<option value="' + $(option).val() + '">' + $(option).text() + '</option>';
        }

    });
    return _time_to_options;
}
$('#assign_form select[name="time_from"]').on('change', function () {
    $('#assign_form select[name="time_to"]').html(getTimeToOptionsHtml(this.value)).select2().val("").trigger("change.select2");
});
$('body').on('change', '#assign_form select[name="time_from"],#assign_form select[name="time_to"]', function () {
    $('#assign_form').submit();
});
$('body').on('change', '#assign_form select[name="booking_type"]', function () {
    var booking_type = this.value;
    if (booking_type == "OD") {
        $('#assign_form').submit();
    }
    else if (booking_type == "WE") {

    }
    else if (booking_type == "BW") {

    }
});
// preload
$('#assign_form select[name="time_from"]').select2().val(time_from).trigger("change.select2");
$('#assign_form select[name="time_to"]').html(getTimeToOptionsHtml(time_from)).select2().val(time_to).trigger("change.select2");
$('#assign_form select[name="booking_type"]').prop('disabled', true);
//
function reloadAvailability() {
    $('#maids-holder').html(`<tr>
                                    <td colspan="4" class="text-center text-success">Checking Availability...</td>
                                </tr>`);
    $('#maids-list-holder ol').empty();
    $('#maids-list-holder').hide(200);
}
var currentRequest = null;
$().ready(function () {
    $('#assign_form input[name="service_start_dateDELETE"]').datepicker({
        format: 'dd/mm/yyyy',
        todayHighlight: false,
        todayBtn: "linked",
        autoclose: true,
        startDate: "today"
    });
    assign_form_validator = $('#assign_form').validate({
        ignore: [],
        rules: {
            service_start_date: {
                required: true,
            },
            booking_type: {
                required: true,
            },
            time_from: {
                required: true,
            },
            time_to: {
                required: true,
            },
        },
        messages: {
            service_start_date: "Select service date",
            booking_type: "Select repeat type.",
            time_from: "Select service start time.",
            time_to: "Select service end time.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "time_to") {
                error.insertAfter(element.parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            /****************************** */
            // format before send
            var values = $("#assign_form").serializeArray();
            var service_start_date_i = values.map(function (o) { return o.name; }).indexOf("service_start_date");
            if (service_start_date_i != -1 && values[service_start_date_i]['value']) {
                values[service_start_date_i]['value'] = moment(values[service_start_date_i]['value'], 'DD/MM/YYYY').format('YYYY-MM-DD');
            }
            var service_end_date_i = values.map(function (o) { return o.name; }).indexOf("service_end_date");
            if (service_end_date_i != -1 && values[service_end_date_i]['value']) {
                values[service_end_date_i]['value'] = moment(values[service_end_date_i]['value'], 'DD/MM/YYYY').format('YYYY-MM-DD');
            }
            values = jQuery.param(values);
            /****************************** */
            currentRequest = $.ajax({
                type: 'GET',
                dataType: "json",
                url: _base_url + "laravel/maid/check_free_maids_for_booking",
                data: values,
                beforeSend: function () {
                    $('.mm-loader').show();
                    reloadAvailability();
                    if (currentRequest != null) {
                        currentRequest.abort();
                    }
                },
                success: function (data) {
                    $('#assign_form input[name="customer_name"]').val(data.customer.customer_name);
                    $('#assign_form input[name="booked_amount"]').val(data.booking_grouped.total_amount);
                    $('#assign_form input[name="convenience_fee"]').val((data.bookings[0].pay_by.toLowerCase() == 'card' ? data.booking_grouped.pay_by_card_charge : 0));
                    $('#assign_form input[name="discount"]').val(data.booking_grouped.discount);
                    $('#assign_form input[name="total_amount"]').val(data.booking_grouped.total_amount + (data.bookings[0].pay_by.toLowerCase() == 'card' ? data.booking_grouped.pay_by_card_charge : 0));
                    var maid_rows_html = ``;
                    if (typeof data.available_maids !== 'undefined' && data.available_maids.length > 0) {
                        $.each(data.available_maids, function (key, maid) {
                            var shifts_row_html = ``;
                            $.each(maid.bookings, function (b_key, booking) {
                                shifts_row_html += `<span class="color-box bg-` + booking.booking_type.toLowerCase() + `" data-booking-id="` + booking.booking_id + `">` + moment(booking.time_from, 'HH:mm A').format('hh:mm A') + ` - ` + moment(booking.time_to, 'HH:mm A').format('hh:mm A') + `
                                                    <br>
                                                    ` + booking.booking_area_name + `
                                                    </span>`;
                            });
                            maid_rows_html += `<tr>
                                                <td class="text-center">`+ (key + 1) + `</td>
                                                <td>
                                                    <div class="n-days">
                                                        <input data-name="`+ maid.maid_name + `" id="maid-` + maid.maid_id + `" type="checkbox" value="` + maid.maid_id + `" name="selected_maids[]">
                                                        <label class="clearfix" for="maid-`+ maid.maid_id + `">
                                                            <span class="border-radius-3"></span>
                                                        </label>
                                                    </div>
                                                </td>
                                                <td data-id="`+ maid.maid_id + `">` + maid.maid_name + `</td>
                                                <td>` + (shifts_row_html == '' ? '<span class="badge badge-light ml-1">No Bookings</span>' : shifts_row_html) + `</td>
                                            </tr>`;
                        });
                    }
                    else {
                        maid_rows_html = `<tr>
                                    <td colspan="4" class="text-center text-danger">No Maids available in your search criteria ! </td>
                                </tr>`;
                    }
                    $('#maids-holder').html(maid_rows_html);
                    /************************************* */
                    // set event handler
                    $('#assign_form input[name="selected_maids[]"]').on('change', function () {
                        if ($('#assign_form input[name="selected_maids[]"]:checked').length == data.input.no_of_maids) {
                            // requested maid counnts selected
                            $('#assign_form input[name="selected_maids[]"]:not(:checked)').attr("disabled", true);
                        }
                        else {
                            // requested maid counts not  selected
                            $('#assign_form input[name="selected_maids[]"]').attr("disabled", false);
                        }
                        if ($('#assign_form input[name="selected_maids[]"]:checked').length > data.input.no_of_maids) {
                            this.checked = false;
                            toast('warning', data.input.no_of_maids + ' maids already selected !');
                        }
                        else {
                        }
                        var maids_list_html = ``;
                        $('#assign_form input[name="selected_maids[]"]:checked').each(function () {
                            maids_list_html += `<li>` + $(this).attr("data-name") + `</li>`;
                        });
                        if (maids_list_html != '') {
                            $('#maids-list-holder ol').html(maids_list_html);
                            $('#maids-list-holder').show(200);
                        }
                        else {
                            $('#maids-list-holder').hide(200);
                        }
                    });
                    /************************************* */
                    $('.mm-loader').hide();
                },
                error: function (data) {
                    $('.mm-loader').hide();
                },
            });
        }
    });
    $('#assign_form').submit();
});
$('#assign_form [data-action="book-now"]').click(function () {
    var maids_requested_count = $('#assign_form input[name="no_of_maids"]').val();
    var maids_selected_count = $('#assign_form input[name="selected_maids[]"]:checked').length;
    if ($('#assign_form').valid()) {
        if (maids_requested_count == maids_selected_count) {
            $('.mm-loader').show();
            /****************************** */
            // format before send
            var values = $("#assign_form").serializeArray();
            var service_start_date_i = values.map(function (o) { return o.name; }).indexOf("service_start_date");
            if (service_start_date_i != -1 && values[service_start_date_i]['value']) {
                values[service_start_date_i]['value'] = moment(values[service_start_date_i]['value'], 'DD/MM/YYYY').format('YYYY-MM-DD');
            }
            var service_end_date_i = values.map(function (o) { return o.name; }).indexOf("service_end_date");
            if (service_end_date_i != -1 && values[service_end_date_i]['value']) {
                values[service_end_date_i]['value'] = moment(values[service_end_date_i]['value'], 'DD/MM/YYYY').format('YYYY-MM-DD');
            }
            values = jQuery.param(values);
            /****************************** */
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "laravel/booking/assign_maids",
                data: values,
                beforeSend: function () {
                },
                success: function (data) {
                    if (data.success == true) {
                        toast('success', data.message);
                        setTimeout(function () {
                            window.location.replace(_base_url + "booking/approvallist");
                        }, 3000);
                    }
                    else {
                        toast('error', data.message);
                        $('.mm-loader').hide();
                    }
                },
                error: function (data) {
                    $('.mm-loader').hide();
                }
            });
        }
        else {
            toast('warning', 'Please select maximum of ' + maids_requested_count + ' maids.');
        }
    }
});