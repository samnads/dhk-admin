// Initiate graphs

var coffees = {
  "Mon" : 3,
  "Tues": 2,
  "Wed" : 3,
  "Thu" : 2,
  "Fri" : 1.5,
  "Sat" : 1,
  "Sun" : 2
   };


  $('.graph-coffees').graphiq({
    data: coffees,
    fluidParent: ".col",
    height: 140,
    xLineCount: 3,
    dotRadius: 5,
    yLines: true,
    xLines: true,
    dots: true,
    colorLine: "#00a651",
    colorDot: "#F00",
    colorXGrid: "#eee",
    colorUnits: "#00a651",
    colorFill: "#9aff00",
    dotStrokeWeight: 3,
  });