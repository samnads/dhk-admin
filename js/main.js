/************************************* Coder : Samnad. S ****************************** */
// This js file is used in the entire site
/************************************* HIGHLIGHT ACTIVE MENU *************************** */
$(function () {
    var current = window.location.href;
    $('#primary_nav_wrap li a').each(function () {
        var $this = $(this);
        // if the current path is like this link, make it active
        if ($this.attr('href') === current) {
            $this.addClass('active');
        }
    })
});
function fancybox_show(id,settings = {}) {
    width = settings.width || 450;
    height = settings.height || 'auto';
    closeClick = settings.closeClick || false;
    padding = settings.padding || 0;
    $.fancybox.open({
        autoCenter: true,
        fitToView: false,
        scrolling: false,
        openEffect: 'fade',
        openSpeed: 1,
        autoSize: false,
        width: width,
        height: height,
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.3)'
                },
                closeClick: closeClick
            }
        },
        padding: 0,
        closeBtn: false,
        content: $('#' + id),
    });
}
function toast(type, message,options = {}) {
    toastr.options.positionClass = 'toast-bottom-right';
    if (type == 'error') {
        toastr.error(message);
    }
    else if (type == 'success') {
        toastr.success(message);
    }
    else if (type == 'warning') {
        toastr.warning(message);
    }
}

if($('.sel2').length > 0) $('.sel2').select2();