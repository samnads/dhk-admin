var paymentlisttable;
$(document).ready(function () {
	paymentlisttable = $('#paymentlisttable').DataTable({
        'bFilter': false,
        'bLengthChange': false,
        'pageLength': 100,
        'processing': true,
        'serverSide': true,
        'bSort': false,
        'serverMethod': 'post',
        'ajax': {
            'url': _base_url + 'customer/list_ajax_customer_payments_list',
            'data': function (data) {
                if ($('#payment-date').length) {
                    var regdate = $('#payment-date').val();
                    data.regdate = regdate;
                } else {
                    data.regdate = '';
                }
                if ($('#payment-date-to').length) {
                    var regdateto = $('#payment-date-to').val();
                    data.regdateto = regdateto;
                } else {
                    data.regdateto = '';
                }
                data.custid = $('#customers_vh_rep_new').val();
                data.status = $('#pay_status').val();
                data.paytype = $('#pay_type').val();
            },
            "complete": function (json, type) {
                var _resp = $.parseJSON(json.responseText);
				$("#totnetamt").text(_resp.totalnetamount);
				$("#paidamt").text(_resp.totalpaidamount);
				$("#balamt").text(_resp.totalbalamount);
            }
        },
        'columns': [
            { data: 'slno' },
            { data: 'date' },
            { data: 'customer' },
            { data: 'name' },
            { data: 'memo' },
            { data: 'receipt' },
            { data: 'type' },
            { data: 'amount' },
            { data: 'paidamount' },
            { data: 'balamount' },
            { data: 'payfrom' },
            { data: 'status' },
            { data: 'action' },
        ],
    });
	
	$('#payment-date, #payment-date-to').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
    });
	
	$('#payment-date, #payment-date-to').change(function () {
        paymentlisttable.draw();
    });
	
	$('#customers_vh_rep_new').change(function () {
        paymentlisttable.draw();
    });
	
	$('#pay_status').change(function () {
        paymentlisttable.draw();
    });
	
	$('#pay_type').change(function () {
        paymentlisttable.draw();
    });
	
	// $("#keyword-search").keyup(function () {
        // var vall = $('#keyword-search').val().length
        //if (vall >= 3) {
		// invoicelisttable.draw();
        //}
    // });
	
	$('#paymentexceldownload').click(function () {
        $("#paymentexceldownload").html('<i class="fa fa-spinner fa-spin"></i>');
        var status = $('#pay_status').val();
        var fromdate = $('#payment-date').val();
        var todate = $('#payment-date-to').val();
        var custid = $('#customers_vh_rep_new').val();
        var paytype = $('#pay_type').val();
        $.ajax({
            type: "POST",
            url: _base_url + 'customerexcel/customerpaymentexcel',
            data: { fromdate: fromdate, todate: todate, status: status, custid: custid, paytype: paytype },
            cache: false,
            success: function (response) {
                $("#paymentexceldownload").html('<i class="fa fa-file-excel-o"></i>');
                window.location = response;
            }
        });
    });
});