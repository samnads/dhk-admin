$('#invoice_month').datepicker({
    format: 'yyyy-mm-dd',
    minViewMode: 1,
    autoclose: true
});
$('#invoice_issue_date').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true
});
$('#invoice_due_date').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true
});