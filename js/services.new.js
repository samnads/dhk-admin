$().ready(function () {
    $('#services_new_form').validate({
        focusInvalid: false,
        rules: {
            service_type_name: {
                required: true,
            },
            customer_app_service_type_name: {
                required: true,
            },
            info_html: {
                required: false,
            },
            'service_type_category[]': {
                required: true,
            },

        },
        messages: {
            service_type_name: "Enter service name.",
            customer_app_service_type_name: "Enter customer app service name.",
            'service_type_category[]': "select a service Category",
            info_html: "Add some html info.",
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "service_type_category[]") {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $('#services_sub').html('Please wait...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "services/save_service",
                data: $('#services_new_form').serialize(),
                success: function (data) {
                    if (data.status == true) {
                        $('#services_sub').html('Saved');
                        alert(data.message);
                        window.location.href = _base_url + "services";
                    }
                    else {
                        alert(data.message);
                        $('#services_sub').html('Save').removeAttr("disabled");
                    }
                },
                error: function (data) {
                    alert("An error occured!");
                    $('#services_sub').html('Save').removeAttr("disabled");
                },
            });
        }
    });
    var quill = new Quill('#quill_editor', {
        theme: 'snow'
    });
    quill.on('text-change', function (delta, oldDelta, source) {
        document.getElementById("info_html").value = quill.root.innerHTML;
    });
});