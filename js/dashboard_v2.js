/********************************************** */
// Coder - Samnad. S                            */
//                                              */
// Created - 02-02-2023                         */
// Updated -                                    */
//                                              */
/********************************************** */
var _dashboard_data = null; // for storing new data
var _last_activity = null; // for storing latest activity
var _first_activity = null; // for storing latest activity
var _loading_html = `<div class="spinner-border text-primary" style="font-size: small;"role="status"></div>`;
function loadActivities() {
    // this can be used for realtime activity updation
    $.ajax({
        url: _base_url + 'dashboard_data_v2/recent_activities',
        cache: false,
        data: {
            last_activity: _last_activity != null ? _last_activity : undefined,
            first_activity: _first_activity != null ? _first_activity : undefined
        },
        dataType: "json",
        success: updateDashboardData
    });
}
/********************************************** */
function renderActivities(data) {
    var template = '';
    $.each(data, function (key, activity) {
        template += `<li>
        <a href="#" style="color:#428bca">
        `+ activity.action_type_icon + `</i>&nbsp;&nbsp;` + activity.content + `
        <span class="activity-time"><i class="fa fa-clock-o"></i>&nbsp;<b>`+ activity.user + `</b><i>&nbsp;` + activity.time + `</i></span>
        <div class="clear"></div></a></li>`;
    });
    $('#recent_activities').empty()
    $('#recent_activities').html(template)
}
/********************************************** */
function updateDashboardData(data) {
    if (_dashboard_data != null && _dashboard_data.recent_activities.length > 0) {
        _last_activity = Math.max(..._dashboard_data.recent_activities.map(o => o.id));
        _first_activity = Math.min(..._dashboard_data.recent_activities.map(o => o.id));
    }
    _dashboard_data = data; // update with new data
    if (_dashboard_data != null) {
        renderActivities(_dashboard_data.recent_activities);
    }
    if (_dashboard_data != null && _dashboard_data.recent_activities.length > 0) {
        _last_activity = Math.max(..._dashboard_data.recent_activities.map(o => o.id));
        _first_activity = Math.min(..._dashboard_data.recent_activities.map(o => o.id));
    }
}
/********************************************** */
function loadBookings() {
    $.ajax({
        url: _base_url + 'dashboard_data_v2/bookings',
        cache: false,
        dataType: "json",
        success: function (data) {
            // bookings
            $('#bookings_company_total').html(Number(data.bookings.total - data.bookings.partner_total).toLocaleString());
            $('#bookings_partner_total').html(Number(data.bookings.partner_total).toLocaleString());
            // booking hours
            $('#bookings_total_hours').html(Number(data.bookings.total_hours).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('#bookings_company_total_hours').html((data.bookings.total_hours - data.bookings.partner_total_hours).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('#bookings_partner_total_hours').html(Number(data.bookings.partner_total_hours).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
        }
    });
}
/********************************************** */
function todayCounts() {
    $.ajax({
        url: _base_url + 'dashboard_data_v2/today_counts',
        cache: false,
        dataType: "json",
        success: function (data) {
            // bookings
            $('#bookings_today_total').html(Number(data.today.total_bookings).toLocaleString());
            // booking hours
            $('#bookings_today_total_hours').html(Number(data.today.total_booking_hours).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
        }
    });
}
/********************************************** */
function monthlyCounts() {
    $.ajax({
        url: _base_url + 'dashboard_data_v2/monthly_counts',
        cache: false,
        dataType: "json",
        success: function (data) {
            // bookings
            $('#bookings_monthly_total').html(data.monthly.total_bookings);
            // booking hours
            $('#bookings_monthly_total_hours').html(Number(data.monthly.total_booking_hours).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
        }
    });
}
/********************************************** */
function loadCancelBookings() {
    $.ajax({
        url: _base_url + 'dashboard_data_v2/cancel_bookings',
        cache: false,
        dataType: "json",
        success: function (data) {
            // bookings cancel
            $('#booking_cancel_today_total').html(data.cancel_bookings.today_total);
            $('#booking_cancel_today_hours').html(Number(data.cancel_bookings.today_hours).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
        }
    });
}
/********************************************** */
function loadCustomers() {
    $.ajax({
        url: _base_url + 'dashboard_data_v2/customers',
        cache: false,
        dataType: "json",
        success: function (data) {
            $('#customers_total').html(Number(data.customers.total).toLocaleString());
        }
    });
}
/********************************************** */
function loadComplaints() {
    $('#complaints_today_total').html(_loading_html);
    $('#complaints_last_week_total').html(_loading_html);
    $('#complaints_this_month_total').html(_loading_html);
    $.ajax({
        url: _base_url + 'dashboard_data_v2/complaints',
        cache: false,
        dataType: "json",
        success: function (data) {
            // complaints
            $('#complaints_today_total').html(data.complaints.today_total);
            $('#complaints_last_week_total').html(data.complaints.last_week_total);
            $('#complaints_this_month_total').html(data.complaints.this_month_total);
        }
    });
}
/********************************************** */
function loadPayments() {
    $.ajax({
        url: _base_url + 'dashboard_data_v2/payments',
        cache: false,
        dataType: "json",
        success: function (data) {
            // payments
            $('#payments_today_total_amount').html(Number(data.payments.today_total_amount).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('#payments_today_collected_amount').html(Number(data.payments.today_collected_amount).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('#payments_today_pending_amount').html(Number(data.payments.today_total_amount - data.payments.today_collected_amount).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            //
            $('#payments_weekly_total_amount').html(Number(data.payments.weekly_total_amount).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('#payments_weekly_collected_amount').html(Number(data.payments.weekly_collected_amount).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('#payments_weekly_pending_amount').html(Number(data.payments.weekly_total_amount - data.payments.weekly_collected_amount).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            //
            $('#payments_monthly_total_amount').html(Number(data.payments.monthly_total_amount).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('#payments_monthly_collected_amount').html(Number(data.payments.monthly_collected_amount).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('#payments_monthly_pending_amount').html(Number(data.payments.monthly_total_amount - data.payments.monthly_collected_amount).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
        }
    });
}
/********************************************** */
function loadChartData() {
    $.ajax({
        url: _base_url + 'dashboard_data_v2/booking_chart_data',
        cache: false,
        dataType: "json",
        success: function (data) {
            // chart
            //$("#chart-3").jqBarGraph("clear");
            $('#chart-3').html('');
            $('#chart-3').jqBarGraph({
                data: data.graph_data,
                colors: [color_bg_booking_od, color_bg_booking_we, color_bg_booking_bw],
                legends: [od_booking_text, we_booking_text, bw_booking_text],
                legend: true,
                //width: 86%,
                //height: 80%,
                color: '#ffffff',
                type: 'multi',
                postfix: '',
                speed: 1
            });
        }
    });
}
/********************************************** */
// first time load
loadActivities();
loadBookings();
todayCounts();
monthlyCounts();
loadCancelBookings();
loadCustomers();
loadComplaints();
loadPayments();
loadChartData();
/********************************************** */
// timer load
$(function () {
    setInterval(loadActivities, 3000);
    setInterval(loadCustomers, 3000);
    setInterval(loadComplaints, 60000);
    setInterval(loadCancelBookings, 3000);
    setInterval(loadBookings, 3000);
    setInterval(todayCounts, 3000);
    setInterval(monthlyCounts, 3000);
    setInterval(loadPayments, 10000);
    setInterval(loadChartData, 60000);
    setInterval(loadSaleStatistics, 3600000);
});
/********************************************** */
var map_data = [];
var map;
var markers = [];
var image = "https://cdn-icons-png.flaticon.com/64/6395/6395395.png";
var infowindow;
// Sets the map on all markers in the array.
function setMapOnAll(map) {
    for (let i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
    }
}
// Removes the markers from the map, but keeps them in the array.
function hideMarkers() {
    setMapOnAll(null);
}
function loadDriversMapData(refresh = false) {
    $.ajax({
        url: _base_url + 'dashboard_data_v2/driver_locations',
        cache: false,
        dataType: "json",
        success: function (data) {
            if (refresh == true) {
                // refresh map marker
                hideMarkers();
                markers = [];
                // load new marker
                $.each(data.data, function (index, driver) {
                    if (index == data.data.length - 1) {
                        // search for new data
                        let search = map_data.filter(obj => obj.tablet_id == driver.tablet_id && obj.added == driver.added)[0];
                        marker = new google.maps.Marker({
                            position: {
                                lat: Number(driver.latitude),
                                lng: Number(driver.longitude)
                            },
                            map: map,
                            icon: image,
                            id: driver.tablet_id,
                            html: '<p><b><i class="fa fa-user"></i>&nbsp;&nbsp;&nbsp;' + driver.tablet_driver_name + '</p>',
                            animation: (search && search.tablet_id) ? undefined : google.maps.Animation.DROP, // add animation only if new found
                        });
                        // set last marker as center
                        map.setCenter(new google.maps.LatLng(Number(driver.latitude), Number(driver.longitude)))
                    }
                    else {
                        marker = new google.maps.Marker({
                            position: {
                                lat: Number(driver.latitude),
                                lng: Number(driver.longitude)
                            },
                            map: map,
                            icon: image,
                            id: driver.tablet_id,
                            html: '<p><b><i class="fa fa-user"></i>&nbsp;&nbsp;&nbsp;' + driver.tablet_driver_name + '</p>'
                        });
                    }
                    markers.push(marker);
                    google.maps.event.addListener(marker, 'click', (function (marker, index) {
                        return function () {
                            infowindow.setContent(marker.html);
                            infowindow.open(map, marker);
                        }
                    })(marker, index));
                });
                map_data = data.data;
            }
            else {
                map_data = data.data;
                // load new marker
                $.each(data.data, function (index, driver) {
                    if (index == data.data.length - 1) {
                        // last marker
                        marker = new google.maps.Marker({
                            position: {
                                lat: Number(driver.latitude),
                                lng: Number(driver.longitude)
                            },
                            map: map,
                            icon: image,
                            id: driver.tablet_id,
                            html: '<p><b><i class="fa fa-user"></i>&nbsp;&nbsp;&nbsp;' + driver.tablet_driver_name + '</p>',
                            animation: google.maps.Animation.DROP,
                        });
                        // set last marker as center
                        map.setCenter(new google.maps.LatLng(Number(driver.latitude), Number(driver.longitude)))
                    }
                    else {
                        marker = new google.maps.Marker({
                            position: {
                                lat: Number(driver.latitude),
                                lng: Number(driver.longitude)
                            },
                            map: map,
                            icon: image,
                            id: driver.tablet_id,
                            html: '<p><b><i class="fa fa-user"></i>&nbsp;&nbsp;&nbsp;' + driver.tablet_driver_name + '</p>'
                        });
                    }
                    /*new MapLabel({
                        text: driver.zone_name,
                        position: new google.maps.LatLng(Number(driver.latitude), Number(driver.longitude)),
                        map: map,
                        fontSize: 15,
                        fontColor: 'red',
                        align: 'center'
                    });*/
                    markers.push(marker);
                    google.maps.event.addListener(marker, 'click', (function (marker, index) {
                        return function () {
                            infowindow.setContent(marker.html);
                            infowindow.open(map, marker);
                        }
                    })(marker, index));
                });
            }
        }
    });
}
/**************************************** MAP CODES ****** */
var mapOptions = {
    zoom: 12,
    center: {
        lat: 25.2048,
        lng: 55.2708
    }
}; // map options
var map_data = []
function initialize() {
    map = new google.maps.Map(document.getElementById('driver-map'), mapOptions);
    infowindow = new google.maps.InfoWindow();
    loadDriversMapData(false);
    setInterval(function () { loadDriversMapData(true); }, 10000);
}
window.addEventListener('load', initialize)
/******************************************************************** */
const ctx = document.getElementById('myChart');
ctx.style.backgroundColor = '#ccffcc';
salesChart = undefined;
function renderSalesChart(labels = [], revenue = [], hours = []) {
    var animation_duration = 0;
    if (salesChart !== undefined) {
        salesChart.destroy();
        animation_duration = 1000;
    }
    salesChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: labels,
            datasets: [{
                label: 'Revenue',
                data: revenue,
                stack: 'combined',
                type: 'bar',
                order: 2,
                backgroundColor: ['52bedb'],
                barPercentage: .65,
            },
            {
                yAxisID: 'y1',
                label: 'Hours',
                data: hours,
                borderWidth: 1,
                borderColor: 'rgb(222 69 87)',
                backgroundColor: 'rgb(222 69 87)',
                stack: 'combined',
                type: 'line',
                order: 1,
                pointStyle: 'rect',
                pointRadius: 4,
            }]
        },
        options: {
            scales: {
                x: {
                    grid: {
                        display: false
                    },
                    stacked: false,
                    ticks: {
                        beginAtZero: true,
                    },
                    title: {
                        display: true,
                        text: 'Year | Month'
                    }
                },
                y: {
                    grid: {
                        display: false
                    },
                    stacked: false,
                    ticks: {
                        beginAtZero: true,
                    },
                    title: {
                        display: true,
                        text: 'Revenue (AED)'
                    }
                },
                y1: {
                    type: 'linear',
                    display: true,
                    position: 'right',
                    grid: {
                        display: false,
                    },
                    title: {
                        display: true,
                        text: 'Hours Worked'
                    }
                },
            },
            animation: {
                duration: animation_duration
            },
            plugins: {
                legend: {
                    display: true
                },
            }
        }
    });
    salesChart.render();
    /******************************************* */
    // change current month color
    var labels = salesChart.data.labels;
    for (i = 0; i < labels.length; i++) {
        if (labels[i] == moment().format('YYYY MMM')) {
            salesChart.data.datasets[0].backgroundColor[i] = "rgb(122, 194, 85)";
            //salesChart.data.datasets[0].borderWidth[i] = "5";
        }
        else {
            salesChart.data.datasets[0].backgroundColor[i] = "#52bedb";
        }
    }
    /******************************************* */
    salesChart.update();
}
renderSalesChart();
function loadSaleStatistics() {
    renderSalesChart([], [], [])
    $.ajax({
        url: _base_url + 'dashboard_data_v2/monthly_sales_statistics_between_dates',
        cache: false,
        data: { filter_from_date: moment($('#filter_from_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD'), filter_to_date: moment($('#filter_to_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD') },
        dataType: "json",
        success: function (data) {
            renderSalesChart(data.chart_data.labels, data.chart_data.monthly_revenue, data.chart_data.monthly_hours);
        }
    });
}
/******************************************************************** */
var monthly_payments_between_dates_Request = null;
function monthly_payments_between_dates() {
    $('#month_range_invoiced_amount').html(_loading_html);
    $('#month_range_collected_amount').html(_loading_html);
    $('#month_range_pending_amount').html(_loading_html);
    monthly_payments_between_dates_Request = $.ajax({
        url: _base_url + 'dashboard_data_v2/monthly_payments_between_dates',
        cache: false,
        data: { filter_from_date: moment($('#filter_from_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD'), filter_to_date: moment($('#filter_to_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD') },
        dataType: "json",
        beforeSend: function () {
            if (monthly_payments_between_dates_Request != null) {
                monthly_payments_between_dates_Request.abort();
            }
        },
        success: function (data) {
            $('#month_range_invoiced_amount').html(Number(data.total_invoiced_amount).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('#month_range_collected_amount').html(Number(data.total_collected_amount).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('#month_range_pending_amount').html(Number(data.total_invoiced_amount - data.total_collected_amount).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
        }
    });
}
monthly_payments_between_dates();
/******************************************************************** */
var hours_data_between_dates_Request = null;
function hours_data_between_dates() {
    $('#month_range_total_hours').html(_loading_html);
    $('#month_range_available_hours').html(_loading_html);
    $('#month_range_available_maids').html(_loading_html);
    $('#month_range_booked_hours').html(_loading_html);
    $('#month_range_free_hours').html(_loading_html);
    $('#month_range_cancelled_hours').html(_loading_html);
    hours_data_between_dates_Request = $.ajax({
        url: _base_url + 'dashboard_data_v2/hours_data_between_dates',
        cache: false,
        data: { filter_from_date: moment($('#filter_from_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD'), filter_to_date: moment($('#filter_to_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD') },
        dataType: "json",
        beforeSend: function () {
            if (hours_data_between_dates_Request != null) {
                hours_data_between_dates_Request.abort();
            }
        },
        success: function (data) {
            $('#month_range_total_hours').html(Number(data.total_hours).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('#month_range_available_hours').html(Number(data.available_hours).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('#month_range_available_maids').html(Number(data.maid_count).toLocaleString('en-US', {
                minimumFractionDigits: 0,
                maximumFractionDigits: 0
            }));
            $('#month_range_booked_hours').html(Number(data.booking_hours).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('#month_range_free_hours').html(Number(data.available_hours - data.booking_hours).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
            $('#month_range_cancelled_hours').html(Number(data.cancelled_hours).toLocaleString('en-US', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }));
        }
    });
}
hours_data_between_dates();
/******************************************************************** */
var booking_counts_between_dates_Request = null;
function booking_counts_between_dates() {
    $('#date_range_od_bookings').html(_loading_html);
    $('#date_range_except_od_bookings').html(_loading_html);
    $('#date_range_cancelled_bookings').html(_loading_html);
    booking_counts_between_dates_Request = $.ajax({
        url: _base_url + 'dashboard_data_v2/booking_counts_between_dates',
        cache: false,
        data: { filter_from_date: moment($('#filter_from_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD'), filter_to_date: moment($('#filter_to_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD') },
        dataType: "json",
        beforeSend: function () {
            if (booking_counts_between_dates_Request != null) {
                booking_counts_between_dates_Request.abort();
            }
        },
        success: function (data) {
            $('#date_range_od_bookings').html(Number(data.od_bookings).toLocaleString('en-US', {
                minimumFractionDigits: 0,
                maximumFractionDigits: 0
            }));
            var date_range_except_od_bookings = Number((data.we_bookings + data.bw_bookings));
            $('#date_range_except_od_bookings').html(date_range_except_od_bookings > 0 ? date_range_except_od_bookings : 0).toLocaleString('en-US', {
                minimumFractionDigits: 0,
                maximumFractionDigits: 0
            });
            $('#date_range_cancelled_bookings').html(Number(data.cancelled_bookings_count).toLocaleString('en-US', {
                minimumFractionDigits: 0,
                maximumFractionDigits: 0
            }));
        }
    });
}
booking_counts_between_dates();
/******************************************************************** */
loadSaleStatistics();
$(function () {
    const elem = document.querySelector('#filter-date-range');
    const date_filter = new DateRangePicker(elem, {
        format: 'dd/mm/yyyy',
        todayButton: true,
        todayButtonMode: 1,
        autohide: true
    });
    date_filter.element.addEventListener('changeDate', (e) => {
        loadSaleStatistics();
        monthly_payments_between_dates();
        hours_data_between_dates();
        booking_counts_between_dates();
    })
});
