$('#invoice_month').datepicker({
    format: 'yyyy-mm-dd',
    minViewMode: 1,
    autoclose: true
});
$('#invoice_issue_date,#invoice_from_date,#invoice_to_date').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true
});
$('#invoice_due_date').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true
});

$('#master_invoice_create').on('click', function (e) {
    if ($(this).is(':checked', true)) {
        $(".sub_inv_chk").prop('checked', true);
    } else {
        $(".sub_inv_chk").prop('checked', false);
    }
});

$('#generate-btn').on('click', function (e) {
    var bukVals = [];
    var dayVals = [];
    // var refVals = [];
    var issuedate = $("#invoice_issue_date").val();
    var duedate = $("#invoice_due_date").val();
    // var orgamt = $(this).attr('data-orgamt');
    // var payid = $(this).attr('data-payid');
    $(".sub_inv_chk:checked").each(function () {
        bukVals.push($(this).attr('data-bookingid'));
        dayVals.push($(this).attr('data-id'));
        // refVals.push($(this).attr('data-ref'));
    });
    if (dayVals.length <= 0) {
        $.fancybox.open({
            autoCenter: true,
            fitToView: false,
            scrolling: false,
            openEffect: 'none',
            openSpeed: 1,
            helpers: {
                overlay: {
                    css: {
                        'background': 'rgba(0, 0, 0, 0.3)'
                    },
                    closeClick: true
                }
            },
            padding: 0,
            closeBtn: true,
            content: _alert_html = '<div id="alert-popup"><div class="head">Error<span class="alert-popup-close pop_close2"></span></div><div class="content padd20">Please select booking...</div><div class="bottom"><input type="button" value="Close" class="assign_no pop_close2"  /></div></div>',
            topRatio: 0.2,

        });
    } else {
        var check = confirm("Are you sure you want to generate invoice?");
        if (check == true) {
            var join_selected_values = bukVals.join(",");
            var join_selected_dayvalues = dayVals.join(",");
			console.log(join_selected_dayvalues);
            // var join_selected_refvalues = refVals.join(",");
			
            $.ajax({
                url: _base_url + "monthly_invoice/generate_invoice",
                type: 'POST',
                // dataType: "json",
                data: { bukids: join_selected_values, dayids: join_selected_dayvalues, issuedate: issuedate, duedate: duedate },
                success: function (response) {
					var _resp = $.parseJSON(response);
                    if (_resp.message == "success") {
                        var messag = '';
                        $.fancybox.open({
                            autoCenter: true,
                            fitToView: false,
                            scrolling: false,
                            openEffect: 'none',
                            openSpeed: 1,
                            helpers: {
                                overlay: {
                                    css: {
                                        'background': 'rgba(0, 0, 0, 0.3)'
                                    },
                                    closeClick: true
                                }
                            },
                            padding: 0,
                            closeBtn: true,
                            content: _alert_html = '<div id="alert-popup"><div class="head">Success<span class="alert-popup-close pop_close2"></span></div><div class="content padd20">Invoice generated successfully.</div><div class="bottom"><input type="button" value="Close" class="assign_no pop_close2"  /></div></div>',
                            topRatio: 0.2,

                        });
                        window.location = _base_url + 'invoice/view_invoice/' + _resp.invoice_id;
                    } else {
						$.fancybox.open({
							autoCenter: true,
							fitToView: false,
							scrolling: false,
							openEffect: 'none',
							openSpeed: 1,
							helpers: {
								overlay: {
									css: {
										'background': 'rgba(0, 0, 0, 0.3)'
									},
									closeClick: true
								}
							},
							padding: 0,
							closeBtn: true,
							content: _alert_html = '<div id="alert-popup"><div class="head">Error<span class="alert-popup-close pop_close2"></span></div><div class="content padd20">Something went wrong. Try again.</div><div class="bottom"><input type="button" value="Close" class="assign_no pop_close2"  /></div></div>',
							topRatio: 0.2,

						});
						window.location.reload();
					}
                },
                error: function (response) {
					var _resp = $.parseJSON(response);
					if (_resp.message == "error")
					{
						$.fancybox.open({
							autoCenter: true,
							fitToView: false,
							scrolling: false,
							openEffect: 'none',
							openSpeed: 1,
							helpers: {
								overlay: {
									css: {
										'background': 'rgba(0, 0, 0, 0.3)'
									},
									closeClick: true
								}
							},
							padding: 0,
							closeBtn: true,
							content: _alert_html = '<div id="alert-popup"><div class="head">Error<span class="alert-popup-close pop_close2"></span></div><div class="content padd20">Something went wrong. Try again.</div><div class="bottom"><input type="button" value="Close" class="assign_no pop_close2"  /></div></div>',
							topRatio: 0.2,

						});
					}
					window.location.reload();
                }
            });
        }
    }
});