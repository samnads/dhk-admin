var customerlistDataTable;
var customerOutDataTable;
let datatable;
$(document).ready(function () {
    datatable = $('#sales-invoice-report').DataTable({
        'bFilter': false,
        'bLengthChange': true,
        'pageLength': 50,
        'processing': true,
        'serverSide': true,
        'bSort': true,
        'serverMethod': 'get',
        'ajax': {
            'url': _base_url + 'laravel/data/report/sales-invoice-report',
            'data': function (data) {
                data.filter = {};
                data.filter.from_date = $('[name="filter[from_date]"]').val() ? moment($('[name="filter[from_date]"]').val(), 'DD/MM/YYYY').format('YYYY-MM-DD') : undefined;
                data.filter.to_date = $('[name="filter[to_date]"]').val() ? moment($('[name="filter[to_date]"]').val(), 'DD/MM/YYYY').format('YYYY-MM-DD') : undefined;
            },
            complete: function (json, type) {
                if (json && json.responseText) {
                    var total = $.parseJSON(json.responseText).total;
                    $('tfoot .total').html(total.line_net_amount_page);
                    $('tfoot .grand-total').html(total.line_net_amount_full);
                    $('tfoot .total_booking_hours .page').html(total.booking_hours_page);
                    $('tfoot .total_booking_hours .full').html(total.booking_hours_full);
                }
            },
            beforeSend: function () {
                if (datatable && datatable.hasOwnProperty('settings')) {
                    // Abort duplicate / last pending request
                    datatable.settings()[0].jqXHR.abort();
                }
            }
        },
        'columns': [
            { data: 'slno', name: 'slno' },
            { data: 'service_date', name: 'service_date' },
            { data: 'booking_hours', name: 'booking_hours' },
            { data: 'line_net_amount_sum', name: 'line_net_amount_sum' }
        ],
        order: [[1, 'asc']],
        "columnDefs": [
            {
                targets: 0,
                orderable: false
            },
            {
                targets: 1,
                orderable: true,
                render: function (data, type, row, meta) {
                    let date = data ? moment(data, 'YYYY/MM/DD').format('DD/MM/YYYY') : '';
                    let day = data ? moment(data, 'YYYY/MM/DD').format('dddd') : '';
                    return `<div><span>${date}</span><span class="float-right small">${day}</span></div>`;
                }
            },
            {
                targets: 2,
                className: 'text-right'
            },
            {
                targets: 3,
                className: 'text-right'
            }]
    });
    $('[name="filter[from_date]"],[name="filter[to_date]"]').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    }).change(function (e) {
        datatable.draw();
    });



    $('[name="filter[from_date]"]').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    }).on("changeDate", function () {
        var startDate = $('[name="filter[from_date]"]').datepicker('getDate');
        var oneDayFromStartDate = moment(startDate).toDate();
        $('[name="filter[to_date]"]').datepicker('setStartDate', oneDayFromStartDate);
        $('[name="filter[to_date]"]').datepicker('setDate', oneDayFromStartDate);
        datatable.draw();
    });

    $('[name="filter[to_date]"]').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    }).on("show", function () {
        var startDate = $('[name="filter[from_date]"]').datepicker('getDate');
        $('.day.disabled').filter(function (index) {
            return $(this).text() === moment(startDate).format('D');
        }).addClass('active');
    }).on("changeDate", function () {
        datatable.draw();
    });

    $('[data-action="excel-export"]').click(function () {
        $('[data-action="excel-export"]').html('<i class="fa fa-spinner fa-spin"></i>');
        let from_date = $('[name="filter[from_date]"]').val() ? moment($('[name="filter[from_date]"]').val(), 'DD/MM/YYYY').format('YYYY-MM-DD') : undefined;
        let to_date = $('[name="filter[to_date]"]').val() ? moment($('[name="filter[to_date]"]').val(), 'DD/MM/YYYY').format('YYYY-MM-DD') : undefined;
        let data = {};
        data.filter = {
            from_date: from_date,
            to_date: to_date
        }
        $.ajax({
            type: "GET",
            url: _base_url + 'laravel/data/report/sales-invoice-report/excel',
            data: data,
            cache: false,
            success: function (response) {
                $('[data-action="excel-export"]').html('<i class="fa fa-file-excel-o"></i>');
                $('#sales-invoice-report-excel tbody').html(response.html);
                var fileName = "Sales Invoice Report " + from_date + " " + to_date + ".xlsx";
                var table = document.getElementById("sales-invoice-report-excel");
                var wb = XLSX.utils.table_to_book(table, { sheet: "Report", raw: true });
                const ws = wb.Sheets['Report'];
                var wscols = [
                    { wch: 10 },
                    { wch: 30 },
                    { wch: 20 }
                ];
                ws['!cols'] = wscols;
                return XLSX.writeFile(wb, fileName);
            }
        });
    });
});