$(".toggle-status").click(function () {
    $.ajax({
        type: 'POST',
        dataType: "json",
        url: _base_url + "service-package/toggle-delete/" + this.getAttribute('data-id'),
        success: function (data) {
            if (data.status == true) {
                alert(data.message);
                window.location.href = _base_url + "service-packages";
            }
            else {
                alert(data.message);
            }
        },
        error: function (data) {
            alert("An error occured!");
        },
    });
});

$.validator.addMethod("lessThanOrEqual",
    function (value, element, param) {
        var $otherElement = $(param);
        return parseFloat(value) <= parseFloat($otherElement.val());
    }
);
$().ready(function () {
    $('#service_package_edit_form').validate({
        focusInvalid: false,
        rules: {
            id: "required",
            description: "required",
            info_html: {
                required: false,
            },
            actual_total_amount: {
                required: true,
                min: 1,
            },
            total_amount: {
                required: true,
                min: 1,
                lessThanOrEqual: "#actual_total_amount"
            },
            cart_limit: {
                required: true,
                min: 1,
            },
            sort_order: {
                required: true,
                min: 1,
            },
            service_time: {
                required: true,
            },
        },
        messages: {
            id: "Package id required.",
            description: "Add some description.",
            info_html: "Add some html info.",
            actual_total_amount: {
                required: "Enter actual amount.",
                min: "Enter a value greater than or equal to 1.",
            },
            cart_limit: {
                required: "Enter Quantity limit.",
                min: "Enter a value greater than or equal to 1.",
            },
            total_amount: {
                required: "Enter sale amount.",
                lessThanOrEqual: "Sale amount must be less than actual amount.",
                min: "Enter a value greater than or equal to 1.",
            },
            sort_order: {
                required: "Enter sort order.",
                min: "Enter a value greater than or equal to 1.",
            },
        },
        submitHandler: function (form) {
            // $('#service_package_edit_form').html('Please wait...').attr("disabled", true);
            $('#package-update-btn').html('Please wait...').attr("disabled", true);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: _base_url + "service-package/update",
                data: $('#service_package_edit_form').serialize(),
                success: function (data) {
                    if (data.status == true) {
                        $('#package-update-btn').html('Saved');
                        alert(data.message);
                        window.location.href = _base_url + "service-packages";
                    }
                    else {
                        alert(data.message);
                        $('#package-update-btn').html('Update').removeAttr("disabled");
                    }
                },
                error: function (data) {
                    alert("An error occured!");
                    $('#package-update-btn').html('Update').removeAttr("disabled");
                },
            });
        }
    });
    /********************************************************************* */
    var quill = new Quill('#quill_editor', {
        theme: 'snow'
    });
    const delta = quill.clipboard.convert($('#info_html').val());
    quill.setContents(delta, 'silent');
    quill.on('text-change', function (delta, oldDelta, source) {
        document.getElementById("info_html").value = quill.root.innerHTML;
    });
    /********************************************************************* */
});