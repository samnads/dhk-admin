var invoicelisttable;
$(document).ready(function () {
	invoicelisttable = $('#invoicelisttable').DataTable({
        'bFilter': false,
        'bLengthChange': false,
        'pageLength': 100,
        'processing': true,
        'serverSide': true,
        'bSort': false,
        'serverMethod': 'post',
        'ajax': {
            'url': _base_url + 'invoice/list_ajax_invoice_list',
            'data': function (data) {
                if ($('#invoice-date').length) {
                    var regdate = $('#invoice-date').val();
                    data.regdate = regdate;
                } else {
                    data.regdate = '';
                }
                if ($('#invoice-date-to').length) {
                    var regdateto = $('#invoice-date-to').val();
                    data.regdateto = regdateto;
                } else {
                    data.regdateto = '';
                }
                data.custid = $('#customers_vh_rep_new').val();
                data.status = $('#inv_status').val();
                data.keywordsearch = $('#keyword-search').val();
            },
            "complete": function (json, type) {
                var _resp = $.parseJSON(json.responseText);
				$("#totnetamt").text(_resp.totalnetamount);
				$("#totpaidamt").text(_resp.totalpaidamount);
				$("#totdueamt").text(_resp.totalbalamount);
            }
        },
        'columns': [
            { data: 'slno' },
            { data: 'number' },
            { data: 'customer' },
            { data: 'invdate' },
            { data: 'duedate' },
            { data: 'total' },
            { data: 'paidamount' },
            { data: 'dueamount' },
            { data: 'status' },
            { data: 'action' },
        ],
    });
	
	$('#inv_status').change(function () {
        invoicelisttable.draw();
    });
	
	$('#customers_vh_rep_new').change(function () {
        invoicelisttable.draw();
    });
	
	$('#invoice-date, #invoice-date-to').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
    });
	
	$('#invoice-date, #invoice-date-to').change(function () {
        invoicelisttable.draw();
    });
	
	$("#keyword-search").keyup(function () {
        var vall = $('#keyword-search').val().length
        //if (vall >= 3) {
		invoicelisttable.draw();
        //}
    });
	
	$('#customerexceldownload').click(function () {
        $("#customerexceldownload").html('<i class="fa fa-spinner fa-spin"></i>');
        var status = $('#inv_status').val();
        var fromdate = $('#invoice-date').val();
        var todate = $('#invoice-date-to').val();
        var custid = $('#customers_vh_rep_new').val();
        var keywordsearch = $('#keyword-search').val();
        $.ajax({
            type: "POST",
            url: _base_url + 'customerexcel/customerinvoiceexcel',
            data: { fromdate: fromdate, todate: todate, status: status, custid: custid, keywordsearch: keywordsearch },
            cache: false,
            success: function (response) {
                $("#customerexceldownload").html('<i class="fa fa-file-excel-o"></i>');
                window.location = response;
            }
        });
    });
});