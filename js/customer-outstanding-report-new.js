var customerlistDataTable;
var customerOutDataTable;
$(document).ready(function () {
	/**
     * Function return table of contents with particular customer's pending invoices
     */
    listCustOutInvoicesTable = function (customer_id) {
        customerOutDataTable = $('#customeroutstandingdetailstable').DataTable({
            'bFilter': false,
            'bLengthChange': false,
            'pageLength': 50,
            'processing': true,
            'serverSide': true,
            "bDestroy": true,
            'bSort': false,
            'serverMethod': 'post',
            'ajax': {
                'url': _base_url + 'invoice/get_customer_outstanding_invoices_datatable',
                'data': function (data) {
                    data.customer_id = customer_id;
                },
                "complete": function (json, type) {
                    var _resp = $.parseJSON(json.responseText);
                }
            },
            'columns': [
                { data: 'slno' },
                { data: 'invoicenum' },
                { data: 'invoicedate' },
                { data: 'duedate' },
                { data: 'total' },
                { data: 'paidamt' },
                { data: 'dueamt' },
            ],
        });
    }
	
	customerlistDataTable = $('#customeroutstandinglisttablenew').DataTable({
        'bFilter': false,
        'bLengthChange': false,
        'pageLength': 50,
        'processing': true,
        'serverSide': true,
        'bSort': false,
        'serverMethod': 'get',
        'ajax': {
            'url': _base_url + 'laravel/data/report/customer_outstanding_report_new',
            'data': function (data) {
                data.filter_date_from = $('#cust_from_date').val() ? moment($('#cust_from_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD') : undefined;
                data.filter_date_to = $('#cust_to_date').val() ? moment($('#cust_to_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD') : undefined;
                data.filter_customer_active = $('#all-customers').val() || undefined;
                data.filter_customer_id = $('#customers_vh_rep_new').val();
            },
            "complete": function (json, type) {
                var _resp = $.parseJSON(json.responseText);
                console.log(_resp);
                console.log(_resp.totalBalance);
                $('#debit-total').text(_resp.debitBalance);
                $('#credit-total').text(_resp.creditBalance);
                $('#balance-total').text(_resp.totalBalance);
            }
        },
        'columns': [
            { data: 'slno' },
            { data: 'customer_name' },
            { data: 'mobile_number_1' },
            { data: 'area_name' },
            // { data: 'invoice_net_amount' },
            // { data: 'paid_amount' },
            { data: 'balance' },
            { data: 'last_invoice_date' },
            { data: 'payment_type' },
            { data: 'action' },
        ],
        order: [[1, 'asc']],
        "columnDefs": [{
            "targets": 0,
            "orderable": false
        },
        {
            "targets": 3,
            "orderable": false
        },
        {
            "targets": 5,
            "orderable": false
        }]
    });
	
	$('#cust_from_date,#cust_to_date').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
    });

    $('#cust_from_date').change(function () {
        customerlistDataTable.draw();
    });

    $('#cust_to_date').change(function () {
        customerlistDataTable.draw();
    });

    $('#customers_vh_rep_new').change(function () {
        customerlistDataTable.draw();
    });

    $('#all-customers').change(function () {
        customerlistDataTable.draw();
    });
	
	$('#customerexcelbtn').click(function () {
        $("#customerexcelbtn").html('<i class="fa fa-spinner fa-spin"></i>');
        var status = $('#all-customers').val() || undefined;
        var fromdate = $('#cust_from_date').val() ? moment($('#cust_from_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD') : undefined;
        var todate = $('#cust_to_date').val() ? moment($('#cust_to_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD') : undefined;
        var custid = $('#customers_vh_rep_new').val();
		
		// data.filter_date_from = $('#cust_from_date').val() ? moment($('#cust_from_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD') : undefined;
		// data.filter_date_to = $('#cust_to_date').val() ? moment($('#cust_to_date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD') : undefined;
		// data.filter_customer_active = $('#all-customers').val() || undefined;
		// data.filter_customer_id = $('#customers_vh_rep_new').val();

        $.ajax({
            type: "GET",
            // url: _base_url + 'customerexcel/customeroutstandingexportexcel',
            url: _base_url + 'laravel/data/report/customer_outstanding_report_excel',
            data: { filter_date_from: fromdate, filter_date_to: todate, filter_customer_active: status, filter_customer_id: custid },
            cache: false,
            success: function (response) {
                $("#customerexcelbtn").html('<i class="fa fa-file-excel-o"></i>');
				$('#tablebody').html(response.aaData);
				console.log(response);
                // window.location = response;
				var fileName = "Customer Outstanding Report";
				  var fileType = "xlsx";
				  var table = document.getElementById("table-for-excel");
				//   var wb = XLSX.utils.table_to_book(table, {sheet: "Report"});
                  var wb = XLSX.utils.table_to_book(table, { sheet: "Report",dateNF:'dd/mm/yyyy;@',cellDates:true, raw: false });
				  const ws = wb.Sheets['Report'];
				  var wscols = [
					{wch:5},
					{wch:20},
					{wch:10},
					{wch:15},
					// {wch:10},
					// {wch:10},
					{wch:10},
					{wch:10},
					{wch:10}
				  ];
				  ws['!cols'] = wscols;
					return XLSX.writeFile(wb, null || fileName + "." + (fileType || "xlsx"));
            }
        });
    });
	
});

function closeFancy() {
    // $.fancybox.close();
	$("#common-popup-wrapper-id").hide(500);
}

/*****************************************************
 * Popup for showing outstanding details
 * ***************************************************/
function viewDetailsModal(){
	$("#common-popup-wrapper-id").show(500);
  // $.fancybox.open({
        // autoCenter : true,
        // fitToView : false,
        // scrolling : false,
        // openEffect : 'none',
        // openSpeed : 1,
        // autoSize: false,
        // width:450,
        // height:'auto',
        // helpers : {
            // overlay : {
                // css : {
                    // 'background' : 'rgba(0, 0, 0, 0.3)'
                // },
                // closeClick: false
            // }
        // },
        // padding : 0,
        // closeBtn : false,
        // content: $('#details-div'),
  // });
}