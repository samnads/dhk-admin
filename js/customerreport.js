var customerReportDataTable;
$(document).ready(function () {
    customerReportDataTable = $('#customerreporttable').DataTable({
        'bFilter': false,
        'bLengthChange': false,
        'pageLength': 50,
        'processing': true,
        'serverSide': true,
        'bSort': false,
        'serverMethod': 'post',
        'ajax': {
            'url': _base_url + 'customerreport/list_ajax_customer_report',
            'data': function (data) {
                if ($('#cust_from_date').length) {
                    var regdate = $('#cust_from_date').val();
                    data.regdate = regdate;
                } else {
                    data.regdate = '';
                }
                if ($('#cust_to_date').length) {
                    var regdateto = $('#cust_to_date').val();
                    data.regdateto = regdateto;
                } else {
                    data.regdateto = '';
                }
                data.useractive = $('#all-customers').val();
            },
            "complete": function (json, type) {
                var _resp = $.parseJSON(json.responseText);
            }
        },
        'columns': [
            { data: 'slno' },
            { data: 'name' },
            { data: 'mobile' },
            { data: 'address' },
            { data: 'source' },
            { data: 'addeddate' },
            { data: 'bookingtype' },
            { data: 'servedby' },
            { data: 'flagged' },
            { data: 'status' },
            { data: 'lastbookingdate' },
        ],
        // "columnDefs": [{
            // "targets": 1,
            // "visible": false
        // }]
    });

    $('#cust_from_date,#cust_to_date').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
    });

    $('#cust_from_date').change(function () {
        customerReportDataTable.draw();
    });

    $('#cust_to_date').change(function () {
        customerReportDataTable.draw();
    });

    $('#customerexcelbtn').click(function () {
        $("#customerexcelbtn").html('<i class="fa fa-spinner fa-spin"></i>');
        var status = $('#all-customers').val();
        var fromdate = $('#cust_from_date').val();
        var todate = $('#cust_to_date').val();

        $.ajax({
            type: "POST",
            url: _base_url + 'customerexcel/customerreportexportexcel',
            data: { fromdate: fromdate, todate: todate, status: status },
            cache: false,
            success: function (response) {
                $("#customerexcelbtn").html('<i class="fa fa-file-excel-o"></i>');
                window.location = response;
            }
        });
    });
});