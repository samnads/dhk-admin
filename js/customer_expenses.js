var expenselisttable;
$(document).ready(function () {
    expenselisttable = $('#expenselisttable').DataTable({
        'bFilter': false,
        'bLengthChange': false,
        'pageLength': 100,
        'processing': true,
        'serverSide': true,
        'bSort': false,
        'serverMethod': 'post',
        'ajax': {
            'url': _base_url + 'expense/list_ajax_expense_list',
            'data': function (data) {
                if ($('#expense-date').length) {
                    var regdate = $('#expense-date').val();
                    data.regdate = regdate;
                } else {
                    data.regdate = '';
                }
                if ($('#expense-date-to').length) {
                    var regdateto = $('#expense-date-to').val();
                    data.regdateto = regdateto;
                } else {
                    data.regdateto = '';
                }
                data.custid = $('#customers_vh_rep_new').val();
                data.qbsync = $('#qbsyncbtn').val();
                // data.keywordsearch = $('#keyword-search').val();
            },
            "complete": function (json, type) {
                var _resp = $.parseJSON(json.responseText);
            }
        },
        'columns': [
            { data: 'slno' },
            { data: 'name' },
            { data: 'mobile' },
            { data: 'expdate' },
            { data: 'ref' },
            { data: 'type' },
            { data: 'desc' },
            { data: 'amount' },
            { data: 'paidamount' },
            { data: 'status' },
        ],
    });    

    // $("#keyword-search").keyup(function () {
        // var vall = $('#keyword-search').val().length
		// quickbookpaymentlisttable.draw();
    // });
	
	$('#qbsyncbtn').change(function () {
        expenselisttable.draw();
    });
	
	$('#customers_vh_rep_new').change(function () {
        expenselisttable.draw();
    });
	
	// $('#customer_pay_mode').change(function () {
        // quickbookpaymentlisttable.draw();
    // });
	
	$('#expense-date, #expense-date-to').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
    });
	
	$('#expense-date, #expense-date-to').change(function () {
        expenselisttable.draw();
    });
});

function closeFancy() {
    $.fancybox.close();
}

function confirm_disable_enable_modal() {
    $.fancybox.open({
		autoCenter: true,
		fitToView: false,
		scrolling: false,
		openEffect: 'none',
		openSpeed: 1,
		autoSize: false,
		width: 450,
		height: 'auto',
		helpers: {
			overlay: {
				css: {
					'background': 'rgba(0, 0, 0, 0.3)'
				},
				closeClick: false
			}
		},
		padding: 0,
		closeBtn: false,
		content: $('#enable-popup'),
	});
}

function sync_expenses() {
	$('.mm-loader').css('display','block');
	var custid = $('#customers_vh_rep_new').val();
	var regdate = $('#expense-date').val();
	var regdateto = $('#expense-date-to').val();
    $.ajax({
        type: "POST",
        url: _base_url + "quickbook/sync_all_expenses_quickbooks",
        data: { custid: custid, regdate: regdate, regdateto: regdateto },
        // dataType: "text",
        cache: false,
        success: function (result) {
			closeFancy();
			$('.mm-loader').css('display','none');
			var _resp = JSON.parse(result);
			console.log(_resp);
            if(_resp.status == "error") {
                toastr.error(_resp.message);
				quickbookpaymentlisttable.draw();
            } else if(_resp.status == "success") {
				$("#sync-error-div").html(_resp.message);
				$.fancybox.open({
					autoCenter: true,
					fitToView: false,
					scrolling: false,
					openEffect: 'none',
					openSpeed: 1,
					autoSize: false,
					width: 'auto',
					height: 'auto',
					helpers: {
						overlay: {
							css: {
								'background': 'rgba(0, 0, 0, 0.3)'
							},
							closeClick: false
						}
					},
					padding: 0,
					closeBtn: false,
					content: $('#disable-popup'),
				});
				quickbookpaymentlisttable.draw();
			} else {
               toastr.error("Something went wrong."); 
			   quickbookpaymentlisttable.draw();
            }
        }
    });
    
	// $('.mm-loader').css('display','block');
    // quickbookpaymentlisttable.draw();
}