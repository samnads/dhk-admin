$('#vehicle_date,#vehicle_date_to').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
});
$("#printButton").click(function () {
    var divContents = $("#divToPrint").html();
    var date = $("#zone_date").val();
    var zone = $("#zone_name").val();
    var day = $("#day").val();
    var printWindow = window.open('', '', 'height=400,width=800');
    printWindow.document.write('<html><head><title></title>');
    printWindow.document.write('</head><body >');
    printWindow.document.write('<h1 align="center">Zone Report</h1>');
    printWindow.document.write('<br />');
    printWindow.document.write('Date &nbsp;&nbsp;&nbsp; : &nbsp;');
    printWindow.document.write(date);
    printWindow.document.write('&nbsp;(' + day + ')');
    printWindow.document.write('&nbsp;&nbsp;&nbsp;&nbsp;');
    printWindow.document.write('<span style="margin-left:300px;">');
    printWindow.document.write('Zone &nbsp;&nbsp;&nbsp; : &nbsp;');
    printWindow.document.write(zone);
    printWindow.document.write('</span>');
    printWindow.document.write('<br /><br />');
    printWindow.document.write(divContents);
    printWindow.document.write('</body></html>');
    printWindow.document.close();
    printWindow.print();
});
$(document).ready(function () {

    exportF = function (elem) {
        var fromDate = $("#vehicle_date").val();
        var toDate = $("#vehicle_date_to").val();
    
       
        // For example, you can format them as: dd-mm-yyyy
         var formattedFromDate = formatDate(fromDate);
        var formattedToDate = formatDate(toDate);
    
        // Construct the fileName
        var fileName = 'Vehicle Report ' + formattedFromDate + '-' + formattedToDate;
    
        var fileType = "xlsx";
        var table = document.getElementById("divPrint");
        var wb = XLSX.utils.table_to_book(table, { sheet: "Report", dateNF: 'dd/mm/yyyy;@', cellDates: true, raw: true });
        const ws = wb.Sheets['Report'];
        
        var wscols = [
            { wch: 5 },
            { wch: 10 },
            { wch: 10 },
            { wch: 15 },
            { wch: 10 },
            { wch: 10 },
            { wch: 20 },
            { wch: 15 },
            { wch: 20 },
            { wch: 15 },
            { wch: 15 },
            { wch: 15 },
            { wch: 15 },
            { wch: 15 },
            { wch: 20 },
            { wch: 10 },
            { wch: 25 },
        ];
        ws['!cols'] = wscols;
    
        return XLSX.writeFile(wb, null || fileName + "." + (fileType || "xlsx"));
    };
    
    // Function to format date if needed
    function formatDate(dateString) {
        // Implement your date formatting logic here
        // Example: Convert from dd/mm/yyyy to dd-mm-yyyy
        var parts = dateString.split("/");
        return parts[0] + '-' + parts[1] + '-' + parts[2];
    }
    




});
$('.show_full').click(function () {
    $('.full_text').hide();
    $('.half_text').show();
    $(this).parent('span').hide();
    $(this).parent('span').parent('td').children('.full_text').show();
});
$('.show_less').click(function () {
    $('.full_text').hide();
    $('.half_text').show();
});
(function (a) {
    a(document).ready(function (b) {
        if (a('#da-ex-datatable-numberpaging').length > 0) {
            a("table#da-ex-datatable-numberpaging").dataTable({
                sPaginationType: "full_numbers", "bSort": false, "iDisplayLength": 100, "scrollY": true,
                "scrollX": true
            });
            a("table#da-ex-datatable-default").dataTable({ "iDisplayLength": 100 });
        }
    });
})(jQuery);
$(function () {
    if ($('#customers_vh_rep').length) {
        //$('#customers_vh_rep').selectize({sortField: 'text'});
        $("#customers_vh_rep").select2({
            ajax: {
                url: _base_url + "reports/report_srch_usr",
                type: "post",
                dataType: 'json',
                delay: 150,
                data: function (params) {
                    return {
                        searchTerm: params.term, // search term
                    };
                },
                processResults: function (response) { return { results: response }; },
                cache: true
            },
        });
    }
});
$('[data-action="excel-export"]').click(function () {
    var fileName = report_file_name;
    var fileType = "xlsx";
    var table = document.getElementById("divPrint");
    var wb = XLSX.utils.table_to_book(table, { sheet: "Report" });
    const ws = wb.Sheets['Report'];
    var wscols = [
        { wch: 15 },
        { wch: 25 },
        { wch: 30 },
        { wch: 25 },
        { wch: 20 }
    ];
    ws['!cols'] = wscols;
    return XLSX.writeFile(wb, null || fileName + "." + (fileType || "xlsx"));
});