Customer IDS
7687
5315
7004
5045
7291
6278
4749
5965
4777
5266
-----------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO `customers` 
SELECT hc.id, NULL, hc.email, hc.mobile1, hc.customer_type, hc.customer_name, hc.customer_name, hc.phone, hc.mobile1, hc.mobile2, hc.mobile3, hc.fax, hc.email, null, hc.contact_person, hc.latitude, hc.longitude, hc.photo, hc.payment_type, null, hc.key_status, hc.hourly_price, hc.extra_hour_rate, hc.weekend_rate, null, hc.added, hc.added, hc.status  FROM hm_customer hc
-----------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO `customer_addresses`(`customer_id`, `area_id`, `customer_address`) 
SELECT hca.customer_id, hc.area_id, hca.address FROM hm_customer_addr hca JOIN hm_customer hc ON hca.customer_id = hc.id
-----------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO `maids` 
SELECT id,null,rf_rfid,maid_name,gender,nationality,address,address,flat,contact_no,null,passport_no,passport_expiry,null,visa_no,visa_expiry,null,null,null,null,null,null,null,null,added,added,photo,status FROM hm_maid
-----------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO bookings 
SELECT hb.id,hb.customer_id,ca.customer_address_id, hs.maid_id,2,DATE(hb.start_date),
(SELECT FIELD( substr( dayname(hb.start_date), 1, 3), 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'))
,IF(hb.shift_id = 1, '08:00:00',IF(hb.shift_id = 2, '10:00:00', IF(hb.shift_id = 3, '12:00:00', IF(hb.shift_id = 4, '14:00:00', '16:00:00')))),IF(hb.shift_id = 1, '10:00:00',IF(hb.shift_id = 2, '12:00:00', IF(hb.shift_id = 3, '14:00:00', IF(hb.shift_id = 4, '16:00:00', '18:00:00')))),'',IF(hb.booking_type_id = 2, 'OD', 'WE'),IF(hb.booking_type_id = 1, 0, 1),DATE(hb.start_date),DATE(hb.start_date),0,0,0,0,'B',hb.added_user, hb.status,hb.added FROM `hm_booking` hb JOIN customers c ON hb.customer_id = c.customer_id LEFT JOIN customer_addresses ca ON c.customer_id=ca.customer_id
JOIN hm_schedule hs ON hb.id = hs.booking_id
----------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO `booking_deletes`(booking_id, service_date,deleted_by)
SELECT booking_id, DATE(cancel_date), added_user FROM `hm_booking_cancel`
-----------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO `day_services`(`booking_id`, `customer_id`, `maid_id`, `customer_name`, `customer_address`, `customer_payment_type`, `total_fee`, `service_date`, `start_time`, `end_time`, `service_status`, `payment_status`, `service_added_by`, `service_added_by_id`)
        SELECT br.booking_id, bc.cust_id, bc.maid_id, c.customer_name, ca.customer_address, IF(c.payment_type = 1, 'D', IF(c.payment_type = 2, 'W', 'M')) as customer_payment_type, ((c.price_hourly * (2 + IF(br.extra_hour > 0, 0, br.extra_hour))) + (c.price_extra * (IF(br.extra_hour > 0, br.extra_hour, 0)))) As total_fee, br.syncfordate, bc.start_time, bc.end_time, IF(bc.payment_serv_status = 0 OR bc.payment_serv_status = 1, 2, 3) as service_status, IF(bc.payment_serv_status = 0 OR bc.payment_serv_status = 2, 0, 1) as payment_status, 'B', bc.added_user FROM `hm_booking_completed_ref` AS br JOIN hm_booking_completed bc ON bc.id = br.booking_completed_id 
JOIN customers c ON bc.cust_id = c.customer_id LEFT JOIN customer_addresses ca ON c.customer_id = ca.customer_id
-----------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO `customer_payments`(`customer_id`, `paid_amount`, `payment_method`, `paid_at`, `paid_at_id`, `paid_datetime`) 
SELECT cust_id, amount, IF(user_type = 1 OR user_type = 4, 0, 1), IF(user_type = 1, 'T', 'B'), tabletOrUserId,payment_date FROM hm_payment