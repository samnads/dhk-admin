<?php
$con = new mysqli('localhost', 'admin', 'admin123', 'elite_maids');

if ($con->connect_errno) {
    echo "Failed to connect to MySQL: " . $mysqli -> connect_error;
    exit();
}

$result = $con->query("SHOW TABLES");

while ($table = $result->fetch_object()) {
    if(isset($table->Tables_in_elite_maids) && strlen(trim($table->Tables_in_elite_maids)) > 0)
    {
        $optimize_query = "OPTIMIZE TABLE `" . $table->Tables_in_elite_maids . "`";
        $con->query($optimize_query);

        //echo $optimize_query . '<br />';
    }
}

$fp = fopen('/var/www/html/elitemaids/optimize/res.txt', 'w');
fwrite($fp, 'Last optimized on - ' . date('d/m/Y H:i:s'));
fclose($fp);
