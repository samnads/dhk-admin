ALTER TABLE `payment_settings` CHANGE `hours` `from_hr` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `payment_settings` ADD `to_hr` VARCHAR(100) NULL AFTER `from_hr`;
ALTER TABLE `payment_settings` CHANGE `from_hr` `from_hr` INT(100) NULL DEFAULT NULL, CHANGE `to_hr` `to_hr` INT(100) NULL DEFAULT NULL;