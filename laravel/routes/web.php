<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MaidsController;
use App\Http\Controllers\BookingsController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\QuickBookController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('maid')->group(function () {
    Route::get('check_free_maids_for_booking', [MaidsController::class, 'check_free_maids_for_booking']);
});
Route::prefix('booking')->group(function () {
    Route::post('assign_maids', [BookingsController::class, 'assign_maids']);
});
Route::prefix('data/report')->group(function () {
    Route::get('service_wise_revenue_report', [ReportController::class, 'service_wise_revenue_report_data']);
	Route::get('customer_outstanding_report', [ReportController::class, 'customer_outstanding_report']);
	Route::get('customer_outstanding_report_new', [ReportController::class, 'customer_outstanding_report_new']);
	Route::get('customer_outstanding_report_excel', [ReportController::class, 'customer_outstanding_report_excel']);
	Route::get('service_report', [ReportController::class, 'service_report']);
	Route::get('service_report_excel', [ReportController::class, 'service_report_excel']);
	Route::get('staff_report', [ReportController::class, 'staff_report']);
	Route::get('staff_report_excel', [ReportController::class, 'staff_report_excel']);
    Route::get('customer_outstanding_balance', [ReportController::class, 'customer_outstanding_balance']);
	Route::get('sales-invoice-report', [ReportController::class, 'sales_invoice_report']);
	Route::get('sales-invoice-report/excel', [ReportController::class, 'sales_invoice_report_excel_download']);
});
Route::get('quickbook/refresh-token', [QuickBookController::class, 'refresh_token']);
Route::get('gallabox/booking-reminder', [BookingsController::class, 'booking_reminder']);
Route::get('quickbook/booking-new', [QuickBookController::class, 'booking_new']);
