<?php
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

function getActiveMaids()
{
    return DB::table('maids as m')
        ->select(
            'm.maid_id',
        )
        ->where(['m.maid_status' => 1])
        ->get();
}
function get_busy_bookings($booking, $except_booking_ids)
{
    /*************************************** */
    $service_week_day = $booking->service_week_day;
    $service_start_date = $booking->service_start_date;
    $service_actual_end_date = $booking->service_actual_end_date;
    $booking_type = $booking->booking_type;
    $time_from = $booking->time_from;
    $time_to = $booking->time_to;
    $where_in_maids = @$booking->where_in_maids;
    /******************************************************************** */
    $data = DB::table('bookings as b')
        ->select(
            'b.booking_id',
            'b.booking_type',
            'b.time_from',
            'b.time_to',
            'm.maid_id',
            'm.maid_name',
            'm.maid_gender',
            'm.maid_nationality',
            'bd.booking_delete_id',
            DB::raw('ROUND(DATEDIFF(b.service_start_date, "' . $service_start_date . '")/7) as service_start_date_week_difference'),
            'caz.zone_id as booking_zone_id',
            'caz.zone_name as booking_zone_name',
            'caa.area_id as booking_area_id',
            'caa.area_name as booking_area_name',
        )
        ->leftJoin('maids as m', 'b.maid_id', 'm.maid_id')
        ->leftJoin('customer_addresses as bca', 'b.customer_address_id', 'bca.customer_address_id')
        ->leftJoin('areas as caa', 'bca.area_id', 'caa.area_id')
        ->leftJoin('zones as caz', 'caa.zone_id', 'caz.zone_id')
        ->leftJoin('booking_deletes as bd', 'b.booking_id', 'bd.booking_id')
        ->whereNotIn('b.booking_id', $except_booking_ids) // slow query :(
        ->where([['m.maid_status', '=', 1], ['b.booking_status', '!=', 2], ['b.service_week_day', '=', $service_week_day]]);
    if ($where_in_maids) {
        $data->whereIn('m.maid_id', $where_in_maids);
    }
    if ($booking->service_end == '0') {
        $data->where(function ($query) use ($service_start_date, $service_actual_end_date) {
            // this is optimized at it's best no more change needed
            $query->where([['b.service_end', '=', 0]])
                ->orWhere([['b.service_end', '=', 1], ['b.service_actual_end_date', '>', $service_start_date]]);
        });
    } else {
        $data->where(function ($query) use ($service_start_date, $service_actual_end_date) {
            $query
                ->where([['b.service_end', '=', 0], ['b.service_start_date', '<=', $service_actual_end_date]])
                ->orWhere([['b.service_end', '=', 1], ['b.service_actual_end_date', '>', $service_start_date], ['b.service_start_date', '<', $service_start_date]])
                ->orWhere([['b.service_end', '=', 1], ['b.service_actual_end_date', '>', $service_start_date], ['b.service_actual_end_date', '<=', $service_start_date]])
                ->orWhere([['b.service_end', '=', 1], ['b.service_start_date', '>', $service_start_date], ['b.service_actual_end_date', '<', $service_actual_end_date]])
                ->orWhere([['b.service_end', '=', 1], ['b.service_start_date', '=', $service_start_date]])
                ->orWhere([['b.service_end', '=', 1], ['b.service_actual_end_date', '=', $service_actual_end_date]]);
        });
    }
    $data->where(function ($query) use ($time_from, $time_to) {
        $query->where([['b.time_from', '=', $time_from]])
            ->orWhere([['b.time_from', '=', $time_from], ['b.time_to', '=', $time_to]])
            ->orWhere([['b.time_from', '>', $time_from], ['b.time_to', '<=', $time_to]])
            ->orWhere([['b.time_from', '<', $time_from], ['b.time_to', '=', $time_to]])
            ->orWhere([['b.time_from', '<', $time_to], ['b.time_to', '>', $time_to]])
            ->orWhere([['b.time_from', '<', $time_from], ['b.time_to', '>', $time_from]])
            ->orWhere([['b.time_from', '<', $time_from], ['b.time_to', '>', $time_to]]);
    });
    return $data->orderBy('b.time_from', 'ASC')->get();
}
function get_busy_bookings_by_date($booking, $except_booking_ids)
{
    /*************************************** */
    $service_week_day = $booking->service_week_day;
    $service_start_date = $booking->service_start_date;
    $service_actual_end_date = $booking->service_actual_end_date;
    $booking_type = $booking->booking_type;
    /******************************************************************** */
    $data = DB::table('bookings as b')
        ->select(
            'b.booking_id',
            'b.booking_type',
            'b.time_from',
            'b.time_to',
            'm.maid_id',
            'm.maid_name',
            'm.maid_gender',
            'm.maid_nationality',
            'bd.booking_delete_id',
            DB::raw('ROUND(DATEDIFF(b.service_start_date, "' . $service_start_date . '")/7) as service_start_date_week_difference'),
            'caz.zone_id as booking_zone_id'
        )
        ->leftJoin('maids as m', 'b.maid_id', 'm.maid_id')
        ->leftJoin('customer_addresses as bca', 'b.customer_address_id', 'bca.customer_address_id')
        ->leftJoin('areas as caa', 'bca.area_id', 'caa.area_id')
        ->leftJoin('zones as caz', 'caa.zone_id', 'caz.zone_id')
        ->leftJoin('booking_deletes as bd', 'b.booking_id', 'bd.booking_id')
        ->whereNotIn('b.booking_id', $except_booking_ids) // slow query :(
        ->where([['m.maid_status', '=', 1], ['b.booking_status', '!=', 2], ['b.service_week_day', '=', $service_week_day]]);
    if ($booking->service_end == '0') {
        $data->where(function ($query) use ($service_start_date, $service_actual_end_date) {
            // this is optimized at it's best no more change needed
            $query->where([['b.service_end', '=', 0]])
                ->orWhere([['b.service_end', '=', 1], ['b.service_actual_end_date', '>', $service_start_date]]);
        });
    } else {
        $data->where(function ($query) use ($service_start_date, $service_actual_end_date) {
            $query
                ->where([['b.service_end', '=', 0], ['b.service_start_date', '<=', $service_actual_end_date]])
                ->orWhere([['b.service_end', '=', 1], ['b.service_actual_end_date', '>', $service_start_date], ['b.service_start_date', '<', $service_start_date]])
                ->orWhere([['b.service_end', '=', 1], ['b.service_actual_end_date', '>', $service_start_date], ['b.service_actual_end_date', '<=', $service_start_date]])
                ->orWhere([['b.service_end', '=', 1], ['b.service_start_date', '>', $service_start_date], ['b.service_actual_end_date', '<', $service_actual_end_date]])
                ->orWhere([['b.service_end', '=', 1], ['b.service_start_date', '=', $service_start_date]])
                ->orWhere([['b.service_end', '=', 1], ['b.service_actual_end_date', '=', $service_actual_end_date]]);
        });
    }
    return $data->get();
}

function curl_gallabox_service($url,$curl_post) {
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $url,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => $curl_post,
		CURLOPT_HTTPHEADER => array(
			'apiKey: 6697907e4407420eac1f6d77',
			'apiSecret: af4e6992cd6d44528202e66573994a99',
			'Content-Type: application/json'
		),
	));

	$response = curl_exec($curl);

	curl_close($curl);
	return $response;
}

function curl_gallabox_get_service($curl_post,$url)
{
    $curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $url,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => $curl_post,
		CURLOPT_HTTPHEADER => array(
			'Content-Type: application/json'
		),
	));
	$response = curl_exec($curl);
    if (curl_errno($curl)) {
        $error_msg = curl_error($curl);
        echo $error_msg;
        exit();
    }
	curl_close($curl);
	return $response;
}
