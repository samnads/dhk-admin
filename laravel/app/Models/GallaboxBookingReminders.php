<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    use HasFactory;
    protected $table = 'gallabox_booking_reminders';
    public $timestamps = false;
    protected $primaryKey = 'id';
}
