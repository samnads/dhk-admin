<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvoiceLineItem extends Model
{
    use HasFactory;
    protected $table = 'invoice_line_items';
    public $timestamps = false;
    protected $primaryKey = 'invoice_line_id';
}
