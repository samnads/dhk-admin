<?php

namespace App\Http\Controllers;

use App\Models\Bookings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;
use stdClass;

class BookingsController extends Controller
{
    public function assign_maids(Request $request)
    {
        $bookings = Bookings::where(['booking_common_id' => $request->booking_id])->get();
        try {
            if (sizeof($bookings) != sizeof($request->selected_maids)) {
                throw new \ErrorException('Mismatch in number of maids !');
            }
            DB::beginTransaction();
            foreach ($bookings as $key => $booking_row) {
                $booking = Bookings::find($booking_row->booking_id);
                $booking->maid_id = $request->selected_maids[$key];
                $booking->booked_by = $request->user_id;
                $booking->booking_status = 1;
                $booking->save();
            }
            DB::commit();
            return Response::json(array('success' => true, 'message' => 'Maids Assigned Successfully !'), 200, array(), JSON_PRETTY_PRINT);
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json(array('success' => false, 'message' => $e->getMessage()), 200, array(), JSON_PRETTY_PRINT);
        }
    }

    public function booking_reminder()
    {
        $tomorrow = date("Y-m-d", strtotime('tomorrow'));
        $service_date_new = date('jS M Y', strtotime($tomorrow));
        $day = date('D', strtotime($tomorrow));
        if($day != "Sun" && $day != "Mon")
        {
            $service_week_day = date('w', strtotime($tomorrow));
            $deletes = DB::table('booking_deletes as bd')
                ->select(
                    'bd.booking_id'
                )
                ->where([['bd.service_date', '=', $tomorrow]]);
            $deletearray = $deletes->get();
            $deleted_bookings = array();
            foreach ($deletearray as $delete) {
                $deleted_bookings[] = $delete->booking_id;
            }

            $bookingdata = DB::table('bookings as b')
                ->select(
                    'b.booking_id',
                    'c.customer_id',
                    'c.customer_name',
                    'c.mobile_number_1',
                    'c.whatsapp_no_1',
                    'c.whatsapp_notification',
                    'c.gallaboxSynStatus',
                    'c.gallaboxSyncId'
                )
                ->leftJoin('customers as c', 'b.customer_id', 'c.customer_id')
                ->leftJoin('maids as m', 'b.maid_id', 'm.maid_id')
                ->leftJoin('customer_addresses as bca', 'b.customer_address_id', 'bca.customer_address_id')
                ->leftJoin('areas as caa', 'bca.area_id', 'caa.area_id')
                ->leftJoin('zones as caz', 'caa.zone_id', 'caz.zone_id')
                ->whereNotIn('b.booking_id', $deleted_bookings)
                ->where([['m.maid_status', '=', 1], ['b.booking_status', '=', 1], ['b.service_week_day', '=', $service_week_day]]);
            $bookingdata->where(function ($query) use ($tomorrow) {
                $query->where([['b.service_end', '=', 0]])
                    ->orWhere([['b.service_end', '=', 1], ['b.service_actual_end_date', '>=', $tomorrow]]);
            });
            $bookingdata->where(function ($query) use ($tomorrow,$service_week_day) {
                $query
                    ->where([['b.service_start_date', '=', $tomorrow], ['b.booking_type', '=', 'OD']])
                    ->orWhere([['b.service_start_date', '<=', $tomorrow], ['b.service_week_day', '=', $service_week_day], ['b.booking_type', '=', 'WE']])
                    ->orWhereRaw('(b.service_start_date <= "'.$tomorrow.'" and b.booking_type = "BW" and MOD(DATEDIFF(DATE("' . $tomorrow . '"), DATE(service_start_date)), 14) = 0)');
            });
            $bookingdata = $bookingdata->get();

            if(!empty($bookingdata))
            {
                $customerarray = array();
                foreach($bookingdata as $booking)
                {
                    if($booking->whatsapp_notification == 'Y')
                    {
                        if (in_array($booking->customer_id, $customerarray)) {
                            $p = new stdClass();
                            $p->created_at = date('Y-m-d H:i:s');
                            $p->booking_id = $booking->booking_id;
                            $p->customer_id = $booking->customer_id;
                            $p->service_date = $tomorrow;
                            $id = DB::table('gallabox_booking_reminders')->insertGetId((array) $p);
                        } else {
                            $input_post = [
                                "channelId" => "62027834e940250004e3e1df",
                                "channelType" => "whatsapp",
                                "recipient" => [
                                      "name" => $booking->customer_name,
                                      "phone" => $booking->whatsapp_no_1
                                ],
                                "replyTo" => [
                                      "assigneeType" => "bot",
                                      "assigneeId" => "64e59f4db359fa7466ce9639",
                                      "overrideAssignment" => true
                                ],
                                "whatsapp" => [
                                    "type" => "template",
                                    "template" => [
                                        "templateName" => "clone_booking_remainder",
                                        "bodyValues" => [
                                            "name" => $booking->customer_name,
                                            "date" => $service_date_new
                                        ]
                                    ]
                                ]
                            ];

                            $newurl = "https://server.gallabox.com/devapi/messages/whatsapp";
                            $input_post_json = json_encode($input_post);
                            $sentmessage = curl_gallabox_service($newurl,$input_post_json);

                            $resultdatamsg = json_decode($sentmessage);
                            if(!empty($resultdatamsg) && $resultdatamsg->status != "ACCEPTED")
                            {
                                $pr = new stdClass();
                                $pr->created_at = date('Y-m-d H:i:s');
                                $pr->booking_id = $booking->booking_id;
                                $pr->customer_id = $booking->customer_id;
                                $pr->service_date = $tomorrow;
                                $pr->gallabox_reponse = $sentmessage;
                                $pr->input_json = $input_post_json;
                                $ids = DB::table('gallabox_logs')->insertGetId((array) $pr);
                            } else if(!empty($resultdatamsg) && $resultdatamsg->id != ""){
                                array_push($customerarray,$booking->customer_id);
                                $pnew = new stdClass();
                                $pnew->created_at = date('Y-m-d H:i:s');
                                $pnew->booking_id = $booking->booking_id;
                                $pnew->customer_id = $booking->customer_id;
                                $pnew->service_date = $tomorrow;
                                $idnew = DB::table('gallabox_booking_reminders')->insertGetId((array) $pnew);

                                $prs = new stdClass();
                                $prs->created_at = date('Y-m-d H:i:s');
                                $prs->booking_id = $booking->booking_id;
                                $prs->customer_id = $booking->customer_id;
                                $prs->service_date = $tomorrow;
                                $prs->gallabox_reponse = $sentmessage;
                                $prs->input_json = $input_post_json;
                                $idss = DB::table('gallabox_logs')->insertGetId((array) $prs);
                            } else {
                                $pr = new stdClass();
                                $pr->created_at = date('Y-m-d H:i:s');
                                $pr->booking_id = $booking->booking_id;
                                $pr->customer_id = $booking->customer_id;
                                $pr->service_date = $tomorrow;
                                $pr->gallabox_reponse = $sentmessage;
                                $pr->input_json = $input_post_json;
                                $ids = DB::table('gallabox_logs')->insertGetId((array) $pr);
                            }
                        }
                    }
                }
            }
        } else {
            echo "Its sunday/Monday";
        }
    }
}
