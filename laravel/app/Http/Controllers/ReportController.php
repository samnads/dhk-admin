<?php

namespace App\Http\Controllers;

use App\Models\InvoiceLineItem;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;
use Illuminate\Support\Str;
use DateTime;

class ReportController extends Controller
{
    public function service_wise_revenue_report_data(Request $request)
    {
        $filter_from_date = $request->filter_from_date ?: null;
        $filter_to_date = $request->filter_to_date ?: null;
        $filter_service_type_id = $request->filter_service_type_id ?: null;
        $data['draw'] = $request->draw ?: 0;
        $revenue_by_service = DB::table('day_services as ds')
            ->select(
                DB::raw('SUM(ds.total_fee) as total_fee'),
                DB::raw('SUM(cp.paid_amount) as total_paid_amount'),
                'st.service_type_id',
            )
            ->leftJoin('customer_payments as cp', 'ds.day_service_id', 'cp.day_service_id')
            ->leftJoin('bookings as b', 'b.booking_id', 'ds.booking_id')
            ->leftJoin('service_types as st', 'st.service_type_id', 'b.service_type_id')
            ->groupBy('st.service_type_id');
        if (isset($filter_from_date)) {
            $revenue_by_service->whereDate('cp.paid_datetime', '>=', $filter_from_date);
        }
        if (isset($filter_to_date)) {
            $revenue_by_service->whereDate('cp.paid_datetime', '<=', $filter_to_date);
        }
        $data['data'] = DB::table('service_types as st')
            ->select(
                DB::raw('st.service_type_name as service_type_name'),
                DB::raw('ROUND(IFNULL(rbs.total_fee,0),2) as ds_total_fee'),
                DB::raw('ROUND(IFNULL(rbs.total_paid_amount,0),2) as cp_total_paid_amount'),
            )
            ->where('st.service_type_status', '=', 1)
            ->leftJoinSub($revenue_by_service, 'rbs', function ($join) {
                $join->on('st.service_type_id', '=', 'rbs.service_type_id');
            })
            ->orderBy('cp_total_paid_amount', 'DESC');
        if (isset($filter_service_type_id)) {
            $data['data']->where('st.service_type_id', '=', $filter_service_type_id);
        }
        /**************************************** */
        $total['cp_total_paid_amount'] = number_format($data['data']->sum('total_paid_amount'), 2);
        $total['ds_total_fee'] = number_format($data['data']->sum('total_fee'), 2);
        /**************************************** */
        $data['data'] = $data['data']->get();
        foreach ($data['data'] as $key => $value) {
            $data['data'][$key]->slno = $key + 1;
        }
        $data['total'] = $total;
        $data['recordsTotal'] = $data['data']->count();
        $data['recordsFiltered'] = $data['data']->count();
        return Response::json($data, 200, [], JSON_PRETTY_PRINT);
    }
    public function customer_outstanding_report(Request $request)
    {
        $response['draw'] = $request['draw'];
        $start = $request['start'];
        $rowperpage = $request['length'];
        $sort_columns = ['name' => 'c.customer_name', 'mobile' => 'c.mobile_number_1', 'area' => '', 'balance' => 'c.balance', 'last_invoice_date' => 'c.last_invoice_date', 'type_of_client' => 'c.payment_type'];
        /*********************************************************** */
        $invoices = DB::table('invoice as i')
            ->select(
                DB::raw('i.customer_id'),
                DB::raw('SUM(i.invoice_net_amount) as invoice_net_amount'),
                DB::raw('MAX(i.invoice_date) as last_invoice_date'),
            )
            ->whereNotIn('i.invoice_status', [0, 2])
            ->where('i.showStatus', 1)
            ->groupBy('i.customer_id');
        $response['recordsTotal'] = $invoices->get()->count();
        //return Response::json($invoices->get(), 200, [], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        /*********************************************************** */
        $customer_payments = DB::table('customer_payments as cp')
            ->select(
                DB::raw('cp.customer_id'),
                DB::raw('SUM(cp.paid_amount) as paid_amount'),
            )
            ->leftJoinSub(
                $invoices,
                'ci',
                function ($join) {
                    $join->on('cp.customer_id', '=', 'ci.customer_id');
                }
            )
            ->where('cp.deleted_at', null)
            ->where('cp.show_status', 1)
            ->groupBy('cp.customer_id');
        //return Response::json($customer_payments->get(), 200, [], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        /*********************************************************** */
        $report_query = DB::table('customers as c')
            ->select(
                // DB::raw('DISTINCT(c.customer_id) AS customer_id'),
                'c.customer_id',
                'c.customer_name',
                'a.area_name',
                'ca.customer_address_id',
                'c.mobile_number_1',
                DB::raw("CASE WHEN c.payment_type = 'D' THEN 'Daily' ELSE (CASE WHEN c.payment_type = 'M' THEN 'Monthly' ELSE (CASE WHEN c.payment_type = 'MA' THEN 'Monthly Advance' ELSE 'Unknown' END) END) END as payment_type"),
                'ci.last_invoice_date',
                DB::Raw('IFNULL(ci.invoice_net_amount,0) as invoice_net_amount'),
                // 'ci.invoice_net_amount',
                // 'cp.paid_amount',
                DB::Raw('IFNULL(cp.paid_amount,0) as paid_amount'),
                DB::raw('(IFNULL(ci.invoice_net_amount,0) - IFNULL(cp.paid_amount,0)) as balance'),
            )
            ->leftjoin('customer_addresses as ca', function ($join) {
                $join->on('c.customer_id', '=', 'ca.customer_id');
                $join->where('ca.default_address', '=', 1);
            })
            ->leftjoin('areas as a', 'ca.area_id', '=', 'a.area_id')
            ->leftJoinSub(
                $invoices,
                'ci',
                function ($join) {
                    $join->on('c.customer_id', '=', 'ci.customer_id');
                }
            )
            ->leftJoinSub(
                $customer_payments,
                'cp',
                function ($join) {
                    $join->on('c.customer_id', '=', 'cp.customer_id');
                }
            )
            ->whereRaw('(IFNULL(ci.invoice_net_amount,0) - IFNULL(cp.paid_amount,0)) > 0');
        // return Response::json($report_query->get(), 200, [], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        if ($request['filter_customer_id']) {
            $report_query->where('c.customer_id', $request['filter_customer_id']);
        }
        if (@$request['filter_customer_active'] > -1) {
            $report_query->where('c.customer_status', $request['filter_customer_active']);
        }
        if ($request->filter_date_from) {
            $report_query->whereRaw('date(c.customer_added_datetime) >= "' . $request->filter_date_from . '"');
        }
        if ($request->filter_date_to) {
            $report_query->whereRaw('date(c.customer_added_datetime) <= "' . $request->filter_date_to . '"');
        }
        $after_filter = $report_query;
        $report_query->groupBy('c.customer_id');
        $report_query->orderBy('balance', 'desc');
        $report_query_result = $report_query->get()->each(function ($row, $index) {
            $row->slno = $index + 1;
            $row->invoice_net_amount = number_format($row->invoice_net_amount, 2, ".", "");
            $row->paid_amount = number_format($row->paid_amount, 2, ".", "");
            $row->balance = number_format($row->balance, 2, ".", "");
            $row->action = '<a class="btn btn-small btn-info" onclick="viewDetailsModal();listCustOutInvoicesTable(' . $row->customer_id . ');" title="View Details"><i class="btn-icon-only fa fa-eye"> </i></a>';
        });
        $recordsFiltered = $report_query_result->count();
        $response['recordsFiltered'] = $recordsFiltered;
        $response['totalBalance'] = number_format($after_filter->get()->sum('balance'), 2);
        //return Response::json($report, 200, [], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        /*********************************************************** */
        $response['rawQuery'] = Str::replaceArray('?', $report_query->getBindings(), $report_query->toSql());
        $response['aaData'] = $report_query_result;
        return Response::json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function customer_outstanding_report_new(Request $request)
    {
        $draw = $request['draw'];
        $start = $request['start'];
        $rowperpage = $request['length'];
        $columnName = 'customer_id';
        $columnSortOrder = 'asc';
        $searchValue = $request['search']['value'];

        $useractive = @$request['filter_customer_active'];
        $regdate = @$request['filter_date_from'];
        $regdateto = @$request['filter_date_to'];
        $custselect = @$request['filter_customer_id'];

        $debit = 0;
        $credit = 0;
        /*********************************************************** */
        $invoices = DB::table('invoice as i')
            ->select(
                DB::raw('i.customer_id'),
                DB::raw('SUM(i.invoice_net_amount) as invoice_net_amount'),
                DB::raw('MAX(i.invoice_date) as last_invoice_date'),
            )
            ->whereNotIn('i.invoice_status', [0, 2])
            ->where('i.showStatus', 1)
            ->groupBy('i.customer_id');
        // $response['recordsTotal'] = $invoices->get()->count();
        /*********************************************************** */
        $customer_payments = DB::table('customer_payments as cp')
            ->select(
                DB::raw('cp.customer_id'),
                DB::raw('SUM(cp.paid_amount) as paid_amount'),
            )
            ->leftJoinSub(
                $invoices,
                'ci',
                function ($join) {
                    $join->on('cp.customer_id', '=', 'ci.customer_id');
                }
            )
            ->where('cp.deleted_at', null)
            ->where('cp.show_status', 1)
            ->where('cp.payment_type', '!=', 'CF')
            ->groupBy('cp.customer_id');
        //return Response::json($customer_payments->get(), 200, [], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        /*********************************************************** */
        $customer_expenses = DB::table('expenses as ep')
            ->select(
                DB::raw('ep.customer_id'),
                DB::raw('SUM(ep.amount) as exp_amount'),
            )
            ->leftJoinSub(
                $invoices,
                'ci',
                function ($join) {
                    $join->on('ep.customer_id', '=', 'ci.customer_id');
                }
            )
            ->where('ep.status', 1)
            ->groupBy('ep.customer_id');

        $report_query = DB::table('customers as c')
            ->select(
                // DB::raw('DISTINCT(c.customer_id) AS customer_id'),
                'c.customer_id',
                'c.customer_name',
                'a.area_name',
                'ca.customer_address_id',
                'c.mobile_number_1',
                DB::raw("CASE WHEN c.payment_type = 'D' THEN 'Daily' ELSE (CASE WHEN c.payment_type = 'B' THEN 'Bulk Paying' ELSE (CASE WHEN c.payment_type = '4W' THEN '4 Week Package' ELSE (CASE WHEN c.payment_type = 'W' THEN 'Weekly Paying' ELSE (CASE WHEN c.payment_type = 'MA' THEN 'Monthly Advance' ELSE (CASE WHEN c.payment_type = 'M' THEN 'Monthly End' ELSE 'Unknown' END) END) END) END) END) END as payment_type"),
                'ci.last_invoice_date',
                DB::Raw('IFNULL(ci.invoice_net_amount,0) as invoice_net_amount'),
                // 'ci.invoice_net_amount',
                // 'cp.paid_amount',
                DB::Raw('IFNULL(cp.paid_amount,0) as paid_amount'),
                DB::raw('(IFNULL(ci.invoice_net_amount,0) - IFNULL(cp.paid_amount,0) + IFNULL(ep.exp_amount,0)) as balance'),
            )
            ->leftjoin('customer_addresses as ca', function ($join) {
                $join->on('c.customer_id', '=', 'ca.customer_id');
                $join->where('ca.default_address', '=', 1);
            })
            ->leftjoin('areas as a', 'ca.area_id', '=', 'a.area_id')
            ->leftJoinSub(
                $invoices,
                'ci',
                function ($join) {
                    $join->on('c.customer_id', '=', 'ci.customer_id');
                }
            )
            ->leftJoinSub(
                $customer_payments,
                'cp',
                function ($join) {
                    $join->on('c.customer_id', '=', 'cp.customer_id');
                }
            )
            ->leftJoinSub(
                $customer_expenses,
                'ep',
                function ($join) {
                    $join->on('c.customer_id', '=', 'ep.customer_id');
                }
            );
        $report_query = $report_query->where(function ($query) {
            $query->orWhereRaw('(IFNULL(ci.invoice_net_amount,0) - IFNULL(cp.paid_amount,0) + IFNULL(ep.exp_amount,0)) > 0');
            $query->orWhereRaw('(IFNULL(ci.invoice_net_amount,0) - IFNULL(cp.paid_amount,0) + IFNULL(ep.exp_amount,0)) < 0');
        });

        // ->whereRaw('(IFNULL(ci.invoice_net_amount,0) - IFNULL(cp.paid_amount,0) + IFNULL(ep.exp_amount,0)) > 0')
        // ->orWhereRaw('(IFNULL(ci.invoice_net_amount,0) - IFNULL(cp.paid_amount,0) + IFNULL(ep.exp_amount,0)) < 0');
        // return Response::json($report_query->get(), 200, [], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        $report_query->groupBy('c.customer_id');
        $report_query->orderBy('balance', 'desc');

        $recordsTotal = $report_query->get()->count();
        // $recordsTotalresult = $report_query->get()->sum('balance');
        if ($request['filter_customer_id']) {
            $report_query->where('c.customer_id', '=', $request['filter_customer_id']);
        }
        if (@$request['filter_customer_active'] > -1) {
            $report_query->where('c.customer_status', $request['filter_customer_active']);
        }
        if ($request['filter_date_from']) {
            $report_query->whereRaw('date(c.customer_added_datetime) >= "' . $request['filter_date_from'] . '"');
        }
        if ($request['filter_date_to']) {
            $report_query->whereRaw('date(c.customer_added_datetime) <= "' . $request['filter_date_to'] . '"');
        }
        $after_filter = $report_query;
        $report_query_result = $after_filter->get();
        foreach ($report_query_result as $resultvals) {
            if ($resultvals->balance > 0) {
                $debit += $resultvals->balance;
            } else {
                $credit += $resultvals->balance;
            }
        }

        $recordsTotalresult = $report_query->get()->sum('balance');
        $recordsTotalFilter = $report_query->get()->count();

        $sort_column = "";
        $sort_order = "";

        $report_query->offset($start);
        $report_query->limit($rowperpage);
        $newresult = $report_query->get();

        // print_r($newresult); exit();
        $i = 1;
        $row = [];
        foreach ($newresult as $val) {
            $row[] = array(
                "slno" => ($i + $start),
                "customer_name" => $val->customer_name,
                "mobile_number_1" => $val->mobile_number_1,
                "area_name" => $val->area_name,
                "invoice_net_amount" => number_format($val->invoice_net_amount, 2, ".", ""),
                "paid_amount" => number_format($val->paid_amount, 2, ".", ""),
                "balance" => number_format($val->balance, 2, ".", ""),
                "last_invoice_date" => $val->last_invoice_date,
                "payment_type" => $val->payment_type,
                "action" => '<a class="btn btn-small btn-info" onclick="viewDetailsModal();listCustOutInvoicesTable(' . $val->customer_id . ');" title="View Details"><i class="btn-icon-only fa fa-eye"> </i></a>',
            );
            $i++;
        }

        // $report_query_result = $newresult->each(function ($row, $index) {
        // $row->slno = ($index + 1);
        // $row->invoice_net_amount = number_format($row->invoice_net_amount, 2, ".", "");
        // $row->paid_amount = number_format($row->paid_amount, 2, ".", "");
        // $row->balance = number_format($row->balance, 2, ".", "");
        // $row->action = '<a class="btn btn-small btn-info" onclick="viewDetailsModal();listCustOutInvoicesTable('.$row->customer_id.');" title="View Details"><i class="btn-icon-only fa fa-eye"> </i></a>';
        // });
        $response['draw'] = $draw;
        $response['iTotalRecords'] = $recordsTotal;
        $response['lastcall'] = $newresult;
        $response['iTotalDisplayRecords'] = $recordsTotalFilter;
        $response['aaData'] = $row;
        $response['debitBalance'] = number_format($debit, 2);
        $response['creditBalance'] = number_format($credit, 2);
        $response['totalBalance'] = number_format($recordsTotalresult, 2);
        $response['rawQuery'] = Str::replaceArray('?', $report_query->getBindings(), $report_query->toSql());
        return Response::json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function customer_outstanding_report_excel(Request $request)
    {
        $useractive = @$request['filter_customer_active'];
        $regdate = @$request['filter_date_from'];
        $regdateto = @$request['filter_date_to'];
        $custselect = @$request['filter_customer_id'];
        $debit = 0;
        $credit = 0;
        /*********************************************************** */
        $invoices = DB::table('invoice as i')
            ->select(
                DB::raw('i.customer_id'),
                DB::raw('SUM(i.invoice_net_amount) as invoice_net_amount'),
                DB::raw('MAX(i.invoice_date) as last_invoice_date'),
            )
            ->whereNotIn('i.invoice_status', [0, 2])
            ->where('i.showStatus', 1)
            ->groupBy('i.customer_id');
        // $response['recordsTotal'] = $invoices->get()->count();
        /*********************************************************** */
        $customer_payments = DB::table('customer_payments as cp')
            ->select(
                DB::raw('cp.customer_id'),
                DB::raw('SUM(cp.paid_amount) as paid_amount'),
            )
            ->leftJoinSub(
                $invoices,
                'ci',
                function ($join) {
                    $join->on('cp.customer_id', '=', 'ci.customer_id');
                }
            )
            ->where('cp.deleted_at', null)
            ->where('cp.show_status', 1)
            ->where('cp.payment_type', '!=', 'CF')
            ->groupBy('cp.customer_id');
        //return Response::json($customer_payments->get(), 200, [], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        /*********************************************************** */
        $customer_expenses = DB::table('expenses as ep')
            ->select(
                DB::raw('ep.customer_id'),
                DB::raw('SUM(ep.amount) as exp_amount'),
            )
            ->leftJoinSub(
                $invoices,
                'ci',
                function ($join) {
                    $join->on('ep.customer_id', '=', 'ci.customer_id');
                }
            )
            ->where('ep.status', 1)
            ->groupBy('ep.customer_id');


        $report_query = DB::table('customers as c')
            ->select(
                // DB::raw('DISTINCT(c.customer_id) AS customer_id'),
                'c.customer_id',
                'c.customer_name',
                'a.area_name',
                'ca.customer_address_id',
                'c.mobile_number_1',
                DB::raw("CASE WHEN c.payment_type = 'D' THEN 'Daily' ELSE (CASE WHEN c.payment_type = 'M' THEN 'Monthly' ELSE (CASE WHEN c.payment_type = 'MA' THEN 'Monthly Advance' ELSE (CASE WHEN c.payment_type = '4W' THEN '4 Week Package' ELSE 'Unknown' END) END) END) END as payment_type"),
                'ci.last_invoice_date',
                DB::Raw('IFNULL(ci.invoice_net_amount,0) as invoice_net_amount'),
                // 'ci.invoice_net_amount',
                // 'cp.paid_amount',
                DB::Raw('IFNULL(cp.paid_amount,0) as paid_amount'),
                DB::raw('(IFNULL(ci.invoice_net_amount,0) - IFNULL(cp.paid_amount,0) + IFNULL(ep.exp_amount,0)) as balance'),
            )
            ->leftjoin('customer_addresses as ca', function ($join) {
                $join->on('c.customer_id', '=', 'ca.customer_id');
                $join->where('ca.default_address', '=', 1);
            })
            ->leftjoin('areas as a', 'ca.area_id', '=', 'a.area_id')
            ->leftJoinSub(
                $invoices,
                'ci',
                function ($join) {
                    $join->on('c.customer_id', '=', 'ci.customer_id');
                }
            )
            ->leftJoinSub(
                $customer_payments,
                'cp',
                function ($join) {
                    $join->on('c.customer_id', '=', 'cp.customer_id');
                }
            )
            ->leftJoinSub(
                $customer_expenses,
                'ep',
                function ($join) {
                    $join->on('c.customer_id', '=', 'ep.customer_id');
                }
            );
        $report_query = $report_query->where(function ($query) {
            $query->orWhereRaw('(IFNULL(ci.invoice_net_amount,0) - IFNULL(cp.paid_amount,0) + IFNULL(ep.exp_amount,0)) > 0');
            $query->orWhereRaw('(IFNULL(ci.invoice_net_amount,0) - IFNULL(cp.paid_amount,0) + IFNULL(ep.exp_amount,0)) < 0');
        });
        // ->whereRaw('(IFNULL(ci.invoice_net_amount,0) - IFNULL(cp.paid_amount,0) + IFNULL(ep.exp_amount,0)) > 0')
        // ->orWhereRaw('(IFNULL(ci.invoice_net_amount,0) - IFNULL(cp.paid_amount,0) + IFNULL(ep.exp_amount,0)) < 0');
        // return Response::json($report_query->get(), 200, [], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        $report_query->groupBy('c.customer_id');
        $report_query->orderBy('balance', 'desc');

        // $recordsTotal = $report_query->get()->count();
        if ($request['filter_customer_id']) {
            $report_query->where('c.customer_id', $request['filter_customer_id']);
        }
        if (@$request['filter_customer_active'] > -1) {
            $report_query->where('c.customer_status', $request['filter_customer_active']);
        }
        if ($request['filter_date_from']) {
            $report_query->whereRaw('date(c.customer_added_datetime) >= "' . $request['filter_date_from'] . '"');
        }
        if ($request['filter_date_to']) {
            $report_query->whereRaw('date(c.customer_added_datetime) <= "' . $request['filter_date_to'] . '"');
        }

        $after_filter = $report_query;
        $report_query_result = $after_filter->get();
        foreach ($report_query_result as $resultvals) {
            if ($resultvals->balance > 0) {
                $debit += $resultvals->balance;
            } else {
                $credit += $resultvals->balance;
            }
        }

        $recordsTotalresult = $report_query->get()->sum('balance');
        $newresult = $report_query->get();

        $i = 1;
        $row = [];
        $html = "";
        foreach ($newresult as $val) {
            $invamt = number_format($val->invoice_net_amount, 2, ".", "");
            $paidamt = number_format($val->paid_amount, 2, ".", "");
            $balamt = number_format($val->balance, 2, ".", "");
            $html .= "<tr>";
            $html .= "<td>" . $i . "</td>";
            $html .= "<td>" . $val->customer_name . "</td>";
            $html .= "<td>" . $val->mobile_number_1 . "</td>";
            $html .= "<td>" . $val->area_name . "</td>";
            // $html.= "<td>".$invamt."</td>";
            // $html.= "<td>".$paidamt."</td>";
            $html .= "<td>" . $balamt . "</td>";
            $html .= "<td>" . $val->last_invoice_date . "</td>";
            $html .= "<td>" . $val->payment_type . "</td>";
            $html .= "</tr>";
            $i++;
        }
        $html .= "<tr><th></th><th></th><th></th><th>Debit</th><th>" . number_format($debit, 2) . "</th><th></th><th></th></tr>";
        $html .= "<tr><th></th><th></th><th></th><th>Credit</th><th>" . number_format($credit, 2) . "</th><th></th><th></th></tr>";
        $html .= "<tr><th></th><th></th><th></th><th>Total</th><th>" . number_format($recordsTotalresult, 2) . "</th><th></th><th></th></tr>";

        // $report_query_result = $newresult->each(function ($row, $index) {
        // $row->slno = ($index + 1);
        // $row->invoice_net_amount = number_format($row->invoice_net_amount, 2, ".", "");
        // $row->paid_amount = number_format($row->paid_amount, 2, ".", "");
        // $row->balance = number_format($row->balance, 2, ".", "");
        // $row->action = '<a class="btn btn-small btn-info" onclick="viewDetailsModal();listCustOutInvoicesTable('.$row->customer_id.');" title="View Details"><i class="btn-icon-only fa fa-eye"> </i></a>';
        // });

        // $response['lastcall'] = $newresult;
        $response['aaData'] = $html;
        $response['rawQuery'] = Str::replaceArray('?', $report_query->getBindings(), $report_query->toSql());
        return Response::json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function service_report(Request $request)
    {
        $draw = $request['draw'];
        $start = $request['start'];
        $rowperpage = $request['length'];
        $columnName = 'service_type_id';
        $columnSortOrder = 'asc';
        $searchValue = $request['search']['value'];

        $regdate = @$request['filter_date_from'];
        $regdateto = @$request['filter_date_to'];
        $serviceselect = @$request['filter_service'];
        /*********************************************************** */
        $services = DB::table('invoice_line_items as l')
            ->select(
                DB::raw('l.day_service_id'),
                DB::raw('st.service_type_id'),
                DB::raw('st.service_type_name'),
                DB::raw('SUM(l.line_net_amount) as invoice_net_amount'),
                DB::raw('SUM(TIME_TO_SEC(TIMEDIFF(b.time_to, b.time_from))/3600) as total_hrs'),
            )
            ->join('invoice as i', 'i.invoice_id', '=', 'l.invoice_id')
            ->join('day_services as ds', 'ds.day_service_id', '=', 'l.day_service_id')
            ->join('bookings as b', 'b.booking_id', '=', 'ds.booking_id')
            ->join('service_types as st', 'st.service_type_id', '=', 'b.service_type_id')
            ->whereNotIn('i.invoice_status', [0, 2])
            ->where('i.showStatus', 1)
            ->where('l.line_status', 1)
            ->groupBy('st.service_type_id');

        $recordsTotal = $services->get()->count();
        // $recordsTotalresult = $report_query->get()->sum('balance');
        if ($request['filter_service']) {
            $services->where('b.service_type_id', $request['filter_service']);
        }
        if ($request->filter_date_from) {
            $services->whereRaw('ds.service_date >= "' . $request->filter_date_from . '"');
        }
        if ($request->filter_date_to) {
            $services->whereRaw('ds.service_date <= "' . $request->filter_date_to . '"');
        }
        $recordsTotalresult = $services->get()->sum('invoice_net_amount');
        $recordsTotalhours = $services->get()->sum('total_hrs');
        $recordsTotalFilter = $services->get()->count();

        $sort_column = "";
        $sort_order = "";

        $services->offset($start);
        $services->limit($rowperpage);
        $newresult = $services->get();

        // print_r($newresult); exit();
        $i = 1;
        $row = [];
        foreach ($newresult as $val) {
            $row[] = array(
                "slno" => ($i + $start),
                "service" => $val->service_type_name,
                "totalhrs" => number_format($val->total_hrs, 2),
                "inv_value" => number_format($val->invoice_net_amount, 2),
            );
            $i++;
        }

        $response['draw'] = $draw;
        $response['iTotalRecords'] = $recordsTotal;
        $response['lastcall'] = $newresult;
        $response['iTotalDisplayRecords'] = $recordsTotalFilter;
        $response['aaData'] = $row;
        $response['totalInvoice'] = number_format($recordsTotalresult, 2);
        $response['totalHrs'] = number_format($recordsTotalhours, 2);
        $response['rawQuery'] = Str::replaceArray('?', $services->getBindings(), $services->toSql());
        return Response::json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function service_report_excel(Request $request)
    {
        $regdate = @$request['filter_date_from'];
        $regdateto = @$request['filter_date_to'];
        $serviceselect = @$request['filter_service_id'];
        /*********************************************************** */
        $services = DB::table('invoice_line_items as l')
            ->select(
                DB::raw('l.day_service_id'),
                DB::raw('st.service_type_id'),
                DB::raw('st.service_type_name'),
                DB::raw('SUM(l.line_net_amount) as invoice_net_amount'),
                DB::raw('SUM(TIME_TO_SEC(TIMEDIFF(b.time_to, b.time_from))/3600) as total_hrs'),
            )
            ->join('invoice as i', 'i.invoice_id', '=', 'l.invoice_id')
            ->join('day_services as ds', 'ds.day_service_id', '=', 'l.day_service_id')
            ->join('bookings as b', 'b.booking_id', '=', 'ds.booking_id')
            ->join('service_types as st', 'st.service_type_id', '=', 'b.service_type_id')
            ->whereNotIn('i.invoice_status', [0, 2])
            ->where('i.showStatus', 1)
            ->where('l.line_status', 1)
            ->groupBy('st.service_type_id');

        if ($request['filter_service_id']) {
            $services->where('b.service_type_id', $request['filter_service_id']);
        }
        if ($request->filter_date_from) {
            $services->whereRaw('ds.service_date >= "' . $request->filter_date_from . '"');
        }
        if ($request->filter_date_to) {
            $services->whereRaw('ds.service_date <= "' . $request->filter_date_to . '"');
        }

        $newresult = $services->get();

        // print_r($newresult); exit();
        $i = 1;
        $row = [];
        $html = "";
        foreach ($newresult as $val) {
            $invamt = number_format($val->invoice_net_amount, 2);
            $total_hrs = number_format($val->total_hrs, 2);
            $html .= "<tr>";
            $html .= "<td>" . $i . "</td>";
            $html .= "<td>" . $val->service_type_name . "</td>";
            $html .= "<td>" . $total_hrs . "</td>";
            $html .= "<td>" . $invamt . "</td>";
            $html .= "</tr>";
            $i++;
        }
        $response['aaData'] = $html;
        $response['rawQuery'] = Str::replaceArray('?', $services->getBindings(), $services->toSql());
        return Response::json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function staff_report(Request $request)
    {
        $draw = $request['draw'];
        $start = $request['start'];
        $rowperpage = $request['length'];
        $columnName = 'maid_id';
        $columnSortOrder = 'asc';
        $searchValue = $request['search']['value'];

        $regdate = $request['filter_date_from'] ?: null;
        $regdateto = $request['filter_date_to'] ?: null;
        $staffselect = @$request['filter_staff'];

        $datetime1 = new DateTime($regdate);
        $datetime2 = new DateTime($regdateto);
        $difference = $datetime1->diff($datetime2);
        $days = ($difference->days + 1);
        $totworkinghrs = ($days * 8);
        /*********************************************************** */
        $invoices = DB::table('invoice_line_items as l')
            ->select(
                DB::raw('l.day_service_id'),
                DB::raw('l.maid_id'),
                DB::raw('SUM(l.line_net_amount) as invoice_net_amount'),
                DB::raw('SUM(TIME_TO_SEC(TIMEDIFF(b.time_to, b.time_from))/3600) as total_hrs'),
                DB::raw('(' . $totworkinghrs . ' - SUM(TIME_TO_SEC(TIMEDIFF(b.time_to, b.time_from))/3600)) as hoursfree'),
            )
            ->join('invoice as i', 'i.invoice_id', '=', 'l.invoice_id')
            ->join('day_services as ds', 'ds.day_service_id', '=', 'l.day_service_id')
            ->join('bookings as b', 'b.booking_id', '=', 'ds.booking_id')
            ->whereNotIn('i.invoice_status', [0, 2])
            ->where('i.showStatus', 1)
            ->where('l.line_status', 1)
            ->groupBy('l.maid_id');
        if (isset($regdate)) {
            $invoices = $invoices->whereDate('ds.service_date', '>=', $regdate);
        }
        if (isset($regdateto)) {
            $invoices = $invoices->whereDate('ds.service_date', '<=', $regdateto);
        }
        // $invoices = $invoices->get();
        // print_r($invoices);exit();
        /*********************************************************** */
        $leaves = DB::table('maid_leave as ml')
            ->select(
                DB::raw('ml.leave_id'),
                DB::raw('ml.maid_id'),
                DB::raw("SUM(CASE WHEN ml.leave_type = '1' THEN 8 ELSE 4 END) AS leavehours"),
            )
            ->where('ml.leave_status', 1)
            ->groupBy('ml.maid_id');
        if (isset($regdate)) {
            $leaves = $leaves->whereDate('ml.leave_date', '>=', $regdate);
        }
        if (isset($regdateto)) {
            $leaves = $leaves->whereDate('ml.leave_date', '<=', $regdateto);
        }

        /*********************************************************** */
        $report_query = DB::table('maids as m')
            ->select(
                'm.maid_name',
                DB::Raw('IFNULL(ci.total_hrs,0) as total_hrs'),
                DB::Raw('IFNULL(ci.hoursfree,0) as hoursfree'),
                DB::Raw('IFNULL(ls.leavehours,0) as leavehours'),
                DB::Raw('IFNULL(ci.invoice_net_amount,0) as invoice_net_amount'),
            )
            ->leftJoinSub(
                $invoices,
                'ci',
                function ($join) {
                    $join->on('m.maid_id', '=', 'ci.maid_id');
                }
            )
            ->leftJoinSub(
                $leaves,
                'ls',
                function ($join) {
                    $join->on('m.maid_id', '=', 'ls.maid_id');
                }
            );
        // return Response::json($report_query->get(), 200, [], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        $report_query->groupBy('m.maid_id');

        $recordsTotal = $report_query->get()->count();
        // $recordsTotalresult = $report_query->get()->sum('balance');
        if ($request['filter_staff']) {
            $report_query->where('m.maid_id', $request['filter_staff']);
        }
        $recordstothoursresult = $report_query->get()->sum('total_hrs');
        $recordshoursfreeresult = $report_query->get()->sum('hoursfree');
        $recordshoursleaveresult = $report_query->get()->sum('leavehours');
        $recordsTotalresult = $report_query->get()->sum('invoice_net_amount');
        $recordsTotalFilter = $report_query->get()->count();

        $sort_column = "";
        $sort_order = "";

        $report_query->offset($start);
        $report_query->limit($rowperpage);
        $newresult = $report_query->get();

        // print_r($newresult); exit();
        $i = 1;
        $row = [];
        foreach ($newresult as $val) {
            $row[] = array(
                "slno" => ($i + $start),
                "staffname" => $val->maid_name,
                "hoursbooked" => number_format($val->total_hrs, 2),
                "hoursfree" => number_format($val->hoursfree, 2),
                "hoursabsent" => number_format($val->leavehours, 2),
                "totalamt" => number_format($val->invoice_net_amount, 2, ".", ""),
                // "amt_paid" => '',
                // "bal_amt" => '',
            );
            $i++;
        }

        // $report_query_result = $newresult->each(function ($row, $index) {
        // $row->slno = ($index + 1);
        // $row->invoice_net_amount = number_format($row->invoice_net_amount, 2, ".", "");
        // $row->paid_amount = number_format($row->paid_amount, 2, ".", "");
        // $row->balance = number_format($row->balance, 2, ".", "");
        // $row->action = '<a class="btn btn-small btn-info" onclick="viewDetailsModal();listCustOutInvoicesTable('.$row->customer_id.');" title="View Details"><i class="btn-icon-only fa fa-eye"> </i></a>';
        // });
        $response['draw'] = $draw;
        $response['iTotalRecords'] = $recordsTotal;
        $response['lastcall'] = $newresult;
        $response['iTotalDisplayRecords'] = $recordsTotalFilter;
        $response['aaData'] = $row;
        $response['totalhoursbooked'] = number_format($recordstothoursresult, 2);
        $response['totalhoursfree'] = number_format($recordshoursfreeresult, 2);
        $response['totalhoursabsent'] = number_format($recordshoursleaveresult, 2);
        $response['totalinvamt'] = number_format($recordsTotalresult, 2);
        $response['rawQuery'] = Str::replaceArray('?', $report_query->getBindings(), $report_query->toSql());
        return Response::json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function staff_report_excel(Request $request)
    {
        $regdate = @$request['filter_date_from'];
        $regdateto = @$request['filter_date_to'];
        $staffselect = @$request['filter_staff_id'];

        $datetime1 = new DateTime($regdate);
        $datetime2 = new DateTime($regdateto);
        $difference = $datetime1->diff($datetime2);
        $days = ($difference->days + 1);
        $totworkinghrs = ($days * 8);
        /*********************************************************** */
        $invoices = DB::table('invoice_line_items as l')
            ->select(
                DB::raw('l.day_service_id'),
                DB::raw('l.maid_id'),
                DB::raw('SUM(l.line_net_amount) as invoice_net_amount'),
                DB::raw('SUM(TIME_TO_SEC(TIMEDIFF(b.time_to, b.time_from))/3600) as total_hrs'),
                DB::raw('(' . $totworkinghrs . ' - SUM(TIME_TO_SEC(TIMEDIFF(b.time_to, b.time_from))/3600)) as hoursfree'),
            )
            ->join('invoice as i', 'i.invoice_id', '=', 'l.invoice_id')
            ->join('day_services as ds', 'ds.day_service_id', '=', 'l.day_service_id')
            ->join('bookings as b', 'b.booking_id', '=', 'ds.booking_id')
            ->whereNotIn('i.invoice_status', [0, 2])
            ->where('i.showStatus', 1)
            ->where('l.line_status', 1)
            ->groupBy('l.maid_id');
        if (isset($regdate)) {
            $invoices = $invoices->whereDate('ds.service_date', '>=', $regdate);
        }
        if (isset($regdateto)) {
            $invoices = $invoices->whereDate('ds.service_date', '<=', $regdateto);
        }
        /*********************************************************** */
        $leaves = DB::table('maid_leave as ml')
            ->select(
                DB::raw('ml.leave_id'),
                DB::raw('ml.maid_id'),
                DB::raw("SUM(CASE WHEN ml.leave_type = '1' THEN 8 ELSE 4 END) AS leavehours"),
            )
            ->where('ml.leave_status', 1)
            ->groupBy('ml.maid_id');
        if (isset($regdate)) {
            $leaves = $leaves->whereDate('ml.leave_date', '>=', $regdate);
        }
        if (isset($regdateto)) {
            $leaves = $leaves->whereDate('ml.leave_date', '<=', $regdateto);
        }
        /*********************************************************** */
        $report_query = DB::table('maids as m')
            ->select(
                'm.maid_name',
                DB::Raw('IFNULL(ci.total_hrs,0) as total_hrs'),
                DB::Raw('IFNULL(ci.hoursfree,0) as hoursfree'),
                DB::Raw('IFNULL(ls.leavehours,0) as leavehours'),
                DB::Raw('IFNULL(ci.invoice_net_amount,0) as invoice_net_amount'),
            )
            ->leftJoinSub(
                $invoices,
                'ci',
                function ($join) {
                    $join->on('m.maid_id', '=', 'ci.maid_id');
                }
            )
            ->leftJoinSub(
                $leaves,
                'ls',
                function ($join) {
                    $join->on('m.maid_id', '=', 'ls.maid_id');
                }
            );
        // return Response::json($report_query->get(), 200, [], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        $report_query->groupBy('m.maid_id');

        if ($request['filter_staff_id']) {
            $report_query->where('m.maid_id', $request['filter_staff_id']);
        }

        $newresult = $report_query->get();

        // print_r($newresult); exit();
        $i = 1;
        $html = "";
        foreach ($newresult as $val) {
            $hoursbooked = number_format($val->total_hrs, 2);
            $hoursfree = number_format($val->hoursfree, 2);
            $hoursabsent = number_format($val->leavehours, 2);
            $totalamt = number_format($val->invoice_net_amount, 2, ".", "");
            $html .= "<tr>";
            $html .= "<td>" . $i . "</td>";
            $html .= "<td>" . $val->maid_name . "</td>";
            $html .= "<td>" . $hoursbooked . "</td>";
            $html .= "<td>" . $hoursfree . "</td>";
            $html .= "<td>" . $hoursabsent . "</td>";
            $html .= "<td>" . $totalamt . "</td>";
            $html .= "</tr>";
            $i++;
        }

        $response['aaData'] = $html;
        $response['rawQuery'] = Str::replaceArray('?', $report_query->getBindings(), $report_query->toSql());
        return Response::json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function customer_outstanding_balance(Request $request)
    {
        $invoices = DB::table('invoice as i')
            ->select(
                DB::raw('i.customer_id'),
                DB::raw('SUM(i.invoice_net_amount) as invoice_net_amount'),
                DB::raw('MAX(i.invoice_date) as last_invoice_date'),
            )
            ->whereNotIn('i.invoice_status', [0, 2])
            ->where('i.showStatus', 1)
            ->where('i.customer_id', $request->customer_id);
        /*********************************************************** */
        $customer_payments = DB::table('customer_payments as cp')
            ->select(
                DB::raw('cp.customer_id'),
                DB::raw('SUM(cp.paid_amount) as paid_amount'),
            )
            ->leftJoinSub(
                $invoices,
                'ci',
                function ($join) {
                    $join->on('cp.customer_id', '=', 'ci.customer_id');
                }
            )
            ->where('cp.deleted_at', null)
            ->where('cp.show_status', 1)
            ->where('cp.payment_type', '!=', 'CF')
            ->where('cp.customer_id', $request->customer_id);
        /*********************************************************** */
        $customer_expenses = DB::table('expenses as ep')
            ->select(
                DB::raw('ep.customer_id'),
                DB::raw('SUM(ep.amount) as exp_amount'),
            )
            ->leftJoinSub(
                $invoices,
                'ci',
                function ($join) {
                    $join->on('ep.customer_id', '=', 'ci.customer_id');
                }
            )
            ->where('ep.status', 1)
            ->where('ep.customer_id', $request->customer_id);

        $report_query = DB::table('customers as c')
            ->select(
                'c.customer_id',
                'c.customer_name',
                'c.quickbook_id',
                DB::Raw('IFNULL(ci.invoice_net_amount,0) as invoice_net_amount'),
                DB::Raw('IFNULL(cp.paid_amount,0) as paid_amount'),
                DB::raw('(IFNULL(ci.invoice_net_amount,0) - IFNULL(cp.paid_amount,0) + IFNULL(ep.exp_amount,0)) as balance'),
            )
            ->leftjoin('customer_addresses as ca', function ($join) {
                $join->on('c.customer_id', '=', 'ca.customer_id');
                $join->where('ca.default_address', '=', 1);
            })
            ->leftjoin('areas as a', 'ca.area_id', '=', 'a.area_id')
            ->leftJoinSub(
                $invoices,
                'ci',
                function ($join) {
                    $join->on('c.customer_id', '=', 'ci.customer_id');
                }
            )
            ->leftJoinSub(
                $customer_payments,
                'cp',
                function ($join) {
                    $join->on('c.customer_id', '=', 'cp.customer_id');
                }
            )
            ->leftJoinSub(
                $customer_expenses,
                'ep',
                function ($join) {
                    $join->on('c.customer_id', '=', 'ep.customer_id');
                }
            );
        // $report_query = $report_query->where(function ($query) {
        //     $query->orWhereRaw('(IFNULL(ci.invoice_net_amount,0) - IFNULL(cp.paid_amount,0) + IFNULL(ep.exp_amount,0)) > 0');
        //     $query->orWhereRaw('(IFNULL(ci.invoice_net_amount,0) - IFNULL(cp.paid_amount,0) + IFNULL(ep.exp_amount,0)) < 0');
        // });
        $report_query->where('c.customer_id', '=', $request->customer_id);


        $newresult = $report_query->get();
        // print_r($newresult);exit();
        $response['balance'] = number_format($newresult[0]->balance, 2);
        $response['quickbookid'] = $newresult[0]->quickbook_id;
        return Response::json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function sales_invoice_report(Request $request)
    {
        $filter['from_date'] = @$request->filter['from_date'];
        $filter['to_date'] = @$request->filter['to_date'];
        //$filter['from_date'] = '2024-05-01';
        //$filter['to_date'] = '2024-05-01';
        $response['draw'] = $request['draw'];
        $request->start = @$request->start ?: 0;
        $request->length = @$request->length ?: 0;
        $rows = InvoiceLineItem::
            select(
                //'invoice_line_items.invoice_line_id',
                //'invoice_line_items.invoice_id',
                //'invoice_line_items.day_service_id',
                //'invoice_line_items.service_from_time',
                //'invoice_line_items.service_to_time',
                //'invoice_line_items.line_amount',
                //'invoice_line_items.line_vat_amount',
                //'invoice_line_items.line_net_amount',
                //'i.invoice_num',
                //'i.customer_id',
                //'c.customer_name',
                //'c.mobile_number_1',
                //'i.invoice_status',
                'ds.service_date',
                'b.time_from',
                'b.time_to',
                DB::raw('ROUND(SUM(invoice_line_items.line_net_amount),2) as line_net_amount_sum'),
                DB::raw('SUM(TIME_TO_SEC(TIMEDIFF(b.time_to, b.time_from))/3600) as booking_hours'),

            )
            ->leftJoin('invoice as i', 'invoice_line_items.invoice_id', 'i.invoice_id')
            ->leftJoin('day_services as ds', 'invoice_line_items.day_service_id', 'ds.day_service_id')
            ->leftJoin('bookings as b', 'ds.booking_id', 'b.booking_id')
            ->where([['ds.service_date', '!=', null]]);
        /**************************************** */
        $rows->where(function ($query) {
            $query->where('i.invoice_status', 1);
            $query->orWhere('i.invoice_status', 3);
        });
        /**************************************** */
        /**
         * Date filter
         */
        $response['recordsTotal'] = $rows->count();
        $rows->groupBy('ds.service_date');
        if (@$filter['from_date']) {
            $rows->where([['ds.service_date', '>=', $filter['from_date']]]);
        }
        if (@$filter['to_date']) {
            $rows->where([['ds.service_date', '<=', $filter['to_date']]]);
        }
        $response['recordsFiltered'] = $rows->get()->count();
        /**************************************** */
        $sort_columns = ['slno', 'ds.service_date', 'booking_hours', 'line_net_amount_sum'];
        if (@$request->order[0]['column'] > 0) {
            $rows->orderBy($sort_columns[$request->order[0]['column']], $request->order[0]['dir']);
        } else {
            $rows->orderBy('invoice_line_items.invoice_line_id', 'DESC');
        }
        $total_get = $rows->get();
        $response['total']['line_net_amount_full'] = number_format($total_get->sum('line_net_amount_sum'), 2);
        $response['total']['booking_hours_full'] = number_format($total_get->sum('booking_hours'), 1);
        $rows = $rows->offset($request->start)->limit($request->length)->get();
        $response['total']['line_net_amount_page'] = number_format($rows->sum('line_net_amount_sum'), 2);
        $response['total']['booking_hours_page'] = number_format($rows->sum('booking_hours'), 1);
        foreach ($rows as $key => &$row) {
            $row['slno'] = $request->start + ($key + 1);
            $row->line_net_amount_sum = number_format($row->line_net_amount_sum, 2);
            $row->booking_hours = number_format($row->booking_hours, 1);
        }
        $response['data'] = $rows;
        return Response::json($response, 200, [], JSON_PRETTY_PRINT);
    }
    public function sales_invoice_report_excel_download(Request $request)
    {
        $filter['from_date'] = @$request->filter['from_date'];
        $filter['to_date'] = @$request->filter['to_date'];
        $rows = InvoiceLineItem::
            select(
                //'invoice_line_items.invoice_line_id',
                //'invoice_line_items.invoice_id',
                //'invoice_line_items.day_service_id',
                //'invoice_line_items.service_from_time',
                //'invoice_line_items.service_to_time',
                //'invoice_line_items.line_amount',
                //'invoice_line_items.line_vat_amount',
                //'invoice_line_items.line_net_amount',
                //'i.invoice_num',
                //'i.customer_id',
                //'c.customer_name',
                //'c.mobile_number_1',
                //'i.invoice_status',
                'ds.service_date',
                'b.time_from',
                'b.time_to',
                DB::raw('ROUND(SUM(invoice_line_items.line_net_amount),2) as line_net_amount_sum'),
                DB::raw('SUM(TIME_TO_SEC(TIMEDIFF(b.time_to, b.time_from))/3600) as booking_hours'),

            )
            ->leftJoin('invoice as i', 'invoice_line_items.invoice_id', 'i.invoice_id')
            ->leftJoin('day_services as ds', 'invoice_line_items.day_service_id', 'ds.day_service_id')
            ->leftJoin('bookings as b', 'ds.booking_id', 'b.booking_id')
            ->where([['ds.service_date', '!=', null]]);
        /**************************************** */
        $rows->where(function ($query) {
            $query->where('i.invoice_status', 1);
            $query->orWhere('i.invoice_status', 3);
        });
        /**************************************** */
        /**
         * Date filter
         */
        if (@$filter['from_date']) {
            $rows->where([['ds.service_date', '>=', $filter['from_date']]]);
        }
        if (@$filter['to_date']) {
            $rows->where([['ds.service_date', '<=', $filter['to_date']]]);
        }
        /**************************************** */
        $sort_columns = ['slno', 'ds.service_date', 'booking_hours', 'line_net_amount_sum'];
        if (@$request->order[0]['column'] > 0) {
            $rows->orderBy($sort_columns[$request->order[0]['column']], $request->order[0]['dir']);
        } else {
            $rows->orderBy('invoice_line_items.invoice_line_id', 'DESC');
        }
        $rows = $rows->groupBy('ds.service_date')->get();
        $response['total']['line_net_amount_full'] = number_format( $rows->sum('line_net_amount_sum'), 2);
        $response['total']['booking_hours_full'] = number_format($rows->sum('booking_hours'), 1);
        $html = '';
        foreach ($rows as $key => &$row) {
            $row['slno'] = $request->start + ($key + 1);
            $html .= '<tr>';
            $html .= "<td>" . $row['slno'] . "</td>";
            $html .= "<td>" . Carbon::parse($row->service_date)->format('d-m-Y') . "</td>";
            $html .= "<td>" . number_format($row->booking_hours, 1) . "</td>";
            $html .= "<td>" . number_format($row->line_net_amount_sum, 2) . "</td>";
            $html .= "</tr>";
        }
        $html .= `<tr>
        <td></td>
        <td>Total</td>
        <td>` . $response['total']['booking_hours_full'] . `</td>
        <td>` . $response['total']['line_net_amount_full'] . `</td>
        </tr>`;
        $response['data'] = $rows;
        $response['html'] = $html;
        return Response::json($response, 200, [], JSON_PRETTY_PRINT);
    }
}
