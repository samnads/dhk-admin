<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Response;
use Illuminate\Support\Facades\Validator;
use PDF;
use Str;
use GuzzleHttp\Client;

class CustomersController extends Controller
{
    public function customer_balance(Request $request)
    {
        $header = $request->header('Content-Type');
        $apiKey = $request->header('apiKey');
        // $header = "application/json";
        // $apiKey = env('API_KEY');
        // $request->phone = "919633351242";
        if($header == "application/json" && $apiKey == env('API_KEY'))
        {
            $validator = Validator::make($request->all(), [
                'phone' => 'required',
            ]);

            $validation_response = [
                'status'  => 'error',
                'message' => "Missing Parameters",
                'response'=> $validator->messages(),
            ];

            if($validator->fails()) {
                return Response::json($validation_response, 200, [], JSON_PRETTY_PRINT);
            }
            // $phone = $request->phone;
            // $request->phone = "919633351242";
            $customer_detail = DB::table('customers as c')
                ->select(
                    'c.customer_id','c.initial_balance', 'c.initial_bal_sign', 'c.initial_bal_date', 'c.customer_name', 'c.gallaboxSynStatus', 'c.whatsapp_no_1', 'c.mobile_number_1','ca.customer_address'
                )
                ->leftJoin('customer_addresses as ca', 'c.customer_id', 'ca.customer_id');
            $customer_detail = $customer_detail->where('c.whatsapp_no_1', '=', $request->phone)->first();
            if(!empty($customer_detail))
            {
                /*get statement detail*/
                $input_post = [
                    "customer_id" => $customer_detail->customer_id
                ];
                // $newurl = "http://localhost/dhk-admin/galabox/get_customer_statement";
                $newurl = "https://booking.emaid.info:3444/dhk/galabox/get_customer_statement";
                $input_post_json = json_encode($input_post);
                $sentmessage = curl_gallabox_get_service($input_post_json,$newurl);
                $s_array = json_decode($sentmessage);
                if(!empty($s_array))
                {
                    $lastpaymentdate = $s_array->last_payment_date;
                    $statementurl = $s_array->statement;
                } else {
                    $lastpaymentdate = "";
                    $statementurl = "";
                }


                $invoices = DB::table('invoice as i')
                        ->select(
                            DB::raw('i.customer_id'),
                            DB::raw('SUM(i.invoice_net_amount) as invoice_net_amount'),
                            DB::raw('MAX(i.invoice_date) as last_invoice_date'),
                        )
                        ->whereNotIn('i.invoice_status', [0, 2])
                        ->where('i.showStatus', 1)
                        ->where('i.customer_id',$customer_detail->customer_id);

                $customer_payments = DB::table('customer_payments as cp')
                        ->select(
                            DB::raw('cp.customer_id'),
                            DB::raw('SUM(cp.paid_amount) as paid_amount'),
                        )
                        ->leftJoinSub(
                            $invoices,
                            'ci',
                            function ($join) {
                                $join->on('cp.customer_id', '=', 'ci.customer_id');
                            }
                        )
                        ->where('cp.deleted_at', null)
                        ->where('cp.show_status', 1)
                        ->where('cp.customer_id',$customer_detail->customer_id);

                $customer_expenses = DB::table('expenses as ep')
                        ->select(
                            DB::raw('ep.customer_id'),
                            DB::raw('SUM(ep.amount) as exp_amount'),
                        )
                        ->leftJoinSub(
                            $invoices,
                            'ci',
                            function ($join) {
                                $join->on('ep.customer_id', '=', 'ci.customer_id');
                            }
                        )
                        ->where('ep.status', 1)
                        ->where('ep.customer_id',$customer_detail->customer_id);

                $report_query = DB::table('customers as c')
                        ->select(
                            'c.customer_id',
                            'c.customer_name',
                            'c.quickbook_id',
                            'ci.last_invoice_date',
                            DB::Raw('IFNULL(ci.invoice_net_amount,0) as invoice_net_amount'),
                            DB::Raw('IFNULL(cp.paid_amount,0) as paid_amount'),
                            DB::raw('(IFNULL(ci.invoice_net_amount,0) - IFNULL(cp.paid_amount,0) + IFNULL(ep.exp_amount,0)) as balance'),
                        )
                        ->leftjoin('customer_addresses as ca', function ($join) {
                            $join->on('c.customer_id', '=', 'ca.customer_id');
                            $join->where('ca.default_address', '=', 1);
                        })
                        ->leftjoin('areas as a', 'ca.area_id', '=', 'a.area_id')
                        ->leftJoinSub(
                            $invoices,
                            'ci',
                            function ($join) {
                                $join->on('c.customer_id', '=', 'ci.customer_id');
                            }
                        )
                        ->leftJoinSub(
                            $customer_payments,
                            'cp',
                            function ($join) {
                                $join->on('c.customer_id', '=', 'cp.customer_id');
                            }
                        )
                        ->leftJoinSub(
                            $customer_expenses,
                            'ep',
                            function ($join) {
                                $join->on('c.customer_id', '=', 'ep.customer_id');
                            }
                        );
                $report_query->where('c.customer_id', '=', $customer_detail->customer_id);
                $newresult = $report_query->get();
                $balance = number_format((float)$newresult[0]->balance, 2, '.', '');
                if($balance >0)
                {
                    $balmessage = "Outstanding till ".$newresult[0]->last_invoice_date;
                    $url = "https://booking.dubaihousekeeping.com/payment?id=".$customer_detail->customer_id."&amount=".$balance."&message=".urlencode($balmessage);
                    $showbalance = $balance;
                } else {
                    $url = "";
                    $showbalance = 0;
                }
                $response['status'] = "success";
                $response['message'] = "Balance fetched successfully.";
                $response['response'] = array(
                                            'balance'=> 'AED '.$showbalance,
                                            'url' => $url,
                                            'lastpaymentdate' => $lastpaymentdate,
                                            'statementurl' => $statementurl
                                        );
                // $response['balance'] = number_format($newresult[0]->balance,2);
                // $response['quickbookid'] = $newresult[0]->quickbook_id;
                return Response::json($response, 200, [], JSON_PRETTY_PRINT);
            } else {
                $response['status'] = "error";
                $response['message'] = "Customer does not exist.";
                $response['response'] = [];
                return Response::json($response, 200, [], JSON_PRETTY_PRINT);
            }
        } else {
            $response['status'] = "error";
            $response['message'] = "Mismatch in credentials.";
            $response['response'] = [];
            return Response::json($response, 200, [], JSON_PRETTY_PRINT);
        }
    }

    public function customer_balance_amount(Request $request)
    {
        $id = $request->id;
        $invoices = DB::table('invoice as i')
                ->select(
                    DB::raw('i.customer_id'),
                    DB::raw('SUM(i.invoice_net_amount) as invoice_net_amount'),
                    DB::raw('MAX(i.invoice_date) as last_invoice_date'),
                )
                ->whereNotIn('i.invoice_status', [0, 2])
                ->where('i.showStatus', 1)
                ->where('i.customer_id',$id);

        $customer_payments = DB::table('customer_payments as cp')
                ->select(
                    DB::raw('cp.customer_id'),
                    DB::raw('SUM(cp.paid_amount) as paid_amount'),
                )
                ->leftJoinSub(
                    $invoices,
                    'ci',
                    function ($join) {
                        $join->on('cp.customer_id', '=', 'ci.customer_id');
                    }
                )
                ->where('cp.deleted_at', null)
                ->where('cp.show_status', 1)
                ->where('cp.customer_id',$id);

        $customer_expenses = DB::table('expenses as ep')
                ->select(
                    DB::raw('ep.customer_id'),
                    DB::raw('SUM(ep.amount) as exp_amount'),
                )
                ->leftJoinSub(
                    $invoices,
                    'ci',
                    function ($join) {
                        $join->on('ep.customer_id', '=', 'ci.customer_id');
                    }
                )
                ->where('ep.status', 1)
                ->where('ep.customer_id',$id);

        $report_query = DB::table('customers as c')
                ->select(
                    'c.customer_id',
                    'c.customer_name',
                    'c.quickbook_id',
                    'ci.last_invoice_date',
                    DB::Raw('IFNULL(ci.invoice_net_amount,0) as invoice_net_amount'),
                    DB::Raw('IFNULL(cp.paid_amount,0) as paid_amount'),
                    DB::raw('(IFNULL(ci.invoice_net_amount,0) - IFNULL(cp.paid_amount,0) + IFNULL(ep.exp_amount,0)) as balance'),
                )
                ->leftjoin('customer_addresses as ca', function ($join) {
                    $join->on('c.customer_id', '=', 'ca.customer_id');
                    $join->where('ca.default_address', '=', 1);
                })
                ->leftjoin('areas as a', 'ca.area_id', '=', 'a.area_id')
                ->leftJoinSub(
                    $invoices,
                    'ci',
                    function ($join) {
                        $join->on('c.customer_id', '=', 'ci.customer_id');
                    }
                )
                ->leftJoinSub(
                    $customer_payments,
                    'cp',
                    function ($join) {
                        $join->on('c.customer_id', '=', 'cp.customer_id');
                    }
                )
                ->leftJoinSub(
                    $customer_expenses,
                    'ep',
                    function ($join) {
                        $join->on('c.customer_id', '=', 'ep.customer_id');
                    }
                );
        $report_query->where('c.customer_id', '=', $id);
        $newresult = $report_query->get();
        $balance = number_format((float)$newresult[0]->balance, 2, '.', '');
        if($balance >0)
        {
            $balmessage = "Outstanding till ".$newresult[0]->last_invoice_date;
            $url = "https://booking.dubaihousekeeping.com/payment?id=".$id."&amount=".$balance."&message=".urlencode($balmessage);
            $showbalance = $balance;
        } else {
            $url = "";
            $showbalance = 0;
        }
        $response['status'] = "success";
        $response['message'] = "Balance fetched successfully.";
        $response['response'] = array(
                                    'balance'=> 'AED '.$showbalance,
                                    'url' => $url,
                                );
        return Response::json($response, 200, [], JSON_PRETTY_PRINT);
    }

    function date_compare($element1, $element2) {
		$datetime1 = strtotime($element1->dateval);
		$datetime2 = strtotime($element2->dateval);
		return $datetime1 - $datetime2;
	}
}
