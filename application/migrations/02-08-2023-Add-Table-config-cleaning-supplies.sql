-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 02, 2023 at 08:10 AM
-- Server version: 8.0.33-0ubuntu0.20.04.2
-- PHP Version: 7.4.3-4ubuntu2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `elite_maids_feb_2023`
--

-- --------------------------------------------------------

--
-- Table structure for table `config_cleaning_supplies`
--

CREATE TABLE `config_cleaning_supplies` (
  `id` bigint NOT NULL,
  `type` varchar(25) NOT NULL,
  `name` varchar(25) NOT NULL,
  `amount` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `config_cleaning_supplies`
--
ALTER TABLE `config_cleaning_supplies`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `config_cleaning_supplies`
--
ALTER TABLE `config_cleaning_supplies`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT;
COMMIT;
