--
-- Add field to table `maids`
--

ALTER TABLE `maids` ADD `off_days` VARCHAR(500) NOT NULL 
COMMENT 'Off days of the maid' AFTER `maid_priority`;