-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 02, 2023 at 08:08 AM
-- Server version: 8.0.33-0ubuntu0.20.04.2
-- PHP Version: 7.4.3-4ubuntu2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `elite_maids_feb_2023`
--

-- --------------------------------------------------------

--
-- Table structure for table `booking_cleaning_supplies`
--

CREATE TABLE `booking_cleaning_supplies` (
  `id` bigint UNSIGNED NOT NULL,
  `booking_id` bigint UNSIGNED NOT NULL,
  `cleaning_supply_id` bigint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `booking_cleaning_supplies`
--
ALTER TABLE `booking_cleaning_supplies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `booking_id` (`booking_id`),
  ADD KEY `cleaning_supply_id` (`cleaning_supply_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking_cleaning_supplies`
--
ALTER TABLE `booking_cleaning_supplies`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `booking_cleaning_supplies`
--
ALTER TABLE `booking_cleaning_supplies`
  ADD CONSTRAINT `booking_cleaning_supplies_ibfk_1` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`booking_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `booking_cleaning_supplies_ibfk_2` FOREIGN KEY (`cleaning_supply_id`) REFERENCES `config_cleaning_supplies` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;
