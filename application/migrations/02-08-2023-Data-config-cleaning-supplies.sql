--
-- Dumping data for table `config_cleaning_supplies`
--

INSERT INTO `config_cleaning_supplies` (`id`, `type`, `name`, `amount`) VALUES
(1, 'PlanBased', 'Standard Cleaning Supplie', 10),
(2, 'PlanBased', 'Ecological Green Clean Su', 20),
(3, 'Custom', 'VACUUM CLEANER', 10),
(4, 'Custom', 'LADDER', 10),
(5, 'Custom', 'MOP BUCKET', 10),
(6, 'Custom', ' ELECTRIC IRON', 10);