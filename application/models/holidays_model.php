<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Package_model Class
 *
 * @author  Samnad. S
 * @since   Version 1.0
 */
class Holidays_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function get_all_holidays_between_dates($filter_from_date, $filter_to_date)
    {
        $this->db->select('hd.*,cbu.user_fullname as created_by_user')
            ->from('holidays as hd')
            ->join('users as cbu','hd.created_by_user = cbu.user_id','left')
            ->where('hd.date >=', $filter_from_date)
            ->where('hd.date <=', $filter_to_date)
            ->where('hd.deleted_at', null)
            ->order_by('hd.date', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }
    public function get_all_holiday_dates_from($date)
    {
        $this->db->select('hd.*')
            ->from('holidays as hd')
            ->where('hd.date >=', $date)
            ->where('hd.deleted_at', null)
            ->order_by('hd.date', 'ASC');
        $query = $this->db->get();
        $result = $query->result_array();
        return array_column($result, 'date');
    }
    public function get_weekends()
    {
        $this->db->select('wd.*')
            ->from('week_days as wd')
            ->where('wd.weekend', 1);
        $query = $this->db->get();
        $result = $query->result_array();
        return array_column($result, 'week_day_id');
    }
}
