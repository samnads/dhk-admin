<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Monthly_invoice_model Class
 * 
 * @author Vishnu
 * @package DHK
 * @version Version 1.0
 */
class Monthly_invoice_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }
	
	function customers_by_payment_types($types)
	{
        $this->db->select('c.customer_id,CONCAT(c.customer_name," (",c.payment_type,") ")as customer_name',false)
        ->from('customers as c')
        ->where('c.customer_status',1)
        ->where_in('c.payment_type',$types);
		$query = $this->db->get();
        return $query->result();
	}
	
	function get_bookings_by_customer_and_between_dates($customer_id,$start_date,$end_date){
        $this->db->select("b.booking_id,b.customer_address_id,b.reference_id,b.maid_id,b.time_from,b.time_to,b.service_week_day,b.service_start_date,b.service_end,b.service_end_date,b.service_actual_end_date,b.booking_type,b.booking_status,
        (TIME_TO_SEC(b.time_to) - TIME_TO_SEC(b.time_from))/3600 as hours,b.price_per_hr,b.discount_price_per_hr,b.net_service_cost,b.discount,b.service_charge,b.vat_charge,b.total_amount,b.cleaning_material,b.cleaning_material_fee,c.customer_name,ca.building,ca.customer_address,c.payment_type,m.maid_name", false)
            ->from('bookings as b')
			->join('customers c', 'b.customer_id = c.customer_id')
			->join('customer_addresses ca', 'b.customer_address_id = ca.customer_address_id')
			->join('maids m', 'b.maid_id = m.maid_id')
            ->where("b.booking_status", 1)
            ->where("b.customer_id", $customer_id)
            ->where("(b.service_start_date BETWEEN '" . $start_date . "' AND '" . $end_date . "' OR b.service_actual_end_date BETWEEN '" . $start_date . "' AND '" . $end_date . "' OR (b.service_end = 0 AND b.service_start_date < '" . $start_date . "') OR (b.service_start_date <= '" . $start_date . "' AND b.service_actual_end_date >= '" . $end_date . "'))", null, false);
        $query = $this->db->get();
        return $query->result();
    }
	
	function get_customer_data_for_invoice($customer_id){
        $this->db->select('c.customer_name,c.mobile_number_1,c.email_address,ca.customer_address,ca.building,ca.unit_no,ca.street,a.area_name')
            ->from('customers as c')
			->join('customer_addresses as ca', 'c.customer_id = ca.customer_id')
			->join('areas as a', 'ca.area_id = a.area_id')
            ->where('c.customer_id', $customer_id)
			->where('ca.default_address',1);
        $query = $this->db->get();
        return $query->row();
    }
	
	function get_booking_service_delete_by_date($booking_id,$service_date){
		$this->db->select('bdr.*')
                ->from('booking_delete_remarks as bdr')
				->where('bdr.booking_id',$booking_id)
				->where('bdr.service_date',$service_date);
		$query = $this->db->get();
        // return $query->row();
		$result = $query->row();
		if(count($result) > 0)
		{
			return $result;
		} else {
			$this->db->select('bc.*')
                ->from('booking_cancel as bc')
				->where('bc.booking_id',$booking_id)
				->where('bc.service_date',$service_date);
			$query1 = $this->db->get();
			// return $query->row();
			$result1 = $query1->row();
			if(count($result1) > 0)
			{
				return $result1;
			} else {
				$this->db->select('bd.*')
					->from('booking_deletes as bd')
					->where('bd.booking_id',$booking_id)
					->where('bd.service_date',$service_date);
				$query2 = $this->db->get();
				// return $query->row();
				$result2 = $query2->row();
				return $result2;
			}
		}
	}
	
	public function check_day_service_booking_new($booking_id, $service_date)
    {
        $this->db->select("ds.*,m.maid_name", FALSE)
            ->from('day_services ds')
			->join('maids m', 'ds.maid_id = m.maid_id');
        $this->db->where('ds.booking_id', $booking_id);
        $this->db->where('ds.service_date', $service_date);
        $get_results_query = $this->db->get();
        return $get_results_query->row();
    }
	
	function add_day_service($fields = array())
	{
		$this->db->set($fields);
		$this->db->insert('day_services'); 
		return $this->db->insert_id();
	}
	
	function get_service_details($id)
	{
		$this->db->select("ds.day_service_id,ds.customer_id,ds.booking_id,ds.maid_id,ds.service_date,b.time_from,b.time_to,b.price_per_hr,b.discount_price_per_hr,b.service_charge,b.vat_charge,b.total_amount,ds.customer_name,ds.customer_address", FALSE)
            ->from('day_services ds')
			->join('bookings b', 'ds.booking_id = b.booking_id');
		$this->db->where_in('ds.day_service_id', $id);
        // $this->db->where('ds.day_service_id', $id);
        $get_results_query = $this->db->get();
		// echo $this->db->last_query();exit();
        return $get_results_query->result();
	}
	
	function add_main_invoice($fields = array())
	{
		$this->db->set($fields);
		$this->db->insert('invoice'); 
		return $this->db->insert_id();
	}
	
	function add_line_invoice($fields = array())
	{
		$this->db->set($fields);
		$this->db->insert('invoice_line_items'); 
		return $this->db->insert_id();
	}
	
	function update_day_service($id, $fields = array())
	{
		$this->db->where_in('day_service_id', $id);
		$this->db->update('day_services', $fields); 

		return $this->db->affected_rows();
	}
	
	function update_service_invoice($invoice_id, $fields = array())
	{
		$this->db->where('invoice_id', $invoice_id);
		$this->db->update('invoice', $fields); 

		return $this->db->affected_rows();
	}
    
}