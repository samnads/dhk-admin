<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Package_model Class
 *
 * @package HM
 * @author  Samnad. S
 * @since   Version 1.0
 */
class Service_package_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function get_all_packages($status="")
    {
        $this->db->select('btrsc.id,btrsc.service_time,btrsc.no_of_maids,btrsc.title,btrsc.description,btrsc.actual_total_amount,btrsc.total_amount,btrsc.sort_order,btrsc.deleted_at,bt.name as building_type,br.name as room,st.service_type_name,btrsc.is_offer,btrsc.customer_app_thumbnail')
        ->from('building_type_room_packages as btrsc')
        ->join('building_rooms as br', 'btrsc.building_room_id	 = br.id', 'left')
        ->join('building_types as bt', 'br.building_type_id = bt.id', 'left')
        ->join('service_types as st', 'bt.service_type_id = st.service_type_id', 'left')
        ->order_by('st.service_type_id', 'ASC')
        ->order_by('btrsc.sort_order', 'ASC');
        if ($status != null) {
            $this->db->where('btrsc.deleted_at IS NOT NULL');
        } else{
            $this->db->where('btrsc.deleted_at IS NULL');
        }
        $query = $this->db->get();
        return $query->result();
    }

    public function get_package_by_id($package_id)
    {
        $this->db->select('btrsc.*')
            ->from('building_type_room_packages as btrsc')
            ->where('btrsc.id', $package_id);
        $query = $this->db->get();
        return $query->row();
    }
    public function update_package_by_id($package_id, $fields)
    {
        $this->db->where('id', $package_id);
        $this->db->update('building_type_room_packages', $fields);
        return $package_id;
    }

    public function get_package_by_id_join_all($package_id)
    {
        $this->db->select('btrsc.id,btrsc.no_of_maids,btrsc.title,btrsc.description,btrsc.info_html,btrsc.actual_total_amount,btrsc.total_amount,btrsc.service_time,btrsc.cart_limit,btrsc.sort_order,btrsc.deleted_at,bt.name as building_type,br.name as room,st.service_type_name,btrsc.is_offer,btrsc.customer_app_thumbnail')
        ->from('building_type_room_packages as btrsc')
        ->join('building_rooms as br', 'btrsc.building_room_id	 = br.id', 'left')
        ->join('building_types as bt', 'br.building_type_id = bt.id', 'left')
        ->join('service_types as st', 'bt.service_type_id = st.service_type_id', 'left')
            ->where('btrsc.id', $package_id);
        $query = $this->db->get();
        return $query->row();
    }
}