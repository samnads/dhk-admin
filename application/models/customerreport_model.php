<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Customers_Model Class
 * 
 * @author Geethu
 * @package HM
 * @version Version 1.0
 */
class Customerreport_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    /**
     * Get Customers
     * 
     * @author Geethu
     * @access public
     * @param  bool
     * @return array
     */
	 
	public function count_all_customers()
    {
        $this->db->select('*')
            ->from('customers');
        $num_results = $this->db->count_all_results();
        return $num_results;
    }
	
	public function get_all_customersnew($useractive = NULL, $regdate = NULL, $regdateto = NULL)
    {
        $this->db->select('c.customer_id')
            ->from('customers c');
        //$this->db->group_by('c.customer_id');
        if ($useractive != '') {
            $this->db->where('c.customer_status', $useractive);
        }
        if ($regdate && $regdateto) {
            if ($regdate != "") {
                $regdate = date('Y-m-d', strtotime(str_replace('/', '-', $regdate)));
            }
            if ($regdateto != "") {
                $regdateto = date('Y-m-d', strtotime(str_replace('/', '-', $regdateto)));
            }
            $this->db->where("DATE(`customer_added_datetime`) BETWEEN '$regdate' AND '$regdateto'");
        }
        if (($regdate != "" && $regdateto == "") || ($regdate == "" && $regdateto != "")) {
            if ($regdate != "") {
                $regdate = date('Y-m-d', strtotime(str_replace('/', '-', $regdate)));
                $this->db->where("DATE(`customer_added_datetime`) = '$regdate'");
            }
            if ($regdateto != "") {
                $regdateto = date('Y-m-d', strtotime(str_replace('/', '-', $regdateto)));
                $this->db->where("DATE(`customer_added_datetime`) = '$regdateto'");
            }
        }
        //$this->db->group_by('c.customer_id');
        $this->db->order_by('c.customer_id', 'DESC');
        $num_results = $this->db->count_all_results();
        return $num_results;
    }
	
	public function get_all_newcustomers($useractive = Null, $columnName = NULL, $columnSortOrder = NULL, $rowperpage = NULL, $start = NULL, $draw = NULL, $recordsTotal = NULL, $recordsTotalFilter = NULL, $regdate = NULL, $regdateto = NULL)
    {
        $response = array();
        
        $this->db->select('c.customer_id, c.customer_name, c.is_flag, c.flag_reason, c.payment_type, c.mobile_number_1, c.customer_status,c.customer_source,c.customer_source_val,c.customer_added_datetime,c.customer_booktype')
            ->from('customers c')
            ->order_by('c.customer_id', 'DESC');

        if ($useractive != '') {
            $this->db->where('c.customer_status', $useractive);
        }
        
        if ($regdate && $regdateto) {
            if ($regdate != "") {
                $regdate = date('Y-m-d', strtotime(str_replace('/', '-', $regdate)));
            }
            if ($regdateto != "") {
                $regdateto = date('Y-m-d', strtotime(str_replace('/', '-', $regdateto)));
            }
            $this->db->where("DATE(`customer_added_datetime`) BETWEEN '$regdate' AND '$regdateto'");
        }
        if (($regdate != "" && $regdateto == "") || ($regdate == "" && $regdateto != "")) {
            if ($regdate != "") {
                $regdate = date('Y-m-d', strtotime(str_replace('/', '-', $regdate)));
                $this->db->where("DATE(`customer_added_datetime`) = '$regdate'");
            }
            if ($regdateto != "") {
                $regdateto = date('Y-m-d', strtotime(str_replace('/', '-', $regdateto)));
                $this->db->where("DATE(`customer_added_datetime`) = '$regdateto'");
            }
            //$this->db->where("DATE(`customer_added_datetime`) = '$regdate' OR  DATE(`customer_added_datetime`) ='$regdateto'");
        }
        if ($columnName != '' && $columnSortOrder != "") {
            $this->db->order_by($columnName, $columnSortOrder);
        }
		$this->db->order_by('c.customer_id', 'DESC');
        $this->db->limit($rowperpage, $start);
        $query = $this->db->get();
        $records = $query->result();
        $data = array();
        $i = 1;

        $customerAddress = $this->getCustomerAddresses($records);

        //print_r($records);die();
        foreach ($records as $record) {
            if (!isset($customerAddress[$record->customer_id])) {
                $customerAddress[$record->customer_id] = (object) [
                    'customer_address' => '-',
                ];
            }

            if (isset($customerAddress[$record->customer_id]->building)) {
                $apartmnt_no = 'Apartment No:' . $customerAddress[$record->customer_id]->building . '<br/>';
            } else {
                $apartmnt_no = "";
            }
            
            if ($record->customer_booktype == "0") {
                $reg = "Non Regular";
            } else if ($record->customer_booktype == "1") {
                $reg = "Regular";
            } else {
                $reg = "";
            }
			
            $last_jobdate = $this->get_last_job_date_by_customerid($record->customer_id);
            if (empty($last_jobdate)) {
                $last_date = "";
            } else {
                $last_date = $last_jobdate->service_date;
            }

            if (!isset($customerAddress[$record->customer_id]->customer_address)) {
                $a_address = 'Building - ' . $customerAddress[$record->customer_id]->building . ', ' . $customerAddress[$record->customer_id]->unit_no . '' . $customerAddress[$record->customer_id]->street;
            } else {
                $a_address = $customerAddress[$record->customer_id]->customer_address;
            }

            if ($record->is_flag == "Y") {
                $isflag = " -- Flagged (" . $record->flag_reason . ")";
            } else if ($record->is_flag == "N") {
                $isflag = "No";
            }

            $statuss = $record->customer_status == 1 ? 'Active' : 'Inactive';
            
            $data[] = array(
                'slno' => '<center>'.($i + $start).'<center>',
				'name' => $record->customer_name,
                'mobile' => $record->mobile_number_1 ?: '-',
				'address' => $apartmnt_no . wordwrap($a_address, 25, "<br>"),
				'source' => $record->customer_source ?: '-',
				'addeddate' => ($record->customer_added_datetime) ? date("d/m/Y H:i:s", strtotime($record->customer_added_datetime)) : "",
				'bookingtype' => $reg,
				'servedby' => $reg,
				'flagged' => $isflag,
				'status' => $statuss,
                'lastbookingdate' => $last_date ?: '-',
            );
            $i++;
        }

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $recordsTotal,
            "lastcall" => $records[0],
            "iTotalDisplayRecords" => $recordsTotalFilter,
            "aaData" => $data
        );

        return $response;
    }
	
	public function get_all_newcustomerreports_excel($regdate = NULL, $regdateto = NULL, $status = NULL)
    {
        $this->db->select('c.customer_id, c.email_address, c.customer_name, c.price_hourly, c.is_flag, c.flag_reason, c.payment_type, c.mobile_number_1, c.customer_status,c.customer_source,c.customer_source_val,c.customer_added_datetime,ca.customer_address, ca.building, ca.unit_no, ca.street, a.area_name, z.zone_name,c.customer_booktype')
            ->from('customers c')
            ->join('customer_addresses ca', 'c.customer_id = ca.customer_id', 'left')
            ->join('areas a', 'ca.area_id = a.area_id', 'left')
            ->join('zones z', 'a.zone_id = z.zone_id', 'left');
        $this->db->group_by('c.customer_id');
        $this->db->order_by('c.customer_id', 'DESC');
        if ($status != '') {
            $this->db->where('c.customer_status', $status);
        }
        if ($regdate && $regdateto) {
            if ($regdate != "") {
                $regdate = date('Y-m-d', strtotime(str_replace('/', '-', $regdate)));
            }
            if ($regdateto != "") {
                $regdateto = date('Y-m-d', strtotime(str_replace('/', '-', $regdateto)));
            }
            $this->db->where("DATE(`customer_added_datetime`) BETWEEN '$regdate' AND '$regdateto'");
        }
        if (($regdate != "" && $regdateto == "") || ($regdate == "" && $regdateto != "")) {
            if ($regdate != "") {
                $regdate = date('Y-m-d', strtotime(str_replace('/', '-', $regdate)));
                $this->db->where("DATE(`customer_added_datetime`) = '$regdate'");
            }
            if ($regdateto != "") {
                $regdateto = date('Y-m-d', strtotime(str_replace('/', '-', $regdateto)));
                $this->db->where("DATE(`customer_added_datetime`) = '$regdateto'");
            }
            //$this->db->where("DATE(`customer_added_datetime`) = '$regdate' OR  DATE(`customer_added_datetime`) ='$regdateto'");
        }
        $query = $this->db->get();
        $records = $query->result();

        foreach ($records as $k => $v) {
            $last_jobdate = $this->get_last_job_date_by_customerid($v->customer_id);
            if (empty($last_jobdate)) {
                $records[$k]->last_date = "";
            } else {
                $records[$k]->last_date = date('d-m-Y', strtotime($last_jobdate->service_date));
            }
        }
        return $records;
    }
	
	protected function getCustomerAddresses($customers = array())
    {
        $customerIds = array();

        foreach ($customers as $customer) {
            $customerIds[] = $customer->customer_id;
        }

        $addresses = $this->db->select('ca.customer_id, ca.customer_address, ca.building, ca.unit_no, ca.street, a.area_name, z.zone_name')
            ->from('customer_addresses ca')
            ->join('areas a', 'ca.area_id = a.area_id', 'left')
            ->join('zones z', 'a.zone_id = z.zone_id')
            ->where_in('ca.customer_id', $customerIds ?: '[]')
            ->where('ca.address_status', 0)
            ->group_by('ca.customer_id')
            ->get()->result();

        $customerAddresses = [];

        foreach ($addresses as $address) {
            $customerAddresses[$address->customer_id] = $address;
        }

        return $customerAddresses;
    }
	
	public function get_last_job_date_by_customerid($customer_id)
    {
        $this->db->select('service_date')
            ->from('day_services')
            ->where('customer_id', $customer_id)
            ->order_by('service_date', 'desc')
            ->limit(1);
        $get_last_job_date_by_customerid_qry = $this->db->get();
        return $get_last_job_date_by_customerid_qry->row();
    }
	
	public function get_service_list()
	{
		$this->db->select('service_type_id, service_type_name')
            ->from('service_types')
            ->where('service_type_status', 1);
        $get_service_list_qry = $this->db->get();
        return $get_service_list_qry->result();
	}
	
	public function get_all_maids()
	{
		$this->db->select('maid_id, maid_name')
            ->from('maids')
            ->where('maid_status', 1);
        $get_maid_list_qry = $this->db->get();
        return $get_maid_list_qry->result();
	}
	
}