<?php if (!defined('BASEPATH')) {
    /*****************************************************/
    //             CODE By  :   Samnad . S               */
    //                                                   */
    //    Last Edit by      :                            */
    //                                                   */
    //                                                   */
    //                                                   */
    /*****************************************************/
    exit('No direct script access allowed');
}
class Dashboard_model_v2 extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        //$CI=&get_instance();
        //$CI->load->database('elite_maids');
    }
    public function hours_data_between_dates($from_date_input, $to_date_input)
    {
        $month_start_date = DateTime::createFromFormat("Y-m-d", $from_date_input)->format("Y-m-01");
        $month_end_date = DateTime::createFromFormat("Y-m-d", $to_date_input)->format("Y-m-t");
        //
        $from_date1 = new DateTime($from_date_input);
        $to_date1 = new DateTime($to_date_input);
        $difference = $from_date1->diff($to_date1);
        $result['total_days'] = $difference->days + 1;
        $result['weekends'] = 0;
        while ($from_date1 <= $to_date1) // Loop will work begin to the end date
        {
            if ($from_date1->format("D") == "Sun") //Check that the day is Sunday here
            {
                //echo $from_date->format("Y-m-d") . "<br>";
                $result['weekends']++;
            }
            $from_date1->modify('+1 day');
        }
        $result['countable_days'] = $result['total_days'] - $result['weekends'];
        /********************************************** */
        $this->db->select("count(m.maid_id) as active_maids_count", false)
            ->from('maids as m')
            ->where('m.maid_status', 1);
        $query = $this->db->get();
        $maid_count = $query->row();
        $result['maid_count'] = $maid_count->active_maids_count;
        $result['total_hours'] = $maid_count->active_maids_count * $result['countable_days'] * 10;
        $result['leave_hours'] = $this->db->select("count(ml.maid_id) as leave_maids_count", false)
            ->from('maid_leave as ml')
            ->where('ml.leave_date >=', $from_date_input)
            ->where('ml.leave_date <=', $to_date_input)->get()->row()->leave_maids_count * 10;
        $result['available_hours'] = $result['total_hours'] - $result['leave_hours'];
        /****************************************************************************************** */
        // get data from main table
        // this includes one day deleted data tooo
        // so we will exclude it in next section
        $this->db->select("b.booking_id,b.time_from,b.time_to,b.service_week_day,b.service_start_date,b.service_end,b.service_end_date,b.service_actual_end_date,b.booking_type,b.booking_status,
        (TIME_TO_SEC(b.time_to) - TIME_TO_SEC(b.time_from))/3600 as hours", false)
            ->from('bookings as b')
            ->where("b.booking_status", 1)
            ->where("(b.service_start_date BETWEEN '" . $from_date_input . "' AND '" . $to_date_input . "' OR b.service_actual_end_date BETWEEN '" . $from_date_input . "' AND '" . $to_date_input . "' OR (b.service_end = 0 AND b.service_start_date < '" . $from_date_input . "'))", null, false);
        $query = $this->db->get();
        $result1 = $query->result_array();
        /************************************************* */
        // get one day deletes between this month
        // only filter not permanent deleted
        // join for hour calculation and exclude deleted
        $this->db->select("bd.booking_id,(TIME_TO_SEC(b.time_to) - TIME_TO_SEC(b.time_from))/3600 as hours", false)
            ->from('booking_deletes as bd')
            ->join('bookings as b', 'b.booking_id = bd.booking_id', 'left')
            ->where("bd.service_date BETWEEN '" . $from_date_input . "' AND '" . $to_date_input . "'", null, false)
            ->where("b.booking_status", 1);
        $query = $this->db->get();
        $dayDeletes = $query->result_array();
        $dayDeleteHours = 0;
        $dayDeleteCount = 0;
        foreach ($dayDeletes as $key => $deleted) {
            $dayDeleteHours += $deleted['hours'];
            $dayDeleteCount += 1;
        }
        /************************************************* */
        $counts = array();
        $counts['total_bookings'] = 0;
        $counts['total_booking_hours'] = 0;
        foreach ($result1 as $key => $booking) {
            // set end date for calculation
            if ($booking['service_end'] == 0) {
                $end_date1 = $to_date_input;
            } else {
                if ($booking['service_actual_end_date'] >= $to_date_input) {
                    $end_date1 = $to_date_input;
                } else {
                    $end_date1 = $booking['service_actual_end_date'];
                }
            }
            // set start date for calculation
            if ($booking['service_start_date'] <= $from_date_input) {
                $start_date1 = $from_date_input;
            } else {
                $start_date1 = $booking['service_start_date'];
            }
            if ($booking['booking_type'] == 'OD') {
                $booking_count = 1;
            } else if ($booking['booking_type'] == 'WE') {
                $booking_count = $this->dayCount($start_date1, $end_date1, $booking['service_week_day']);
            } else if ($booking['booking_type'] == 'BW') {
                $booking_count = ceil($this->dayCount($start_date1, $end_date1, $booking['service_week_day']) / 2); // .5 to next integer (eg. 2.5 to 3)
            }
            $counts['total_bookings'] += $booking_count;
            $counts['total_booking_hours'] += ($booking_count * $booking['hours']);
        }
        $counts['total_bookings'] = $counts['total_bookings'] - $dayDeleteCount;
        $counts['total_booking_hours'] = $counts['total_booking_hours'] - $dayDeleteHours;
        //$counts['total_booking_hours'] = number_format((float) $counts['total_booking_hours'], 2, '.', '');
        /************************************************* */
        $result['bookings'] = $counts['total_bookings'];
        $result['booking_hours'] = $counts['total_booking_hours'];
        /****************************************************************************************** */
        $this->db->select("count(bc.booking_cancel_id) as total,
        IFNULL(SUM(CASE WHEN DATE(bc.service_date) >= '" . $from_date_input . "' AND DATE(bc.service_date) <= '" . $to_date_input . "' THEN 1 ELSE 0 END),0) as cancelled_total,
        IFNULL(ROUND(SUM(CASE WHEN DATE(bc.service_date) >= '" . $from_date_input . "' AND DATE(bc.service_date) <= '" . $to_date_input . "' THEN TIME_TO_SEC(b.time_to) - TIME_TO_SEC(b.time_from) ELSE 0 END)/3600,2),0) as cancelled_hours", false)
            ->from('booking_cancel as bc')
            ->join('bookings as b', 'bc.booking_id = b.booking_id', 'left')
            ->where('b.booking_status', 1);
        $query = $this->db->get();
        $cancelled = $query->row();
        $result['cancelled_bookings'] = $cancelled->cancelled_total;
        $result['cancelled_hours'] = $cancelled->cancelled_hours;
        /****************************************************************************************** */
        return $result;
    }
    public function monthly_payments_between_dates($from_date, $to_date)
    {
        //$start_date = DateTime::createFromFormat("Y-m-d", $from_date)->format("Y-m-01");
        //$end_date = DateTime::createFromFormat("Y-m-d", $to_date)->format("Y-m-t");
        $start_date = $from_date;
        $end_date = $to_date;
        $result['total_invoiced_amount'] = 0;
        $result['total_collected_amount'] = 0;
        while ($start_date <= $end_date) {
            $result['labels'][] = DateTime::createFromFormat("Y-m-d", $start_date)->format("Y M");
            $month_start_date = $start_date;
            $month_end_date = DateTime::createFromFormat("Y-m-d", $month_start_date)->format("Y-m-t");
            /************************************************* */
            $this->db->select("
            SUM(CASE WHEN (DATE(b.service_actual_end_date) >= '" . $month_end_date . "') OR (DATE(b.service_actual_end_date) BETWEEN '" . $month_start_date . "' AND '" . $month_end_date . "') THEN b.total_amount ELSE 0 END) as monthly_total_amount,
            SUM(CASE WHEN (DATE(cp.paid_datetime) BETWEEN '" . $month_start_date . "' AND '" . $month_end_date . "') AND ds.payment_status = 1 THEN cp.paid_amount ELSE 0 END) as monthly_collected_amount,", false)
                ->from('day_services as ds')
                ->join('bookings as b', 'ds.booking_id = b.booking_id', 'left')
                ->join('customer_payments as cp', 'ds.day_service_id = cp.day_service_id', 'left');
            $query = $this->db->get();
            $query_result = $query->row();
            $result['total_invoiced_amount'] += $query_result->monthly_total_amount ?: 0;
            $result['total_collected_amount'] += $query_result->monthly_collected_amount ?: 0;
            $result['invoiced_amounts'][] = $query_result->monthly_total_amount ?: 0;
            $result['collected_amounts'][] = $query_result->monthly_collected_amount ?: 0;
            /************************************************* */
            $start_date = new DateTime($start_date);
            $start_date->modify('+1 month');
            $start_date = $start_date->format('Y-m-d');
        }
        return $result;
    }
    public function monthly_sales_statistics_between_dates($from_date, $to_date)
    {
        /*********************************************************** */
        $start_date = DateTime::createFromFormat("Y-m-d", $from_date)->format("Y-m-01");
        $end_date = DateTime::createFromFormat("Y-m-d", $to_date)->format("Y-m-t");
        /*********************************************************** */
        while ($start_date <= $end_date) {
            $monthly_revenue = 0;
            $monthly_hours = 0;
            $result['labels'][] = DateTime::createFromFormat("Y-m-d", $start_date)->format("Y M");
            $month_start_date = $start_date;
            $month_end_date = DateTime::createFromFormat("Y-m-d", $month_start_date)->format("Y-m-t");
            //echo $month_start_date . " - " . $month_end_date . "\n";
            /************************************************* */
            // bookings in the selected date range, this includes deleted also
            $this->db->select("b.booking_id", false)
                ->from('bookings as b')
                ->join('maids m', 'b.maid_id = m.maid_id')
            //->where_in("b.service_week_day", $week_days)
                ->where("b.booking_status", 1)
                ->where('b.booking_category', 'C')
                ->where('m.maid_status', 1)
                ->where("(b.service_start_date BETWEEN '" . $month_start_date . "' AND '" . $month_end_date . "' OR b.service_actual_end_date BETWEEN '" . $month_start_date . "' AND '" . $month_end_date . "' OR (b.service_end = 0 AND b.service_start_date <= '" . $month_start_date . "'))", null, false);
            $query = $this->db->get();
            $bookings_all = $query->result_array();
            $bookings_all_ids = array_column($bookings_all, 'booking_id');
            //print_r($bookings_all_ids);die();
            /************************************************* */
            // get deleted bookings data
            $deleted_bookings_data = $this->get_booking_deletes_data_between_dates($month_start_date, $month_end_date, $bookings_all_ids);
            $deleted_booking_ids = array_column($deleted_bookings_data, 'booking_id');
            /************************************************* */
            $this->db->select("b.booking_id,b.time_from,b.time_to,b.total_amount,b.service_week_day,b.service_start_date,b.service_end,b.service_end_date,b.service_actual_end_date,b.booking_type,b.booking_status,(TIME_TO_SEC(b.time_to) - TIME_TO_SEC(b.time_from))/3600 as hours", false)
                ->from('bookings as b')
                ->join('maids m', 'b.maid_id = m.maid_id')
                ->where_in('b.booking_id', $bookings_all_ids ?: '[]')
                ->where_not_in('b.booking_id', $deleted_booking_ids ?: '[]');
            $query = $this->db->get();
            $bookings = $query->result_array();
            /************************************************* */
            foreach ($bookings as $key => $booking) {
                // set end date for calculation
                if ($booking['service_end'] == 0) {
                    $end2_date = $month_end_date;
                } else {
                    if ($booking['service_actual_end_date'] >= $month_end_date) {
                        $end2_date = $month_end_date;
                    } else {
                        $end2_date = $booking['service_actual_end_date'];
                    }
                }
                // set start date for calculation
                if ($booking['service_start_date'] <= $month_start_date) {
                    $start1_date = $month_start_date;
                } else {
                    $start1_date = $booking['service_start_date'];
                }
                if ($booking['booking_type'] == 'OD') {
                    $booking_count = 1;
                    $counts['od_bookings'] += $booking_count;
                } else if ($booking['booking_type'] == 'WE') {
                    $booking_count = $this->dayCount($start1_date, $end2_date, $booking['service_week_day']);
                    $counts['we_bookings'] += $booking_count;
                } else if ($booking['booking_type'] == 'BW') {
                    $booking_count = ceil($this->dayCount($start1_date, $end2_date, $booking['service_week_day']) / 2); // .5 to next integer (eg. 2.5 to 3)
                    $counts['bw_bookings'] += $booking_count;
                }
                $counts['total_booking_hours'] += ($booking_count * $booking['hours']);
                $monthly_revenue += ($booking_count * $booking['total_amount']);
                $monthly_hours += ($booking_count * $booking['hours']);
            }
            $result['monthly_revenue'][] = $monthly_revenue;
            $result['monthly_hours'][] = $monthly_hours;
            /************************************************* */
            $start_date = new DateTime($start_date);
            $start_date->modify('+1 month');
            $start_date = $start_date->format('Y-m-01');
        }
        return $result;
    }
    public function booking_counts()
    {
        $today_deleted_booking_ids = $this->get_booking_deletes_by_date(date('Y-m-d')) ?: '[]';
        $this->db->select("count(b.booking_id) as total,
        ROUND(IFNULL(SUM(TIME_TO_SEC(b.time_to) - TIME_TO_SEC(b.time_from))/3600,0),2) AS total_hours,
        IFNULL(SUM(CASE WHEN c.is_company = 'Y' THEN 1 ELSE 0 END),0) as partner_total,
        ROUND(IFNULL(SUM(CASE WHEN c.is_company = 'Y' THEN (TIME_TO_SEC(b.time_to) - TIME_TO_SEC(b.time_from)) ELSE 0 END)/3600,0),2) as partner_total_hours", false)
            ->from('bookings as b')
            ->join('customers as c', 'b.customer_id = c.customer_id', 'left')
            ->where('b.booking_status', 1)
            ->where_not_in('b.booking_id', $today_deleted_booking_ids);
        $query = $this->db->get();
        //die($this->db->last_query());
        return $query->row_array();
    }
    public function customer_counts()
    {
        $this->db->select("count(c.customer_id) as total", false)
            ->from('customers as c');
        $query = $this->db->get();
        //die($this->db->last_query());
        return $query->row_array();
    }
    public function cancel_bookings()
    {
        $this->db->select("count(bc.booking_cancel_id) as total,
        IFNULL(SUM(CASE WHEN DATE(bc.service_date) = CURRENT_DATE() THEN 1 ELSE 0 END),0) as today_total,
        IFNULL(ROUND(SUM(CASE WHEN DATE(bc.service_date) = CURRENT_DATE() THEN TIME_TO_SEC(b.time_to) - TIME_TO_SEC(b.time_from) ELSE 0 END)/3600,2),0) as today_hours", false)
            ->from('booking_cancel as bc')
            ->join('bookings as b', 'bc.booking_id = b.booking_id', 'left')
            ->where('b.booking_status', 1);
        $query = $this->db->get();
        //die($this->db->last_query());
        return $query->row_array();
    }
    public function complaint_counts()
    {
        $previous_week = strtotime("-1 week +1 day");
        $start_week = strtotime("last sunday midnight", $previous_week);
        $end_week = strtotime("next saturday", $start_week);
        $start_week = date("Y-m-d", $start_week);
        $end_week = date("Y-m-d", $end_week);
        //die($last_week);
        $this->db->select("count(c.cmp_id) as total,
        IFNULL(SUM(CASE WHEN DATE(c.added_time) >= CURRENT_DATE() THEN 1 ELSE 0 END),0) as today_total,
        IFNULL(SUM(CASE WHEN DATE(c.added_time) >= '" . $start_week . "' AND DATE(c.added_time) <= '" . $end_week . "' THEN 1 ELSE 0 END),0) as last_week_total,
        IFNULL(SUM(CASE WHEN MONTH(c.added_time) = MONTH(CURRENT_DATE()) AND YEAR(c.added_time) = YEAR(CURRENT_DATE()) THEN 1 ELSE 0 END),0) as this_month_total", false)
            ->from('complaints_new as c');
        $query = $this->db->get();
        $result = $query->row_array();
        $result['last_week_date_range']['from'] = $start_week;
        $result['last_week_date_range']['to'] = $end_week;
        return $result;
    }
    public function payment_collected_and_pending()
    {
        $previous_week = strtotime("-1 week +1 day");
        $start_week = strtotime("last sunday midnight", $previous_week);
        $end_week = strtotime("next saturday", $start_week);
        $start_week = date("Y-m-d", $start_week);
        $end_week = date("Y-m-d", $end_week);
        $this->db->select("
        SUM(CASE WHEN CURRENT_DATE() BETWEEN b.service_start_date AND b.service_actual_end_date THEN b.total_amount ELSE 0 END) as today_total_amount,
        SUM(CASE WHEN DATE(cp.paid_datetime) = CURRENT_DATE() AND ds.payment_status = 1 THEN cp.paid_amount ELSE 0 END) as today_collected_amount,
        SUM(CASE WHEN (DATE(b.service_actual_end_date) >= '" . $end_week . "') OR (DATE(b.service_actual_end_date) BETWEEN '" . $start_week . "' AND '" . $end_week . "') THEN b.total_amount ELSE 0 END) as weekly_total_amount,
        SUM(CASE WHEN (DATE(cp.paid_datetime) BETWEEN '" . $start_week . "' AND '" . $end_week . "') AND ds.payment_status = 1 THEN cp.paid_amount ELSE 0 END) as weekly_collected_amount,
        SUM(CASE WHEN (DATE(b.service_actual_end_date) >= '" . date('Y-m-t') . "') OR (DATE(b.service_actual_end_date) BETWEEN '" . date('Y-m-01') . "' AND '" . date('Y-m-t') . "') THEN b.total_amount ELSE 0 END) as monthly_total_amount,
        SUM(CASE WHEN (DATE(cp.paid_datetime) BETWEEN '" . date('Y-m-01') . "' AND '" . date('Y-m-t') . "') AND ds.payment_status = 1 THEN cp.paid_amount ELSE 0 END) as monthly_collected_amount,", false)
            ->from('day_services as ds')
            ->join('bookings as b', 'ds.booking_id = b.booking_id', 'left')
            ->join('customer_payments as cp', 'ds.day_service_id = cp.day_service_id', 'left');
        $query = $this->db->get();
        $result = $query->row_array();
        $result['week_date_range']['from'] = $start_week;
        $result['week_date_range']['to'] = $end_week;
        $result['month_date_range']['from'] = date('Y-m-01');
        $result['month_date_range']['to'] = date('Y-m-t');
        return $result;
    }
    public function recent_activities($lastid = null, $fromid = null)
    {
        $this->db->select("ua.activity_id as id,
        ua.action_content as content,
        u.user_fullname as user,
         ua.action_type as action_type,
        ua.addeddate as time", false);
        $this->db->from('user_activity as ua')
            ->join('users as u', 'ua.added_user = u.user_id', 'left');
        if ($lastid != null) {
            $this->db->where('ua.activity_id >=', $fromid);
            $query = $this->db->order_by('activity_id', "desc")->get();
        } else {
            $query = $this->db->order_by('activity_id', "desc")->limit(20)->get();
        }
        $results = $query->result_array();
        foreach ($results as $key => $value) {
            $results[$key]['time'] = datetime_to_time_elapsed_string($results[$key]['time']);
            $results[$key]['action_type_icon'] = activity_icon($results[$key]['action_type']) ?: '<i class="icon-question"></i>';
        }
        return $results;
    }
    public function booking_counts_between_dates($from_date, $to_date)
    {
        /*************************************************************************** */
        // get week days between dates
        for ($i = 0; $i <= 6; $i++) {
            $date = new DateTime($from_date);
            $date->modify('+' . $i . ' day');
            $week_days[] = $date->format('w');
            if ($date->format('Y-m-d') >= $to_date) {
                $i = 7;
            }
        }
        /*************************************************************************** */
        // bookings between dates, this includes deleted also
        $this->db->select("b.booking_id", false)
            ->from('bookings as b')
            ->join('maids m', 'b.maid_id = m.maid_id')
            ->where_in("b.service_week_day", $week_days)
            ->where("b.booking_status", 1)
            ->where('b.booking_category', 'C')
            ->where('m.maid_status', 1)
            ->where("(b.service_start_date BETWEEN '" . $from_date . "' AND '" . $to_date . "' OR b.service_actual_end_date BETWEEN '" . $from_date . "' AND '" . $to_date . "' OR (b.service_end = 0 AND b.service_start_date <= '" . $from_date . "'))", null, false);
        $query = $this->db->get();
        $bookings_all = $query->result_array();
        $bookings_all_ids = array_column($bookings_all, 'booking_id');
        //print_r($bookings_all_ids);die();
        /*************************************************************************** */
        $deleted_bookings_data = $this->get_booking_deletes_data_between_dates($from_date, $to_date, $bookings_all_ids);
        $counts['cancelled_od_bookings'] = count(array_filter($deleted_bookings_data, function ($booking) {
            return ($booking['booking_type'] == 'OD');
        }));
        $counts['cancelled_we_bookings'] = count(array_filter($deleted_bookings_data, function ($booking) {
            return ($booking['booking_type'] == 'WE');
        }));
        $counts['cancelled_bw_bookings'] = count(array_filter($deleted_bookings_data, function ($booking) {
            return ($booking['booking_type'] == 'BW');
        }));
        $counts['cancelled_bookings_count'] = $counts['cancelled_od_bookings'] + $counts['cancelled_we_bookings'] + $counts['cancelled_bw_bookings'];
        $deleted_booking_ids = array_column($deleted_bookings_data, 'booking_id');
        //print_r($deleted_booking_ids);die();
        /*************************************************************************** */
        // get data from main table
        // this includes one day deleted data tooo
        // so we will exclude it in next section
        $this->db->select("b.booking_id,b.time_from,b.time_to,b.service_week_day,b.service_start_date,b.service_end,b.service_end_date,b.service_actual_end_date,b.booking_type,b.booking_status,
        (TIME_TO_SEC(b.time_to) - TIME_TO_SEC(b.time_from))/3600 as hours", false)
            ->from('bookings as b')
            ->join('maids m', 'b.maid_id = m.maid_id')
            ->where_in("b.booking_id", $bookings_all_ids ?: '[]')
            ->where_not_in('b.booking_id', $deleted_booking_ids ?: '[]');
        //->where_in("b.service_week_day", $week_days)
        //->where("b.booking_status", 1)
        //->where('b.booking_category', 'C')
        //->where('m.maid_status', 1)
        //->where("(b.service_start_date BETWEEN '" . $from_date . "' AND '" . $to_date . "' OR b.service_actual_end_date BETWEEN '" . $from_date . "' AND '" . $to_date . "' OR (b.service_end = 0 AND b.service_start_date <= '" . $from_date . "'))", null, false);
        $query = $this->db->get();
        $bookings = $query->result_array();
        //print_r($bookings);
        /*************************************************************************** */
        // get one day deletes between this month
        // only filter not permanent deleted
        // join for hour calculation and exclude deleted
        $this->db->select("bd.booking_id,(TIME_TO_SEC(b.time_to) - TIME_TO_SEC(b.time_from))/3600 as hours", false)
            ->from('booking_deletes as bd')
            ->join('bookings as b', 'b.booking_id = bd.booking_id', 'left')
            ->where("bd.service_date BETWEEN '" . $from_date . "' AND '" . $to_date . "'", null, false)
            ->where("b.booking_status", 1);
        $query = $this->db->get();
        $dayDeletes = $query->result_array();
        $dayDeleteHours = 0;
        $dayDeleteCount = 0;
        foreach ($dayDeletes as $key => $deleted) {
            $dayDeleteHours += $deleted['hours'];
            $dayDeleteCount += 1;
        }
        //print_r($dayDeletes);
        /*************************************************************************** */
        $counts['total_booking_hours'] = 0;
        $counts['od_bookings'] = 0;
        $counts['we_bookings'] = 0;
        $counts['bw_bookings'] = 0;
        foreach ($bookings as $key => $booking) {
            // set end date for calculation
            if ($booking['service_end'] == 0) {
                $end_date = $to_date;
            } else {
                if ($booking['service_actual_end_date'] >= $to_date) {
                    $end_date = $to_date;
                } else {
                    $end_date = $booking['service_actual_end_date'];
                }
            }
            // set start date for calculation
            if ($booking['service_start_date'] <= $from_date) {
                $start_date = $from_date;
            } else {
                $start_date = $booking['service_start_date'];
            }
            if ($booking['booking_type'] == 'OD') {
                $booking_count = 1;
                $counts['od_bookings'] += $booking_count;
            } else if ($booking['booking_type'] == 'WE') {
                $booking_count = $this->dayCount($start_date, $end_date, $booking['service_week_day']);
                $counts['we_bookings'] += $booking_count;
            } else if ($booking['booking_type'] == 'BW') {
                $booking_count = ceil($this->dayCount($start_date, $end_date, $booking['service_week_day']) / 2); // .5 to next integer (eg. 2.5 to 3)
                $counts['bw_bookings'] += $booking_count;
            }
            $counts['total_booking_hours'] += ($booking_count * $booking['hours']);
        }
        $counts['total_booking_hours'] = $counts['total_booking_hours'] - $dayDeleteHours;
        $counts['total_booking_hours'] = number_format((float) $counts['total_booking_hours'], 2, '.', '');
        return $counts;
    }
    public function chart_counts()
    {
        /*************************************************************************** */
        // this day of week
        $service_date = date('Y-m-d');
        $week_day = date('w');
        $today_deleted_booking_ids = $this->get_booking_deletes_by_date(date('Y-m-d')) ?: '[]';
        $this->db->select("
        SUM(CASE WHEN b.service_week_day = '" . $week_day . "' AND b.booking_type = 'OD' AND cancel_date IS NULL AND b.booking_status = '1' THEN 1 ELSE 0 END) as today_od_count,
        SUM(CASE WHEN b.service_week_day = '" . $week_day . "' AND b.booking_type = 'WE' AND cancel_date IS NULL AND b.booking_status = '1' THEN 1 ELSE 0 END) as today_we_count,
        SUM(CASE WHEN b.service_week_day = '" . $week_day . "' AND b.booking_type = 'BW' AND cancel_date IS NULL AND b.booking_status = '1' THEN 1 ELSE 0 END) as today_bw_count", false)
            ->from('bookings as b')
            ->where_not_in('b.booking_id', $today_deleted_booking_ids)
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND b.service_end = 1) OR (b.service_end = 0))", null, false)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND b.service_week_day = " . $week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(b.service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", null, false);
        $query = $this->db->get();
        $result1 = $query->row_array();
        /*************************************************************************** */
        // same day prev week
        $service_date = date('Y-m-d', strtotime("-1 week"));
        $week_day = date('w', strtotime("-1 week"));
        $today_deleted_booking_ids = $this->get_booking_deletes_by_date(date('Y-m-d'), strtotime("-1 week")) ?: '[]';
        $this->db->select("
        SUM(CASE WHEN ('" . $service_date . "' BETWEEN b.service_start_date AND b.service_actual_end_date) AND b.service_week_day = '" . $week_day . "' AND b.booking_type = 'OD' AND cancel_date IS NULL AND b.booking_status = '1' THEN 1 ELSE 0 END) as prev_od_count,
        SUM(CASE WHEN ('" . $service_date . "' BETWEEN b.service_start_date AND b.service_actual_end_date) AND b.service_week_day = '" . $week_day . "' AND b.booking_type = 'WE' AND cancel_date IS NULL AND b.booking_status = '1' THEN 1 ELSE 0 END) as prev_we_count,
        SUM(CASE WHEN ('" . $service_date . "' BETWEEN b.service_start_date AND b.service_actual_end_date) AND b.service_week_day = '" . $week_day . "' AND b.booking_type = 'BW' AND cancel_date IS NULL AND b.booking_status = '1' THEN 1 ELSE 0 END) as prev_bw_count", false)
            ->from('bookings as b')
            ->where_not_in('b.booking_id', $today_deleted_booking_ids)
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND b.service_end = 1) OR (b.service_end = 0))", null, false)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND b.service_week_day = " . $week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(b.service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", null, false);
        $query = $this->db->get();
        $result2 = $query->row_array();
        /*************************************************************************** */
        // same day prior week
        $service_date = date('Y-m-d', strtotime("-2 week"));
        $week_day = date('w', strtotime("-2 week"));
        $today_deleted_booking_ids = $this->get_booking_deletes_by_date(date('Y-m-d'), strtotime("-2 week")) ?: '[]';
        $this->db->select("
        SUM(CASE WHEN ('" . $service_date . "' BETWEEN b.service_start_date AND b.service_actual_end_date) AND b.service_week_day = '" . $week_day . "' AND b.booking_type = 'OD' AND cancel_date IS NULL AND b.booking_status = '1' THEN 1 ELSE 0 END) as prior_od_count,
        SUM(CASE WHEN ('" . $service_date . "' BETWEEN b.service_start_date AND b.service_actual_end_date) AND b.service_week_day = '" . $week_day . "' AND b.booking_type = 'WE' AND cancel_date IS NULL AND b.booking_status = '1' THEN 1 ELSE 0 END) as prior_we_count,
        SUM(CASE WHEN ('" . $service_date . "' BETWEEN b.service_start_date AND b.service_actual_end_date) AND b.service_week_day = '" . $week_day . "' AND b.booking_type = 'BW' AND cancel_date IS NULL AND b.booking_status = '1' THEN 1 ELSE 0 END) as prior_bw_count", false)
            ->from('bookings as b')
            ->where_not_in('b.booking_id', $today_deleted_booking_ids)
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND b.service_end = 1) OR (b.service_end = 0))", null, false)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND b.service_week_day = " . $week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(b.service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", null, false);
        $query = $this->db->get();
        $result3 = $query->row_array();
        /*************************************************************************** */
        $result = (array) array_merge((array) $result1, (array) $result2, (array) $result3);
        return $result;
    }
    public function today_counts()
    {
        /*************************************************************************** */
        $service_date = date('Y-m-d');
        $week_day = date('w');
        $today_deleted_booking_ids = $this->get_booking_deletes_by_date($service_date) ?: '[]';
        $this->db->select("
        IFNULL(SUM(CASE WHEN b.service_week_day = '" . $week_day . "' AND cancel_date IS NULL AND b.booking_status = '1' THEN 1 ELSE 0 END),0) as total_bookings,
        ROUND(IFNULL(SUM(CASE WHEN b.service_week_day = '" . $week_day . "' AND cancel_date IS NULL AND b.booking_status = '1' THEN (TIME_TO_SEC(b.time_to) - TIME_TO_SEC(b.time_from)) ELSE 0 END)/3600,0),2)  as total_booking_hours", false)
            ->from('bookings as b')
            ->join('maids m', 'b.maid_id = m.maid_id')
            ->where_not_in('b.booking_id', $today_deleted_booking_ids)
            ->where('b.booking_status', 1)
            ->where('b.booking_category', 'C')
            ->where('m.maid_status', 1)
            ->where("((b.service_actual_end_date >= " . $this->db->escape($service_date) . " AND b.service_end = 1) OR (b.service_end = 0))", null, false)
            ->where("((b.service_start_date = " . $this->db->escape($service_date) . " AND b.booking_type = 'OD') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND b.service_week_day = " . $week_day . " AND b.booking_type = 'WE') OR (b.service_start_date <= " . $this->db->escape($service_date) . " AND MOD(DATEDIFF(DATE(" . $this->db->escape($service_date) . "), DATE(b.service_start_date)), 14) = 0 AND b.booking_type = 'BW'))", null, false);
        $query = $this->db->get();
        $result = $query->row_array();
        /*************************************************************************** */
        return $result;
    }
    public function monthly_counts_sql()
    {

        /*************************************************************************** */
        $today_deleted_booking_ids = $this->get_booking_deletes_by_date($service_date) ?: '[]';
        $this->db->select("
         IFNULL(SUM(CASE WHEN (b.service_start_date BETWEEN '" . date('Y-m-01') . "' AND '" . date('Y-m-t') . "' OR b.service_actual_end_date BETWEEN '" . date('Y-m-01') . "' AND '" . date('Y-m-t') . "' OR b.service_end = 0) THEN
         CASE
            WHEN (b.booking_type = 'OD')
                THEN 1
            WHEN (b.booking_type = 'WE')
                THEN
                CEIL(((DATEDIFF(IF(b.service_end = 0, '" . date('Y-m-t') . "',IF(b.service_actual_end_date >= '" . date('Y-m-t') . "','" . date('Y-m-t') . "',b.service_actual_end_date)),DATE_ADD(IF(b.service_start_date >= '" . date('Y-m-01') . "', b.service_start_date,'" . date('Y-m-01') . "'), INTERVAL (9 - IF(DAYOFWEEK(IF(b.service_start_date >= '" . date('Y-m-01') . "', b.service_start_date,'" . date('Y-m-01') . "'))=1, 8, DAYOFWEEK(IF(b.service_start_date >= '" . date('Y-m-01') . "', b.service_start_date,'" . date('Y-m-01') . "')))) DAY)))+1)/7) + IF(DAYOFWEEK(CURDATE())=b.service_week_day+1,1,0)
            WHEN (b.booking_type = 'BW')
                THEN ceil((CEIL(((DATEDIFF(IF(b.service_end = 0, '" . date('Y-m-t') . "',IF(b.service_end_date >= '" . date('Y-m-t') . "','" . date('Y-m-t') . "',b.service_end_date)),DATE_ADD(IF(b.service_start_date >= '" . date('Y-m-01') . "', b.service_start_date,'" . date('Y-m-01') . "'), INTERVAL (9 - IF(DAYOFWEEK(IF(b.service_start_date >= '" . date('Y-m-01') . "', b.service_start_date,'" . date('Y-m-01') . "'))=1, 8, DAYOFWEEK(IF(b.service_start_date >= '" . date('Y-m-01') . "', b.service_start_date,'" . date('Y-m-01') . "')))) DAY)))+1)/7) + IF(DAYOFWEEK(CURDATE())=b.service_week_day+1,1,0))/2)
         END


         ELSE 0 END),0) as total_bookings,

         IFNULL(ROUND(SUM(CASE WHEN (b.service_start_date BETWEEN '" . date('Y-m-01') . "' AND '" . date('Y-m-t') . "' OR b.service_actual_end_date BETWEEN '" . date('Y-m-01') . "' AND '" . date('Y-m-t') . "' OR b.service_end = 0) THEN
         CASE
            WHEN (b.booking_type = 'OD')
                THEN ((TIME_TO_SEC(b.time_to) - TIME_TO_SEC(b.time_from))/3600)
            WHEN (b.booking_type = 'WE')
                THEN (CEIL(((DATEDIFF(IF(b.service_end = 0, '" . date('Y-m-t') . "',IF(b.service_end_date >= '" . date('Y-m-t') . "','" . date('Y-m-t') . "',b.service_end_date)),DATE_ADD(IF(b.service_start_date >= '" . date('Y-m-01') . "', b.service_start_date,'" . date('Y-m-01') . "'), INTERVAL (9 - IF(DAYOFWEEK(IF(b.service_start_date >= '" . date('Y-m-01') . "', b.service_start_date,'" . date('Y-m-01') . "'))=1, 8, DAYOFWEEK(IF(b.service_start_date >= '" . date('Y-m-01') . "', b.service_start_date,'" . date('Y-m-01') . "')))) DAY)))+1)/7) + IF(DAYOFWEEK(CURDATE())=b.service_week_day+1,1,0))*((TIME_TO_SEC(b.time_to) - TIME_TO_SEC(b.time_from))/3600)
            WHEN (b.booking_type = 'BW')
                THEN (CEIL((CEIL(((DATEDIFF(IF(b.service_end = 0, '" . date('Y-m-t') . "',IF(b.service_end_date >= '" . date('Y-m-t') . "','" . date('Y-m-t') . "',b.service_end_date)),DATE_ADD(IF(b.service_start_date >= '" . date('Y-m-01') . "', b.service_start_date,'" . date('Y-m-01') . "'), INTERVAL (9 - IF(DAYOFWEEK(IF(b.service_start_date >= '" . date('Y-m-01') . "', b.service_start_date,'" . date('Y-m-01') . "'))=1, 8, DAYOFWEEK(IF(b.service_start_date >= '" . date('Y-m-01') . "', b.service_start_date,'" . date('Y-m-01') . "')))) DAY)))+1)/7) + IF(DAYOFWEEK(CURDATE())=b.service_week_day+1,1,0))/2))*((TIME_TO_SEC(b.time_to) - TIME_TO_SEC(b.time_from))/3600)
         END


         ELSE 0 END),2),0) as total_booking_hours,



        SUM(CASE WHEN (b.service_start_date BETWEEN '" . date('Y-m-01') . "' AND '" . date('Y-m-t') . "' OR b.service_actual_end_date BETWEEN '" . date('Y-m-01') . "' AND '" . date('Y-m-t') . "' OR b.service_end = 0) THEN 1 ELSE 0 END) as total_bookings_old,
        ROUND(SUM(CASE WHEN (b.service_start_date BETWEEN '" . date('Y-m-01') . "' AND '" . date('Y-m-t') . "' OR b.service_actual_end_date BETWEEN '" . date('Y-m-01') . "' AND '" . date('Y-m-t') . "' OR b.service_end = 0) THEN (TIME_TO_SEC(b.time_to) - TIME_TO_SEC(b.time_from)) ELSE 0 END)/3600)  as total_booking_hours_old", false)
            ->from('bookings as b')
            ->where_not_in('b.booking_id', $today_deleted_booking_ids);
        $query = $this->db->get();
        $result = $query->row_array();
        /*************************************************************************** */
        return $result;
    }
    public function dayCount($from, $to, $day)
    {
        // count number of same week day between date range
        $from = new DateTime($from);
        $to = new DateTime($to);
        $wF = $from->format('w');
        $wT = $to->format('w');
        if ($wF < $wT) {
            $isExtraDay = $day >= $wF && $day <= $wT;
        } else if ($wF == $wT) {
            $isExtraDay = $wF == $day;
        } else {
            $isExtraDay = $day >= $wF || $day <= $wT;
        }
        return floor($from->diff($to)->days / 7) + $isExtraDay;
    }

    public function monthly_counts_php()
    {
        /*************************************************************************** */
        // get data from main table
        // this includes one day deleted data tooo
        // so we will exclude it in next section
        $this->db->select("b.booking_id,b.time_from,b.time_to,b.service_week_day,b.service_start_date,b.service_end,b.service_end_date,b.service_actual_end_date,b.booking_type,b.booking_status,
        (TIME_TO_SEC(b.time_to) - TIME_TO_SEC(b.time_from))/3600 as hours", false)
            ->from('bookings as b')
            ->where("b.booking_status", 1)
            ->where("(b.service_start_date BETWEEN '" . date('Y-m-01') . "' AND '" . date('Y-m-t') . "' OR b.service_actual_end_date BETWEEN '" . date('Y-m-01') . "' AND '" . date('Y-m-t') . "' OR (b.service_end = 0 AND b.service_start_date < '" . date('Y-m-01') . "'))", null, false);
        $query = $this->db->get();
        $result = $query->result_array();
        /*************************************************************************** */
        // get one day deletes between this month
        // only filter not permanent deleted
        // join for hour calculation and exclude deleted
        $this->db->select("bd.booking_id,(TIME_TO_SEC(b.time_to) - TIME_TO_SEC(b.time_from))/3600 as hours", false)
            ->from('booking_deletes as bd')
            ->join('bookings as b', 'b.booking_id = bd.booking_id', 'left')
            ->where("bd.service_date BETWEEN '" . date('Y-m-01') . "' AND '" . date('Y-m-t') . "'", null, false)
            ->where("b.booking_status", 1);
        $query = $this->db->get();
        $dayDeletes = $query->result_array();
        $dayDeleteHours = 0;
        $dayDeleteCount = 0;
        foreach ($dayDeletes as $key => $deleted) {
            $dayDeleteHours += $deleted['hours'];
            $dayDeleteCount += 1;
        }
        /*************************************************************************** */
        $counts = array();
        $counts['total_bookings'] = 0;
        $counts['total_booking_hours'] = 0;
        foreach ($result as $key => $booking) {
            // set end date for calculation
            if ($booking['service_end'] == 0) {
                $end_date = date('Y-m-t');
            } else {
                if ($booking['service_actual_end_date'] >= date('Y-m-t')) {
                    $end_date = date('Y-m-t');
                } else {
                    $end_date = $booking['service_actual_end_date'];
                }
            }
            // set start date for calculation
            if ($booking['service_start_date'] <= date('Y-m-01')) {
                $start_date = date('Y-m-01');
            } else {
                $start_date = $booking['service_start_date'];
            }
            if ($booking['booking_type'] == 'OD') {
                $booking_count = 1;
            } else if ($booking['booking_type'] == 'WE') {
                $booking_count = $this->dayCount($start_date, $end_date, $booking['service_week_day']);
            } else if ($booking['booking_type'] == 'BW') {
                $booking_count = ceil($this->dayCount($start_date, $end_date, $booking['service_week_day']) / 2); // .5 to next integer (eg. 2.5 to 3)
            }
            $counts['total_bookings'] += $booking_count;
            $counts['total_booking_hours'] += ($booking_count * $booking['hours']);
        }
        $counts['total_bookings'] = $counts['total_bookings'] - $dayDeleteCount;
        $counts['total_booking_hours'] = $counts['total_booking_hours'] - $dayDeleteHours;
        $counts['total_booking_hours'] = number_format((float) $counts['total_booking_hours'], 2, '.', '');
        return $counts;
    }
    public function get_booking_deletes_by_date($service_date)
    {
        $this->db->select('bd.booking_id')
            ->from('booking_deletes as bd')
            ->where('bd.service_date', $service_date);
        $query = $this->db->get();
        return array_column($query->result_array(), 'booking_id');
    }
    public function get_booking_deletes_between_dates($from_date, $to_date)
    {
        $this->db->select('bd.booking_id')
            ->from('booking_deletes as bd')
            ->join('bookings as b', 'bd.booking_id = b.booking_id', 'left')
            ->where('bd.service_date >=', $from_date)
            ->where('bd.service_date <=', $to_date);
        $query = $this->db->get();
        return array_column($query->result_array(), 'booking_id');
    }
    public function get_booking_deletes_data_between_dates($from_date, $to_date, $where_in_booking_ids = '[]')
    {
        return $this->db->select('bd.booking_id,b.booking_type,bd.service_date')
            ->from('booking_deletes as bd')
            ->join('bookings as b', 'bd.booking_id = b.booking_id', 'left')
            ->where_in('b.booking_id', $where_in_booking_ids ?: '[]')
            ->where('bd.service_date >=', $from_date)
            ->where('bd.service_date <=', $to_date)
        //->where("(bd.service_date BETWEEN '" . $from_date . "' AND '" . $to_date . "')", null, false)
            ->get()->result_array();
    }
    public function test()
    {
        $this->db->select('bd.booking_id')
            ->from('booking_deletes as bd')
            ->where('bd.service_date', $service_date);
        $query = $this->db->get();
        return array_column($query->result_array(), 'booking_id');
    }
}
