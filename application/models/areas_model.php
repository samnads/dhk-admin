<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 /** 
  * Areas_model Class 
  * 
  * @package	Emaid
  * @author		Habeeb Rahman 
  * @since		Version 1.0
  */
class Areas_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	/** 
	 * Add area
	 * 
	 * @author	Habeeb Rahman
	 * @acces	public 
	 * @param	bool
	 * @return	array
	 */
	function add_area($fields = array())
	{
		$fields['area_status'] = isset($fields['area_status']) ? $fields['area_status'] : 1;

		$this->db->set($fields);
		$this->db->insert('areas'); 
		return $this->db->insert_id();
	}
        
	/** 
	 * Update area
	 * 
	 * @author	Geethu
	 * @acces	public 
	 * @param	int, array
	 * @return	int
	 */
	function update_area($area_id, $fields = array())
	{
		$this->db->where('area_id', $area_id);
		$this->db->update('areas', $fields); 

		return $this->db->affected_rows();
	}
        /** 
	 * Delete area
	 * 
	 * @author   Geethu
	 * @acces    public 
	 * @param    int
	 * @return   int
	 */
	function delete_area($area_id)
	{
            
                $this->db->where('area_id', $area_id);
		$this->db->delete('areas'); 

		return $this->db->affected_rows(); 
           
	}
	/** 
	 * Get areas
	 * 
	 * @author	Habeeb Rahman
	 * @acces	public 
	 * @param	bool
	 * @return	array
	 */
	function get_areas($active_only = TRUE)
	{
		$this->db->select('a.area_id, a.zone_id, a.area_name, a.area_charge, a.area_status, z.zone_name')
				->from('areas a')
				->join('zones z', 'a.zone_id = z.zone_id')
				->order_by('a.zone_id, a.area_name');
		
		if($active_only)
		{
			$this->db->where('area_status', 1);
			$this->db->where('zone_status', 1);
		}
		
		$get_areas_qry = $this->db->get();
		
		return $get_areas_qry->result();
	}
	
	/** 
	 * Get area by id
	 * 
	 * @author	Habeeb Rahman
	 * @acces	public 
	 * @param	bool
	 * @return	array
	 */
	function get_area_by_id($area_id)
	{
		$this->db->select('a.area_id, a.zone_id, a.area_name, a.area_charge, a.area_status, z.zone_name')
				->from('areas a')
				->join('zones z', 'a.zone_id = z.zone_id')
				->where('a.area_id', $area_id);

		$get_area_by_id_qry = $this->db->get();
		
		return $get_area_by_id_qry->row();
	}
	
	/** 
	 * Get zone by area zone and area name
	 * 
	 * @author	Habeeb Rahman
	 * @acces	public 
	 * @param	int, str
	 * @return	obj
	 */
	function get_area_by_zone_and_area_name($zone_id, $area_name)
	{
		$this->db->select('area_id')
				 ->from('areas')
				 ->where('zone_id', $zone_id)
				 ->where('area_name', $area_name)
				 ->limit(1);
		
		$get_area_by_zone_and_area_name_qry = $this->db->get();
		
		return $get_area_by_zone_and_area_name_qry->row();
	}
}