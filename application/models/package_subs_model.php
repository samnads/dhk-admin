<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/** 
 * Package_model Class 
 * 
 * @package HM
 * @author  Samnad. S
 * @since   Version 1.0
 */
class Package_subs_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    function get_all_subscriptions($status)
    {
        //error_reporting(E_ALL);
        $this->db->select('ps.*,p.*,c.*,
        CASE WHEN ps.status = 1 THEN "Active" WHEN ps.status = 0 THEN "Inactive" WHEN ps.status = 2 THEN "Deleted" END AS subscription_status,psp.payment_status,psp.payment_mode', FALSE)
            ->from('package_subscription as ps')
            ->join('package as p', 'ps.package_id = p.package_id', 'left')
            ->join('package_subscription_payment as psp', 'ps.id = psp.package_subscription_id', 'left')
            ->join('customers as c', 'ps.customer_id = c.customer_id', 'left')
            ->order_by('ps.id','desc');
        $this->db->where('psp.status', 1);
        if ($status != null) {
            $this->db->where('ps.status', $status);
        }
        $query = $this->db->get();
        //header('Content-Type: application/json'); echo json_encode($query->result_array(), JSON_PRETTY_PRINT); die();
        return $query->result_array();
    }
    function get_subscription_for_edit($id)
    {
        $this->db->select('ps.*,p.package_name,c.customer_name,psp.amount,psp.payment_status,psp.payment_mode')
            ->from('package_subscription as ps')
            ->join('package as p', 'ps.package_id = p.package_id', 'left')
            ->join('package_subscription_payment as psp', 'ps.id = psp.package_subscription_id', 'left')
            ->join('customers as c', 'ps.customer_id = c.customer_id', 'left')
            ->where('ps.id', $id);
            $this->db->where('psp.status', 1);
        $query = $this->db->get();
        //header('Content-Type: application/json'); echo json_encode($query->row_array(), JSON_PRETTY_PRINT); die();
        if ($query) {
            return $query->row_array();
        } else {
            return $query->row_array();
        }
    }
    function get_customers()
    {
        //error_reporting(E_ALL);
        $this->db->select('c.customer_id,c.customer_name,c.mobile_number_1,CONCAT(c.customer_name, " , ",c.mobile_number_1) as label', FALSE)
            ->from('customers as c')
            ->order_by('c.customer_id')
            ->where('c.customer_status', 1)
            ->order_by("c.customer_id", "asc")
            ->limit(50);
        $query = $this->db->get();
        //header('Content-Type: application/json'); echo json_encode($query->result_array(), JSON_PRETTY_PRINT); die();
        return $query->result_array();
    }
    function customer_search($query)
    {
        header('Content-Type: application/json; charset=utf-8');
        $this->db->select('c.customer_id as id,CONCAT(c.customer_name, " , ",c.mobile_number_1) as text', FALSE)
            ->from('customers as c')
            ->where('c.customer_status', 1)
            ->order_by("c.customer_name", "asc");
        if ($query) {
            $this->db->where("(c.customer_name LIKE '%" . $query . "%' OR c.mobile_number_1 LIKE '%" . $query . "%')", NULL, FALSE);
        }
        $query = $this->db->limit(100)->get();
        //header('Content-Type: application/json'); echo json_encode($query->result_array(), JSON_PRETTY_PRINT); die();
        $data['results'] = $query->result_array();
        echo json_encode($data);
    }
    function package_search($query)
    {
        header('Content-Type: application/json; charset=utf-8');
        $this->db->select('p.package_id as id,CONCAT(p.package_name) as text,p.amount as amount', FALSE)
            ->from('package as p')
            ->where('p.status', 1)
            ->order_by("p.package_name", "asc");
        if ($query) {
            $this->db->where("(p.package_name LIKE '%" . $query . "%' OR p.description LIKE '%" . $query . "%')", NULL, FALSE);
        }
        $query = $this->db->limit(100)->get();
        //header('Content-Type: application/json'); echo json_encode($query->result_array(), JSON_PRETTY_PRINT); die();
        $data['results'] = $query->result_array();
        echo json_encode($data);
    }
    function save_subscription($data)
    {
        $data['addedDateTime'] = date('Y-m-d H:m:s');
        $data['addedBy'] = $this->session->userdata['user_logged_in']['user_id'];
        $data['notes'] = $data['notes'] ?: NULL;
        $this->db->insert('package_subscription', $data);
        return $this->db->insert_id();
    }
    function update_subscription($data)
    {
        try {
            $this->db->trans_start();
            /***************************************************** */
            // update subscription table
            $subscription['notes'] = $data['notes'];
            $this->db->where('id', $data['id']);
            $this->db->update('package_subscription', $subscription);
            /***************************************************** */
            // update payment table
            if($data['payment_status']){
                $payment['payment_status'] = $data['payment_status'];
            }
            $this->db->where('package_subscription_id', $data['id']);
            $this->db->where('status', 1);

            $this->db->update('package_subscription_payment', $payment);
            /***************************************************** */
            $this->db->trans_complete();
            return true;
        }
         catch (Exception $e) {
            return false;
         }
    }
    function change_status($id, $new_status)
    {
        $this->db->where('id', $id);
        $data = array('status' => $new_status, 'updatedDateTime' => date('Y-m-d H:m:s'), 'updatedBy' =>  $this->session->userdata['user_logged_in']['user_id']);
        $this->db->update('package_subscription', $data);
        if ($this->db->affected_rows())
            return true;
        else
            return false;
    }
}
