<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Expense_model extends CI_Model{
	
	public function count_all_expenses()
    {
        $this->db->select('e.expense_id')
            ->from('expenses e')
			->join('customers c', 'e.customer_id = c.customer_id');
		$this->db->where('e.status', 1);
        $query = $this->db->get();
        $records = $query->result();
        return $records;
    }
	
	public function get_all_expensesnew($custid = NULL, $regdate = NULL, $regdateto = NULL, $qbsync = NULL)
    {
        $this->db->select('e.expense_id')
            ->from('expenses e')
			->join('customers c', 'e.customer_id = c.customer_id');
        if ($custid != "") {
            $this->db->where('e.customer_id', $custid);
        }
		if ($qbsync != "") {
            $this->db->where('e.quickbook_expense_sync_status', $qbsync);
        }
		if ($regdate && $regdateto) {
            $this->db->where("DATE(`expense_date`) BETWEEN '$regdate' AND '$regdateto'");
        }
        if (($regdate != "" && $regdateto == "") || ($regdate == "" && $regdateto != "")) {
            if ($regdate != "") {
                $this->db->where("DATE(`expense_date`) = '$regdate'");
            }
            if ($regdateto != "") {
                $this->db->where("DATE(`expense_date`) = '$regdateto'");
            }
        }
		$this->db->where('e.status', 1);
		$query = $this->db->get();
        $records = $query->result();
        return $records;
    }
	
	public function get_all_newexpenses($columnName = NULL, $columnSortOrder = NULL, $rowperpage = NULL, $start = NULL, $draw = NULL, $recordsTotal = NULL, $recordsTotalFilter = NULL, $custid = NULL, $regdate = NULL, $regdateto = NULL, $qbsync = NULL)
    {
        $response = array();
        $this->db->select('e.expense_id,e.customer_id, e.payment_method, e.expense_date, e.amount, e.allocated_amount, e.expense_ref, e.description, e.quickbook_expense_sync_status, c.customer_name, c.mobile_number_1')
            ->from('expenses e')
			->join('customers c', 'e.customer_id = c.customer_id');
        if ($custid != "") {
            $this->db->where('e.customer_id', $custid);
        }
		if ($qbsync != "") {
            $this->db->where('e.quickbook_expense_sync_status', $qbsync);
        }
		if ($regdate && $regdateto) {
            $this->db->where("DATE(`expense_date`) BETWEEN '$regdate' AND '$regdateto'");
        }
        if (($regdate != "" && $regdateto == "") || ($regdate == "" && $regdateto != "")) {
            if ($regdate != "") {
                $this->db->where("DATE(`expense_date`) = '$regdate'");
            }
            if ($regdateto != "") {
                $this->db->where("DATE(`expense_date`) = '$regdateto'");
            }
        }
        $this->db->where('e.status', 1);
        if ($columnName != '' && $columnSortOrder != "") {
            $this->db->order_by($columnName, $columnSortOrder);
        }
        $this->db->limit($rowperpage, $start);
        $query = $this->db->get();
        $records = $query->result();
        $data = array();
        $i = 1;

        foreach ($records as $record) {
			if($record->payment_method == 0)
			{
				$paymethod = "Cash";
			} else if($record->payment_method == 1){
				$paymethod = "Card";
			}
			else if($record->payment_method == 2){
				$paymethod = "Cheque";
			}
			else if($record->payment_method == 3){
				$paymethod = "Bank";
			}
			else if($record->payment_method == 4){
				$paymethod = "Credit Card";
			}
			if ($record->quickbook_expense_sync_status == 0) {
                $icon = '<i class="n-btn-icon red-btn fa fa-exclamation"> </i>';
            } else {
                $icon = '<i class="n-btn-icon green-btn fa fa-check"> </i>';
            }
			
            $data[] = array(
                'slno' => '<center>'.($i + $start).'<center>',
				'name' => $record->customer_name,
				'mobile' => $record->mobile_number_1,
				'expdate' => $record->expense_date,
				'ref' => $record->expense_ref,
				'type' => 'Refund - '.$paymethod,
				'desc' => $record->description,
				'amount' => $record->amount,
				'paidamount' => $record->allocated_amount,
				'status' => $icon,
            );
            $i++;
        }

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $recordsTotal,
            "lastcall" => $records[0],
            "iTotalDisplayRecords" => $recordsTotalFilter,
            "aaData" => $data
        );

        return $response;
    }
	
	public function get_expense_detailbyid($expenseid)
	{
		$this->db->select('e.*')
                ->from('expenses e')
				->where('e.expense_id',$expenseid)
				->where('e.status',1)
				->order_by('e.expense_date','asc');
		$get_invoice_qry = $this->db->get();
        return $get_invoice_qry->result();
	}
	
	function update_expensepay_detail($expenseid,$fields = array())
	{
		$this->db->where('expense_id', $expenseid);
		$this->db->update('expenses', $fields);
		return $this->db->affected_rows();
	}
}