<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Booking_cleaning_supplies_model extends CI_Model {

  // Method to insert a new record into the booking_cleaning_supplies table
  public function insert_booking_cleaning_supply($data)
  {
      $this->db->insert('booking_cleaning_supplies', $data);
      return $this->db->insert_id();
  }

  // Method to insert records into the booking_cleaning_supplies table
  public function insert_batch_booking_cleaning_supply($data)
  {
      $this->db->insert_batch('booking_cleaning_supplies', $data);
      return $this->db->insert_id();
  }

  // Method to update an existing record in the booking_cleaning_supplies table
  public function update_booking_cleaning_supply($booking_id, $data)
  {
      $this->db->where('booking_id', $booking_id);
      return $this->db->update('booking_cleaning_supplies', $data);
  }

  // Method to delete a record from the booking_cleaning_supplies table
  public function delete_booking_cleaning_supply($booking_id)
  {
      $this->db->where('booking_id', $booking_id);
      return $this->db->delete('booking_cleaning_supplies');
  }

  // Method to get all cleaning supplies for a specific booking
  public function get_cleaning_supplies_by_booking($booking_id)
  {
    $this->db->select('bcs.id, bcs.booking_id, bcs.cleaning_supply_id, ccs.name, ccs.amount');
    $this->db->from('booking_cleaning_supplies AS bcs');
    $this->db->join('config_cleaning_supplies AS ccs', 'bcs.cleaning_supply_id = ccs.id');
    $this->db->where('bcs.booking_id', $booking_id);
    return $this->db->get()->result();
  }

}