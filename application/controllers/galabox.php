<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Galabox extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
		$this->load->model('customers_model');
		$this->load->model('settings_model');
		$this->load->helper('curl_helper');
    }
	
	public function index()
    {
		$array = array(
			'name' => 'Vishnu Azinova',
			'email' => array('vishnu.m@azinova.info'),
			'phone' => array('+919633351242'),
			'fieldValues' => array(
				'Address' => 'Dubai Airport, UAE',
				'source__new_format'  => 'DHK APP'
			)
		);
		$post = json_encode($array);
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => 'https://server.gallabox.com/devapi/contacts/upsert',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => $post,
			CURLOPT_HTTPHEADER => array(
				'apiKey: 6697907e4407420eac1f6d77',
				'apiSecret: af4e6992cd6d44528202e66573994a99',
				'Content-Type: application/json'
			),
		));

		$response = curl_exec($curl);

		curl_close($curl);
		echo $response;
	}
	
	public function get_all_customers()
	{
		$customerlist = $this->customers_model->get_all_customers_by_num();
		
		// foreach ($customerlist as $list)
		// {
			// $phone_number = $list->whatsapp_no_1;
			// if (substr($phone_number, 0, 1) === '0') {
			   // $phone_number = '971' . substr($phone_number, 1);
			// }
			// $update_customer_array = array();
			// $update_customer_array['whatsapp_no_1'] = $phone_number;
			// $this->customers_model->update_customers($update_customer_array,$list->customer_id);
			// echo "updated";
			// echo '<br/>';
		// }
		echo "<pre>";
		print_r($customerlist);
		exit();
	}

	public function sent_customer_statement()
	{
		$this->load->library('pdf');
		$statementtype = $this->input->post('statementtype');
		$customer_id = $this->input->post('customer_id');
		$startdate = $this->input->post('startdate');
        $enddate = $this->input->post('enddate');
		$newmonth = date("F Y", strtotime($startdate));
		$month = date("F-Y", strtotime($startdate));

		if($statementtype == 1)
        {
            $get_startingbalance = $this->customers_model->getinitialbal($customer_id);
            
            $get_customer_statement = $this->customers_model->get_customer_statement_new_opening($customer_id);
			
            usort($get_customer_statement, array($this, 'date_compare'));
            
            if(!empty($get_startingbalance))
            {
                $data['initial_balance'] = $get_startingbalance->initial_balance;
                $data['initial_bal_sign'] = $get_startingbalance->initial_bal_sign;
                $data['initial_bal_date'] = $get_startingbalance->initial_bal_date;
                $data['customername'] = $get_startingbalance->customer_name;
                $data['address'] = $get_startingbalance->customer_address;
				$customernamme = $get_startingbalance->customer_name;
				$gallaboxsyncstatus = $get_startingbalance->gallaboxSynStatus;
				$mobile1 = $get_startingbalance->whatsapp_no_1;
            } else {
                $data['initial_balance'] = 0;
                $data['initial_bal_sign'] = 'Dr';
                $data['initial_bal_date'] = '';
                $data['customername'] = "";
                $data['address'] = "";
				$customernamme = "";
				$gallaboxsyncstatus = "";
				$mobile1 = "";
            }
            $data['customer_statement'] = $get_customer_statement;
        } else if($statementtype == 2)
		{
			$statement_all = $this->customers_model->get_customer_statement_new_opening($customer_id);
			$statement_before = [];
			$data['total_credit_before_start_date'] = 0;
			$data['total_debit_before_start_date'] = 0;
			foreach($statement_all as $key => $statement){
				if($statement->dateval < $startdate){
					$statement_before[] = $statement;
					if($statement->stattype == 'A'){
						$data['total_debit_before_start_date'] += $statement->amount;
					}
					else{
						$data['total_credit_before_start_date'] += $statement->amount;
					}      
				}
			}
			/************************************************ */
			$get_startingbalance = $this->customers_model->getinitialbal($customer_id);
			// $get_customer_statement = $this->customers_model->get_customer_statement_new($customer_id,$startdate,$enddate);
			$get_customer_statement = $this->customers_model->get_customer_statement_new_opening_dates($customer_id,$startdate,$enddate);
			//usort($get_customer_statement, "date_compare");
			usort($get_customer_statement, array($this, 'date_compare'));
			// echo '<pre>';
			// print_r($get_customer_statement);
			// exit();
				
			if(!empty($get_startingbalance))
			{
				$data['initial_balance'] = $get_startingbalance->initial_balance;
				$data['initial_bal_sign'] = $get_startingbalance->initial_bal_sign;
				$data['initial_bal_date'] = $get_startingbalance->initial_bal_date;
				$data['customername'] = $get_startingbalance->customer_name;
				$data['address'] = $get_startingbalance->customer_address;
				$customernamme = $get_startingbalance->customer_name;
				$gallaboxsyncstatus = $get_startingbalance->gallaboxSynStatus;
				$mobile1 = $get_startingbalance->whatsapp_no_1;
			} else {
				$data['initial_balance'] = 0;
				$data['initial_bal_sign'] = 'Dr';
				$data['initial_bal_date'] = '';
				$data['customername'] = "";
				$data['address'] = "";
				$customernamme = "";
				$gallaboxsyncstatus = "";
				$mobile1 = "";
			}
			$data['customer_statement'] = $get_customer_statement;
		} else {
			$data['initial_balance'] = 0;
            $data['initial_bal_sign'] = 'Dr';
            $data['initial_bal_date'] = '';
            $data['customername'] = "";
            $data['address'] = "";
            $data['customer_statement'] = array();
		}
		$data['settings'] = $this->settings_model->get_settings();
		$data['startdate'] = $startdate;
		$data['enddate'] = $enddate;
		$data['customeridd'] = $customer_id;
		$html_content = $this->load->view('galaboxstatementpdf', $data, TRUE);
		$this->pdf->loadHtml($html_content);
		$this->pdf->set_paper("a4", "portrait");
		$this->pdf->render();
		$pdfname = 'Statement_'.date('m-d-Y_hisa');
		// file_put_contents('/var/www/booking/dhk-developer-demo/statements/'.$pdfname.'.pdf', $this->pdf->output());
		file_put_contents('/var/www/booking/dhk/statements/'.$pdfname.'.pdf', $this->pdf->output());
		// file_put_contents('C:\xampp/htdocs/dhk-admin/statements/'.$pdfname.'.pdf', $this->pdf->output());

		$pdfurl = base_url().'statements/'.$pdfname.'.pdf';
		//sync to gallabox
		// if($gallaboxsyncstatus == 0)
		// {
		// 	$mobilearray = array();
        //     if(!empty($get_startingbalance)){
        //         if($mobile1 != "")
        //         {
        //             array_push($mobilearray,'+'.$mobile1);
        //         }
        //         if($mobile1 != $get_startingbalance->mobile_number_1)
        //         {
        //             array_push($mobilearray,'+'.$get_startingbalance->mobile_number_1);
        //         }

        //         $customer_array = array(
        //             'name' => $customernamme,
        //             'phone' => $mobilearray,
        //             'fieldValues' => array(
        //                 'emaid_sync_id'  => $customer_id
        //             )
        //         );
        //         $post_json = json_encode($customer_array);
        //         $url = "https://server.gallabox.com/devapi/contacts/upsert";

        //         $customerUpdate = curl_gallabox_service($url, $post_json);
        //         $resultdata = json_decode($customerUpdate);
        //         if(!empty($resultdata) && $resultdata->status != "")
        //         {
        //             $return = array();
        //             $return['status'] = 'error';
        //             $return['message'] = $resultdata->message;
        //             echo json_encode($return);
        //             exit();
        //         } else if(!empty($resultdata) && $resultdata->accountId != ""){
        //             $updatefield = array();
		// 			$updatefield['gallaboxSynStatus'] = 1;
		// 			$updatefield['gallaboxSyncId'] = $resultdata->accountId;
		// 			$updatess = $this->customers_model->update_customers($updatefield,$customer_id);
        //         } else {
        //             $return = array();
        //             $return['status'] = 'error';
        //             $return['message'] = "Something went wrong. Try again.";
        //             echo json_encode($return);
        //             exit();
        //         }
        //     } else {
        //         $return = array();
        //         $return['status'] = 'error';
        //         $return['message'] = "Customer doenont exist...";
        //         echo json_encode($return);
        //         exit();
        //     }
		// }

		//Sent to whatsapp
		$input_post = [
            "channelId" => "62027834e940250004e3e1df", 
            "channelType" => "whatsapp", 
            "recipient" => [
                  "name" => $customernamme,
                  "phone" => $mobile1 
            ], 
            "whatsapp" => [
                "type" => "template", 
                "template" => [
                    "templateName" => "auto_account_statement_em", 
					"headerValues" => [
                        "mediaUrl" => $pdfurl,
                        "mediaName" => $customernamme." Statement"
                    ],
                    "bodyValues" => [
                        "1" => $customernamme 
                        // "2" => "account statement",
                        // "3" => $newmonth
                    ] 
                ] 
            ] 
        ];

        $newurl = "https://server.gallabox.com/devapi/messages/whatsapp";
        $input_post_json = json_encode($input_post);
        $sentmessage = curl_gallabox_service($newurl,$input_post_json);

        $resultdatamsg = json_decode($sentmessage);
        if(!empty($resultdatamsg) && $resultdatamsg->status != "ACCEPTED")
        {
            $returnmsg = array();
            $returnmsg['status'] = 'error';
            $returnmsg['message'] = $resultdatamsg->message;
            echo json_encode($returnmsg);
            exit();
        } else if(!empty($resultdatamsg) && $resultdatamsg->id != ""){
            $returnmsg = array();
            $returnmsg['status'] = 'success';
            $returnmsg['message'] = "Statement sent successfully";
            echo json_encode($returnmsg);
            exit();
        } else {
            $return = array();
            $return['status'] = 'error';
            $return['message'] = "Something went wrong. Try again.";
            echo json_encode($return);
            exit();
        }
	}

	public function get_customer_statement()
	{
		$handle = fopen('php://input', 'r');
		$jsonInput = fgets($handle);
		//$jsonInput  = '{"name":"Sarumol","email":"saranya.12@gmail.com","phone":"7561859284","password":"kalidas","device_type":"ios","device_id":"fvlvndnflvdndkvdnfvkdvdfv dvdlkfndlvdnfvldnvld vndvndlvdv"}'; // normal login from mobile
		$json_arr = json_decode($jsonInput, true);
		if (!empty($json_arr))
		{
			if ($json_arr['customer_id'] && strlen($json_arr['customer_id']) > 0) 
			{
				$this->load->library('pdf');
				$customer_id = $json_arr['customer_id'];
				$last_payment = $this->customers_model->get_last_payment_byid($customer_id);
				if(!empty($last_payment))
				{
					$startdate = $last_payment->paiddate;
					$enddate = date('Y-m-d');
					$statement_all = $this->customers_model->get_customer_statement_new_opening($customer_id);
					$statement_before = [];
					$data['total_credit_before_start_date'] = 0;
					$data['total_debit_before_start_date'] = 0;
					foreach($statement_all as $key => $statement){
						if($statement->dateval < $startdate){
							$statement_before[] = $statement;
							if($statement->stattype == 'A'){
								$data['total_debit_before_start_date'] += $statement->amount;
							}
							else{
								$data['total_credit_before_start_date'] += $statement->amount;
							}      
						}
					}
					/************************************************ */
					$get_startingbalance = $this->customers_model->getinitialbal($customer_id);
					// $get_customer_statement = $this->customers_model->get_customer_statement_new($customer_id,$startdate,$enddate);
					$get_customer_statement = $this->customers_model->get_customer_statement_new_opening_dates($customer_id,$startdate,$enddate);
					//usort($get_customer_statement, "date_compare");
					usort($get_customer_statement, array($this, 'date_compare'));
					// echo '<pre>';
					// print_r($get_customer_statement);
					// exit();
						
					if(!empty($get_startingbalance))
					{
						$data['initial_balance'] = $get_startingbalance->initial_balance;
						$data['initial_bal_sign'] = $get_startingbalance->initial_bal_sign;
						$data['initial_bal_date'] = $get_startingbalance->initial_bal_date;
						$data['customername'] = $get_startingbalance->customer_name;
						$data['address'] = $get_startingbalance->customer_address;
						$customernamme = $get_startingbalance->customer_name;
						$gallaboxsyncstatus = $get_startingbalance->gallaboxSynStatus;
						$mobile1 = $get_startingbalance->whatsapp_no_1;
					} else {
						$data['initial_balance'] = 0;
						$data['initial_bal_sign'] = 'Dr';
						$data['initial_bal_date'] = '';
						$data['customername'] = "";
						$data['address'] = "";
						$customernamme = "";
						$gallaboxsyncstatus = "";
						$mobile1 = "";
					}
					$data['customer_statement'] = $get_customer_statement;
				} else {
					$startdate = "";
					$enddate = "";
					$get_startingbalance = $this->customers_model->getinitialbal($customer_id);
					
					$get_customer_statement = $this->customers_model->get_customer_statement_new_opening($customer_id);
					
					usort($get_customer_statement, array($this, 'date_compare'));
					
					if(!empty($get_startingbalance))
					{
						$data['initial_balance'] = $get_startingbalance->initial_balance;
						$data['initial_bal_sign'] = $get_startingbalance->initial_bal_sign;
						$data['initial_bal_date'] = $get_startingbalance->initial_bal_date;
						$data['customername'] = $get_startingbalance->customer_name;
						$data['address'] = $get_startingbalance->customer_address;
						$customernamme = $get_startingbalance->customer_name;
						$gallaboxsyncstatus = $get_startingbalance->gallaboxSynStatus;
						$mobile1 = $get_startingbalance->whatsapp_no_1;
					} else {
						$data['initial_balance'] = 0;
						$data['initial_bal_sign'] = 'Dr';
						$data['initial_bal_date'] = '';
						$data['customername'] = "";
						$data['address'] = "";
						$customernamme = "";
						$gallaboxsyncstatus = "";
						$mobile1 = "";
					}
					$data['customer_statement'] = $get_customer_statement;
				}
				$data['settings'] = $this->settings_model->get_settings();
				$data['startdate'] = $startdate;
				$data['enddate'] = $enddate;
				$data['customeridd'] = $customer_id;
				$html_content = $this->load->view('galaboxstatementpdf', $data, TRUE);
				$this->pdf->loadHtml($html_content);
				$this->pdf->set_paper("a4", "portrait");
				$this->pdf->render();
				$pdfname = 'Statement_'.date('m-d-Y_hisa');
				// file_put_contents('/var/www/booking/dhk-developer-demo/statements/'.$pdfname.'.pdf', $this->pdf->output());
				file_put_contents('/var/www/booking/dhk/statements/'.$pdfname.'.pdf', $this->pdf->output());
				// file_put_contents('C:\xampp/htdocs/dhk-admin/statements/'.$pdfname.'.pdf', $this->pdf->output());

				$pdfurl = base_url().'statements/'.$pdfname.'.pdf';
				$returnmsg = array();
				$returnmsg['status'] = 'success';
				$returnmsg['statement'] = $pdfurl;
				$returnmsg['last_payment_date'] = $startdate;
				$returnmsg['message'] = "Statement sent successfully";
				echo json_encode($returnmsg);
			}
		} else {
			$returnmsg = array();
			$returnmsg['status'] = 'error';
			$returnmsg['message'] = "Error...Try again.";
			echo json_encode($returnmsg);
		}
	}

	function date_compare($element1, $element2) {
		$datetime1 = strtotime($element1->dateval);
		$datetime2 = strtotime($element2->dateval);
		return $datetime1 - $datetime2;
	}
	
}