<?php
/********************************** */
// Author : Samnad. S
// Usage : For getting data by ajax
// Created on : 19-09-2023
// Last edited by :
// Last edited on :
//
/********************************** */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Data extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        header('Content-Type: application/json');
        if (!is_user_loggedin()) {
            $result['message'] = 'Authentication Error !';
            $result['success'] = false;
            echo json_encode($result, TRUE);
        }
    }
    public function get_booking()
    {
        $booking_id = $this->input->get('booking_id');
        $response['booking'] = $this->db->select('b.*,ROUND(TIME_TO_SEC(timediff(b.time_to,b.time_from))/3600,2) AS _service_hours',false)->from('bookings as b')->where('b.booking_id', $booking_id)->get()->row();
        $response['cleaning_supplies'] = $this->db->select('bcs.*,ccs.name as cleaning_supply_name,ccs.type')->from('booking_cleaning_supplies as bcs')->join('config_cleaning_supplies as ccs', 'bcs.cleaning_supply_id = ccs.id', 'left')->where('bcs.booking_id', $booking_id)->get()->result();
        $response['extra_services'] = $this->db->select('bes.*,es.service as extra_service_name')->from('booking_extra_services as bes')->join('extra_services as es', 'bes.extra_service_id = es.id', 'left')->where('bes.booking_id', $booking_id)->get()->result();
        echo json_encode($response, JSON_PRETTY_PRINT);
    }
}