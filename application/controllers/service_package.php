<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Service_package extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!is_user_loggedin()) {
            redirect('logout');
        }
        $this->load->model('service_package_model');
        $this->load->model('settings_model');
    }

    public function index()
    {
		$status = $this->input->get('status');
        $packages = $this->service_package_model->get_all_packages($status);
        foreach ($packages as $key => $package) {
            $data['packages'][$package->service_type_name][] = $package;
        }
        $data['status'] = $status;
        $data['settings'] = $this->settings_model->get_settings();
        //echo '<pre>';print_r($data['packages']);echo '<pre>';die();
        $layout_data['content_body'] = $this->load->view('service_package/package_list', $data, true);
        $layout_data['page_title'] = 'Service Packages';
		$layout_data['meta_description'] = 'Service Packages';
		$layout_data['css_files'] = array('demo.css');
		$layout_data['external_js_files'] = array();
		$layout_data['js_files'] = array('service_package.js');
		$this->load->view('layouts/default', $layout_data);
    }

    public function toggle_deleted_at()
    {
        header('Content-Type: application/json; charset=utf-8');
        $package = $this->service_package_model->get_package_by_id($this->uri->segment(3));
        if ($this->service_package_model->update_package_by_id($package->id, array('deleted_at' => $package->deleted_at ? null : date('Y-m-d H:i:s')))) {
            if ($package->deleted_at) {
                $response['message'] = "Package Enabled Successfully !";
            } else {
                $response['message'] = "Package Disabled Successfully !";
            }
            $response['status'] = true;
        } else {
            $response['message'] = "Package status change failed !";
            $response['status'] = false;
        }
        echo json_encode($response);
    }

    public function new_package()
    {
        $data['services_types'] = $this->db->select('*')->from('service_types')->where('service_type_model_id', '2')->get()->result_array();
        $data['building_types'] = $this->db->select('bt.id,bt.name')->from('building_types as bt')->where('bt.deleted_at', null)->get()->result();
        $data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('service_package/service_package_new', $data, true);
        $layout_data['page_title'] = 'New Package';
        $layout_data['meta_description'] = '';
        $layout_data['css_files'] = array();
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('service_package.new.js?v=' . time());
        $this->load->view('layouts/default', $layout_data);
    }
    public function get_building_type_rooms_by_building_type()
    {
        header('Content-Type: application/json; charset=utf-8');
        $data['data'] = $this->db->select('btr.id,br.name as building_room_name')->from('building_type_room as btr')->join('building_rooms as br', 'btr.building_room_id = br.id', 'left')->where('btr.deleted_at', null)->where('btr.building_type_id', $this->input->get("building_type_id"))->get()->result();
        echo json_encode($data, JSON_PRETTY_PRINT);
    }

    public function get_building_types_for_service()
    {
        header('Content-Type: application/json; charset=utf-8');
        $data['data'] = $this->db->select('bt.id,bt.name')
        ->from('building_types as bt')
        ->where('bt.deleted_at', null)
        ->where('bt.service_type_id', $this->input->get("service_type_id"))
        ->get()
        ->result();
        echo json_encode($data, JSON_PRETTY_PRINT);
    }

    public function get_building_rooms_by_building_type()
    {
        header('Content-Type: application/json; charset=utf-8');
        $data['data'] = $this->db->select('br.id,br.name')
        ->from('building_rooms as br')
        ->where('br.deleted_at', null)
        ->where('br.building_type_id', $this->input->get("building_type_id"))
        ->get()
        ->result();
        echo json_encode($data, JSON_PRETTY_PRINT);
    }

    public function save_package()
    {
        header('Content-Type: application/json; charset=utf-8');
        // print_r($this->input->post());die();
        // $data['customer_app_thumbnail '] = $this->input->post("thumbnail_image_base64");
        $thumbinal_image = $this->input->post("thumbnail_image_base64");
        if ($thumbinal_image != null) {
            $base64 = $this->input->post("thumbnail_image_base64");
            list($type, $base64) = explode(';', $base64);
            list(, $base64) = explode(',', $base64);
            $base64 = base64_decode($base64);
            $file_name = date('d-m-Y_') . md5(time()) . ".jpg";
            // unset($thumbinal_image);
            if (file_put_contents(DIR_UPLOAD_SERVICE_PACKAGE_THUMBNAIL_IMG . $file_name, $base64)) {
                $data['customer_app_thumbnail'] = $file_name;
            }
        }
        unset($thumbinal_image);
        //$data['service_type_id'] = $this->input->post("service_type_id");
        $data['service_time'] = $this->input->post("service_time");
        $data['building_room_id'] = $this->input->post("building_type_room_id");
        $data['title'] = $this->input->post("title");
        $data['is_offer'] = $this->input->post("is_offer") == 1 ? 1 : null;
        $data['description'] = $this->input->post("description") ?: null;
        $data['info_html'] = $this->input->post("info_html") ?: null;
        $data['actual_total_amount'] = $this->input->post("actual_total_amount");
        $data['total_amount'] = $this->input->post("total_amount");
        $data['cart_limit'] = $this->input->post("cart_limit");
        $data['sort_order'] = $this->input->post("sort_order");
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['updated_at'] = date('Y-m-d H:i:s');
        $data['deleted_at'] = $this->input->post("deleted_at") == 1 ? date('Y-m-d H:i:s') : null;
        $data['customer_app_thumbnail'] = $file_name;
        // print_r($data);die();
        $this->db->set($data);
        $this->db->insert('building_type_room_packages');
        $insert_id = $this->db->insert_id();
        $response['status'] = true;
        $response['message'] = "Service Package added successfully !";
        echo json_encode($response);
    }

    public function edit()
    {
        $packages_id = $this->uri->segment(3);
        $data['package'] = $this->service_package_model->get_package_by_id_join_all($packages_id);
        $data['settings'] = $this->settings_model->get_settings();
        // print_r($data['package']);die();
        $layout_data['content_body'] = $this->load->view('service_package/service_package_edit', $data, true);
        $layout_data['page_title'] = 'Edit Service Package';
        $layout_data['meta_description'] = 'Edit Service Package';
        $layout_data['css_files'] = array();
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('service_package.js?v=' . time());
        $this->load->view('layouts/default', $layout_data);
    }
    public function update_package()
    {
        header('Content-Type: application/json; charset=utf-8');
        $packages_id = $this->input->post("id");
        $package = $this->service_package_model->get_package_by_id($packages_id);
       
        //unset($thumbinal_image);
        // $_POST['customer_app_thumbnail'] = $file_name ?: null;
        $_POST['deleted_at'] = $_POST['deleted_at'] == 1 ? null : date('Y-m-d H:i:s');
        $_POST['is_offer'] = $_POST['is_offer'] ?: null;
        $_POST['info_html'] = $_POST['info_html'] ?: null;
        $_POST['service_time'] = $_POST['service_time'] ?: null;
        if ($this->input->post("thumbnail_image_base64") != null) {
            $base64 = $this->input->post("thumbnail_image_base64");
            list($type, $base64) = explode(';', $base64);
            list(, $base64) = explode(',', $base64);
            $base64 = base64_decode($base64);
            $file_name = $package->id.date('_d-m-Y_') . md5(time()) . ".jpg";
            // unset($thumbinal_image);
            if (file_put_contents(DIR_UPLOAD_SERVICE_PACKAGE_THUMBNAIL_IMG . $file_name, $base64)) {
                $_POST['customer_app_thumbnail'] = $file_name;
            }
        }
        //unset($_POST['deleted_at']);
        unset($_POST['thumbnail_image_base64']);
        //  print_r($_POST);die();
        if ($this->service_package_model->update_package_by_id($package->id, $_POST)) {
            $response['status'] = true;
            $response['message'] = "Package updated successfully !";
        } else {
            $response['message'] = "Package update failed !";
            $response['status'] = false;
        }
        echo json_encode($response);
    }
}