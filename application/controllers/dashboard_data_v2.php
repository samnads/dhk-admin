<?php if (!defined('BASEPATH')) {
/*****************************************************/
//             CODE By  :   Samnad . S               */
    //                                                   */
    //    Last Edit by      :                            */
    //                                                   */
    //                                                   */
    //                                                   */
    /*****************************************************/
    exit('No direct script access allowed');
}

class Dashboard_data_v2 extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        header('Content-Type: application/json; charset=utf-8');
        if (!is_user_loggedin()) {
            die(json_encode(array('success' => false, 'message' => 'Please login to continue', 'redirect' => base_url('login')), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
        }
        $this->load->model('dashboard_model_v2');
        $this->load->model('driver_model');
    }
    public function index()
    {
        echo 'Hello World !';
    }
    public function booking_counts_between_dates()
    {
        $from_date = $this->input->get('filter_from_date');
        $to_date = $this->input->get('filter_to_date');
        $data = $this->dashboard_model_v2->booking_counts_between_dates($from_date, $to_date);
        $this->load->view('dashboard_data_view', $data = array('data' => $data));
    }
    public function hours_data_between_dates()
    {
        $from_date = $this->input->get('filter_from_date');
        $to_date = $this->input->get('filter_to_date');
        $data = $this->dashboard_model_v2->hours_data_between_dates($from_date, $to_date);
        $this->load->view('dashboard_data_view', $data = array('data' => $data));
    }
    public function monthly_payments_between_dates()
    {
        $from_date = $this->input->get('filter_from_date');
        $to_date = $this->input->get('filter_to_date');
        $data = $this->dashboard_model_v2->monthly_payments_between_dates($from_date, $to_date);
        $this->load->view('dashboard_data_view', $data = array('data' => $data));
    }
    public function monthly_sales_statistics_between_dates()
    {
        $from_date = $this->input->get('filter_from_date');
        $to_date = $this->input->get('filter_to_date');
        $data['chart_data'] = $this->dashboard_model_v2->monthly_sales_statistics_between_dates($from_date, $to_date);
        $this->load->view('dashboard_data_view', $data = array('data' => $data));
    }
    public function bookings()
    {
        $this->output->cache(5);
        $data['bookings'] = $this->dashboard_model_v2->booking_counts();
        $this->load->view('dashboard_data_view', $data = array('data' => $data));
    }
    public function cancel_bookings()
    {
        //$this->output->cache(5);
        $data['cancel_bookings'] = $this->dashboard_model_v2->cancel_bookings();
        $this->load->view('dashboard_data_view', $data = array('data' => $data));
    }
    public function customers()
    {
        //$this->output->cache(2);
        $data['customers'] = $this->dashboard_model_v2->customer_counts();
        $this->load->view('dashboard_data_view', $data = array('data' => $data));
    }
    public function complaints()
    {
        //$this->output->cache(2);
        $data['complaints'] = $this->dashboard_model_v2->complaint_counts();
        $this->load->view('dashboard_data_view', $data = array('data' => $data));
    }
    public function payments()
    {
        $this->output->cache(1);
        $data['payments'] = $this->dashboard_model_v2->payment_collected_and_pending();
        $this->load->view('dashboard_data_view', $data = array('data' => $data));
    }
    public function recent_activities()
    {
        //$this->output->cache(1);
        $data['recent_activities'] = $this->dashboard_model_v2->recent_activities($this->input->get('last_activity'), $this->input->get('first_activity'));
        $this->load->view('dashboard_data_view', $data = array('data' => $data));
    }
    public function booking_chart_data()
    {
        $this->output->cache(1);
        $data['booking_chart_data'] = $this->dashboard_model_v2->chart_counts();
        //
        $data['graph_data'][] = [[(int) $data['booking_chart_data']['prior_od_count'], (int) $data['booking_chart_data']['prior_we_count'], (int) $data['booking_chart_data']['prior_bw_count']], date("l", strtotime("-2 week")) . ' <br><small>(' . date("d-m-y", strtotime("-2 week")) . ')</small>'];
        $data['graph_data'][] = [[(int) $data['booking_chart_data']['prev_od_count'], (int) $data['booking_chart_data']['prev_we_count'], (int) $data['booking_chart_data']['prev_bw_count']], date("l", strtotime("-1 week")) . ' <br><small>(' . date("d-m-y", strtotime("-1 week")) . ')</small>'];
        $data['graph_data'][] = [[(int) $data['booking_chart_data']['today_od_count'], (int) $data['booking_chart_data']['today_we_count'], (int) $data['booking_chart_data']['today_bw_count']], 'Today <br><small>(' . date("d-m-y") . ')</small>'];
        $this->load->view('dashboard_data_view', $data = array('data' => $data));
    }
    public function today_counts()
    {
        //$this->output->cache(5);
        $data['today'] = $this->dashboard_model_v2->today_counts();
        $this->load->view('dashboard_data_view', $data = array('data' => $data));
    }
    public function monthly_counts()
    {
        //$this->output->cache(5);
        $data['monthly'] = $this->dashboard_model_v2->monthly_counts_php();
        $this->load->view('dashboard_data_view', $data = array('data' => $data));
    }
    public function monthly_counts_sql()
    {
        //$this->output->cache(5);
        $data['monthly'] = $this->dashboard_model_v2->monthly_counts_sql();
        $this->load->view('dashboard_data_view', $data = array('data' => $data));
    }
    public function driver_locations()
    {
        //$this->output->cache(5);
        $data['data'] = $this->driver_model->get_tablets_location_by_date(date('Y-m-d'));
        $this->load->view('dashboard_data_view', $data = array('data' => $data));
    }
}
