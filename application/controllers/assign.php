<?php
/********************************** */
// Author : Samnad. S
// Usage : Foe getting data by ajax
// Created on : 21-09-2023
// Last edited by :
// Last edited on :
//
/********************************** */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Assign extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!is_user_loggedin()) {
            $result['message'] = 'Authentication Error !';
            $result['success'] = false;
            echo json_encode($result, true);
        }
        $this->load->model('customers_model');
        $this->load->model('maids_model');
        $this->load->model('package_model');
        $this->load->model('package_subs_model');
        $this->load->model('service_types_model');
        $this->load->model('bookings_model');
        $this->load->model('tablets_model');
        $this->load->model('zones_model');
        $this->load->model('settings_model');
        $this->load->library('upload');
        $this->load->helper('google_api_helper');
        $this->load->helper('curl_helper');
    }
    public function index()
    {
        $booking_id = $this->input->get('booking_id');
        $data = array();
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $data['booking'] = $this->db->select('b.*')->from('bookings as b')->where('b.booking_id', $booking_id)->where('b.booking_status', 0)->get()->row();
        /******************************************************* */
        $starttime = '00:00'; // your start time
        $endtime = '23:59'; // End time
        $duration = '30'; // split by 30 mins
        $data['time_slots'] = array();
        $start_time = strtotime($starttime); //change to strtotime
        $end_time = strtotime($endtime); //change to strtotime
        $add_mins = $duration * 60;
        while ($start_time <= $end_time) // loop between time
        {
            $data['time_slots'][date("H:i:s", $start_time)] = date("h:i A", $start_time);
            $start_time += $add_mins; // to check endtie=me
        }
        /******************************************************* */
        $layout_data['content_body'] = $this->load->view('assign_maid', $data, true);
        $layout_data['page_title'] = 'Assign Maid - ' . $data['booking']->reference_id;
        $layout_data['meta_description'] = '';
        $layout_data['css_files'] = array('datepicker.css','toastr.min.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('bootstrap-datepicker.js','maid-assign.js');
        $this->load->view('layouts/default', $layout_data);
    }
    public function test()
    {
        /******************************************************* */
        $starttime = '00:00'; // your start time
        $endtime = '23:59'; // End time
        $duration = '30'; // split by 30 mins
        $data['time_slots'] = array();
        $start_time = strtotime($starttime); //change to strtotime
        $end_time = strtotime($endtime); //change to strtotime
        $add_mins = $duration * 60;
        while ($start_time <= $end_time) // loop between time
        {
            $data['time_slots'][date("H:i:s", $start_time)] = date("h:i A", $start_time);
            $start_time += $add_mins; // to check endtie=me
        }
        /******************************************************* */
        echo '<pre>';
        print_r($data['time_slots']);
        echo '</pre>';

    }
}
