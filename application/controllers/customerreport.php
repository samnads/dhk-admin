<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Customerreport extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!is_user_loggedin()) {
            redirect('logout');
        }
        if (!user_permission(user_authenticate(), 6)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        $this->load->model('customerreport_model');
        $this->load->model('settings_model');
        $this->load->model('reports_model');
    }

    public function index()
    {
		
	}
	
	public function customer_report()
    {
        $data = array();
        $data['active'] = 2;
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        // $data['customer_sources'] = $this->db->select('cs.*')->from('customer_sources as cs')->where('cs.deleted_at',null)->order_by('cs.sort_order')->get()->result_array();
        $layout_data['content_body'] = $this->load->view('reports/customer_report', $data, TRUE);
        $layout_data['page_title'] = 'Customer Report';
        $layout_data['meta_description'] = 'customers';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css');
        $layout_data['external_js_files'] = array();
        $layout_data['customer_active'] = '1';
        $layout_data['js_files'] = array('bootstrap-datepicker.js', 'jquery.dataTables.min.js', 'customerreport.js');
        $this->load->view('layouts/default', $layout_data);
    }
	
	public function list_ajax_customer_report()
    {
        $orders = array();
        $draw = $_POST['draw'];
        $start = $_POST['start'];
        $rowperpage = $_POST['length'];
        $columnIndex = $_POST['order'][0]['column'];
        $columnName = $_POST['columns'][$columnIndex]['data'];
        $columnSortOrder = $_POST['order'][0]['dir'];
        $searchValue = $_POST['search']['value'];

        // Custom search filter 
        $useractive = $_POST['useractive'];
        $regdate = $_POST['regdate'];
        $regdateto = $_POST['regdateto'];

        $recordsTotal = $this->customerreport_model->count_all_customers();
        $recordsTotalFilter = $this->customerreport_model->get_all_customersnew($useractive, $regdate, $regdateto);

        $orders = $this->customerreport_model->get_all_newcustomers($useractive, $columnName, $columnSortOrder, $rowperpage, $start, $draw, $recordsTotal, $recordsTotalFilter, $regdate, $regdateto);
        
        echo json_encode($orders);
        exit();
    }
	
	public function staff_report_old()
	{
		// error_reporting(E_ALL);
		// ini_set('display_errors', 1);
		$data = array();
        $schedule_report = array();
        if ($this->input->post('search_date') && strlen($this->input->post('search_date')) > 0) {
            $payment_date = trim($this->input->post('search_date'));
        } else {
            $payment_date = date('d/m/Y');
        }
        if ($this->input->post('search_date_to') && strlen($this->input->post('search_date_to')) > 0) {
            $payment_date_to = trim($this->input->post('search_date_to'));
        } else {
            $payment_date_to = date('d/m/Y');
        }
        $date = date('Y-m-d', strtotime(str_replace("/", "-", $payment_date)));
        $date_to = date('Y-m-d', strtotime(str_replace("/", "-", $payment_date_to)));
        if ($this->input->post('zones')) {
            $zone_id = $this->input->post('zones');
        } else {
            $zone_id = "";
        }
        $data['payment_date'] = $payment_date;
        $data['payment_date_to'] = $payment_date_to;
        $data['servicedate'] = $date;
        $data['servicedateto'] = $date_to;
        $data['filter_service_type_id'] =  $this->input->post('filter_service_type_id') ?: null;
        $data['filter_maid_id'] =  $this->input->post('filter_maid_id') ?: null;
        while (strtotime($date) <= strtotime($date_to)) {
            $per_report = $this->reports_model->schedule_report_service_wise($date, $zone_id,$data['filter_service_type_id'],$data['filter_maid_id']);
            array_push($schedule_report, $per_report);
            $date = date('Y-m-d', strtotime("+1 day", strtotime($date)));
        }
        $rows = [];
        foreach($schedule_report as $key1 => $date_report){
            foreach($date_report as $key2 => $service){
                //echo '<pre>'; print_r($service->total_amount);echo '</pre>';die();
                if($rows[$service->service_type_id]){
                    $rows[$service->service_type_id]['total_amount'] = $rows[$service->service_type_id]['total_amount'] + $service->total_amount;
                }
                else{
                    $rows[$service->service_type_id] = array('service_type_id' => $service->service_type_id,'service_type_name' => $service->service_type_name,'total_amount' => $service->total_amount);
                }
            }
        }
        $data['rows'] = $rows;
        $data['maids'] = $this->reports_model->get_all_maids();
        //echo '<pre>'; print_r($data['maids']);echo '</pre>';die();
        //$reports = $this->reports_model->get_schedule_report_lat($date);
        $data['reports'] = $schedule_report;
        $data['zones'] = $this->settings_model->get_zones();
        $data['zone_id'] = $zone_id;
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $data['service_types'] = $this->settings_model->get_services();
        $layout_data['content_body'] = $this->load->view('reports/schedule_report_service_wise', $data, TRUE);
        $layout_data['page_title'] = 'Schedule Report Service Wise';
        $layout_data['meta_description'] = 'Schedule Report Service Wise';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('bootstrap-datepicker.js', 'jquery.dataTables.min.js','main.js');
        $this->load->view('layouts/default', $layout_data);
	}
	
	public function service_report_new()
	{
		// error_reporting(E_ALL);
		// ini_set('display_errors', 1);
		$data = array();
        $schedule_report = array();
        if ($this->input->post('search_date') && strlen($this->input->post('search_date')) > 0) {
            $payment_date = trim($this->input->post('search_date'));
        } else {
            $payment_date = date('d/m/Y');
        }
        if ($this->input->post('search_date_to') && strlen($this->input->post('search_date_to')) > 0) {
            $payment_date_to = trim($this->input->post('search_date_to'));
        } else {
            $payment_date_to = date('d/m/Y');
        }
        $date = date('Y-m-d', strtotime(str_replace("/", "-", $payment_date)));
        $date_to = date('Y-m-d', strtotime(str_replace("/", "-", $payment_date_to)));
        if ($this->input->post('zones')) {
            $zone_id = $this->input->post('zones');
        } else {
            $zone_id = "";
        }
        $data['payment_date'] = $payment_date;
        $data['payment_date_to'] = $payment_date_to;
        $data['servicedate'] = $date;
        $data['servicedateto'] = $date_to;
        $data['filter_service_type_id'] =  $this->input->post('filter_service_type_id') ?: null;
        $data['filter_maid_id'] =  $this->input->post('filter_maid_id') ?: null;
        while (strtotime($date) <= strtotime($date_to)) {
            $per_report = $this->reports_model->schedule_report_service_wise($date, $zone_id,$data['filter_service_type_id'],$data['filter_maid_id']);
            array_push($schedule_report, $per_report);
            $date = date('Y-m-d', strtotime("+1 day", strtotime($date)));
        }
        $rows = [];
        foreach($schedule_report as $key1 => $date_report){
            foreach($date_report as $key2 => $service){
                //echo '<pre>'; print_r($service->total_amount);echo '</pre>';die();
                if($rows[$service->service_type_id]){
                    $rows[$service->service_type_id]['total_amount'] = $rows[$service->service_type_id]['total_amount'] + $service->total_amount;
                }
                else{
                    $rows[$service->service_type_id] = array('service_type_id' => $service->service_type_id,'service_type_name' => $service->service_type_name,'total_amount' => $service->total_amount);
                }
            }
        }
        $data['rows'] = $rows;
        $data['maids'] = $this->reports_model->get_all_maids();
        //echo '<pre>'; print_r($data['maids']);echo '</pre>';die();
        //$reports = $this->reports_model->get_schedule_report_lat($date);
        $data['reports'] = $schedule_report;
        $data['zones'] = $this->settings_model->get_zones();
        $data['zone_id'] = $zone_id;
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $data['service_types'] = $this->settings_model->get_services();
        $layout_data['content_body'] = $this->load->view('reports/schedule_report_service_wise', $data, TRUE);
        $layout_data['page_title'] = 'Schedule Report Service Wise';
        $layout_data['meta_description'] = 'Schedule Report Service Wise';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css', 'booking.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('bootstrap-datepicker.js', 'jquery.dataTables.min.js','main.js');
        $this->load->view('layouts/default', $layout_data);
	}
	
	public function service_report()
	{
		$data = array();
        $data['active'] = 2;
		$data['currentdate'] = date('d/m/Y');
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $data['servicelist'] = $this->customerreport_model->get_service_list();
        $layout_data['content_body'] = $this->load->view('reports/service_report', $data, TRUE);
        $layout_data['page_title'] = 'Service Report';
        $layout_data['meta_description'] = 'Service Report';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('bootstrap-datepicker.js', 'jquery.dataTables.min.js', 'service-report.js');
        $this->load->view('layouts/default', $layout_data);
	}
	
	public function staff_report()
	{
		$data = array();
        $data['active'] = 2;
		$data['currentdate'] = date('d/m/Y');
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $data['maidlist'] = $this->customerreport_model->get_all_maids();
        $layout_data['content_body'] = $this->load->view('reports/staff_report', $data, TRUE);
        $layout_data['page_title'] = 'Staff Report';
        $layout_data['meta_description'] = 'Staff Report';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('bootstrap-datepicker.js', 'jquery.dataTables.min.js', 'staff-report.js');
        $this->load->view('layouts/default', $layout_data);
	}
	
}