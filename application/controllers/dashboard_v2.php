<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
class Dashboard_v2 extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        if (!is_user_loggedin()) {
            redirect('logout');
        }

        $this->load->model('settings_model');
    }
    public function index()
    {
        if (!user_permission(user_authenticate(), 1)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'reports">Go to Reports</a>', 401, 'Access denied');
        }
        $data = array();
        $data['settings'] = $this->settings_model->get_settings();
        $filter_from_date = new DateTime(date('Y-m-d'));
        $filter_from_date->modify('-5 months');
        //$filter_from_date = $filter_from_date->format('01/m/Y');
        $data['filter_from_date'] = $filter_from_date->format('d/m/Y');
        $filter_to_date = new DateTime(date('Y-m-d'));
        $filter_to_date = $filter_to_date->format('d/m/Y');
        //$data['filter_from_date'] = $filter_to_date;
        $data['filter_to_date'] = $filter_to_date;
        $layout_data['content_body'] = $this->load->view('dashboard_v2', $data, true);
        $layout_data['page_title'] = 'Dashboard v2';
        $layout_data['meta_description'] = 'Dashboard v2';
        $layout_data['css_files'] = array('pignose.calendar.min.css');
        $layout_data['js_files'] = array('jqBarGraph.1.1.js','pignose.calendar.full.min.js','dashboard_v2.js?v='.time());
        $layout_data['external_js_files'] = array('https://cdn.jsdelivr.net/npm/chart.js');
        $this->load->view('layouts/default_dashboard', $layout_data);
    }
}

/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */
