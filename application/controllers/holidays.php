<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Holidays extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!is_user_loggedin()) {
            redirect('logout');
        }
        if (!user_permission(user_authenticate(), 3)) {
            show_error('You may not have the appropriate permissions to access the file. <br /><a href="' . base_url() . 'dashboard">Go to Dashboard</a>', 401, 'Access denied');
        }
        $this->load->model('customers_model');
        $this->load->model('maids_model');
        $this->load->model('package_model');
        $this->load->model('package_subs_model');
        $this->load->model('service_types_model');
        $this->load->model('bookings_model');
        $this->load->model('tablets_model');
        $this->load->model('zones_model');
        $this->load->model('settings_model');
        $this->load->library('upload');
        $this->load->helper('google_api_helper');
        $this->load->helper('curl_helper');
        $this->load->model('holidays_model');
    }

    public function index()
    {
        if ($this->input->post('save_holiday')) {
            $data['created_at'] = date('Y-m-d H:m:s');
            $data['updated_at'] = date('Y-m-d H:m:s');
            $data['created_by_user'] = $this->session->userdata['user_logged_in']['user_id'];
            $date = DateTime::createFromFormat('d/m/Y', $this->input->post('date'));
            $date = $date->format('Y-m-d');
            $data['date'] = $date;
            $data['holiday_name'] = $this->input->post('holiday_name');
            $this->db->insert('holidays', $data);
            redirect('/holidays', 'refresh');
        }
        $data = array();
        $status = $this->input->get('status');
        $data['packages'] = $this->package_model->get_all_packages($status);
        $data['holiday_dates'] = $this->holidays_model->get_all_holiday_dates_from(date('Y-m-d'));
        $data['weekends'] = $this->holidays_model->get_weekends(date('Y-m-d'));
        //print_r($data['weekends']);die();
        $data['filter_from_date'] = $this->input->post('filter_from_date') ?: date('01/m/Y');
        $data['filter_to_date'] = $this->input->post('filter_to_date') ?: date('t/m/Y');
        $data['holidays_list'] = $this->holidays_model->get_all_holidays_between_dates(DateTime::createFromFormat('d/m/Y', $data['filter_from_date'])->format('Y-m-d'), DateTime::createFromFormat('d/m/Y', $data['filter_to_date'])->format('Y-m-d'));
        $data['status'] = $status;
        $data['settings'] = $layout_data['settings'] = $this->settings_model->get_settings();
        $layout_data['content_body'] = $this->load->view('holidays/holidays_list', $data, true);
        $layout_data['page_title'] = 'Holidays';
        $layout_data['meta_description'] = 'Holidays';
        $layout_data['css_files'] = array('demo.css', 'datepicker.css');
        $layout_data['external_js_files'] = array();
        $layout_data['js_files'] = array('jquery.dataTables.min.js', 'bootstrap-datepicker.js');
        $this->load->view('layouts/default', $layout_data);
    }
	public function delete()
    {
		$this->db->where('id', $this->input->post('holiday_id'));
        $this->db->update('holidays', array('deleted_at' => date('Y-m-d H:m:s')));
		redirect('/holidays', 'refresh');
	}
}
