<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Expense extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!is_user_loggedin()) {
            redirect('logout');
        }
        
		$this->load->model('expense_model');
		$this->load->model('invoice_model');
    }

    public function index()
    {
		
	}
	
	public function customer_expenses()
	{
		$data = array();
        $data['settings'] = $layout_data['settings'] = get_quick_book_values();
		$data['customerlist'] = $this->invoice_model->get_customer_list();
        $layout_data['content_body'] = $this->load->view('expense/customer_expense_list', $data, TRUE);
        $layout_data['page_title'] = 'Customer Expenses';
        $layout_data['meta_description'] = 'Customer Expenses';
        $layout_data['css_files'] = array('datepicker.css', 'demo.css','toastr.min.css');
        $layout_data['external_js_files'] = array();
        $layout_data['customer_active'] = '1';
        $layout_data['js_files'] = array('bootstrap-datepicker.js', 'jquery.dataTables.min.js', 'customer_expenses.js');
        $this->load->view('layouts/default', $layout_data);
	}
	
	public function list_ajax_expense_list()
    {
        $orders = array();
        $draw = $_POST['draw'];
        $start = $_POST['start'];
        $rowperpage = $_POST['length'];
        $columnIndex = $_POST['order'][0]['column'];
        $columnName = $_POST['columns'][$columnIndex]['data'];
        $columnSortOrder = $_POST['order'][0]['dir'];
        $searchValue = $_POST['search']['value'];

        // Custom search filter
        $regdate = $_POST['regdate'];
        $regdateto = $_POST['regdateto'];
        $custid = $_POST['custid'];
        $qbsync = $_POST['qbsync'];
        // $keywordsearch = $_POST['keywordsearch'];

        $recordsTotal = count($this->expense_model->count_all_expenses());
        $recordsTotalFilter = count($this->expense_model->get_all_expensesnew($custid,$regdate,$regdateto,$qbsync));

        $orders = $this->expense_model->get_all_newexpenses($columnName, $columnSortOrder, $rowperpage, $start, $draw, $recordsTotal, $recordsTotalFilter, $custid,$regdate,$regdateto,$qbsync);
        //$orders_sum  = $this->login_model->get_all_neworders($searchQuery, $columnName, $columnSortOrder, $rowperpage, $start, $draw, $recordsTotal, $recordsTotalFilter);

        echo json_encode($orders);
        exit();
    }
}