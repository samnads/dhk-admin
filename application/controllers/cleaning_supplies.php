<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Cleaning_supplies extends CI_Controller {
    
  public function __construct()
  {
    parent::__construct();
    $this->load->model('booking_cleaning_supplies_model',"b_cleaning_supplies_m");
  }

  /**===================================================================
   * Function to get cleaning supplies selected when booking was added.
   =====================================================================*/
   public function get_cleaning_supplies_by_booking($booking_id) {
    $cleaning_supplies = $this->b_cleaning_supplies_m->get_cleaning_supplies_by_booking($booking_id);
    // Set the response content type to JSON
    header('Content-Type: application/json');
    // Output the JSON data
    echo json_encode($cleaning_supplies);
    exit();
  }
}