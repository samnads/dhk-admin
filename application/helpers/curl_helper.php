<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');



function curl_api_service($curl_post,$url)
{
	$curl_header = array(
		'Content-Type: application/json',
		'Content-Length: ' . strlen($curl_post)
	);
	$ch2 = curl_init();
	curl_setopt($ch2, CURLOPT_URL, $url);
	curl_setopt($ch2, CURLOPT_FOLLOWLOCATION, TRUE);
	curl_setopt($ch2, CURLOPT_POST, 1);
	curl_setopt($ch2, CURLOPT_POSTFIELDS, $curl_post);
	curl_setopt($ch2, CURLOPT_HTTPHEADER, $curl_header);
	curl_setopt($ch2, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch2, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, 0);
	$user_datas = curl_exec($ch2);
	
	if (curl_errno($ch2)) {
		echo $error_msg = curl_error($ch2);
		exit();
	}
	
	return $user_datas;
}

function curl_gallabox_service($url,$curl_post) {
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $url,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => $curl_post,
		CURLOPT_HTTPHEADER => array(
			'apiKey: 6697907e4407420eac1f6d77',
			'apiSecret: af4e6992cd6d44528202e66573994a99',
			'Content-Type: application/json'
		),
	));

	$response = curl_exec($curl);

	curl_close($curl);
	return $response;
}
