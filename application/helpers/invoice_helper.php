<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
function create_invoice_on_service_stop($service_id, $amount)
{
    $CI = get_instance();
    $CI->load->model('settings_model');
    $CI->load->model('day_services_model');
    $CI->load->model('invoice_model');
    $settings = $CI->settings_model->get_settings();
    $service_details = $CI->day_services_model->getservicedetails($service_id);
    $dayservice_id_array = array();
    $invoice_array = array();
    if (!empty($service_details)) {
        $ds_id = $service_details->day_service_id;
        if (!in_array($ds_id, $dayservice_id_array)) {
            $total_fee = $amount;
            $serviceamount = $service_details->serviceamount;
            $vat_fee = $service_details->vatamount;
            if (!is_numeric($vat_fee)) {
                $vat_fee = 0.05 * $serviceamount;
                $vat_fee = sprintf("%0.2f", $vat_fee);
            }
            $total_time = ((strtotime($service_details->end_time) - strtotime($service_details->start_time)) / 3600);
            $unit_price = $service_details->price_hourly;

            $invoice_array['customer_id'] = $service_details->customer_id;
            $invoice_array['customer_name'] = $service_details->customer_name;
            $invoice_array['bill_address'] = $service_details->customer_address;
            $invoice_array['service_date'] = $service_details->service_date;
            $invoice_array['billed_amount'] = $amount;

            $linearray = array();
            $linearray[0]['day_service_id'] = $service_details->day_service_id;
            $linearray[0]['booking_id'] = $service_details->booking_id;
            $linearray[0]['day_service_reference'] = $service_details->day_service_reference;
            $linearray[0]['maid_id'] = $service_details->maid_id;
            $linearray[0]['maid_name'] = $service_details->maid_name;
            $linearray[0]['service_from_time'] = $service_details->start_time;
            $linearray[0]['service_to_time'] = $service_details->end_time;
            $linearray[0]['line_bill_address'] = $service_details->customer_address;
            $linearray[0]['description'] = "Service from " . $service_details->time_from . " to " . $service_details->time_to . " For " . $service_details->customer_address;
            $linearray[0]['service_hrs'] = $total_time;
            $linearray[0]['inv_unit_price'] = (float) $unit_price;
            $linearray[0]['line_discount'] = 0;
            $linearray[0]['line_amount'] = (float) $serviceamount;
            $linearray[0]['line_vat_amount'] = (float) $vat_fee;
            $linearray[0]['line_net_amount'] = (float) $total_fee;

            array_push($dayservice_id_array, $ds_id);

            $related_bookings = $CI->day_services_model->get_related_bookings_for_invoice($service_details->customer_id, $service_details->service_date, $ds_id);

            $i = 1;
            foreach ($related_bookings as $relbuks) {
                $ds_id = $relbuks->day_service_id;
                $total_time = ((strtotime($relbuks->end_time) - strtotime($relbuks->start_time)) / 3600);
                $total_fee_l = $relbuks->total_fee;
                $serviceamount_l = $relbuks->serviceamount;
                $vat_fee_l = $relbuks->vatamount;
                if (!is_numeric($vat_fee_l)) {
                    $vat_fee_l = 0.05 * $serviceamount_l;
                    $vat_fee_l = sprintf("%0.2f", $vat_fee_l);
                }
                $unit_price = $service_details->price_hourly;

                $linearray[$i]['day_service_id'] = $ds_id;
                $linearray[$i]['booking_id'] = $relbuks->booking_id;
                $linearray[$i]['day_service_reference'] = $relbuks->day_service_reference;
                $linearray[$i]['maid_id'] = $relbuks->maid_id;
                $linearray[$i]['maid_name'] = $relbuks->maid_name;
                $linearray[$i]['service_from_time'] = $relbuks->start_time;
                $linearray[$i]['service_to_time'] = $relbuks->end_time;
                $linearray[$i]['line_bill_address'] = $relbuks->customer_address;
                $linearray[$i]['description'] = "Service from " . $relbuks->time_from . " to " . $relbuks->time_to . " For " . $relbuks->customer_address;
                $linearray[$i]['service_hrs'] = $total_time;
                $linearray[$i]['inv_unit_price'] = (float) $unit_price;
                $linearray[$i]['line_discount'] = 0;
                $linearray[$i]['line_amount'] = (float) $serviceamount_l;
                $linearray[$i]['line_vat_amount'] = (float) $vat_fee_l;
                $linearray[$i]['line_net_amount'] = (float) $total_fee_l;

                array_push($dayservice_id_array, $ds_id);
                $i++;
            }

            $invoice_array['line_item'] = $linearray;
            $added_date_time = date('Y-m-d H:i:s');

            // echo '<pre>';
            // print_r($invoice_array);
            // exit();

            $main_inv_array = array();
            $main_inv_array['customer_id'] = $service_details->customer_id;
            $main_inv_array['customer_name'] = $service_details->customer_name;
            $main_inv_array['bill_address'] = $service_details->customer_address;
            $main_inv_array['added'] = $added_date_time;
            $main_inv_array['service_date'] = $service_details->service_date;
            $main_inv_array['invoice_date'] = $service_details->service_date;
            $main_inv_array['invoice_due_date'] = $service_details->service_date;
            //$main_inv_array['billed_amount'] = $inv_val['billed_amount'];
            $main_inv_array['invoice_added_by'] = null;

            $invoice_id = $CI->day_services_model->add_main_invoice($main_inv_array);




            $invoice_amt = $amount;
            $cust_id = $service_details->customer_id;
            $get_customer_detail = $CI->invoice_model->get_customer_detail($cust_id);
            if (!empty($get_customer_detail)) {
                $inv_amt = $get_customer_detail->total_invoice_amount;
                $bal_amount = $get_customer_detail->balance;
                $total_invoice = ($inv_amt + $invoice_amt);
                $total_balance = ($bal_amount + $invoice_amt);
                $update_invoice = $CI->invoice_model->update_invoice_detail($invoice_id, $cust_id, $total_invoice, $total_balance);
            }





            if ($invoice_id > 0) {
                $tot_bill_amount = 0;
                $total_vat_amt = 0;
                $total_net_amt = 0;
                $dayids = array();
                foreach ($linearray as $line_val) {
                    //$tot_bill_amount += $line_val['line_amount'];
                    //$total_vat_amt += $line_val['line_vat_amount'];
                    //$total_net_amt += $line_val['line_net_amount'];
                    $main_inv_line = array();
                    $main_inv_line['invoice_id'] = $invoice_id;
                    $main_inv_line['day_service_id'] = $line_val['day_service_id'];
                    $main_inv_line['day_service_reference'] = $line_val['day_service_reference'];
                    $main_inv_line['maid_id'] = $line_val['maid_id'];
                    $main_inv_line['maid_name'] = $line_val['maid_name'];
                    $main_inv_line['service_from_time'] = $line_val['service_from_time'];
                    $main_inv_line['service_to_time'] = $line_val['service_to_time'];
                    $main_inv_line['line_bill_address'] = $line_val['line_bill_address'];
                    $main_inv_line['description'] = $line_val['description'];
                    $main_inv_line['service_hrs'] = $line_val['service_hrs'];
                    $main_inv_line['inv_unit_price'] = $line_val['inv_unit_price'];
                    $main_inv_line['line_discount'] = $line_val['line_discount'];
                    //$main_inv_line['line_amount'] = $line_val['line_amount'];
                    //$main_inv_line['line_vat_amount'] = $line_val['line_vat_amount'];
                    $main_inv_line['line_vat_amount'] = ($line_val['line_net_amount'] * $settings->service_vat_percentage) / (100 + $settings->service_vat_percentage);
                    $total_vat_amt += $main_inv_line['line_vat_amount'];
                    $main_inv_line['line_amount'] = $line_val['line_net_amount'] - $main_inv_line['line_vat_amount'];
                    $tot_bill_amount += $main_inv_line['line_amount'];
                    $main_inv_line['line_net_amount'] = $line_val['line_net_amount'];
                    $total_net_amt += $main_inv_line['line_net_amount'];
                    $invoice_line_id = $CI->day_services_model->add_line_invoice($main_inv_line);
                    if ($invoice_line_id > 0) {
                        $ds_fields = array();
                        $ds_fields['serv_invoice_id'] = $invoice_id;
                        $ds_fields['invoice_status'] = 1;

                        $CI->day_services_model->update_day_service($line_val['day_service_id'], $ds_fields);
                        array_push($dayids, $line_val['booking_id']);
                    }
                }
                $update_main_inv = array();
                $update_main_inv['invoice_num'] = "INV-" . date('Y') . "-" . sprintf('%04s', $invoice_id);
                //$update_main_inv['billed_amount'] = $tot_bill_amount;
                $update_main_inv['invoice_total_amount'] = $tot_bill_amount;
                $update_main_inv['invoice_tax_amount'] = $total_vat_amt;
                $update_main_inv['invoice_net_amount'] = $total_net_amt;
                $update_main_inv['balance_amount'] = $total_net_amt;

                $updateinv = $CI->day_services_model->update_service_invoice($invoice_id, $update_main_inv);
                if ($updateinv) {
                    /*$return = array();
                    $return['status'] = 'success';
                    $return['dayids'] = $dayids;

                    echo json_encode($return);
                    exit();*/
                } else {
                    /*$return = array();
                    $return['status'] = 'error';
                    $return['dayids'] = $dayids;

                    echo json_encode($return);
                    exit();*/
                }
            } else {
                /*$return = array();
                $return['status'] = 'error';
                $return['dayids'] = $dayids;

                echo json_encode($return);
                exit();*/
            }
        }
    } else {
        /*$return = array();
        $return['status'] = 'error';
        $return['dayids'] = $dayids;

        echo json_encode($return);
        exit();*/
    }
}

function create_invoice_by_day_service_ids($day_service_ids = null){
    $CI = get_instance();
    $CI->load->model('settings_model');
    $CI->load->model('day_services_model');
    $CI->load->model('invoice_model');
    $CI->load->model('crew_appapi_lat_model');
    if (is_array($day_service_ids)) {
        /******************************************************* */
        // create invoice
        $CI->db->select('ds.*')
            ->from('day_services as ds')
            ->where('ds.day_service_id', $day_service_ids[0])
            ->limit(1);
        $day_service = $CI->db->get();
        $day_service = $day_service->row();
        $main_inv_array = array();
        $main_inv_array['customer_id'] = $day_service->customer_id;
        $main_inv_array['day_service_id'] = null;
        $main_inv_array['customer_name'] = $day_service->customer_name;
        $main_inv_array['bill_address'] = $day_service->customer_address;
        $main_inv_array['added'] = date('Y-m-d H:i:s');
        $main_inv_array['service_date'] = $day_service->service_date;
        $main_inv_array['invoice_date'] = $day_service->service_date;
        $main_inv_array['invoice_due_date'] = $day_service->service_date;
        $main_inv_array['billed_amount'] = null;
        $main_inv_array['invoice_added_by'] = null;
        $invoice_id = $CI->day_services_model->add_main_invoice($main_inv_array);
        /******************************************************* */
        $update_main_inv = array();
        $update_main_inv['invoice_num'] = "INV-" . date('Y') . "-" . sprintf('%04s', $invoice_id);
        $update_main_inv['invoice_total_amount'] = 0;
        $update_main_inv['invoice_tax_amount'] = 0;
        $update_main_inv['invoice_net_amount'] = 0;
        $update_main_inv['balance_amount'] = 0;
        foreach ($day_service_ids as $day_service_id) {
            $CI->db->select('ds.*,m.maid_name')
                ->from('day_services as ds')
                ->join('maids as m', 'ds.maid_id = m.maid_id')
                ->where('ds.day_service_id', $day_service_id)
                ->limit(1);
            $day_service = $CI->db->get();
            $day_service = $day_service->row();
            if ($day_service->service_status != 3 && $day_service->invoice_status != 1) {
                $booking = $CI->crew_appapi_lat_model->get_booking_by_id($day_service->booking_id);
                $service_details = $CI->day_services_model->getservicedetails($day_service_id);
                /******************************************************* */
                // add line items
                $total_time = ((strtotime($service_details->end_time) - strtotime($service_details->start_time)) / 3600);
                $vat_fee = $service_details->vatamount;
                $serviceamount = $booking->total_amount;
                $vat_fee = 0.05 * $serviceamount;
                $vat_fee = sprintf("%0.2f", $vat_fee);
                $total_fee = $booking->total_amount;
                $main_inv_line = array();
                $main_inv_line['invoice_id'] = $invoice_id;
                $main_inv_line['day_service_id'] = $day_service->day_service_id;
                $main_inv_line['day_service_reference'] = null;
                $main_inv_line['maid_id'] = $day_service->maid_id;
                $main_inv_line['maid_name'] = $day_service->maid_name;
                $main_inv_line['service_from_time'] = $booking->time_from;
                $main_inv_line['service_to_time'] = $booking->time_to;
                $main_inv_line['line_bill_address'] = $day_service->customer_address;
                $main_inv_line['description'] = "Service from " . $service_details->time_from . " to " . $service_details->time_to . " For " . $day_service->customer_address;
                $main_inv_line['service_hrs'] = $total_time;
                $main_inv_line['inv_unit_price'] = (float) $service_details->price_hourly;
                $main_inv_line['line_amount'] = (float) $booking->total_amount - (float) $vat_fee;
                $main_inv_line['line_discount'] = 0;
                $main_inv_line['line_vat_amount'] = (float) $vat_fee;
                $main_inv_line['line_net_amount'] = (float) $booking->total_amount;
                $invoice_line_id = $CI->day_services_model->add_line_invoice($main_inv_line);
                /******************************************************* */
                $update_main_inv['invoice_total_amount'] += $main_inv_line['line_amount'];
                $update_main_inv['invoice_tax_amount'] += $main_inv_line['line_vat_amount'];
                $update_main_inv['invoice_net_amount'] += $main_inv_line['line_net_amount'];
                $update_main_inv['balance_amount'] += $main_inv_line['line_net_amount'];
                // update invoice sattus
                $ds_fields = array();
                $ds_fields['serv_invoice_id'] = $invoice_id;
                $ds_fields['invoice_status'] = 1;
                $CI->crew_appapi_lat_model->update_day_service($day_service_id,$ds_fields);
            }
        }
        // update invoice
        $CI->day_services_model->update_service_invoice($invoice_id, $update_main_inv);
        return $invoice_id;
    }
}

function create_payment_url_on_service_stop($curl_post)
{
    $url = base_url()."laravel/api/gallabox/customer-balance-amount";
    $curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $url,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => $curl_post,
		CURLOPT_HTTPHEADER => array(
			'Content-Type: application/json'
		),
	));
	$response = curl_exec($curl);
    if (curl_errno($curl)) {
        $error_msg = curl_error($curl);
        echo $error_msg;
        exit();
    }
	curl_close($curl);
	return $response;
}