<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

function android_push($reg_ids = array(), $post_fields = array())
{
    $CI = &get_instance();
    $url = 'https://android.googleapis.com/gcm/send';
    $post_fields['price'] = $post_fields['message'];
    unset($post_fields['message']);
    $fields = array(
        'registration_ids' => $reg_ids,
        'data' => $post_fields,
    );
    $key = $CI->config->item('google_api_key');
    $key = null;
    $headers = array(
        'Authorization: key=' . $key,
        'Content-Type: application/json',
    );
    // Open connection
    $ch = curl_init();
    // Set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // Disabling SSL Certificate support temporarly
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    // Execute post
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}

function android_customer_push($fields)
{
    $fileput = json_encode($fields) . ' time ' . date('H-m-i A') . ' function: ' . 'push' . "\r\n";
    $date = date('d-m-Y');
    $fn = "log/" . $date . ".txt";
    file_put_contents($fn, $fileput, FILE_APPEND | LOCK_EX);
    $CI = &get_instance();
    // $key = "AAAAnBAAAtI:APA91bHWvia2YpYY_iqbt3PB5IvTZHr00-rEePqpTdz8Q5ko9MZFdsmOXWxlhxYnA2Da9UrBgJVRNlVc0XoVd8nR2oKhJOrUnGWqD9vTKgZLU4M0UXEqK28DEtx4xoFCN_VDsvhPpjEv";
    // Set POST variables
    $key = "AAAAQHmlp4w:APA91bF_dCkbaLATJ4cS878Ho3BeJ8gx_QMSqQFYDnOX09-ROpoHJj3yZ5CRpeeO0epiaVQvapw_xow9FtaLn4fBcvJH009min7MhYORLotu--mlnmuVNVGEm_G7baii4zTLSczzhmFr";
    $key = null;
    // Set POST variables
    $url = 'https://fcm.googleapis.com/fcm/send';
    $headers = array(
        'Authorization: key=' . $key,
        'Content-Type: application/json',
    );
    // Open connection
    $ch = curl_init();
    // Set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // Disabling SSL Certificate support temporarly
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    // Execute post
    $result = curl_exec($ch);
    if ($result === false) {
        die('Curl failed: ' . curl_error($ch));
    }
    // Close connection
    curl_close($ch);
    return $result;
}

function android_customer_app_push($fields)
{
    $CI = &get_instance();
    $key = "AAAAQHmlp4w:APA91bF_dCkbaLATJ4cS878Ho3BeJ8gx_QMSqQFYDnOX09-ROpoHJj3yZ5CRpeeO0epiaVQvapw_xow9FtaLn4fBcvJH009min7MhYORLotu--mlnmuVNVGEm_G7baii4zTLSczzhmFr";
    $key = null;
    // Set POST variables
    $url = 'https://fcm.googleapis.com/fcm/send';
    $headers = array(
        'Authorization: key=' . $key,
        'Content-Type: application/json',
    );
    // Open connection
    $ch = curl_init();
    // Set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // Disabling SSL Certificate support temporarly
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    // Execute post
    $result = curl_exec($ch);
    if ($result === false) {
        die('Curl failed: ' . curl_error($ch));
    }
    // Close connection
    curl_close($ch);
    return $result;
}
function android_customer_offer_push($fields)
{
    $CI = &get_instance();
    $key = "AAAAokehKwA:APA91bEx7843nNdkSt85xRMnrnsBueJ6I0wcYMWoYaOAhQVMumCJSbXw-lNnjRKq1_UVvcTfG3Y5WfGAgoxCT0-Z9_ERbVu34FbGYdziUUMEnwdV-1728clQTJbbo1qv-sb221Lb9KVa";
    $key = null;
    // Set POST variables
    $url = 'https://fcm.googleapis.com/fcm/send';
    $headers = array(
        'Authorization: key=' . $key,
        'Content-Type: application/json',
    );
    // Open connection
    $ch = curl_init();
    // Set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // Disabling SSL Certificate support temporarly
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    // Execute post
    $result = curl_exec($ch);
    if ($result === false) {
        die('Curl failed: ' . curl_error($ch));
    }
    // Close connection
    curl_close($ch);
    return $result;
}