<?php
/*********************************** */
/*                                   */
/*      File Author : Samnad. S      */
/*                                   */
/*********************************** */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
function showPrepareTimeError($schedule_date, $hour_from, $book_type, $repeat_days = [], $response_type = 'json')
{
    // Code by : Samnad. S @ 31-03-2023
    /*********************************** */
    // This function is used to ensure atleast 1 hour (60 minutes) free time available for new booking.
    // Function can run while creating new booking
    // For eg. If new booking time is 04:00 PM booking must be made before 03.00 PM (means 60 minute prepare time needed)
    /*********************************** */
    $CI = get_instance();
    $CI->load->model('settings_model');
    $settings = $CI->settings_model->get_settings();
    $waitTime = $settings->booking_preparation_time;
    $schedule_date = str_replace('/', '-', $schedule_date);
    if (strtotime($schedule_date) == strtotime(date('d-M-Y'))) {
        // checking not required for future dates
        $response = array();
        $current_week_day = date('w');
        $hour_from = date('H:i', trim($hour_from));
        $hour_now = date('H:i');
        $diffMinutes = (strtotime($hour_from) - strtotime($hour_now)) / 60;
        if ($book_type == 'OD' && $diffMinutes < $waitTime) {
            if ($response_type == 'json') {
                $response['status'] = 'error';
                $response['message'] = 'Please select a booking time after ' . date('h:i A', time() + $waitTime * 60) . '.';
                die(json_encode($response));
            } else {
                die('<span class="field-error text-danger">Please select a booking time after ' . date('h:i A', time() + $waitTime * 60) . '.</p>');
            }
        } else if ($book_type == 'WE') {
            if (empty($repeat_days)) {
                $repeat_days[] = $current_week_day; // in some pages multiple days selection not shown, so we take today as weekly day
            }
            foreach ($repeat_days as $key => $week) {
                if ($week == $current_week_day && $diffMinutes < $waitTime) {
                    if ($response_type == 'json') {
                        $response['status'] = 'error';
                        $response['message'] = 'Please select a booking time after ' . date('h:i A', time() + $waitTime * 60) . '.';
                        die(json_encode($response));
                    } else {
                        die('<span class="field-error text-danger">Please select a booking time after ' . date('h:i A', time() + $waitTime * 60) . '.</p>');
                    }

                }
            }
        } else if ($book_type == 'BW') {
            if (empty($repeat_days)) {
                $repeat_days[] = $current_week_day; // in some pages multiple days selection not shown, so we take today as bi-weekly day
            }
            foreach ($repeat_days as $key => $week) {
                if ($week == $current_week_day && $diffMinutes < $waitTime) {
                    if ($response_type == 'json') {
                        $response['status'] = 'error';
                        $response['message'] = 'Please select a booking time after ' . date('h:i A', time() + $waitTime * 60) . '.';
                        die(json_encode($response));
                    } else {
                        die('<span class="field-error text-danger">Please select a booking time after ' . date('h:i A', time() + $waitTime * 60) . '.</p>');
                    }
                }
            }
        }
        //die(json_encode(array('status' => 'error', 'message' => 'Booking Allowed !'))); // test
    }
}
function getFutureDayServiceDates($weekday, $dateFromString, $dateToString, $booking_type)
{
    $booking_type = strtoupper($booking_type);
    if ($booking_type == 'OD') {
        $dates[] = $dateFromString;
        return $dates;
    }
    $weekMapping = array(1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday', 7 => 'Sunday');
    $format = 'Y-m-d';
    $weekday = $weekMapping[$weekday];
    $dateFrom = new \DateTime($dateFromString);
    $dateTo = new \DateTime($dateToString);
    $dates = [];
    if ($dateFrom > $dateTo) {
        return $dates;
    }
    if (date('N', strtotime($weekday)) != $dateFrom->format('N')) {
        $dateFrom->modify("next $weekday");
    }
    while ($dateFrom <= $dateTo) {
        $dates[] = $dateFrom->format($format);
        if ($booking_type == 'WE') {
            $dateFrom->modify('+1 week');
        } elseif ($booking_type == 'BW') {
            $dateFrom->modify('+2 week');
        }
    }
    return $dates;
}
function is_any_ongoing_day_service_for_booking_by_service_date($booking_id,$service_date)
{
    /*********************************** */
    // Author : Samnad. S @ 10-05-2023
    // check for any none finished day services by booking id and service date
    // returns TRUE if service(s) found else returns FALSE
    /*********************************** */
    // $service_date = DateTime::createFromFormat('d-m-Y', $service_date)->format('Y-m-d');
    $CI = get_instance();
    $CI->load->model('day_services_model');
    $day_services = $CI->day_services_model->get_day_services_where(array('ds.booking_id' => $booking_id, 'ds.service_status' => 1,'ds.service_date' => $service_date)); // service_status 1 means ongoing
    if(count($day_services)){
        return true;
    }
    return false;
}
