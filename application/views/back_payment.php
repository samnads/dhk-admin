<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
    .topiconnew{cursor: pointer;}
</style>
<div class="row m-0">   
    <div class="col-sm-12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form class="form-horizontal" method="post" action="<?php echo base_url() . 'backpayment' ?>">
                    <div class="book-nav-top">
            <ul>
              <li><i class="icon-th-list"></i><h3>Back Payment</h3></li>
              
              <li class="mr-2">
                  <input type="text" readonly="readonly" id="payment_date" name="payment_date" value="<?php echo $payment_date ?>">
              </li>
      
      
      
              <li>
                  <input type="submit" class="n-btn" value="Go" name="add_payment"> 
              </li>
      
             

              <li class="mr-0 float-right">
                  
                  <div class="topiconnew border-0 green-btn">
                       <a href="<?php echo base_url() . 'backpayment/add'?>" title="Add Back Payment"> <i class="fa fa-plus"></i></a>
                  </div>
                  
                  <div class="topiconnew border-0 green-btn">
                       <a oonclick="exportF(this)" title="Download to Excel"> <i class="fa fa-download"></i></a>
                  </div> 
                  
              </li>
      
      
      
              <div class="clear"></div>
            </ul>
     </div>
                    
                </form>   
            </div>
            
            
            
            





            
            
            

            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px;"> Sl No.</th>
                            <th style="line-height: 18px;"> Customer</th>
                            <th style="line-height: 18px;"> Zone</th>
                            <th style="line-height: 18px;"> Collected Date</th>                            
                            <th style="line-height: 18px;"> Collected Time</th>     
                            <th style="line-height: 18px;"> Requested Amount</th>  
                            <th style="line-height: 18px;"> Collected Amount</th>    
                            <th style="line-height: 18px;"> Balance Amount</th>  
                        </tr>			

                    </thead>
                    <tbody>
                        <?php
                        if(!empty($back_payment))
                        {
                            $i = 0;
                            $total_req_amount = 0;
                            $total_collected_amount = 0;
                            $total_balance_amount = 0;
                            foreach ($back_payment as $payment)
                            {
                                //Payment Type
                                    if($payment->payment_type == "D")
                                    {
                                        $paytype = "(D)";
                                    } else if($payment->payment_type == "W")
                                    {
                                        $paytype = "(W)";
                                    } else if($payment->payment_type == "M")
                                    {
                                        $paytype = "(M)";
                                    } else
                                    {
                                        $paytype = "";
                                    }
                                
                                $total_req_amount += $payment->requested_amount;
                                $total_collected_amount += $payment->collected_amount;
                                $total_balance_amount += $payment->balance;
                                
                                echo '<tr>'
                                        . '<td style="line-height: 18px;">' . ++$i . '</td>'
                                        . '<td style="line-height: 18px;">' . $payment->customer_name.' '.$paytype. '</td>'
                                        . '<td style="line-height: 18px;">' . $payment->zone_name . '</td>'
                                        . '<td style="line-height: 18px;">' . $payment->collected_date . '</td>'
                                        . '<td style="line-height: 18px;">' . $payment->collected_time . '</td>'
                                        . '<td style="line-height: 18px;">' . $payment->requested_amount . '</td>'
                                        . '<td style="line-height: 18px;">' . $payment->collected_amount . '</td>'
                                        . '<td style="line-height: 18px;">' .  $payment->balance . '</td>'
                                    .'</tr>';
                            }
                            echo '<tr>'
                                        . '<td style="line-height: 18px;"></td>'
                                        . '<td style="line-height: 18px;"></td>'
                                        . '<td style="line-height: 18px;"></td>'
                                        . '<td style="line-height: 18px;"></td>'
                                        . '<td style="line-height: 18px;"><b>Total<b></td>'
                                        . '<td style="line-height: 18px;"><b>' . $total_req_amount . '</b></td>'
                                        . '<td style="line-height: 18px;"><b>' . $total_collected_amount . '</b></td>'
                                        . '<td style="line-height: 18px;"><b>' . $total_balance_amount . '</b></td>'
                                    .'</tr>';
                        }
                        
                        ?>
                    </tbody>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>


<div id="divToPrint" style="display: none">
    <div class="widget-content" style="margin-bottom:30px">
        <table border="1" width="100%" cellspacing="0" cellpadding = "10" style="font-size:11px; border-color: #ccc;" class="ptable">
           <thead>
                        <tr>
                            <th style="line-height: 18px;"> Sl No.</th>
                            <th style="line-height: 18px;"> Customer</th>
                            <th style="line-height: 18px;"> Zone</th>
                            <th style="line-height: 18px;"> Collected Date</th>                            
                            <th style="line-height: 18px;"> Collected Time</th>     
                            <th style="line-height: 18px;"> Requested Amount</th>  
                            <th style="line-height: 18px;"> Collected Amount</th>    
                            <th style="line-height: 18px;"> Balance Amount</th>  
                        </tr>           

                    </thead>
                    <tbody>
                        <?php
                        if(!empty($back_payment))
                        {
                            $i = 0;
                            $total_req_amount = 0;
                            $total_collected_amount = 0;
                            $total_balance_amount = 0;
                            foreach ($back_payment as $payment)
                            {
                                //Payment Type
                                    if($payment->payment_type == "D")
                                    {
                                        $paytype = "(D)";
                                    } else if($payment->payment_type == "W")
                                    {
                                        $paytype = "(W)";
                                    } else if($payment->payment_type == "M")
                                    {
                                        $paytype = "(M)";
                                    } else
                                    {
                                        $paytype = "";
                                    }
                                
                                $total_req_amount += $payment->requested_amount;
                                $total_collected_amount += $payment->collected_amount;
                                $total_balance_amount += $payment->balance;
                                
                                echo '<tr>'
                                        . '<td style="line-height: 18px;">' . ++$i . '</td>'
                                        . '<td style="line-height: 18px;">' . $payment->customer_name.' '.$paytype. '</td>'
                                        . '<td style="line-height: 18px;">' . $payment->zone_name . '</td>'
                                        . '<td style="line-height: 18px;">' . $payment->collected_date . '</td>'
                                        . '<td style="line-height: 18px;">' . $payment->collected_time . '</td>'
                                        . '<td style="line-height: 18px;">' . $payment->requested_amount . '</td>'
                                        . '<td style="line-height: 18px;">' . $payment->collected_amount . '</td>'
                                        . '<td style="line-height: 18px;">' .  $payment->balance . '</td>'
                                    .'</tr>';
                            }
                            echo '<tr>'
                                        . '<td style="line-height: 18px;"></td>'
                                        . '<td style="line-height: 18px;"></td>'
                                        . '<td style="line-height: 18px;"></td>'
                                        . '<td style="line-height: 18px;"></td>'
                                        . '<td style="line-height: 18px;"><b>Total<b></td>'
                                        . '<td style="line-height: 18px;"><b>' . $total_req_amount . '</b></td>'
                                        . '<td style="line-height: 18px;"><b>' . $total_collected_amount . '</b></td>'
                                        . '<td style="line-height: 18px;"><b>' . $total_balance_amount . '</b></td>'
                                    .'</tr>';
                        }
                        
                        ?>
                    </tbody> 
        </table>
    </div>
</div>


<script type="text/javascript">
  function exportF(elem) 
  {
      var table = document.getElementById("divToPrint");
      var html = table.outerHTML;
      var url = 'data:application/vnd.ms-excel,' + escape(html); // Set your html table into url 
      elem.setAttribute("href", url);
      elem.setAttribute("download", "BackPayments.xls"); // Choose the file name
      return false;
  }  
</script>
