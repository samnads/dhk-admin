<style type="text/css">
.bg-od,.schedule_bubble.od {
  background-color: <?= $settings->color_bg_booking_od; ?> !important;
  color: <?= $settings->color_bg_text_booking_od; ?> !important;
}
.bg-we,.schedule_bubble.we {
  background-color: <?= $settings->color_bg_booking_we; ?> !important;
  color: <?= $settings->color_bg_text_booking_we; ?> !important;
}
.bg-bw,.schedule_bubble.bw {
  background-color: <?= $settings->color_bg_booking_bw; ?> !important;
  color: <?= $settings->color_bg_text_booking_bw; ?> !important;
}
.schedule_bubble.od:after {
    border-color: transparent <?= $settings->color_bg_booking_od; ?>;
}
.schedule_bubble.we:after {
    border-color: transparent <?= $settings->color_bg_booking_we; ?>;
}
.schedule_bubble.bw:after {
    border-color: transparent <?= $settings->color_bg_booking_bw; ?>;
}
</style>
<style>
/*    .save-but{
	  background: #4897e6 !important;
  }*/



.select2-arrow {
	visibility : hidden;
}
.select2-container .select2-choice {
	-moz-appearance: none;
	background: #fff url("http://demo.azinova.info/php/mymaid/css/../images/ash-arrow.png") no-repeat scroll right 7px top 11px;
	border: 1px solid #ccc;
	border-radius: 3px;
	cursor: pointer;
	font-size: 14px;
	height: 30px;
	line-height: 24px;
	padding: 3px 0 3px 10px;
	text-indent: 0.01px;/*width:200px;*/
}
.leave-but {
	color: #FFF;
	text-transform: uppercase;
	font-size: 14px;
	float: left;
	text-decoration: none;
	border: 0px;
	cursor: pointer;
	padding: 4px 15px;
	background-color: #0756a5;
	border-radius: 4px;
	font-weight: bold;
	font-family: Arial, Helvetica, sans-serif;
	margin-right: 20px;
}
.popup-main-box .booking_form .row .cell1 {
	width: 160px !important;
}
#schedule-top .select2 {
	width: 210px !important;
}
.row.maid-schedule-top {
	margin: 0 0 0 10px !important;
}
.n-form-set-main {
	padding-left: 15px;
	padding-right: 15px;
}
.n-form-set-main .select2-container {
	width: 100% !important;
}

.n-days {padding-right: 15.45%;}

#customer-info .select2 {
  width: 100% !important;
}

</style>
<div id="alert-popup" style="background:none;display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head"> <span id="alert-title"></span> </div>
    <div class="modal-body">
      <h3 id="alert-message"></h3>
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn mb-0 pop_close">OK</button>
    </div>
  </div>
</div>


<div class="content-wrapper">
  <div id="schedule-top" class="pt-0 pb-2">
    <form method="post" action="<?php echo base_url() . 'maid/schedule'?>">
      <div class="row maid-schedule-top" style="margin: 0 0 0 10px !important;">
        <div class="book-nav-top">
          <ul>
            <li>
              <div class="mm-color-box bg-od"><?= $settings->od_booking_text; ?></div>
              <div class="mm-color-box bg-we"><?= $settings->we_booking_text; ?></div>
              <div class="mm-color-box bg-bw"><?= $settings->bw_booking_text; ?></div>
            </li>
            <li>
              <select name="maid_id" id="search-maid-id" class="sel2" style="width:250px !important;">
                <option value="0">-- Select Maid --</option>
                <?php
								  foreach($maids as $maid)
								  {       
										  $selected = $maid_id == $maid->maid_id ? 'selected="selected"' : '';
										  echo '<option value="' . $maid->maid_id . '" ' . $selected . '>' . html_escape($maid->maid_name) . '</option>';
								  }
								  ?>
              </select>
            </li>
            <li>
              <input type="text" name="start_date" id="start-date" class="n-calendar-icon" value="<?php echo $start_date ?>" readonly>
            </li>
            <li>
              <input type="text" name="end_date" id="end-date" class="n-calendar-icon" value="<?php echo $end_date ?>" readonly>
            </li>
            <li>
              <input type="submit" class="n-btn mb-0" name="search_maid_schedule" style="float: none;" value="Search" onclick="$('.mm-loader').show()" />
            </li>
            <li class="no-right-margin">
              <input type="button" class="n-btn purple-btn mb-0" name="mark_maid_leave" id="mark_maid_leave" style="float: none;display:none; " value="Mark Leave" />
            </li>
            <div class="clear"></div>
          </ul>
        </div>
      </div>
    </form>
  </div>
  <style>
.row.maid-schedule-top { margin: 0 auto !important; display: table !important;}
.n-time-section { width: 100%; position:relative;}
.n-grid-section {width: 100%;}
#schedule .time_grid .grids .row .slot { height: 70px;}
.slot { background: #fafafa !important;}

#schedule { margin: 0 auto !important;}

</style>
  <div id="schedule-wrapper"><!-- style="min-height: <?php //echo ((count($maids) * 80) + 70); ?>px;" -->
    <div id="schedule">
      <div class="n-time-section">
        <div id="tb-slide-left" class="time-nav previous-time" title="Previous Time">&nbsp;</div>
        <div class="head">Schedule Day</div>
        <div class="time_line">
          <div class="time_slider">
            <div style="width:3408px;">
              <?php
						  foreach($times as $time_index=>$time)
						  {
							  echo '<div><span>' . $time->display .' </span></div>';
						  }
						  ?>
            </div>
          </div>
        </div>
        <div id="tb-slide-right" class="time-nav next-time" title="Next Time">&nbsp;</div>
        <div class="next-time-bg">&nbsp;</div>
        <div class="clear"></div>
      </div>
      <div class="n-grid-section">
        <div class="maids">
          <?php
				  foreach($maid_schedule as $maid)
				  {
					  $nameOfDay = date('l', strtotime($maid->date));
					  ?>
          <div class="maid"><?php echo html_escape(date("d/m/Y", strtotime($maid->date))); ?> (<?php echo $nameOfDay; ?>)</div>
          <?php
				  }
				  ?>
        </div>
        <!--Maid section-->
        
        <div class="time_grid" style="height: <?php echo (count($maid_schedule) * 71.1); ?>px;"> 
          <!--<div class="time_grid" style="height: <?php// echo (count($maid_schedule) * 79) + count($maid_schedule) + 1; ?>px;">-->
          <div class="grids">
            <div id="schedule-grid-rows"><?php echo $schedule_grid; ?></div>
          </div>
        </div>
        <div class="clear"></div>
      </div>
    </div>
  </div>
</div>


<input type="hidden" id="all-maids" value='<?php echo $all_maids; ?>' />
<input type="hidden" id="time-slots" value='<?php echo json_encode($times); ?>' />
<input type="hidden" id="repeate-end-start" value="<?php echo html_escape($repeate_end_start_c); ?>" />
<div class="n-booking-popup-wrapper" id="booking-popup" style="display:none;">
  <div class="popup-main-box">
    
    <div class="col-sm-12 green-popup-head">
        <span id="b-maid-name"></span> <span id="b-time-slot"></span> <span id="b-booking-id"></span> <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    
    <div class="col-sm-12 pl-0 pr-0">
    <div class="row n-form-set-main m-0">
      <div class="col-sm-6 n-form-set-left">
        <div class="col-12 p-0" id="customer-info">
          <div class="row m-0 n-field-main">
            <p>Customer</p>
            <div class="n-field-box" id="b-customer-id-cell">
              <select name="customer_id" id="b-customer-id" data-placeholder="Select customer" class="">
                <option></option>
              </select>
              <div id="customer-picked-address"></div>
            </div>
          </div>
        </div>
        
        
        <div id="customer-address-panel">
          <div class="head">Pick One Address <span class="close"><div class="n-close-btn">&nbsp;</div></span></div>
          <div class="inner"> Loading<span class="dots_loader"></span> </div>
        </div>
        
        <div id="maids-panel">
          <div class="head"> Pick Maids <span class="close">
            <div class="n-close-btn">&nbsp;</div>
            </span> </div>
          <div class="controls">
            <div class="row m-0 n-field-main pt-2" style="border-bottom: 1px solid #ddd;">
              <div class="col-sm-3 n-field-box">
                <div class="n-end n-pick-maids-set">
                  <input id="pick-maids-all" type="radio" value="0" name="same_zone" class="" checked="">
                  <label for="pick-maids-all">
                  <span class="border-radius-3"></span>
                  <p>All</p>
                  </label>
                </div>
              </div>
              <div class="col-sm-3 n-field-box">
                <div class="n-end n-pick-maids-set">
                  <input id="pick-maids-zone" type="radio" value="1" name="same_zone" class="">
                  <label for="pick-maids-zone">
                  <span class="border-radius-3"></span>
                  <p>Same Zone</p>
                  </label>
                </div>
              </div>
            </div>
          </div>
          <div class="inner"> Loading<span class="dots_loader"></span> </div>
        </div>
        
        
        <div class="row m-0 n-field-main">
          <p>Service Type</p>
          <div class="n-field-box sertype_sectn">
            <select name="service_type_id" id="b-service-type-id" data-placeholder="Select service type" class="sel2">
              <option></option>
              <?php
						  foreach($service_types as $service_type)
						  {
															  $selected = $service_type->service_type_id == 1 ? 'selected="selected"' : '';
							  echo '<option value="' . $service_type->service_type_id . '" ' . $selected . '>' . html_escape($service_type->service_type_name) . '</option>';
						  }
						  ?>
            </select>
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <div class="col-sm-6 pl-0 n-field-box">
            <p>From Time</p>
            <select name="time_from" id="b-from-time" data-placeholder="Select" class="sel2 small mr15">
              <option></option>
              <?php
							  foreach($times as $time_index=>$time)
							  {
								  echo '<option value="' . $time->stamp . '">' . $time->display . '</option>';
							  }
							  ?>
            </select>
          </div>
          <div class="col-sm-6 pr-0 n-field-box">
            <p>To Time</p>
            <select name="time_to" id="b-to-time" data-placeholder="Select" class="sel2 small">
              <option></option>
              <?php
							  foreach($times as $time_index=>$time)
							  {
								  if($time_index == 't-0')
								  {                                                                                
									  continue;
								  }
								  
								  echo '<option value="' . $time->stamp . '">' . $time->display . '</option>';
							  }
							  ?>
            </select>
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <p>Repeats</p>
          <div class="n-field-box">
            <select name="booking_type" id="b-booking-type" data-placeholder="Select repeat type" class="sel2">
              <option></option>
              <option value="OD"><?= $settings->od_booking_text; ?></option>
              <option value="WE"><?= $settings->we_booking_text; ?></option>
              <option value="BW"><?= $settings->bw_booking_text; ?></option>
            </select>
          </div>
        </div>
        <div class="row m-0 n-field-main" id="repeat-days">
          <p>Repeat On</p>
          <div class="col-sm-12 pl-0 pr-0 n-field-box n-field-day mb-0">
            <div class="n-days">
              <input id="repeat-on-0" type="checkbox" value="0" name="w_day[]" <?php if($day_number == 0) { echo 'checked="checked"'; } ?>>
              <label for="repeat-on-0">
              <span class="border-radius-3"></span>
              <p>Sun</p>
              </label>
            </div>
            <div class="n-days">
              <input id="repeat-on-1" type="checkbox" value="1" name="w_day[]" <?php if($day_number == 0) { echo 'checked="checked"'; } ?>>
              <label for="repeat-on-1">
              <span class="border-radius-3"></span>
              <p>Mon</p>
              </label>
            </div>
            <div class="n-days">
              <input id="repeat-on-2" type="checkbox" value="2" name="w_day[]" <?php if($day_number == 0) { echo 'checked="checked"'; } ?>>
              <label for="repeat-on-2">
              <span class="border-radius-3"></span>
              <p>Tue</p>
              </label>
            </div>
            <div class="n-days">
              <input id="repeat-on-3" type="checkbox" value="3" name="w_day[]" <?php if($day_number == 0) { echo 'checked="checked"'; } ?>>
              <label for="repeat-on-3">
              <span class="border-radius-3"></span>
              <p>Wed</p>
              </label>
            </div>
            <div class="n-days">
              <input id="repeat-on-4" type="checkbox" value="4" name="w_day[]" <?php if($day_number == 0) { echo 'checked="checked"'; } ?>>
              <label for="repeat-on-4">
              <span class="border-radius-3"></span>
              <p>Thu</p>
              </label>
            </div>
            <div class="n-days">
              <input id="repeat-on-5" type="checkbox" value="5" name="w_day[]" <?php if($day_number == 0) { echo 'checked="checked"'; } ?>>
              <label for="repeat-on-5">
              <span class="border-radius-3"></span>
              <p>Fri</p>
              </label>
            </div>
            <div class="n-days pr-0">
              <input id="repeat-on-6" type="checkbox" value="6" name="w_day[]" <?php if($day_number == 0) { echo 'checked="checked"'; } ?>>
              <label for="repeat-on-6">
              <span class="border-radius-3"></span>
              <p>Sat</p>
              </label>
            </div>
            <div class="clear"></div>
          </div>
        </div>
        <div class="row m-0 n-field-main" id="repeat-ends">
          <div class="col-sm-6 pl-0 n-field-box n-end-main">
            <p>Never Ends</p>
            <div class="n-end">
              <input id="repeat-end-never" type="radio" value="never" name="repeat_end" class="" checked>
              <label for="repeat-end-never"> <span class="border-radius-3"></span></label>
            </div>
          </div>
          <div class="col-sm-6 pr-0 n-field-box">
            <p>Ends</p>
            <div class="n-end">
              <input id="repeat-end-ondate" type="radio" value="ondate" name="repeat_end" class="">
              <label for="repeat-end-ondate"> <span class="border-radius-3"></span></label>
            </div>
            <div class="n-end-field">
              <input type="text" class="end_datepicker" id="repeat-end-date" data-date-format="dd/mm/yyyy" readonly="readonly" disabled="disabled" style="position: relative; top: -4px; height: 26px;" />
            </div>
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <div class="col-sm-6 n-field-box pl-0">
            <p>Cleaning Materials</p>
            <div class="switch-main">
              <label class="switch">
                <input type="checkbox" name="cleaning_materials" id="b-cleaning-materials" value="Y">
                <span class="slider round"></span> </label>
            </div>
          </div>
          <div class="col-sm-6 n-field-box pr-0">
            <p>Lock Booking</p>
            <div class="switch-main">
              <label class="switch">
                <input type="checkbox" name="lock_booking" id="lock-booking">
                <span class="slider round"></span> </label>
            </div>
          </div>
        </div>
      </div>
      
      <div class="col-sm-6 n-form-set-right">
        <div class="row m-0 n-field-main">
          <p>Total Amount</p>
          <div class="col-sm-6 pl-0 n-field-box">
            <label class=" position-relative">
            <div class="n-right-position">/hr</div>
            <input type="hidden" id="total_prebook_hrs" value="0">
            <input type="hidden" id="total_week_days" value="1">
            <input name="tot_amout" id="tot_amout" value="" type="number" autocomplete="off" class="popup-disc-fld"/>
            </label>
          </div>
          <div class="col-sm-6 pr-0 n-field-box">
            <label class=" position-relative">
            <div class="n-left-position" style="padding:0px 10px;"><i class="fa fa-dollar"></i></div>
            <div class="n-right-position">/hr</div>
            <input name="rate_per_hr" id="b-rate_per_hr" readonly="" value="" type="number" class="popup-disc-fld text-center"/>
            </label>
          </div>
        </div>
        <div class="row m-0 n-field-main">
          <div class="col-sm-6 pl-0 n-field-box">
            <p>Pending Amount</p>
            <label>
              <input name="pending_amount" id="b-pending-amount" type="number" class="popup-disc-fld"/>
            </label>
          </div>
          <div class="col-sm-6 pr-0 n-field-box">
            <p>Discount</p>
            <label>
              <input name="discount" id="b-discount" type="number" class="popup-disc-fld"/>
            </label>
          </div>
        </div>
        <div style="clear:both; padding: 5px;"></div>
        <div class="row m-0 n-field-main driver-note">
          <p>Note to Driver</p>
          <div class="n-field-box pop-main-cont-fld-box">
            <textarea name="booking_note" id="booking-note" class="popup-note-fld"></textarea>
          </div>
        </div>
      </div>
      
      <div class="col-sm-12 n-form-set-left">
        <div class="text-danger" id="b-error"></div>
      </div>
      
      <div class="col-sm-12 n-popup-btn-main">
        <div class="col-sm-12 pop-main-button">
          <input type="hidden" class="n-btn" id="booking-id" />
          <input type="hidden" class="n-btn" id="customer-address-id" />
          <input type="hidden" class="n-btn" id="maid-id" />
          <input type="hidden" class="n-btn" id="service-date" />
          <input type="button" class="n-btn" id="save-booking" value="Save" />
          <input type="button" class="n-btn red-btn" id="delete-booking" value="Delete" />
          <div class="clear"></div>
        </div>
      </div>
    </div>
    </div>
    
  </div>
</div>

<div id="schedule-report" style="display: none !important;"></div>
