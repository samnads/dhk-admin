<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
    .topiconnew{cursor: pointer;}
</style>
<div class="row m-0">  
    <div class="col-md-12">
    
    
        <div class="widget widget-table action-table">
            <div class="widget-header"> 
                <form id="edit-profile" class="form-horizontal" method="post" >
                <ul>
                <li>
                    <i class="icon-th-list"></i>
                    <h3>Maid Vehicle Report</h3>    
                    </li>
                    
                    <li class="mr-2">               
                    <input type="text" style="width: 160px;" id="OneDayDate" name="search_date" value="<?php echo isset($search_date) ? $search_date : date('d/m/Y'); ?>">
                    </li>
                    <li>
                    <input type="submit" class="n-btn" value="Go" name="vehicle_report">
                   
                   </li>
                   
                   <li class="mr-0 float-right">
                    
                    <div class="topiconnew border-0 green-btn">
                    	<a onclick="exportF(this)" title="Download to Excel"> <i class="fa fa-download"></i></a>
                    </div>
                    </li>
                    </ul>
                </form>   
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead class="new-table-boxes">
                        <tr>
                                                    
                            
                            <?php
                            
                            foreach($zones as $zone)
                            {
                                echo '<th style="line-height: 18px">' . $zone->zone_name . '</th>';
                            }
                            ?>
                        </tr>
                    </thead>                    
                    <tbody>
                       <?php
                    //foreach($maids as $maid)
                    //{
                        
                        //echo '<td style="line-height: 18px">' . $maid->maid_name . '</td>';
                        $i = 0;
                        foreach ($reports as $rpt)
                        {
                            echo '<tr>';
                            foreach($zones as $zone)
                            {
                                echo '<td style="line-height: 18px"> ';
                                if(isset($rpt[$zone->zone_id]['maid_name']))
                                {
                                    $color = $rpt[$zone->zone_id]['attendance_status'] == 'IN' ? 'green' : 'red';
                                    
                                    echo $rpt[$zone->zone_id]['maid_name'] 
                                            . ' (<span style="color:'. $color . '">' . 
                                            $rpt[$zone->zone_id]['attendance_status']  
                                            . '</span>)';
                                    
                                }
                                
                                
                                echo '</td>';
                            }
                            echo '</tr>';
                            ++$i;
                          
                        }
                       
                    //}
                    ?> 
                    </tbody>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>


<div id="divToPrint" style="display: none">
    <div class="widget-content">
        <table border="1" width="100%" cellspacing="0" cellpadding = "10" style="font-size:11px; border-color: #ccc;" class="ptable">
            <thead class="new-table-boxes">
                <tr>
                                            
                    
                    <?php
                    
                    foreach($zones as $zone)
                    {
                        echo '<th style="line-height: 18px">' . $zone->zone_name . '</th>';
                    }
                    ?>
                </tr>
            </thead>                    
            <tbody>
               <?php
            //foreach($maids as $maid)
            //{
                
                //echo '<td style="line-height: 18px">' . $maid->maid_name . '</td>';
                $i = 0;
                foreach ($reports as $rpt)
                {
                    echo '<tr>';
                    foreach($zones as $zone)
                    {
                        echo '<td style="line-height: 18px"> ';
                        if(isset($rpt[$zone->zone_id]['maid_name']))
                        {
                            $color = $rpt[$zone->zone_id]['attendance_status'] == 'IN' ? 'green' : 'red';
                            
                            echo $rpt[$zone->zone_id]['maid_name'] 
                                    . ' (<span style="color:'. $color . '">' . 
                                    $rpt[$zone->zone_id]['attendance_status']  
                                    . '</span>)';
                            
                        }
                        
                        
                        echo '</td>';
                    }
                    echo '</tr>';
                    ++$i;
                  
                }
               
            //}
            ?> 
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript">
  function exportF(elem) 
  {
      var table = document.getElementById("divToPrint");
      var html = table.outerHTML;
      var url = 'data:application/vnd.ms-excel,' + escape(html); // Set your html table into url 
      elem.setAttribute("href", url);
      elem.setAttribute("download", "MaidVehicleReport.xls"); // Choose the file name
      return false;
  }  
</script>
