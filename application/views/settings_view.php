<link rel="stylesheet" href="https://fengyuanchen.github.io/cropperjs/css/cropper.css">
<script src="https://fengyuanchen.github.io/cropperjs/js/cropper.js"></script>
<div id="crop-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Image Cropper</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn" onclick="closeCropper()">&nbsp;</span>
    </div>
    <div id="" class="col-12 p-0">
      <div class="modal-body">
        <div class="img-container">
          <img id="image" src="#">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="n-btn red-btn mb-0" onclick="closeCropper()">Cancel</button>
        <button type="button" class="n-btn mb-0" id="crop">Crop</button>
        <div class="btn-group">
          <button type="button" class="btn n-btn mr-2" data-method="zoom" data-option="0.1" title="Zoom In">
            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.zoom(0.1)">
              <span class="fa fa-search-plus"></span>
            </span>
          </button>
          <button type="button" class="btn n-btn" data-method="zoom" data-option="-0.1" title="Zoom Out">
            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.zoom(-0.1)">
              <span class="fa fa-search-minus"></span>
            </span>
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row m-0">
    <div class="col-sm-12">
        <div class="widget">
            <div class="widget-header">
                <div class="book-nav-top">
                    <ul>
                        <li><i class="icon-th-list"></i><h3>Settings</h3></li>
                        <div class="clear">
                        </div>
                    </ul>
                </div>
            </div>
            <div class="widget-content">
                <?= $this->session->flashdata('success'); ?>
                <form method="post" autocomplete="off">
                <!--<div class="col-sm-4 n-field-main">
                    <p>Normal Hours Fee</p>
                    <div class="col-sm-12 p-0 n-field-box">
                        <input type="number" class="" id="normal_hours_fee" name="normal_hours_fee" value="<?= $settings->normal_hours_fee; ?>">
                    </div>
                </div>
                <div class="col-sm-4 n-field-main">
                    <p>Extra Hours Fee</p>
                    <div class="col-sm-12 p-0 n-field-box">
                        <input type="number" class="" id="extra_hours_fee" name="extra_hours_fee" value="<?= $settings->extra_hours_fee; ?>">
                    </div>
                </div>
                <div class="col-sm-4 n-field-main">
                    <p>Weekend Hours Fee</p>
                    <div class="col-sm-12 p-0 n-field-box">
                        <input type="number" class="" id="weekend_hours_fee" name="weekend_hours_fee" value="<?= $settings->weekend_hours_fee; ?>">
                    </div>
                </div>-->
				
				<div class="col-sm-12">
				<div class="row m-0">
				     <div class="col-md-6 p-0">
				
                <div class="row m-0 n-field-main">
                    <p>Website Logo (<?= $settings->website_logo_width; ?> x <?= $settings->website_logo_height; ?>)</p>
                    <label style="width: 200px;">
                        <img class="img-thumbnail" id="website_logo_show" src="<?= check_and_get_img_url('./uploads/images/settings/'.$settings->website_logo, 'website-logo.png'); ?>" style="width: 100%;">
                        <input type="file" class="input-image" accept="image/*" data-selector="website_logo" data-crop-width="<?= $settings->website_logo_width; ?>" data-crop-height="<?= $settings->website_logo_height; ?>">
                    </label>
                    <input type="hidden" name="website_logo_base64" id="website_logo_base64">
                </div>
				
				
                <div class="row m-0 n-field-main">
                    <p>Login Page Logo (<?= $settings->login_page_logo_width; ?> x <?= $settings->login_page_logo_height; ?>)</p>
                    <label style="width: 200px;">
                        <img class="img-thumbnail" id="login_page_logo_show" src="<?= check_and_get_img_url('./uploads/images/settings/'.$settings->login_page_logo, 'login-page-logo.png'); ?>" style="width: 100%;">
                        <input type="file" class="input-image" accept="image/*" data-selector="login_page_logo" data-crop-width="<?= $settings->login_page_logo_width; ?>" data-crop-height="<?= $settings->login_page_logo_height; ?>">
                    </label>
                    <input type="hidden" name="login_page_logo_base64" id="login_page_logo_base64">
                </div>
				
				</div>
				<div class="col-md-6 p-0">
				
                <div class="row m-0 n-field-main">
                    <p>Invoice Logo (<?= $settings->invoice_logo_width; ?> x <?= $settings->invoice_logo_height; ?>)</p>
                    <label style="width: 200px;">
                        <img class="img-thumbnail" id="invoice_logo_show" src="<?= check_and_get_img_url('./uploads/images/settings/'.$settings->invoice_logo, 'invoice-logo.png'); ?>" style="width: 100%;">
                        <input type="file" class="input-image" accept="image/*" data-selector="invoice_logo" data-crop-width="<?= $settings->invoice_logo_width; ?>" data-crop-height="<?= $settings->invoice_logo_height; ?>">
                    </label>
                    <input type="hidden" name="invoice_logo_base64" id="invoice_logo_base64">
                </div>
				
				
                <div class="row m-0 n-field-main">
                    <p>Favicon</p>
                    <label style="height: 50px; width: 50px;">
                        <img class="img-thumbnail" id="site_favicon_show" src="<?= check_and_get_img_url('./uploads/images/settings/'.$settings->site_favicon, 'favicon.ico'); ?>" style="width:50px;">
                        <input type="file" class="input-image" accept="image/*" data-selector="site_favicon" data-crop-width="32" data-crop-height="32">
                    </label>
                    <input type="hidden" name="site_favicon_base64" id="site_favicon_base64">
                </div>
				
				</div>
				</div>
				</div>
				
				
                <div class="form-row n-settings-main">
				
				
                    <div class="col-md-4 p-0">
						<div class="col-md-12 n-field-main">
							<p>Booking Background Color [ One Day ]</p>
							<div class="col-sm-12 p-0 n-field-box n-color-field">
								<input type="color" id="color_bg_booking_od" name="color_bg_booking_od" pattern="^#+([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$" value="<?= $settings->color_bg_booking_od; ?>" title="Click to change color"> 
							</div>
						</div>

						<div class="col-md-12 n-field-main">
							<p>Booking Background Text Color [ One Day ]</p>
							<div class="col-sm-12 p-0 n-field-box n-color-field">
								<input type="color" id="color_bg_text_booking_od" name="color_bg_text_booking_od" pattern="^#+([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$" value="<?= $settings->color_bg_text_booking_od; ?>" title="Click to change color">
							</div>
						</div>

						<div class="col-md-12 n-field-main">
							<p>Booking Text [ One Day ]</p>
							<div class="col-sm-12 p-0 n-field-box">
								<input autocomplete="off" type="text" class="" id="od_booking_text" name="od_booking_text" value="<?= $settings->od_booking_text; ?>">
							</div>
						</div>
                    </div>
					
					
                    <div class="col-md-4 p-0">
					
						<div class="col-md-12 n-field-main">
							<p>Booking Background Color [ Weekly ]</p>
							<div class="col-sm-12 p-0 n-field-box n-color-field">
								<input type="color" id="color_bg_booking_we" name="color_bg_booking_we" pattern="^#+([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$" value="<?= $settings->color_bg_booking_we; ?>" title="Click to change color">
							</div>
						</div>

						<div class="col-md-12 n-field-main">
							<p>Booking Background Text Color [ Weekly ]</p>
							<div class="col-sm-12 p-0 n-field-box  n-color-field">
								<input type="color" id="color_bg_text_booking_we" name="color_bg_text_booking_we" pattern="^#+([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$" value="<?= $settings->color_bg_text_booking_we; ?>" title="Click to change color">
							</div>
						</div>

						<div class="col-md-12 n-field-main">
							<p>Booking Text [ Weekly ]</p>
							<div class="col-sm-12 p-0 n-field-box">
								<input autocomplete="off" type="text" class="" id="we_booking_text" name="we_booking_text" value="<?= $settings->we_booking_text; ?>">
							</div>
						</div>
						
                    </div>
					
					
                    <div class="col-md-4 p-0">

						<div class="col-md-12 n-field-main">
							<p>Booking Background Color [ Bi-Weekly ]</p>
							<div class="col-sm-12 p-0 n-field-box n-color-field">
								<input type="color" id="color_bg_booking_bw" name="color_bg_booking_bw" pattern="^#+([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$" value="<?= $settings->color_bg_booking_bw; ?>" title="Click to change color">
							</div>
						</div>

						<div class="col-md-12 n-field-main">
							<p>Booking Background Text Color [ Bi-Weekly ]</p>
							<div class="col-sm-12 p-0 n-field-box n-color-field">
								<input type="color" id="color_bg_text_booking_bw" name="color_bg_text_booking_bw" pattern="^#+([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$" value="<?= $settings->color_bg_text_booking_bw; ?>" title="Click to change color">
							</div>
						</div>

						<div class="col-md-12 n-field-main">
							<p>Booking Text [ Bi-Weekly ]</p>
							<div class="col-sm-12 p-0 n-field-box">
								<input autocomplete="off" type="text" class="" id="bw_booking_text" name="bw_booking_text" value="<?= $settings->bw_booking_text; ?>">
							</div>
						</div>
						
                    </div>
					
					
                </div>
                <div class="col-sm-4 n-field-main">
                    <p>App / Site Name</p>
                    <div class="col-sm-12 p-0 n-field-box">
                        <input autocomplete="off" type="text" class="" id="site_name" name="site_name" value="<?= $settings->site_name; ?>">
                    </div>
                </div>
                <div class="col-sm-4 n-field-main">
                    <p>Company Name</p>
                    <div class="col-sm-12 p-0 n-field-box">
                        <input autocomplete="off" type="text" class="" id="company_name" name="company_name" value="<?= $settings->company_name; ?>">
                    </div>
                </div>
                <div class="col-sm-4 n-field-main">
                    <p>Company Address Lines (html)</p>
                    <div class="col-sm-12 p-0 n-field-box">
                        <input autocomplete="off" type="text" class="" id="company_address_lines_html" name="company_address_lines_html" value="<?= $settings->company_address_lines_html; ?>">
                    </div>
                </div>
                <div class="col-sm-4 n-field-main">
                    <p>Company TRN</p>
                    <div class="col-sm-12 p-0 n-field-box">
                        <input autocomplete="off" type="text" class="" id="company_trn" name="company_trn" value="<?= $settings->company_trn; ?>">
                    </div>
                </div>
                <div class="col-sm-4 n-field-main">
                    <p>Company Telephone Number</p>
                    <div class="col-sm-12 p-0 n-field-box">
                        <input autocomplete="off" type="text" class="" id="company_tel_number" name="company_tel_number" value="<?= $settings->company_tel_number; ?>">
                    </div>
                </div>
                <div class="col-sm-4 n-field-main">
                    <p>Company Email</p>
                    <div class="col-sm-12 p-0 n-field-box">
                        <input autocomplete="off" type="text" class="" id="company_email" name="company_email" value="<?= $settings->company_email; ?>">
                    </div>
                </div>
                <div class="col-sm-4 n-field-main">
                    <p>Company Website URL</p>
                    <div class="col-sm-12 p-0 n-field-box">
                        <input autocomplete="off" type="text" class="" id="company_website_url" name="company_website_url" value="<?= $settings->company_website_url; ?>">
                    </div>
                </div>
                <div class="col-sm-4 n-field-main">
                    <p>Company Website URL Label</p>
                    <div class="col-sm-12 p-0 n-field-box">
                        <input autocomplete="off" type="text" class="" id="company_website_label" name="company_website_label" value="<?= $settings->company_website_label; ?>">
                    </div>
                </div>
                <div class="col-sm-4 n-field-main">
                    <p>Server Root Directory</p>
                    <div class="col-sm-12 p-0 n-field-box">
                        <input autocomplete="off" type="text" class="" id="root_directory" name="root_directory" value="<?= $settings->root_directory; ?>" readonly>
                    </div>
                </div>
                <div class="col-sm-4 n-field-main">
                    <p>Service VAT %</p>
                    <div class="col-sm-12 p-0 n-field-box">
                        <input autocomplete="off" type="number" step="any" class="" id="service_vat_percentage" name="service_vat_percentage" value="<?= $settings->service_vat_percentage; ?>">
                    </div>
                </div>
                <div class="col-sm-4 n-field-main">
                    <p>Google Maps API Key</p>
                    <div class="col-sm-12 p-0 n-field-box">
                        <input autocomplete="off" type="text" class="" id="google_map_api_key" name="google_map_api_key" value="<?= $settings->google_map_api_key; ?>">
                    </div>
                </div>
                <div class="col-sm-4 n-field-main">
                    <p>Site Footer Copyright Line (html)</p>
                    <div class="col-sm-12 p-0 n-field-box">
                        <input autocomplete="off" type="text" class="" id="site_footer_copyright_line_html" name="site_footer_copyright_line_html" value="<?= $settings->site_footer_copyright_line_html; ?>">
                    </div>
                </div>
                <div class="col-sm-4 n-field-main">
                    <p>Booking Preparation Time (in minutes)</p>
                    <div class="col-sm-12 p-0 n-field-box">
                        <input autocomplete="off" type="number" class="" id="booking_preparation_time" name="booking_preparation_time" value="<?= $settings->booking_preparation_time; ?>">
                    </div>
                </div>
                <div class="col-sm-4 n-field-main">
                    <p>Staff Listing Priority Type</p>
                    <div class="col-sm-12 p-0 n-field-box">
                        <select id="maid_listing_priority_type" name="maid_listing_priority_type">
                            <?php
                            $maid_listing_priority_types = array(
                                'lifo'=>'Last Added First (LIFO)',
                                'fifo'=>'First Added First (FIFO)',
                                'priority_number_asc'=>'Priority Number (Smallest First)',
                                'priority_number_desc'=>'Priority Number (Largest First)',
                                'maid_name_asc'=>'Staff Name (A-Z)',
                                'maid_name_desc'=>'Staff Name (Z-A)');
                            ?>
                            <?php foreach($maid_listing_priority_types as $value => $label): ?>
                                <option value="<?= $value; ?>" <?= $settings->maid_listing_priority_type == $value ? 'selected' : ''; ?>><?= $label; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-4 n-field-main">
                    <p>SMTP Host</p>
                    <div class="col-sm-12 p-0 n-field-box">
                        <input autocomplete="off" type="text" class="" id="smtp_host" name="smtp_host" value="<?= $settings->smtp_host; ?>">
                    </div>
                </div>
                <div class="col-sm-4 n-field-main">
                    <p>SMTP Port</p>
                    <div class="col-sm-12 p-0 n-field-box">
                        <input autocomplete="off" type="number" id="smtp_port" name="smtp_port" value="<?= $settings->smtp_port; ?>">
                    </div>
                </div>
                <div class="col-sm-4 n-field-main">
                    <p>SMTP Username</p>
                    <div class="col-sm-12 p-0 n-field-box">
                        <input type="text" id="smtp_username" name="smtp_username" value="<?= $settings->smtp_username; ?>" autocomplete="off">
                    </div>
                </div>
                <div class="col-sm-4 n-field-main">
                    <p>SMTP Password</p>
                    <div class="col-sm-12 p-0 n-field-box">
                        <input type="password" id="smtp_password" name="smtp_password" value="<?= $settings->smtp_password; ?>" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');">
                    </div>
                </div>
                <div class="col-sm-4 n-field-main">
                    <p>Mail Protocol</p>
                    <div class="col-sm-12 p-0 n-field-box">
                        <input autocomplete="off" type="text" id="mail_protocol" name="mail_protocol" value="<?= $settings->mail_protocol; ?>">
                    </div>
                </div>
                <div class="col-sm-12">
                    <input type="submit" class="n-btn" value="Update" name="settings_update">
                </div>
            </form>
            </div>
        </div>
    </div>
</div>
<script>
$(function() {
	let current = window.location.href;
	$('#primary_nav_wrap li a').each(function() {
		var $this = $(this);
		// if the current path is like this link, make it active
		if ($this.attr('href') === _base_url+'settings') {
			$this.addClass('active');
		}
	})
})
var cropper;
function showCropper(cropWidth,cropHeight) {
	cropper = new Cropper(image, {
		aspectRatio: cropWidth / cropHeight,
		viewMode: 0,
	});
	$.fancybox.open({
		autoCenter: true,
		fitToView: false,
		scrolling: false,
		openEffect: 'none',
		openSpeed: 1,
		autoSize: false,
		width: 600,
		height: 'auto',
		helpers: {
			overlay: {
				css: {
					'background': 'rgba(0, 0, 0, 0.3)'
				},
				closeClick: false
			}
		},
		padding: 0,
		closeBtn: false,
		content: $('#crop-popup'),
	});
}
window.addEventListener('DOMContentLoaded', function() {
    var selector = null;
    var selectorWidth = null;
    var selectorHeight = null;
	var image = document.getElementById('image');
    [...document.querySelectorAll('.input-image')].forEach(function(item) {
        item.addEventListener('change', function(e) {
            selector = e.target.getAttribute('data-selector');
            selectorWidth = e.target.getAttribute('data-crop-width');
            selectorHeight = e.target.getAttribute('data-crop-height');
            var files = e.target.files;
            var done = function(url) {
                item.value = '';
                image.src = url;
                showCropper(selectorWidth,selectorHeight);
            };
            var reader;
            var file;
            var url;
            if (files && files.length > 0) {
                file = files[0];
                if (URL) {
                    done(URL.createObjectURL(file));
                } else if (FileReader) {
                    reader = new FileReader();
                    reader.onload = function(e) {
                        done(reader.result);
                    };
                    reader.readAsDataURL(file);
                }
            }
        });
    });
	document.getElementById('crop').addEventListener('click', function() {
		var canvas;
		if (cropper) {
			canvas = cropper.getCroppedCanvas({
				width: selectorWidth,
				height: selectorHeight,
			});
			var src = canvas.toDataURL();
            document.getElementById(selector+"_base64").value = src; // reflect changed value
            document.getElementById(selector+"_show").src = src; // show changed image
		}
		closeCropper();
	});
});
function closeCropper() {
	cropper.destroy();
	cropper = null;
	$.fancybox.close();
}
</script>