<link href="<?php echo base_url() ?>css/pages/signin.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="css/bootstrap4.css" >
<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css"/>
<style>
@font-face {
	font-family:'Open-Sans-regular';
	src:url('../fonts/Open-Sans-regular.eot');
	src:local('☺'),url('../fonts/Open-Sans-regular.woff') format("woff"),url('../fonts/Open-Sans-regular.ttf') format("truetype"), url('../fonts/Open-Sans-regular.svg') format("svg");
	font-weight:400;
	font-style:normal;
}
@font-face {
	font-family: 'MyriadPro-Regular';
	src: url('../fonts/MyriadPro-Regular.eot');
	src: local('☺'), url('../fonts/MyriadPro-Regular.woff') format('woff'), url('../fonts/MyriadPro-Regular.ttf') format('truetype'), url('../fonts/MyriadPro-Regular.svg') format('svg');
	font-weight: normal;
	font-style: normal;
}
@font-face {
	font-family: 'MyriadPro-Bold';
	src: url('../fonts/MyriadPro-Bold.eot');
	src: local('☺'), url('../fonts/MyriadPro-Bold.woff') format('woff'), url('../fonts/MyriadPro-Bold.ttf') format('truetype'), url('../fonts/MyriadPro-Bold.svg') format('svg');
	font-weight: normal;
	font-style: normal;
}



img,object,embed,video{max-width:100%;display:block;width:100%}
.ie6 img{width:100%;display:block}
body { position: relative;}
body,div,ul,li,h1,h2,h3,h4,h5,h6,p,blockquote,th,td,img{ font-family:'MyriadPro-Regular',Sans-Serif; margin:0;padding:0;border:0; color: #555;}
.clear{clear:both}
*{outline:none}
.hide{display:none}
::-moz-selection{background:none repeat scroll 0 0 #70c831;color:#FFF}
label { margin: 0px;}

textarea, input:matches( [type="email"], [type="number"], [type="password"], [type="search"], [type="tel"], [type="text"], [type="url"] ) { -webkit-appearance: none;}
input[type="number"] {-moz-appearance: textfield;}
input[type="number"]:hover, input[type="number"]:focus { -moz-appearance: number-input;}


h1 { font-family:'MyriadPro-Regular',Sans-Serif; font-size: 45px; color: #555; line-height: 45px;}
h2 { font-family:'MyriadPro-Regular',Sans-Serif; font-size: 35px; color: #555; line-height: 40px;}
h3 { font-family:'MyriadPro-Regular',Sans-Serif; font-size: 30px; color: #555; line-height: 35px;}
h4 { font-family:'MyriadPro-Regular',Sans-Serif; font-size: 25px; color: #555; line-height: 30px;}
h5 { font-family:'MyriadPro-Regular',Sans-Serif; font-size: 20px; color: #555; line-height: 25px;}
h6 { font-family:'MyriadPro-Regular',Sans-Serif; font-size: 15px; color: #555; line-height: 20px;}

.Open-Sans {font-family:'Open-Sans-regular',Sans-Serif !important;}
.MyriadPro-Regular {font-family:'MyriadPro-Regular',Sans-Serif !important;}
.MyriadPro-Bold {font-family:'MyriadPro-Bold',Sans-Serif !important;}

p {font-family:'Open-Sans-regular',Sans-Serif; font-size:15px; color:#555; line-height:22px; text-align: left; padding:5px 0}

a { color: #505e7a; text-decoration: none; -moz-transition: all 0.6s ease 0s; -o-transition: all 0.6s ease 0s; -webkit-transition: all 0.6s ease 0s; transition: all 0.6s ease 0s; }
a:hover { color: #000; text-decoration: underline; }

::-webkit-input-placeholder{color:#000}
:-moz-placeholder{color:#000}
::-moz-placeholder{color:#000}
:-ms-input-placeholder{color:#000; }

.text-center{text-align:center}
.text-right{text-align:right !important}
.text-white{color:#FFF !important}
.text-black{color:#555 !important}
.text-ash{ color: #999 !important;}
.text-green{color:#32b16e !important}
.text-blue{color:#0072bb !important}
.text-yellow{color:#f7941d !important}
.text-light-green{color:#42c17e !important}
.text-red { color: #da1f27 !important;}
.text-violet { color: #7d1771 !important;}
.text-orange { color: #e94c26 !important;}
.text-capital { text-transform: uppercase !important;}
.bold { font-weight: bold;}

.width-auto { width: auto !important;}

input::-webkit-input-placeholder, textarea::-webkit-input-placeholder {color: #777;opacity: 0.5;}
.text-field::-ms-expand {display: none;}

.wrapper-main{width:100%; height:auto; min-height: 100vh; overflow: hidden; background: #f9ffff; padding-bottom: 8vh;}

.login-admin { margin-left: 10px; cursor: pointer;}
.col.w-auto.admin-pic { max-width: 59px !important; }
.admin-text { font-size: 14px; display: flex; justify-content:center !important; flex-direction:column; padding: 0px 20px 0px 10px }
.admin-text p i { padding-left:10px;}

.cm-field-main { margin-bottom: 15px;}
.cm-field-main p { font-size: 12px; line-height: 14px; padding: 0px 0px 5px;}
.cm-field-main .input-field { width: 100%; height: 36px; font-size: 14px; line-height: 20px; border: 0px; background: #fcfcfc; padding: 0px 15px; border-radius: 5px; }
.cm-field-main .field-big {width: 100%; height: 100px; font-size: 14px; line-height: 20px; border: 0px; background: #fcfcfc; padding: 5px 15px; border-radius: 5px; border:1px solid #CCC;}

.cm-field-main select { width: 100%; height: 36px; font-size: 14px; line-height: 30px; color: #555; text-indent: 0.01px; padding: 0px 15px; border: 0px solid #e4e4e4; -moz-appearance: none; -webkit-appearance: none; -0-appearance: none;  border-radius: 5px; cursor: pointer; background:url(../images/down-arrow-ash.png) no-repeat right 15px center / 13px #FFF;}
.cm-field-main select > option { font-size: 13px; padding: 10px 14px !important; border: 0px !important; display: block !important; line-height: 40px !important;}

.cm-field-main.position-relative { text-align: center;}
.cm-field-main.position-relative i { position:absolute; left: 15px; top: 30%; z-index: 9; color: #8781bd;}
.cm-field-main.position-relative .field-overlap-text { height: 34px; position:absolute; left: 1px; top: 20px; z-index: 9; font-size: 13px; color: #8781bd; background: #eee; padding: 7px 9px 0px; border-radius: 5px 0px 0px 5px;}
.cm-field-main.position-relative .input-field { padding-left: 35px;}

.cm-field-main.position-relative.p-icon-left i { left: 15px; top: 55%; right: auto;}
.cm-field-main.position-relative.p-icon-left .input-field { padding-left: 15px !important;}
.cm-field-main.position-relative.p-icon-right i { right: 15px; top: 55%; left: auto;}
.cm-field-main.position-relative.p-icon-right .input-field { padding: 0px 15px !important;}



/*.cm-field-main.cm-field { padding: 0px 0px;}*/
/*.cm-field-main.cm-field p { font-size: 12px; line-height: 14px; padding: 0px 0px 5px;}*/
.cm-field-main.cm-field .input-field { background: #FFF;  border: 1px solid #e4e4e4; border-radius: 5px; }

.cm-field-main.cm-field .field-big {width: 100%; height: 100px; font-size: 14px; line-height: 20px; border: 0px; background: #FFF; padding: 5px 15px; border-radius: 5px; border:1px solid #e4e4e4;}

.cm-field-main.cm-field select { border: 1px solid #e4e4e4;}
.cm-field-main.cm-field select > option { font-size: 13px; padding: 10px 14px !important; border: 0px !important; display: block !important; line-height: 40px !important;}



.input-field.calendar { background: url(../images/calendar.jpg) no-repeat center right 10px / 14px #FFF !important; padding: 0px 15px !important; cursor: pointer;}

.field-btn { width: 100% !important; font-size: 14px; line-height: 20px; color: #FFF; background: #395C6A; font-weight: bold; padding: 10px 30px 6px; border-radius: 10px; margin-right: 10px; display: inline-block; text-decoration: none; text-transform: uppercase; letter-spacing: 1px; border: 0px;}

.field-btn:hover {color: #FFF; background: #8793D6;}

.cm-field-btn { margin-top: 27px;}
.cm-field-btn .field-btn { padding: 10px 30px;}

/*------: BUTTONS :------*/

.CM.field-btn, .CM.filter-btn { background: #0072bc;}
.Primary.field-btn, .Primary.filter-btn { background: #007bf7;}
.Secondary.field-btn, .Secondary.filter-btn { background: #6b757c;}
.Success.field-btn , .Success.filter-btn{ background: #00a752;}
.Warning.field-btn, .Warning.filter-btn { background: #ffbe40;}
.Danger.field-btn, .Danger.filter-btn { background: #ff7f7f;}
.Error.field-btn, .Error.filter-btn { background: #e41d49;}
.field-btn:hover, .filter-btn:hover { background: #8793D6;}


 a.field-btn:hover { color: #fff; text-decoration: none;}


/*------: LOGIN PAGE :------*/

.login-left { height: 100vh; background: url(images/login-bg.jpg) no-repeat center center / cover; position: relative;}
.login-right { height: 100vh; background: #FFF; text-align:center; justify-content:center !important;flex-direction: column; }
.login-left h1 { width: 100%; color: #FFF; position: absolute; bottom: 0px; padding: 300px 80px 80px;
background: -moz-linear-gradient(top,  rgba(247,247,255,0) 0%, rgba(211,233,245,0) 15%, rgba(8,156,191,1) 99%, rgba(6,155,190,1) 100%);
background: -webkit-linear-gradient(top,  rgba(247,247,255,0) 0%,rgba(211,233,245,0) 15%,rgba(8,156,191,1) 99%,rgba(6,155,190,1) 100%);
background: linear-gradient(to bottom,  rgba(247,247,255,0) 0%,rgba(211,233,245,0) 15%,rgba(8,156,191,1) 99%,rgba(6,155,190,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00f7f7ff', endColorstr='#069bbe',GradientType=0 );}
.login-left h1 span { font-size: 30px;}
.login-main { width: 400px; height:auto; padding: 0px 40px 0px 40px; border-radius: 10px; position: relative;}
.login-logo img { width: 150px; padding-bottom: 10px;}
.login-main .cm-field-main .input-field { width: 100%; height: 40px; border: 1px solid #ddd; border-radius: 10px;}
/*.close-btn:hover { background: #F00;}*/

.log-remb { font-size: 12px; text-align: left;}
.log-remb label { cursor: pointer;}
.log-remb input[type="checkbox"] { display: none;}
.log-remb input[type="checkbox"] + label span { width: 26px; height: 26px; margin-top: -3px; vertical-align: middle; display: inline-block; border-radius: 50%; border: 1px solid #e4e4e4; background: #fff url("images/tick-ash.png") no-repeat scroll center center;}
.log-remb input[type="checkbox"]:checked + label span { background: #0072bc url("images/tick-white.png") no-repeat scroll center center; border: 0px;}
.log-remb input[type="checkbox"] + label:hover span { border: 1px solid #0072bc;}
.log-forgot  { text-align: right;}
.log-forgot a { font-size: 12px; text-align: right; position: relative; top: -3px;}
.remember-set { padding: 20px 0px 15px 0px;}
input:focus::placeholder {color: transparent;}

footer { width: 100%; position: absolute; right: 0; bottom: 20px; left: 0;}
.az-section { width: 170px; margin: 0 auto;}
.az-section p { font-family:'Open-Sans-regular',Sans-Serif; font-size: 13px; text-align: right; float: right;}
.azinova-logo { width: 75px; height: 15px; float: right; background: url(../images/azinova.png) no-repeat left top / 150px; cursor: pointer; margin-top: 9px; margin-left: 10px;
-moz-transition: all 0.6s ease 0s; -o-transition: all 0.6s ease 0s; -webkit-transition: all 0.6s ease 0s; transition: all 0.6s ease 0s;}
.azinova-logo:hover {  background: url(../images/azinova.png) no-repeat left -75px top  / 150px;}

</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>

<div class="wrapper-main pb-0">

  <div class="row login-section m-0">
          <div class="col-lg-8 col-md-6 login-left p-0">
               <!--<h1 class="MyriadPro-Bold text-capital"><span class="MyriadPro-Regular">Welcome to</span><br>Church Management</h1>-->
          </div>
          <div class="col-lg-4 col-md-6 login-right d-flex mx-auto my-auto">

              <div class="login-main mx-auto">
                     <div class="col-sm-12 login-logo pb-0"><img src="<?= check_and_get_img_url('./uploads/images/settings/'.$settings->login_page_logo, 'login-page-logo.png'); ?>" class="mx-auto"></div>
                     <div id="login-error"><p>&nbsp;</p></div>
                     <?php echo form_open(base_url('login'), array('id' => 'login-form')); ?>
                     <?php echo isset($error) ? '<div class="error">' . $error . '</div>' : ''; ?>
                     <input type="hidden" name="login" value="1" />
                     <div class="col-sm-12 cm-field-main position-relative p-0">
                          <i class="fa fa-user"></i>
                     	  <input id="username" name="username" value="" class="input-field" placeholder="Username" type="text">
                          <!--<input id="username" name="username" value="" class="login username-field" placeholder="Username"  type="text"/>-->
                     </div>

                     <div class="col-sm-12 cm-field-main position-relative p-0">
                          <i class="fa fa-unlock-alt"></i>
                     	  <input id="password" name="password" value="" placeholder="Password" class="input-field" type="password">
                          <!--<input id="password" name="password" value="" placeholder="Password" class="login password-field" type="password"/>-->
                     </div>

                     <div class="col-sm-12 remember-set p-0">
                          <div class="col-md-6 log-remb p-0">

                              <input id="option-01" name="Field" type="checkbox">
                              <label for="option-01"> <span class="border-radius-3"></span> &nbsp; Remember Me</label>

                              <!--<input id="Field" name="Field" class="field login-checkbox" value="First Choice" tabindex="4" type="checkbox"/>
                              <label for="Field" class=""> <span></span> &nbsp; Remember Me</label>-->

                          </div>
                          <div class="col-md-6 log-forgot p-0">
                              <!--<a href="#">Forgot Password...?</a>-->
                          </div>
                     </div>
                     <div class="col-sm-12 cm-field-btn p-0">
                             <input type="submit" id="submit-button" class="text-field-but field-btn font-weight-normal col-sm-12" value="Login">
                             <!--<button class="text-field-but">Sign In</button>-->
                     </div>

                     </form>


                </div>

              <footer>
                 <div class="col-md-12 text-center">
                      <p class="no-padding text-center" style="font-size: 12px;"><?=$settings->site_footer_copyright_line_html;?></p>
                 </div>
              </footer>
    </div>
     </div>
<script type="text/javascript" src="js/jquery-3.6.0.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
</div>
<script>
$('#login-form input').on('input',function(e){
   $('#login-error p').html('&nbsp;');
});
  $("#login-form").submit(function(e) {
    $("#submit-button").prop('disabled', true).val('Authenticating...');
    e.preventDefault();
    var form = $(this);
    var action_url = form.attr('action');
    $.ajax({
        type: "POST",
        url: action_url,
        data: form.serialize(), // serializes the form's elements.
        success: function(data)
        {
          if(data.status == true){
            $('#submit-button').val('Logging in...');
            window.location.replace(data.redirect);
          }
          else{
            $("#login-error").html('<p class="text-center text-danger animate__animated animate__headShake">'+data.message+'</p>');
            $("#submit-button").prop('disabled', false).val('Login');
          }
        },
        error: function(data)
        {
          $("#login-error").html('<p class="text-center text-danger animate__animated animate__headShake">'+data.statusText+'</p>');
          $("#submit-button").prop('disabled', false).val('Login');
        }
    });
});
</script>