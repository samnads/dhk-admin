<div class="row m-0">   
    <div class="col-sm-12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form class="form-horizontal" method="post">
                <ul>
                <li>
                    <i class="icon-th-list"></i>
                    <h3>User Login Info</h3>
                    </li>
                    <li class="mr-0 float-right">
                    <div class="topiconnew border-0 green-btn"> <a href="<?php echo base_url(); ?>users" title="Users"> <i class="fa fa-users"></i></a> </div></li>
                    </ul>
                    
                </form>   
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px;"> Sl No.</th>
                            <th style="line-height: 18px;"> Name</th>
                            <th style="line-height: 18px;"> Login IP</th>
                            <th style="line-height: 18px;"> Agent</th>                            
                            <th style="line-height: 18px;"> Login Date Time</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(!empty($userlogininfo))
                        {
                            $i = 0;
                            
                            foreach ($userlogininfo as $userlogin)
                            {
                                echo '<tr>'
                                        . '<td style="line-height: 18px;">' . ++$i . '</td>'
                                        . '<td style="line-height: 18px;">' . $userlogin->user_fullname . '</td>'
                                        . '<td style="line-height: 18px;">' . $userlogin->login_ip . '</td>'
                                        . '<td style="line-height: 18px;">' . $userlogin->user_agent . '</td>'
                                        . '<td style="line-height: 18px;">' . $userlogin->login_date_time . '</td>'
                                    .'</tr>';
                            }
                        }
                        else
                        {
                            //echo '<tr><td colspan="5">No Results!</td></tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>
