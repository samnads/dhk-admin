<style>
.finished {
	width: 50px;
	height: auto;
background: url(<?php echo base_url();
?>images/complete.png) no-repeat left top / cover;
	display: block;
	margin: 0 auto;
	height: 30px;
	width: 30px;
}
.not-started {
	width: 50px;
	height: auto;
background: url(<?php echo base_url();
?>images/not-start.png) no-repeat left top / cover;
	display: block;
	margin: 0 auto;
	height: 30px;
	width: 30px;
}
.on-going {
	width: 50px;
	height: auto;
background: url(<?php echo base_url();
?>images/on-going.png) no-repeat left top / cover;
	display: block;
	margin: 0 auto;
	height: 30px;
	width: 30px;
}
.cancelled {
	width: 50px;
	height: auto;
background: url(<?php echo base_url();
?>images/cancel.png) no-repeat left top / cover;
	display: block;
	margin: 0 auto;
	height: 30px;
	width: 30px;
}

.n-details-list-view { display: none;}
.n-details-list-view, .n-details-grid-view { width: 100% !important;}
</style>

<div class="row m-0">
  <div class="col-md-12">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/owl.carousel.css"/>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-3.6.0.min.js"></script> 
    <script type="text/javascript" src="<?php echo base_url(); ?>js/owl.carousel.js"></script> 
    <script>
$(document).ready(function() {

	var owl = $("#owl-demo");
	owl.owlCarousel({
	items : 3, //10 items above 1000px browser width
	itemsDesktop : [1200,2], //5 items between 1000px and 901px
	itemsDesktopSmall : [900,2], // 3 items betweem 900px and 601px
	itemsTablet: [600,2], //2 items between 600 and 0;
	itemsMobile : [320,2], // itemsMobile disabled - inherit from itemsTablet option	
    });
  
  $(".next").click(function(){
    owl.trigger('owl.next');
  });
  
  
  $(".prev").click(function(){
    owl.trigger('owl.prev');
  });
  
  
  
  $('.grid-view-btn').click(function(){
	  $(".n-details-list-view").hide(500);
	  $(".n-details-grid-view").show(500);
  });
  
  
  $('.list-view-btn').click(function(){
	  $(".n-details-grid-view").hide(500);
	  $(".n-details-list-view").show(500);
  });

	
});
</script>
    <div class="row m-0">
      <div class="col-md-12 p-0">
           <div class="widget-header">
           <form class="form-horizontal" method="post">
     <div class="book-nav-top">
            <ul>
              <li><i class="icon-th-list"></i><h3>Activity</h3></li>
              
              <li class="mr-2">
                  <input type="text" style="width: 160px;" id="vehicle_date" name="service_date" value="<?php echo $service_date ?>">
              </li>
      
      
      
              <li>
                  <input type="hidden" id="formatted-date" value="<?php echo $formatted_date ?>"/>
              </li>
      
      
      
              <li>
                  <input type="submit" class="n-btn" value="Go" name="vehicle_report">
              </li>
      
      
      
              <li>
                  <a id="synch-to-odoo-new" href="#" class="n-btn">Synchronize</a>
              </li>
      
      
      
              <li class="mr-0 float-right">
                  <div class="topiconnew border-0 green-btn" title="Print"> <a href="<?php echo base_url() . 'reports/activity_summary_view/' . $formatted_date; ?>" target="_blank"><i class="fa fa-print"></i></a> </div>
                  
                  <div class="topiconnew border-0 green-btn" title="Download to Excel"> <a href="<?php echo base_url(); ?>activity/plan_export/<?php echo $formatted_date; ?>" target="_blank"> <i class="fa fa-file-excel-o"></i></a> </div>
                  
                <div class="topiconnew border-0 green-btn list-view-btn" title="List View"> <i class="fa fa-bars"></i> </div>
                
                <div class="topiconnew border-0 green-btn grid-view-btn" title="Grid View"> <i class="fa fa-th"></i> </div>
              </li>

              <div class="clear"></div>
            </ul>
     </div>
     </form>
</div>

      </div>

      
      
      
      
      <div class="col-md-12 p-0  n-details-grid-view">
        <div class="row m-0">
          <div id="owl-demo" class="owl-carousel">
            <div class="item">
              <h2>Zone A</h2>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="purple">One day</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="orange">Bi-Week</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="green">Weekly</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="orange">Bi-Week</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="orange">Bi-Week</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="orange">Bi-Week</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="purple">One day</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 zone-bottom-space">&nbsp;</div>
            </div>
            
            <div class="item">
              <h2>Zone B</h2>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="green">Weekly</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="orange">Bi-Week</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="orange">Bi-Week</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="orange">Bi-Week</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="purple">One day</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="purple">One day</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="orange">Bi-Week</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 zone-bottom-space">&nbsp;</div>
            </div>
            
            <div class="item">
              <h2>Zone C</h2>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="orange">Bi-Week</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="orange">Bi-Week</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="purple">One day</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="purple">One day</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="orange">Bi-Week</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="green">Weekly</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="orange">Bi-Week</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 zone-bottom-space">&nbsp;</div>
            </div>
            
            <div class="item">
              <h2>Zone D</h2>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="purple">One day</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="orange">Bi-Week</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="green">Weekly</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="orange">Bi-Week</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="orange">Bi-Week</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="orange">Bi-Week</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="purple">One day</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 zone-bottom-space">&nbsp;</div>
            </div>
            
            <div class="item">
              <h2>Zone E</h2>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="green">Weekly</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="orange">Bi-Week</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="orange">Bi-Week</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="orange">Bi-Week</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="purple">One day</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="purple">One day</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="orange">Bi-Week</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 zone-bottom-space">&nbsp;</div>
            </div>
            
            <div class="item">
              <h2>Zone F</h2>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="orange">Bi-Week</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="orange">Bi-Week</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="purple">One day</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="purple">One day</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="orange">Bi-Week</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="green">Weekly</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="orange">Bi-Week</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 zone-bottom-space">&nbsp;</div>
            </div>
            
            <div class="item">
              <h2>Zone G</h2>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="purple">One day</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="orange">Bi-Week</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="green">Weekly</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="orange">Bi-Week</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="orange">Bi-Week</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="orange">Bi-Week</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="purple">One day</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 zone-bottom-space">&nbsp;</div>
            </div>
            
            <div class="item">
              <h2>Zone H</h2>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="green">Weekly</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="orange">Bi-Week</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="orange">Bi-Week</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="orange">Bi-Week</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="purple">One day</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="purple">One day</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="orange">Bi-Week</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 zone-bottom-space">&nbsp;</div>
            </div>
            
            <div class="item">
              <h2>Zone I</h2>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="orange">Bi-Week</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="orange">Bi-Week</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="purple">One day</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="purple">One day</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="orange">Bi-Week</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="green">Weekly</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 plan-thumb-main">
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text plan-thumb-number pl-0 pr-0">
                    <p>EL 71568 <span class="orange">Bi-Week</span></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text plan-thumb-status-box border-right-0 n-plan-completed text-center p-0"> <a title="Not-Started" href="javascript:void(0)" data-bind="" style="text-decoration:none;" onClick="get_activity(0, 92127, this);"> <span class="not-started"></span>
                    <label>Not Started</label>
                    </a> </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text">
                    <p><span>Maid Name</span>Ma Cecilia Regis Villaver</p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Customer Name</span>Mathew Thomas</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set m-0">
                  <div class="col-md-6 plan-thumb-text n-time-section">
                    <p><span>Working Hours</span> 08:30
                      <label>AM</label>
                      &nbsp; 06:00
                      <label>PM</label>
                      <strong class="text-red"> (10 hrs)</strong></p>
                  </div>
                  <div class="col-md-6 plan-thumb-text border-right-0">
                    <p><span>Method of payment</span>Credit Card</p>
                  </div>
                </div>
                <div class="row plan-thumb-cont-set border-bottom-0 m-0">
                  <div class="col-md-4 plan-thumb-text plan-thumb-amount light-green-bg">
                    <p><span>Billed Amount</span>
                      <label>AED</label>
                      4865</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-collected light-blue-bg">
                    <p><span>Collected Amount</span>
                      <label>AED</label>
                      4000</p>
                  </div>
                  <div class="col-md-4 plan-thumb-text plan-thumb-pending border-right-0 light-red-bg">
                    <p><span>Pending Amount</span>
                      <label>AED</label>
                      865</p>
                  </div>
                </div>
              </div>
              <div class="col-md-12 zone-bottom-space">&nbsp;</div>
            </div>
          </div>
        </div>
      </div>
      
      
      <div class="widget widget-table action-table n-details-list-view" style="margin-bottom:30px">
      <div class="widget-content" style="margin-bottom:30px">
        <table class="table da-table table-hover" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th style="line-height: 18px; width: 10px; text-align: center;"> No.</th>
              <th style="line-height: 18px;"> Maid</th>
              <th style="line-height: 18px;"> Customer</th>
              <th style="line-height: 18px;"> Working Hours</th>
              <th style="line-height: 18px;"> M.O.P</th>
              <th style="line-height: 18px;"> Billed Amount</th>
              <th style="line-height: 18px;"> Collected Amount</th>
              <th style="line-height: 18px;"> Balance Amount</th>
              <th style="line-height: 18px;"> Receipt No</th>
              <th style="line-height: 18px;"> Status</th>
              <!--<th style="line-height: 18px; width: 123px;"> Actions</th>--> 
              
            </tr>
          </thead>
          <tbody>
            <?php
                        if(!empty($dayservices))
                        {
                            $i = 0;
                            $zone_id = 0;                            
                            $total_fee = 0;
                            $material_fee = 0;
                            $collected_total_fee = 0;
                            $total_wrk_hrs = 0;
                            $collected_amount = 0;
                            $ztotal_wrk_hrs = 0;
                            foreach ($dayservices as $service)
                            {
								$check_buk_exist = $this->bookings_model->get_booking_exist($service->booking_id,$formatted_date);
								if(!empty($check_buk_exist))
								{
									$mop = $check_buk_exist->mop;
									//$ref = $check_buk_exist->just_mop_ref;
								} 


                                else {
                                     if($service->pay_by!=""){
                                      $mop = $service->pay_by;
                                     }
                                     else{
									$mop = $service->payment_mode;
                                       }
									//$ref = "";
								}
                                //Payment Type
                                if($service->payment_type == "D")
                                {
                                    $paytype = "(D)";
                                } else if($service->payment_type == "W")
                                {
                                    $paytype = "(W)";
                                } else if($service->payment_type == "M")
                                {
                                    $paytype = "(M)";
                                } else
                                {
                                    $paytype = "";
		                }

                                if($service->maid_status==0)
                                {
                                    if(strtotime($formatted_date)>strtotime($service->maid_disabled_datetime))
                                    {
                                        continue;
                                    }
                                }
                                                                                                                               
//                                $service_status = $service->service_status == 1 ? 'ON GOING' : ($service->service_status == 2 ? 'FINISHED' : ($service->service_status == 3 ? 'CANCELLED' : 'NOT STARTED'));                                
                                $service_status = $service->service_status == 1 ? '' : ($service->service_status == 2 ? '' : ($service->service_status == 3 ? '' : ''));                                
                                $class = $service->service_status == 1 ? 'on-going' : ($service->service_status == 2 ? 'finished' : ($service->service_status == 3 ? 'cancelled' : 'not-started'));
                                $title = $service->service_status == 1 ? 'On-Going' : ($service->service_status == 2 ? 'Finished' : ($service->service_status == 3 ? 'Cancelled' : 'Not-Started'));
                                


//$service_status .=  $service->payment_status == 1 ? (' ' . $service->total_fee) : ($service->payment_status == 0  && $service->service_status == 2 ? ' NP': '');
                                
                                $activity_status = 0;
                                //$activity_status = $service->service_status ? $service->service_status : 0;                                                                
                                $activity_status = $service->service_status == 1 ? 1 : ($service->service_status == 2 ? 2 : ($service->service_status == 3 ? 3 : 0));                                                                
                                $activity_status =  $service->payment_status == 1 ? 5 : ($service->payment_status == 0  && $service->service_status == 2 ? 6: ($service->service_status == 1 && $service->payment_status != NULL  ? 1 : ( $service->service_status == 3 ? 3 : 0)));
                                $service->total_fee = $service->service_status == 2 ? $service->total_fee : '';
                                $service->material_fee = $service->service_status == 2 ? $service->material_fee : '';
								// if($service->t_zone != "")
								// {
									// $service->zone_id = $service->t_zone;
								// } else {
									// $service->zone_id = $service->zone_id;
								// }
				$transfer_text=(strlen($service->transferred_zone)>0)?"<br><b> - Transferred Zone - ".$service->transferred_zone."</b>":"";
                                if($zone_id != $service->zone_id)
                                {
                                    if($ztotal_wrk_hrs != 0)
                                    {

                                        echo '<tr>'
                                                . '<td colspan="3" style="line-height: 18px;"></td>'
                                                . '<td style="line-height: 18px; text-align : right;"><b>' . $ztotal_wrk_hrs . ' Hrs</b></td>'
                                                . '<td colspan="6" style="line-height: 18px;"></td>'
                                                //. '<td style="line-height: 18px;"></td>'
                                             . '</tr>';
                                        $ztotal_wrk_hrs = 0;
                                    }
                                    
                                    $zone_id = $service->zone_id;
                                    echo '<tr>'
                                            . '<td style="line-height: 18px;"></td>'
                                            . '<td style="line-height: 18px;"><b style="color:#CB3636;">' . $service->zone_name . '</b></td>'
                                            . '<td colspan="8" style="line-height: 18px;"></td>'
                                            //. '<td style="line-height: 18px;"></td>'
                                         . '</tr>';
                                    
                                    
                                }
                                if($service->total_fee == ""){
                                    $tot_hrs = ((strtotime($service->end_time) - strtotime($service->start_time))/ 3600);
                                    
                                    if($service->cleaning_material == 'Y')
                                    {
                                        $materialfee = ($tot_hrs * 5);
                                        $materialview = '('.$materialfee.')';
                                    } else {
                                        $materialview = "";
                                    }
                                    //$servicetotalfee = (($service->price_hourly)*((strtotime($service->end_time) - strtotime($service->start_time))/ 3600));total_amount
									$servicetotalfee = $service->total_amount;
                                } else {
                                    $servicetotalfee = $service->total_fee;
                                    
                                    if($service->cleaning_material == 'Y')
                                    {
                                        $materialfee = $service->material_fee;;
                                        $materialview = '('.$materialfee.')';
                                    } else {
                                        $materialview = "";
                                    }
                                    
                                }
                                //$total_fee += $service->total_fee;
								$total_fee += $servicetotalfee;
                                $material_fee += $service->material_fee;
                                $collected_total_fee += $service->collected_amount;
                                $collected_amount += ($service->payment_status == 1 ? $service->total_fee : 0);
                                $total_wrk_hrs += ((strtotime($service->end_time) - strtotime($service->start_time))/ 3600);
                                $ztotal_wrk_hrs += ((strtotime($service->end_time) - strtotime($service->start_time))/ 3600);
                                //$total_wrk_hrs += ($service->service_status == 2 ? ((strtotime($service->end_time) - strtotime($service->start_time))/ 3600) : 0);
                                //$ztotal_wrk_hrs += ($service->service_status == 2 ? ((strtotime($service->end_time) - strtotime($service->start_time))/ 3600) : 0);
                                echo '<tr>'
                                        . '<td style="line-height: 18px;" class="book_type_'.$service->booking_type.'">' . ++$i . '</td>'
                                        . '<td style="line-height: 18px;" class="book_type_'.$service->booking_type.'">' . $service->maid_name . '</td>'
                                        . '<td style="line-height: 18px;" class="book_type_'.$service->booking_type.'">' . $service->customer_name . '('.$service->odoo_customer_id.') <b style="color:#006600;">(' . ($service->actual_zone ? substr($service->actual_zone, 2) : $service->zone_name) . '</b>) '.$paytype.$transfer_text.'</td>'
                                        . '<td style="line-height: 18px; text-align : right;" class="book_type_'.$service->booking_type.'">' . $service->time_from . '-' . $service->time_to . ' [' . ((strtotime($service->end_time) - strtotime($service->start_time))/ 3600) . ']' . '</td>'
										. '<td style="line-height: 18px; text-align : center;" class="book_type_'.$service->booking_type.'">' . $mop .'</td>'
                                        //. '<td style="line-height: 18px; text-align : right;" class="book_type_'.$service->booking_type.'">' . $service->total_fee . '</td>'
                                        . '<td style="line-height: 18px; text-align : right;" class="book_type_'.$service->booking_type.'">' . $servicetotalfee .'</td>'
                                        . '<td style="line-height: 18px; text-align : right;" class="book_type_'.$service->booking_type.'">' . $service->collected_amount . '</td>'
                                        . '<td style="line-height: 18px; text-align : right;" class="book_type_'.$service->booking_type.'">' . $service->balance.$service->signed. '</td>'
                                        . '<td style="line-height: 18px; text-align : right;" class="book_type_'.$service->booking_type.'">' . $service->ps_no . '</td>';
                                  if($service->ps_no != ""){
                                      $pinkslip_no = $service->ps_no;
                                  } else {
                                      $pinkslip_no = "0";
                                  }
                                  if($service->collected_amount != ""){
                                        echo '<td style="line-height: 18px;"><a title='.$title.' href="javascript:void(0)" data-bind="'.$service->collected_amount.'" style="text-decoration:none;" onclick="get_activity_new(' . $activity_status . ', ' . $service->booking_id . ', this, '.$service->collected_amount.', '.$service->service_status.','.$service->payment_status.','.$pinkslip_no.');"><span class="' . $class . '">' . $service_status . '</span></a></td>'; //title="' . $service->service_status . '-' . $service->payment_status . '"
                                  } else {
                                        echo '<td style="line-height: 18px;"><a  title='.$title.'  href="javascript:void(0)" data-bind="'.$service->collected_amount.'" style="text-decoration:none;" onclick="get_activity(' . $activity_status . ', ' . $service->booking_id . ', this);"><span class="' . $class . '">' . $service_status . '</span></a></td>'; //title="' . $service->service_status . '-' . $service->payment_status . '"
                                  }
                                        /*. '<td style="line-height: 18px;">'
                                        . '<a class="btn btn-small btn-success start-stop" title="Start" href="javascript:void" ' . $play . '><i class="icon-play"> </i></a>'
                                        . '<a class="btn btn-small btn-info start-stop" title="Stop" href="#payment-modal" data-toggle="modal" ' . $stop . '><i class="icon-stop"> </i></a>'
                                        . '<a class="btn btn-small btn-warning start-stop" title="Transfer" href="#transfer-modal" data-toggle="modal" ' . $transfer . '><i class="icon-share"> </i></a>'
                                        . '<a class="btn btn-small btn-danger start-stop" title="Cancel" href="javascript:void" ' . $cancel . '><i class="icon-ban-circle"> </i></a>'
                                        . '</td>'*/
                                   echo '</tr>';
                                
                                
                            }
                            echo '<tr style="font-weight:bold;">'
                                    . '<td style="line-height: 18px;"></td>'
                                    . '<td style="line-height: 18px;"></td>'
                                    . '<td style="line-height: 18px;">TOTALS</td>'
                                    . '<td style="line-height: 18px; text-align : right;">' . $total_wrk_hrs . '</td>'
									. '<td style="line-height: 18px;"></td>'
                                    . '<td style="line-height: 18px; text-align : right;">' . number_format($total_fee, 2) . '('. number_format($material_fee, 2) .')</td>'
                                    . '<td style="line-height: 18px; text-align : right;">' . number_format($collected_total_fee, 2) . '</td>'
                                    . '<td style="line-height: 18px; text-align : right;">' . /*number_format($collected_amount, 2) . */'</td>'
                                    //. '<td style="line-height: 18px;"></td>'
                                .'</tr>';
                        }else {
                            ?>
            <tr>
              <td colspan="9"  style="line-height: 18px; text-align:justify;">No records found</td>
            </tr>
            <?php
                        }
                        
                        ?>
          </tbody>
        </table>
      </div>
      <!-- /widget-content --> 
      
    </div>
    </div>
    
    
    <!-- /widget --> 
  </div>
  <!-- /span12 --> 
</div>
<div id="activity-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 style="color:white; padding-bottom: 0px !important;">Action</h3>
  </div>
  <div class="modal-body">
  <form >
    <p> <a class="btn btn-medium btn-success" id="start" title="Start" href="javascript:void(0)" ><i class="icon-play"> </i>START</a> <a class="btn btn-medium btn-primary" id="restart" title="Restart" href="javascript:void(0)" ><i class="icon-play"> </i>RESTART</a> <a class="btn btn-medium btn-warning" title="Stop" href="javascript:void(0)" ><i class="icon-stop"> </i>STOP</a> <a class="btn btn-medium btn-info" title="Transfer" href="javascript:void(0)"><i class="icon-share"> </i>TRANSFER</a> 
      <!--<a class="btn btn-medium btn-danger" title="Cancel" href="javascript:void" ><i class="icon-ban-circle"> </i>CANCEL</a>--> 
    </p>
    <div class="controls" id="payment-details">
      <label class="radio inline">
        <input id="paymnt" type="radio" name="payment_type" value="1">
        Payment </label>
      <label class="radio inline">
        <input id="no-paymnt" type="radio" name="payment_type" value="0">
        Payment Not Received </label>
      <label class="radio inline">
        <input id="no-service" type="radio" name="payment_type" value="3">
        Service Not Done </label>
    </div>
    <div style="padding: 15px 0; display:none;" id="pinkid">
      <label class="inline" style="display: inline-block;">
        <input class="inline" type="text" id="paid-amount" style="width: 50px;"/>
      </label>
      <label class="inline" style="display: inline-block; padding-left: 15px;">PS No
        <input type="text" id="ps-no" style="width: 100px;"/>
      </label>
    </div>
    <p id="frm-transfer"> 
      <!--<form id="frm-transfer">--> 
      <br>
      <select id="transfer-zone-id" required>
        <option value="">-- Select Driver --</option>
        <?php
                    foreach ($tablets as $val)
                    {
                        echo '<option value="' . $val->tablet_id . '">' . $val->tablet_driver_name . '</option>';
                    }

                    ?>
      </select>
      <!--</form>--> 
    </p>
    </div>
    <div class="modal-footer">
      <button class="btn red-btn" data-dismiss="modal" aria-hidden="true">Close</button>
      <button class="btn green-btn" href="javascript:void(0)" type="submit" onclick="add_activity();">Submit</button>
    </div>
  </form>
</div>
