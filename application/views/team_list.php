<div class="row">
    <div class="span6" id="edit_team" style="display: none;">      		
	<div class="widget ">
	    <div class="widget-header">
        <ul>
        <li>
	      	<i class="icon-globe"></i>
                    <h3>Edit Team</h3></li>
                    <li class="mr-0 float-right">
                    
                    
                    <div class="topiconnew border-0 green-btn">
                       <a onclick="hideedit_team();" title="Hide"> <i class="fa fa-minus"></i></a>
                    </div> 
                    
                    </li>
                    </ul>
            </div> <!-- /widget-header -->
	
            <div class="widget-content">
		<div class="tabbable">
                    <div class="tab-content">
                        <form id="team" class="form-horizontal" method="post">
                            <fieldset>
                                    <div class="control-group">											
					<label class="control-label" for="teamname">Team Name</label>
                                            <div class="controls">
                                                <input type="text" class="span6" style="width: 300px;" id="edit_teamname" name="edit_teamname" required="required">
                                                <input type="hidden" class="span6" style="width: 300px;" id="edit_teamid" name="edit_teamid">
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
										
<!--                                    <div class="control-group">											
					<label class="control-label" for="drivername">Driver Name</label>
                                            <div class="controls">
                                                <input type="text" class="span6" style="width: 300px;" id="edit_drivername" name="edit_drivername" required="required">
                                            </div>  /controls 				
                                    </div>  /control-group -->
                                    
                                    
<!--                                    <div class="control-group">											
                                        <label class="control-label">Is Spare Zone</label>
                                            <div class="controls">
                                                <label class="radio inline"><input type="radio"  name="edit_spare" value="Y"> Yes</label>
                                                <label class="radio inline"><input type="radio" name="edit_spare" value="N"> No</label>
                                            </div>	 /controls 			
                                    </div>  /control-group -->
										
                                    <br />
										
                                    <div class="form-actions">
                                        <input type="submit" class="btn mm-btn pull-right" value="Submit" name="team_edit">
<!--					<button class="btn">Cancel</button>-->
                                    </div> <!-- /form-actions -->
                            </fieldset>
                        </form>
			
                    </div>
		</div>
            </div> <!-- /widget-content -->
	</div> <!-- /widget -->
    </div> <!-- /span6 -->
    
    <!-- /span6 --> 
    <div class="span6" id="add_team" style="display: none;">      		
	<div class="widget ">
	    <div class="widget-header">
	      	<i class="icon-globe"></i>
                    <h3>Add Team</h3>
                    <a style="float:right ; margin-right:15px; cursor:pointer;" onclick="hideadd_team();"><img src="<?php echo base_url();?>img/minus.png" title="Hide"/></a>
            </div> <!-- /widget-header -->
	
            <div class="widget-content">
		<div class="tabbable">
                    <div class="tab-content">
<!--                        <form id="zone" class="form-horizontal" method="post" action="<?php //echo base_url();?>zones/add_zone">-->
                        <form id="team" class="form-horizontal" method="post">
                            <fieldset>
                                    <div class="control-group">											
					<label class="control-label" for="teamname">Team Name</label>
                                            <div class="controls">
                                                <input type="text" class="span6" style="width: 300px;" id="teamname" name="teamname" required="required">
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
										
<!--                                    <div class="control-group">											
					<label class="control-label" for="drivername">Driver Name</label>
                                            <div class="controls">
                                                <input type="text" class="span6" style="width: 300px;" id="drivername" name="drivername" required="required">
                                            </div>  /controls 				
                                    </div>  /control-group -->
                                    
                                    
<!--                                    <div class="control-group">											
                                        <label class="control-label">Is Spare Zone</label>
                                            <div class="controls">
                                                <label class="radio inline"><input type="radio"  name="spare" checked="checked" value="Y"> Yes</label>
                                                <label class="radio inline"><input type="radio" name="spare" value="N"> No</label>
                                            </div>	 /controls 			
                                    </div>  /control-group -->
										
                                    <br />
										
                                    <div class="form-actions">
                                        <input type="submit" class="btn mm-btn pull-right" value="Submit" name="team_sub">
<!--					<button class="btn">Cancel</button>-->
                                    </div> <!-- /form-actions -->
                            </fieldset>
                        </form>
			
                    </div>
		</div>
            </div> <!-- /widget-content -->
	</div> <!-- /widget -->
    </div> <!-- /span6 -->
    
    <div class="span12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Team</h3>
              <a style="float:right ; margin-right:15px; cursor:pointer;" onclick="add_team();"><img src="<?php echo base_url();?>img/add.png" title="Add Zone"/></a>
                    
            </div>
            <!-- /widget-header -->
             <div class="widget-content">
               
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th style="line-height: 18px;"> Sl.No. </th>
                            <th style="line-height: 18px"> Team Name </th>
                            <!--<th style="line-height: 18px"> Driver Name</th>-->
                            <th style="line-height: 18px" class="td-actions">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($teams) > 0) {
                            $i = 1;
                            foreach ($teams as $team_val) {
                                ?>  
                        <tr>
                            <td style="line-height: 18px; width: 100px; text-align: center;"><?php echo $i; ?></td>
                            <td style="line-height: 18px"><?php echo $team_val->team_name; ?></td>
                            <!--<td style="line-height: 18px"><?php// echo $zones_val['driver_name'] ?></td>-->
                            <td style="line-height: 18px" class="td-actions">
                                <a href="javascript:;" class="btn btn-small btn-warning" onclick="edit_team(<?php echo $team_val->team_id; ?>);"><i class="btn-icon-only icon-pencil"> </i></a>
                                <!--<a href="javascript:;" class="btn btn-danger btn-small" onclick="delete_zone(<?php// echo $zones_val['zone_id'] ?>);"><i class="btn-icon-only icon-remove"> </i></a>-->
                            </td>
                        </tr>
                        <?php
                                $i++;
                            }
                        }
                        ?>   
                        
                    </tbody>
                </table>
                 
            </div>
            <!-- /widget-content --> 
        </div>
        <!-- /widget --> 
    </div>
    
    
    
    
</div>

     