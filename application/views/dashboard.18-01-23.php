<script type="text/javascript" src="https://www.google.com/jsapi"></script>


<link rel="stylesheet" type="text/css" href="css/bootstrap4.css" >
<style>
.container, .container-sm, .container-md, .container-lg, .container-xl { max-width: 100%; }
</style>

<section>
  <div class="row dash-top-wrapper no-left-right-margin">


      <div class="col-md-3 col-sm-3 colour-box-main">
        <div class="colour-box purple">
          <div class="n-icon"><img src="images/jobs.webp"  /></div>
          <h2 class="text-center text-white">One Day Booking</h2>
          <div class="row m-0">
            <div class="col-sm-6 text-right pl-0">
              <div class="total-booking-text">Total Count</div>
              <div class="total-booking-num">246</div>
            </div>
            <div class="col-sm-6 pr-0">
              <div class="total-booking-text">Total Hours</div>
              <div class="total-booking-num">3546</div>
            </div>
          </div>
          <div class="view-all-booking"> <a href="#" title="Create one day booking"><span class="colour-rit-icon no-left-padding"> <i class="fa fa-pencil-square"></i></span> Create
            <div class="clear"></div>
            </a> <a class="text-right" href="#" title="View all one day bookings"> View <span class="colour-rit-icon no-right-padding"><i class="fa fa-arrow-circle-o-right"></i></span>
            <div class="clear"></div>
            </a>
            <div class="clear"></div>
          </div>
        </div>
      </div>
      
      <div class="col-md-3 col-sm-3 colour-box-main">
        <div class="colour-box green">
          <div class="n-icon"><img src="images/scheduled.webp"  /></div>
          <h2 class="text-center text-white">Regular Booking</h2>
          <div class="row m-0">
            <div class="col-sm-6 text-right pl-0">
              <div class="total-booking-text">Total Count</div>
              <div class="total-booking-num">174</div>
            </div>
            <div class="col-sm-6 pr-0">
              <div class="total-booking-text">Total Hours</div>
              <div class="total-booking-num">7234</div>
            </div>
          </div>
          <div class="view-all-booking"> <a href="#" title="Create regular booking"><span class="colour-rit-icon no-left-padding"> <i class="fa fa-pencil-square"></i></span> Create
            <div class="clear"></div>
            </a> <a class="text-right" href="#" title="View all regular bookings"> View <span class="colour-rit-icon no-right-padding"><i class="fa fa-arrow-circle-o-right"></i></span>
            <div class="clear"></div>
            </a>
            <div class="clear"></div>
          </div>
        </div>
      </div>
      
      <div class="col-md-3 col-sm-3 colour-box-main">
        <div class="colour-box red">
          <div class="n-icon"><img src="images/cancel-booking.webp" /></div>
          <h2 class="text-center text-white">Cancel Booking</h2>
          <div class="row m-0">
            <div class="col-sm-6 text-right pl-0">
              <div class="total-booking-text">Total Count</div>
              <div class="total-booking-num">86</div>
            </div>
            <div class="col-sm-6 pr-0">
              <div class="total-booking-text">Total Hours</div>
              <div class="total-booking-num">428</div>
            </div>
          </div>
          <div class="view-all-booking"> <a href="#" title="View all cancel bookings" class="full-width text-center"> View <span class="colour-rit-icon no-right-padding"><i class="fa fa-arrow-circle-o-right"></i></span>
            <div class="clear"></div>
            </a>
            <div class="clear"></div>
          </div>
        </div>
      </div>
      
      <div class="col-md-3 col-sm-3 colour-box-main">
        <div class="colour-box blue">
          <div class="n-icon"><img src="images/customers.webp"  /></div>
          <h2 class="text-center text-white">Customers</h2>
          <div class="row m-0">
            <div class="col-sm-6 text-right pl-0">
              <div class="total-booking-text">Total</div>
              <div class="total-booking-num">3264</div>
            </div>
            <div class="col-sm-6 pr-0">
              <div class="total-booking-text">New</div>
              <div class="total-booking-num">264</div>
            </div>
          </div>
          <div class="view-all-booking"> <a href="#" title="Create new customer"><span class="colour-rit-icon no-left-padding"> <i class="fa fa-pencil-square"></i></span> Create
            <div class="clear"></div>
            </a> <a class="text-right" href="#" title="View all customers"> View <span class="colour-rit-icon no-right-padding"><i class="fa fa-arrow-circle-o-right"></i></span>
            <div class="clear"></div>
            </a>
            <div class="clear"></div>
          </div>
        </div>
      </div>

  </div>
  <!--row content-wrapper end--> 
</section>


<section>
  <div class="row content-wrapper m-0">
    <div class="col-sm-12">
      <div class="row n-box-summary-main m-0">
        <div class="col-sm-7 n-box-summary-left">
          <div class="row colour-box m-0">
            <div class="col-sm-6 colour-box-main n-booking-summary pb-0">
              <div class="colour-box p-0">
                <h2 class="text-black pl-3 strong">Outside Booking</h2>
                <div class="row m-0">
                  <div class="col-sm-6">
                    <div class="total-booking-text text-black">Total Count</div>
                    <div class="total-booking-num text-black">321</div>
                  </div>
                  <div class="col-sm-6">
                    <div class="total-booking-text text-black">Total Hours</div>
                    <div class="total-booking-num text-black">897</div>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="col-sm-6 colour-box-main n-booking-summary pb-0">
              <div class="colour-box p-0">
                <h2 class="text-black pl-3 strong">Partners Booking</h2>
                <div class="row m-0">
                  <div class="col-sm-6">
                    <div class="total-booking-text text-black">Total Count</div>
                    <div class="total-booking-num text-black">157</div>
                  </div>
                  <div class="col-sm-6">
                    <div class="total-booking-text text-black">Total Hours</div>
                    <div class="total-booking-num text-black">6548</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <div class="row  m-0">
            <div class="dash-suppurate-box">&nbsp;</div>
          </div>
          
          <div class="row colour-box m-0">
            <div class="col-sm-3">
              <div class="n-icon"><img src="images/complaints-r.webp"  /></div>
            </div>
            
            <div class="col-sm-9 colour-box-main n-booking-summary pb-0">
              <div class="colour-box p-0">
                <h2 class="text-red pl-3 strong">Complaints</h2>
                <div class="row m-0">
                  <div class="col-sm-4">
                    <div class="total-booking-text text-red">Today</div>
                    <div class="total-booking-num text-red">37</div>
                  </div>
                  <div class="col-sm-4">
                    <div class="total-booking-text text-red">Last Week</div>
                    <div class="total-booking-num text-red">321</div>
                  </div>
                  <div class="col-sm-4">
                    <div class="total-booking-text text-red">This Month</div>
                    <div class="total-booking-num text-red">1897</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-sm-5 n-box-summary-right dark">
          <div class="n-payment-icon">
            <div class="cm-sale-con-graph"><img src="images/money.gif" alt=""></div>
          </div>
          
          <h2 class="text-center text-white pb-3 strong">Payments</h2>
          <div class="row m-0">
            <div class="col-sm-6 text-right">
              <div class="total-booking-text" style="color:#04d712;">Collected</div>
              <div class="total-booking-num" style="color:#04d712;"><span>AED</span> 24587321</div>
            </div>
            <div class="col-sm-6">
              <div class="total-booking-text" style="color:#fd4040;">Pending</div>
              <div class="total-booking-num" style="color:#fd4040;"><span>AED</span> 352856</div>
            </div>
          </div>
        </div>
        
      </div>
    </div>
     
  </div>
</section>





<section class="mt-4">
  <div class="row content-wrapper m-0">
       <div class="col-sm-4">
            <div class="col-sm-12 n-bot-box-main p-0">
                 <h2><i class="fa fa-map-marker">&nbsp;</i> Driver Position</h2>
                 
                 
                 <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d28929.0801388544!2d54.984624777124004!3d24.995527717503638!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f11221e58afed%3A0x27a723be256b7bfe!2sdon%20pollo%20island!5e0!3m2!1sen!2sin!4v1673512273306!5m2!1sen!2sin" width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                 
            </div>
       </div>
       
       
       <div class="col-sm-4">
            <div class="col-sm-12 n-bot-box-main p-0">
                 <h2 class=""><i class="fa fa-calendar">&nbsp;</i> Bookings</h2>
                 
                 <div id="chart-3" class="pt-4"></div>
            </div>
       </div>
       
       
       
       <div class="col-sm-4">
            
                 <div class="col-md-12 n-main-box p-0">
          <div class="col-md-12 n-cont-main-box p-0">
            <div class="col-md-12 col-sm-12 tab-hed no-left-right-padding">
              <h2><i class="fa fa-clock-o">&nbsp;</i> Recent Activity <span class="pull-right"> <a href="<?php echo base_url() ?>reports/useractivity" title="View All Recent Activities"> <img src="images/view-all.webp" class="n-n" /> <img src="images/view-all1.webp" class="n-h" /> </a> </span> </h2>
            </div>
            <!--tab-hed end-->
            
            <div class="col-md-12 col-sm-12 tab-cont no-padding rcnt-actvty-box" >
              <div class="report-cont-det">
                <?php
                if(!empty($recent_activity))
                {
                ?>
                <ul>
                  <?php
                        foreach ($recent_activity as $recent)
                        {
                            
                    ?>
                  <li><a href="#" <?php if(strpos($recent->action_type, 'delete') || strpos($recent->action_type, 'disable') || strpos($recent->action_type, 'Cancel') ) { ?> style="color:#f00;" <?php } else { ?> style="color:#428bca" <?php }?>><?php echo activity_icon($recent->action_type) ?>&nbsp;&nbsp;<?php echo $recent->action_content; ?><span class="activity-time"><i class="fa fa-clock-o"></i> <b><?php echo $recent->user_fullname; ?></b> <i><?php echo time_elapsed_string($recent->addeddate); ?></i> </span>
                    <div class="clear"></div>
                    </a></li>
                  <?php
                        }
                    ?>
                </ul>
                <?php }else { ?>
                <p>No recent activities!</p>
                <?php }?>
              </div>
              <!--report-cont-det end--> 
              
            </div>
            <!--tab-hed end--> 
            
          </div>
        </div>
           
       </div>
  </div>
</section>







<?php 
function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}
?>
<script>
    google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
     var data = new google.visualization.DataTable();
        data.addColumn('date', 'Date');
        data.addColumn('number', 'Schedules');
        data.addColumn({type: 'string', role: 'tooltip'});
        //data.addColumn('number', 'persone2');
        //data.addColumn('number', 'persone3');
        data.addRows([
            <?php
            foreach ($grapharray as $graphval)
            {
            ?>
    [<?php echo $graphval; ?>],
            <?php } ?>
])

        var options = {
          legend: 'none',
          //hAxis: { minValue: 0, maxValue: 9 },
          //curveType: 'function',
          pointSize: 10,
      };

      var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
        chart.draw(data, options);
    
    
    //     var chart = new google.visualization.AnnotatedTimeLine(document.getElementById('chart_div'));
    //  chart.draw(data, {displayAnnotations: true});
      }
    
    </script> 
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js'></script> 
<script src='js/graphiq.js'></script> 
<script  src="js/script.js"></script>

<script type='text/javascript' src="js/jqBarGraph.1.1.js"></script>
<script type="text/javascript">

arrayOfData1 = new Array(
[[20,50,30],'November'],
[[25,40,20],'December'],
[[10,45,30],'January']
);

$('#chart-3').jqBarGraph({data: arrayOfData1,
	colors: ['#8793d6','#7ac255','#fd8e41'],
	legends: ['One Day','Weekly','Bi-Weekly'],
	legend: true,
	//width: 86%,
	//height: 80%,
	color: '#ffffff',
	type: 'multi',
	postfix: '%'
	//title: 'lorum'
});


 </script>