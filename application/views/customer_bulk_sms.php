<style>
.bootstrap-tagsinput {

    background-color: #fff;
    border: 1px solid #ccc;
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    display: inline-block;
    padding: 4px 6px;
    color: #555;
    vertical-align: middle;
    border-radius: 4px;
    max-width: 100%;
    line-height: 22px;
    cursor: text;
    overflow-y: scroll;
    max-height: 300px;
    width:350px;

}
</style>
<div class="row m-0">
    <!-- /span6 --> 
    <div class="col-sm-12" id="add_user" style="float: none; margin: 0 auto;">      		
	<div class="widget ">
	    <div class="widget-header">
        <ul>
        <li>
	      	<i class="icon-globe"></i>
                    <h3>Send Bulk Sms</h3>
                    
                    </li>
                    </ul>
                    
            </div> <!-- /widget-header -->
            
            <div class="col-sm-12 pl-4 bg-white">
            <div class="row m-0">
	
            <div class="col-sm-4 ">
		
                        <form id="sms" action="<?php echo base_url(); ?>customer/bulk_sms" class="form-horizontal" method="post">
                            <fieldset>
                            
                                   
                                    <div class="control-group">
                                        <div class="error" style="text-align: center;">
										 <?php if($this->session->flashdata('sms_success')){ ?>
										 <p style="color:green;text-align:center;"><?php echo $this->session->flashdata('sms_success') ?></p>
										 
										 <?php } ?>
										 <?php if($this->session->flashdata('sms_error')){ ?>
										 <p style="color:red;text-align:center;"><?php echo $this->session->flashdata('sms_error') ?></p>
										 
										 <?php } ?>
										</div>
                                    </div>
                                    
                                    
                                    
                                    
                                    <div class="row m-0 n-field-main">
                                        <p>Customer Type</p>
                                        <div class="col-sm-12 p-0 n-field-box">
                                             <select class="form-control select2" name="customer_type" id="customer_type_bulk">
                                                    <option value="1">Regular</option>
                                                    <option value="0">One-time customer</option>
                                                    <?php foreach ($sms_groups as $sms_group) {?>
                                                     <option value="<?php echo $sms_group->sms_group_id;?>"><?php echo $sms_group->sms_group_name;?></option>   
                                                    <?php }?>
						</select>
                                        </div>
                                    </div>
                  
                                    
                                    
                                    
                                    
                                    
                                    <div class="row m-0 n-field-main">
                                        <p>Mobile number</p>
                                        <div class="col-sm-12 p-0 n-field-box">
                                        <style>
										.bootstrap-tagsinput { width: 100% !important;}
										</style>
                                             <input type="text" name="mobnumber_list" id="mobnumber_list" value="<?php echo $customer_mob_numbers ?>" data-role="tagsinput" style="width: 100% !important;" />
                                        </div>
                                    </div>


                                    
                                    
                                    
                                    
                                    
                                    <div class="row m-0 n-field-main">
                                        <p>Message</p>
                                        <div class="col-sm-12 p-0 n-field-box">
                                             <textarea name="sms_message" id="sms_message" class="form-control"></textarea>
                                        </div>
                                    </div>
									
								   
                                   
                                    
                                    
                                    
                                    
                                    <div class="row m-0 n-field-main">
                                        <div class="col-sm-12 p-0 ">
                                             <input type="submit" class="n-btn col-sm-6" value="Send" name="send_sms">
                                        </div>
                                    </div>
                                    
                                   
										
                                    <div class="form-actions">
                                        

                                    </div> <!-- /form-actions -->
                            </fieldset>
                        </form>
			
            </div> <!-- /widget-content -->
            </div>
            </div>
            
            
	</div> <!-- /widget -->
    </div> <!-- /span6 -->
    
</div>