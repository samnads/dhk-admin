<div class="row">
    <div class="span7">
        <div class="widget ">
        
        <div class="widget-header">
     <div class="book-nav-top">
            <ul>
              <li><i class="icon-th-list"></i><h3>Add Back Payment</h3></li>
              




      
              <li class="mr-0 float-right">
                  <div class="topiconnew border-0 green-btn">
                    	<a href="<?php echo base_url(); ?>" title=""> <i class="fa fa-users"></i></a>
                    </div>
              </li>
      
      
      
              <div class="clear"></div>
            </ul>
     </div>
</div>





            <div class="widget-content">



                <div class="tabbable">


                    <div class="tab-content">

                        <form id="edit-profile" class="form-horizontal" method="post">
                            <div class="alert alert-<?php echo $errors['class']; ?>"
                                style="display: <?php echo !empty($errors) ? 'block' : 'none' ?>">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <?php echo $errors['message']; ?>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="customer">Customer</label>
                                <div class="controls">
                                    <select class="span3" name="customer_id" id="customer-id" required></select>
                                </div> <!-- /controls -->
                            </div> <!-- /control-group -->
                            <div class="control-group" id="req-balance-amount" style="display: none;">
                                <label class="control-label" for="requested-amount">Balance Amount</label>
                                <div class="controls">
                                    <input type="text" class="span3 disabled" id="requested-amount"
                                        name="requested_amount" value="" readonly="readonly">
                                </div> <!-- /controls -->
                            </div> <!-- /control-group -->
                            <div class="control-group">
                                <label class="control-label" for="collected-amount">Collection Amount</label>
                                <div class="controls">
                                    <input type="text" class="span3" id="collected-amount" name="collected_amount"
                                        required="required">
                                </div> <!-- /controls -->
                            </div> <!-- /control-group -->
                            <div class="control-group">
                                <label class="control-label" for="collected-amount">Collection Date</label>
                                <div class="controls">
                                    <input type="text" class="span3" id="payment_date" readonly="readonly"
                                        name="collected_date" value="<?php echo $collected_date; ?>"
                                        required="required">
                                </div> <!-- /controls -->
                            </div> <!-- /control-group -->
                            <div class="control-group">
                                <label class="control-label" for="tablet">Tablet</label>
                                <div class="controls">
                                    <select class="span3 sel2" name="tablet_id" data-placeholder="Select Tablet"
                                        required="required">
                                        <option></option>
                                        <?php
                                        foreach ($tablets as $tablet) {
                                            echo '<option value="' . $tablet->tablet_id . '-' . $tablet->zone_id . '">' . $tablet->zone_name . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div> <!-- /controls -->
                            </div> <!-- /control-group -->

                            <br />


                            <div class="form-actions">
                                <button type="submit" name="add_back_payment" value="1" class="btn mm-btn">Save</button>
                                <button class="btn">Cancel</button>
                            </div> <!-- /form-actions -->

                        </form>




                    </div>


                </div>





            </div>
        </div> <!-- /widget -->
    </div> <!-- /span8 -->
</div> <!-- /row -->