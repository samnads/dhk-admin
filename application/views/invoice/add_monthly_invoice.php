<div class="row">
    <div class="span12" style="width: 97% !important;">      		
        <div class="widget">
            <div class="widget-header">
                <i class="icon-user"></i>
                <h3>Create Monthly Invoice</h3>
                <!--<a style="float:right ; margin-right:20px; cursor:pointer;" href="<?php// echo base_url();?>maids"><img src="<?php// echo base_url(); ?>img/list_female_user.png" title="Maid List"/></a>-->
                <div class="topiconnew"><a href="<?php echo base_url('invoice/list_customer_monthly_invoices'); ?>"><img src="<?php echo base_url(); ?>images/maid-list-icon.png" title="Invoice List"/></a></div>
            </div> <!-- /widget-header -->
            <div class="widget-content">
                <div class="col-md-12 col-sm-12 confirm-det-cont-box  borderbox pl-3 pb-3 pr-3">
					<form class="form-horizontal" id="invoice-ma-form" method="post" enctype="multipart/form-data"> 
						
						<div class="col-sm-4">
							<div class="row m-0 n-field-main">
								<div class="col-sm-12 p-0 n-field-box">
									<label class="control-label" for="basicinput">Customer&nbsp;<font style="color: #C00">*</font></label>
									<div class="controls">
										<select name="customer" id="customers_vh_rep_new" class="sel2 span7" required>
											<option value="">Select customer</option>
											<?php
											foreach($customerlist as $c_val)
											{
												if($c_val->customer_id == $customerId)
												{
													$selected = 'selected="selected"';
												} else {
													$selected = '';
												}
											?>
											<option value="<?php echo $c_val->customer_id; ?>" <?php echo $selected; ?>><?php echo $c_val->customer_name; ?></option>
											<?php
											}
											?>                                                                  
										</select>
									</div>
								</div>
							</div>
							<div class="row m-0 n-field-main">
								<div class="col-sm-12 p-0 n-field-box">                                       
									<label class="control-label" for="firstname">Issue Date &nbsp;<font style="color: #C00">*</font></label>
									<div class="controls">
										<input type="text" class="span7" id="invoice_issue_date" name="invoice_issue_date" value="<?php echo ($issuedate != '') ? $issuedate : date('Y-m-d'); ?>" required>
									</div> <!-- /controls -->
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="row m-0 n-field-main">
								<div class="col-sm-12 p-0 n-field-box">                                         
									<label class="control-label" for="invoice_from_date">From Date &nbsp;<font style="color: #C00">*</font></label>
									<div class="controls">
										<input type="text" class="span7" id="invoice_from_date" name="invoice_from_date" value="<?php echo ($fromdate != '') ? $fromdate : date('Y-m-d'); ?>" required>
									</div> <!-- /controls --> 
								</div>									
							</div> <!-- /control-group -->
							<div class="row m-0 n-field-main">
								<div class="col-sm-12 p-0 n-field-box">                                        
									<label class="control-label" for="firstname">Due Date &nbsp;<font style="color: #C00">*</font></label>
									<div class="controls">
										<input type="text" class="span7" id="invoice_due_date" name="invoice_due_date" value="<?php echo ($duedate != '') ? $duedate : date('Y-m-d',strtotime('+14 days')); ?>" required>
									</div> <!-- /controls --> 
								</div>									
							</div> <!-- /control-group -->
						</div>
						<div class="col-sm-4">
							<div class="row m-0 n-field-main">
								<div class="col-sm-12 p-0 n-field-box">                                          
									<label class="control-label" for="invoice_to_date">To Date &nbsp;<font style="color: #C00">*</font></label>
									<div class="controls">
										<input type="text" class="span7" id="invoice_to_date" name="invoice_to_date" value="<?php echo ($todate != '') ? $todate : date('Y-m-d'); ?>" required>
									</div> <!-- /controls -->               
								</div> <!-- /control-group -->
							</div>
							<div class="row m-0 n-field-main">
								<div class="col-sm-6 p-0 n-field-box">
								</div>
								<div class="col-sm-6 p-0 n-field-box">
									<input type="submit" class="btn mm-btn" value="Submit" name="add_ma_invoice" onclick="return validate_cust_monthly_inv_add();">
								</div>
							</div>
						</div>
					</form>
				</div>
				<?php
				if(!empty($service_data))
				{
				?>
				<div class="col-md-12 col-sm-12 no-left-right-padding">
					<div class="widget-header" style="margin-bottom: 0; margin-top: 10px;">
						<i class="icon-th-list"></i>
						<h3>Active Bookings</h3>
					</div>
					<div class="widget-content borderbox" style="margin-bottom:30px">
						<table id="" class="table da-table" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Sl.No</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Service date</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Time</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Customer</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Maid</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> Amount</th>
									<th style="line-height: 18px; padding: 5px 10px; text-align: center;"> <input type="checkbox" id="master_invoice_create" style="margin-right: 5px;">Select All</th>
								</tr>
							</thead>
							<tbody>
							<?php
							$grand_due = 0;
							$i = 1;
							foreach ($service_data as $inv)
							{
								$grand_due += $inv['total_fee'];
								// echo date("h:i a",strtotime($inv['time_from']));
								// exit();
							?>
								<tr>
									<td style="line-height: 18px; text-align: center"><?php echo $i; ?> </td>
									<td style="line-height: 18px; text-align: center"><?php echo $inv['service_date']; ?> </td>
									<td style="line-height: 18px; text-align: center"><?php echo date("h:i a",strtotime($inv['time_from'])); ?> - <?php echo date("h:i a",strtotime($inv['time_to'])); ?></td>
									<td style="line-height: 18px; text-align: center"><?php echo $inv['customer_name']; ?> </td>
									<td style="line-height: 18px; text-align: center"><?php echo $inv['maid_name']; ?></td>
									<td style="line-height: 18px; text-align: center"><?php echo $inv['total_fee']; ?></td>
									<td style="line-height: 18px; text-align: center">
										<input type="checkbox" class="sub_inv_chk" data-bookingid="<?php echo $inv['booking_id']; ?>" data-id="<?php echo $inv['day_service_id']; ?>">
									</td>
								</tr>
							<?php
							$i++;	
							}
							?>
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td style="text-align: center">Total</td>
									<td style="text-align: center"><?php echo $grand_due; ?></td>
									<td></td>
								</tr>
							</tbody>
						</table>
						
						
					</div><!-- /widget-content --> 
				</div>
				<div class="col-sm-12 p-0 n-field-box">
					<button class="btn mm-btn" name="generate-btn" id="generate-btn" style="background: green; float: right; margin: 5px 0px;">Generate Invoice</button>
				</div>
				<?php
				} else {
				?>
				<div class="col-md-12 col-sm-12 no-left-right-padding">
					<div class="widget-header" style="margin-bottom: 0; margin-top: 10px;">
						<i class="icon-th-list"></i>
						<h3>Active Bookings</h3>
					</div>
					<div class="widget-content borderbox" style="margin-bottom:30px">
						<p style="color: red;">No bookings are available...</p>
					</div>
				</div>
				<?php
				}
				?>
				
            </div> <!-- /widget-content -->					
        </div> <!-- /widget -->	      		
    </div> <!-- /span8 -->	      		      		      		      	
</div> <!-- /row -->
