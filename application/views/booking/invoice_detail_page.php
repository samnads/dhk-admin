<div class="col-md-12 col-sm-12 invoice-det-table no-left-right-padding">
	<div class="Table table-top-style-box no-top-border">
		<div class="Heading table-head">
			<div class="Cell">
				<p><strong>Sl.No</strong></p>
			</div>
			<div class="Cell">
				<p><strong>Service ID</strong></p>
			</div>
			<div class="Cell">
				<p><strong>Date</strong></p>
			</div>
			<div class="Cell">
				<p><strong>Maid</strong></p>
			</div>
			<div class="Cell">
				<p><strong>From</strong></p>
			</div>
			<div class="Cell">
				<p><strong>To</strong></p>
			</div>
			<div class="Cell">
				<p><strong>Description</strong></p>
			</div>
			<div class="Cell">
				<p><strong>Hours</strong></p>
			</div>
			<div class="Cell">
				<p><strong>Amount</strong></p>
			</div>
			<div class="Cell">
				<p><strong>VAT</strong></p>
			</div>
			<div class="Cell">
				<p><strong>Net Amount</strong></p>
			</div>
			<div class="Cell">
				<p><strong>Action</strong></p>
			</div>
		</div>
		<?php
		$i = 1;
		foreach ($invoice_detail as $jobs)
		{
			$tot_hrs = ((strtotime($jobs->service_to_time) - strtotime($jobs->service_from_time))/ 3600);
			
		?>
		<div class="Row">
			<div class="Cell">
				<p><?php echo $i; ?></p>
			</div>
			<div class="Cell">
				<p><?php echo $jobs->day_service_id; ?></p>
			</div>
			<div class="Cell">
				<p><?php echo date('d/m/Y', strtotime($jobs->service_date)); ?></p>
			</div>
			<div class="Cell">
				<p><?php echo $jobs->maid_name; ?></p>
			</div>
			<div class="Cell">
				<p><?php echo date('H:i A', strtotime($jobs->service_from_time)); ?></p>
			</div>
			<div class="Cell">
				<p><?php echo date('H:i A', strtotime($jobs->service_to_time)); ?></p>
				<input type="hidden" value="<?php echo $jobs->received_amount; ?>" id="balanceamountval<?php echo $jobs->invoice_line_id;?>" />
			</div>
			<div class="Cell">
				<p class="jobdes<?php echo $jobs->invoice_line_id;?>"><span class="jobdestext<?php echo $jobs->invoice_line_id;?>"><?php echo $jobs->description; ?></span>
				<button class="btn btn-small btn-info text-right descedit" style="margin-top:10px;float: right;"data-lineid="<?php echo $jobs->invoice_line_id;?>"><i class="btn-icon-only fa fa-edit"> </i></button>
				</p>
				<p class="jobtxa<?php echo $jobs->invoice_line_id;?> jobtxa" style="display: none;">
					<textarea rows="4" style="width:100%;" class="newdesc_<?php echo $jobs->invoice_line_id;?>"><?php echo $jobs->description; ?></textarea>

					<br/>
					<button class="btn btn-small btn-info descupdatebtn" style="margin-top:10px;" data-lineid="<?php echo $jobs->invoice_line_id;?>">Update</button>
					<i class="fa fa-spinner fa-spin descload<?php echo $jobs->invoice_line_id;?>" style="font-size:20px;position: relative;top: 7px;display: none;"></i>
				</p>
			</div>
			<div class="Cell">
				<p><?php echo $tot_hrs; ?></p>
			</div>
			<div class="Cell">
				<p class="lineamt<?php echo $jobs->invoice_line_id;?>">
				<span class="lineamttext<?php echo $jobs->invoice_line_id;?>"><?php echo $jobs->line_amount; $x='p';?></span>
				<button class="btn btn-small btn-info text-right amtedit" style="margin-top:10px;float: right;" data-lineid="<?php echo $jobs->invoice_line_id;?>" ><i class="btn-icon-only fa fa-edit"> </i></button>  
				</p>
				<p class="amttxt<?php echo $jobs->invoice_line_id;?> amttxt" style="display: none;">
					<input type="text" class="amtinp_<?php echo $jobs->invoice_line_id;?> lineamtval" data-lineamtedit="<?php echo $jobs->invoice_line_id; ?>" value="<?php echo $jobs->line_amount; ?>" style="width:87px;">
					<br/>
					<button class="btn btn-small btn-info amtupdatebtn" style="margin-top:10px;" data-lineid="<?php echo $jobs->invoice_line_id;?>" data-invstat="<?php echo $invoice_detail[0]->invoice_status?>">Update</button>
					<i class="fa fa-spinner fa-spin amtload<?php echo $jobs->invoice_line_id;?>" style="font-size:20px;position: relative;top: 7px;display: none;"></i>
				</p>
			</div>
			<div class="Cell">
				<p>
					<span class="linevatamttext<?php echo $jobs->invoice_line_id;?>">
						<?php echo $jobs->line_vat_amount; ?> 
					</span>
				</p>
			</div>
			<div class="Cell">
				<p>
				   <span class="linenetamttext<?php echo $jobs->invoice_line_id;?>">
					<?php echo $jobs->line_net_amount; ?>
				   </span>
				   <input type="hidden" class="lineoldamtval<?php echo $jobs->invoice_line_id;?>" value="<?php echo $jobs->line_net_amount; ?>" />
				   <input type="hidden" class="linenetamtval<?php echo $jobs->invoice_line_id;?>" value="<?php echo $jobs->line_net_amount; ?>" />
				   <input type="hidden" class="lineserviceamtval<?php echo $jobs->invoice_line_id;?>" value="<?php echo $jobs->total_amount; ?>" />
				</p>
			</div>
			<div class="Cell">
				<?php if(count($invoice_detail) > 1){ ?>
				<button class="btn btn-small btn-danger text-right deletelineitem" style="margin-top:10px;float: right;" data-lineid="<?php echo $jobs->invoice_line_id;?>" data-serviceid="<?php echo $jobs->day_service_id;?>" data-invoiceid="<?php echo $jobs->invoice_id;?>"><i class="btn-icon-only fa fa-trash"> </i></button>
				<?php } ?>
			</div>
		</div>
		<?php
		$i++; } ?>
		<div class="Row">
			<div class="Cell total-box no-right-border">
			  &nbsp;
			</div>
			<div class="Cell total-box no-right-border">
			  &nbsp;
			</div>
			<div class="Cell total-box no-right-border">
			  &nbsp;
			</div>
			<div class="Cell total-box no-right-border">
			  &nbsp;
			</div>
			<div class="Cell total-box no-right-border">
			  &nbsp;
			</div>
			<div class="Cell total-box no-right-border">
			  &nbsp;
			</div>
			<div class="Cell total-box no-right-border">
			  &nbsp;
			</div>
			<div class="Cell total-box no-left-border">
			  &nbsp;
			  <span class="total-text text-right"><b class="pull-right">Total &nbsp;&nbsp; </b></span>
			</div>
			<div class="Cell total-box light-green">
			   <span class="total-text text-right"><b class="invtotamt">AED <?php echo $invoice_detail[0]->invoice_total_amount; ?></b></span>
			</div>
			<div class="Cell total-box light-green">
				<span class="total-text text-right"><b class="invtaxamt">AED <?php echo $invoice_detail[0]->invoice_tax_amount; ?></b></span>
			</div>
			<div class="Cell total-box light-green">
				<span class="total-text text-right"><b class="invnetamt">AED <?php echo $invoice_detail[0]->invoice_net_amount; ?></b></span>
			</div>
			<div class="Cell total-box no-right-border">
			  &nbsp;
			</div>
		</div>
	</div><!--Table table-top-style-box end--> 
</div>

<script type="text/javascript">
$( document ).ready(function() {

    $(".descedit").click(function() {
        var lineid=$(this).data("lineid");
        $(".jobtxa").hide();
        $(".jobdes"+lineid).hide();        
        $(".jobtxa"+lineid).show();
    });
	
	$(".descupdatebtn").click(function() {
        var lineid=$(this).data("lineid");
        $(this).prop('disabled', true);
        var description=$(".newdesc_"+lineid).val();   
                
        $(".descload"+lineid).show();
        $.ajax({
		   url: "<?php echo base_url();?>booking_edit/update_line_desc",
		   type: "post",
		   data: {line_description:description,lineid:lineid} ,
		   success: function (response) {
				$(".descload"+lineid).hide();
				$(".descupdatebtn[data-lineid='"+lineid+"']").prop('disabled', false);
				$(".jobdestext"+lineid).html(description); 
				$(".jobdes"+lineid).show();        
				$(".jobtxa"+lineid).hide();

		   },
		   error: function(jqXHR, textStatus, errorThrown) {
				$(".descload"+lineid).hide();
				$(".descupdatebtn[data-lineid='"+lineid+"']").prop('disabled', false);
				alert("Error!Try again later.");

		   }
	   });
    });
	
	$('body').on('click',".amtedit",function() {
        var lineid=$(this).data("lineid");
        $(".lineamt"+lineid).hide();        
        $(".amttxt"+lineid).show();
    });
	
	$('body').on('input',".lineamtval",function() {
        var lineid=$(this).data("lineamtedit");
		var amtval = $(this).val();
		var vatamt = parseFloat(amtval * 0.05).toFixed(2);
		var netamt = (parseFloat(amtval) + parseFloat(vatamt));
		$(".linevatamttext"+lineid).text(vatamt);
		$(".linenetamttext"+lineid).text(netamt);
		$(".linenetamtval"+lineid).val(netamt);
    });
	
	$(".amtupdatebtn").click(function() {
        var lineid=$(this).data("lineid");
		var balanceamountval = $("#balanceamountval"+lineid).val();
        var custid='<?php echo $invoice_detail[0]->customer_id; ?>';
        var invstat=$(this).data("invstat");
        var updateamt=$(".amtinp_"+lineid).val();
        var line_old_amount=$(".lineamttext"+lineid).html();
        var line_old_net_amount=$.trim($(".lineoldamtval"+lineid).val());
        var line_new_net_amount=$.trim($(".linenetamtval"+lineid).val());
        var servceamt = $(".lineserviceamtval"+lineid).val();
		var invid='<?php echo $invoice_detail[0]->invoice_id; ?>';
		if(parseFloat(line_new_net_amount) != parseFloat(servceamt))
		{
			alert('Service amount is different. Please check and try again');
			editServiceInvoice(invid);
		} else {
			if(updateamt.length==0 || isNaN(updateamt)){alert("Please check the entered amount");return false;}
			$(".amtload"+lineid).show();
			$(this).prop('disabled', true);

			$.ajax({
			   url: "<?php echo base_url();?>booking_edit/update_line_amt",
			   type: "post",
			   dataType: "json",
			   data: {line_amount:updateamt,invoice_id:invid,lineid:lineid,line_old_amount:line_old_amount,custid:custid,line_old_net_amount:line_old_net_amount,invstat:invstat,balanceamountval:balanceamountval,line_new_net_amount:line_new_net_amount} ,
			   success: function (response) {
					$(".amtload"+lineid).hide();
					$(".amtupdatebtn[data-lineid='"+lineid+"']").prop('disabled', false);
					$(".lineamttext"+lineid).html(updateamt); 
					$(".lineamt"+lineid).show();        
					$(".amttxt"+lineid).hide();
					$(".linevatamttext"+lineid).html(response.line_vat_amount);
					$(".linenetamttext"+lineid).html(response.line_net_amount);

					$(".invtotamt").html('AED '+response.invoice_total_amount);
					$(".invtaxamt").html('AED '+response.invoice_tax_amount);
					$(".invnetamt").html('AED '+response.invoice_net_amount);
					viewServiceInvoice(invid);

			   },
			   error: function(jqXHR, textStatus, errorThrown) {
					$(".amtload"+lineid).hide();
					$(".amtupdatebtn[data-lineid='"+lineid+"']").prop('disabled', false);

			   }
			});
		}
    });
	
	$('body').on('click', '.deletelineitem', function() {
		var lineid = $(this).data("lineid");
		var serviceid = $(this).data("serviceid");
		var invoiceid = $(this).data("invoiceid");
		var balanceamountval = $("#balanceamountval"+lineid).val();
		var custid='<?php echo $invoice_detail[0]->customer_id; ?>';
		var invstat='<?php echo $invoice_detail[0]->invoice_status?>';
        var line_old_amount=$(".lineamttext"+lineid).html();
        var line_old_net_amount=$.trim($(".lineoldamtval"+lineid).val());
		var invid='<?php echo $invoice_detail[0]->invoice_id; ?>';
		
		$.ajax({
		   url: "<?php echo base_url();?>booking_edit/delete_line_item",
		   type: "post",
		   dataType: "json",
		   data: {invoice_id:invid,lineid:lineid,line_old_amount:line_old_amount,custid:custid,line_old_net_amount:line_old_net_amount,invstat:invstat,balanceamountval:balanceamountval,serviceid:serviceid} ,
		   success: function (response) {
				viewServiceInvoice(invid);
				editServiceInvoice(invid);

		   },
		   error: function(jqXHR, textStatus, errorThrown) {
				viewServiceInvoice(invid);
				editServiceInvoice(invid);
		   }
		});
	});
});
</script>