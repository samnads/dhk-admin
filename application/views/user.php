<div id="disable-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Disable ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to disable this user ?</h3>
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn mb-0" data-dismiss="modal" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn red-btn mb-0" data-dismiss="modal" onclick="confirm_disable()">Disable</button>
    </div>
  </div>
</div>
<div id="enable-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Enable ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to enable this user ?</h3>
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn red-btn mb-0" data-dismiss="modal" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn mb-0" data-dismiss="modal" onclick="confirm_enable()">Enable</button>
    </div>
  </div>
</div>
<div class="row m-0">  
    <div class="col-md-12">
        <div class="widget widget-table action-table">
            <div class="widget-header">
            <ul>
            <li>
            <i class="icon-th-list"></i>
                <h3>Admin Users</h3></li>
                <li>

                <div class="topiconnew border-0 green-btn">
              	   <a href="<?php echo base_url(); ?>users/add" title="Add"> <i class="fa fa-plus"></i>
                </a>
              </div>
              </li>
              <li>
				<span style="margin-left:5px;"></span>
				<div class="mm-drop">
					<select style="width: 100px;" id="user-status">
						<option value="1" <?= $user_status == '1' ? 'selected' : '' ?>>Active</option>
						<option value="0" <?= $user_status == '0' ? 'selected' : '' ?>>Inactive</option>
					</select>
				</div>
               </li>
              </ul>
            </div>            
            <div class="widget-content">

                <table id="user-list-table" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; width: 30px; text-align:center;"> Sl.No. </th>
                            <th style="line-height: 18px;">Name </th>
                            <th style="line-height: 18px;"> Is Admin?</th>
                            <th style="line-height: 18px;"> Delete Permission?</th>
                            <th style="line-height: 18px;">User Name </th>
                            <th style="line-height: 18px;"> Last Logged in</th>
                            <th style="line-height: 18px;"> Last Logged in IP </th>
                            <th style="line-height: 18px; text-align: center; width: 30px;"> Status</th>
                            <th style="line-height: 18px; text-align: center;" class="td-actions">Permission</th>
                            <th style="line-height: 18px; text-align: center;" class="td-actions2">Info</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($users) > 0) {
                            $i = 1;
                            foreach ($users as $user) {
                                ?>  
                                <tr>
                                    <td style="line-height: 18px; width: 20px; text-align:center;"><?php echo $i; ?></td>
                                    <td style="line-height: 18px;"><?php echo $user->user_fullname ?></td>
                                    <td style="line-height: 18px;"><?php echo $user->is_admin ?></td>
                                    <td style="line-height: 18px;"><?php echo $user->delete_permission ?></td>
                                    <td style="line-height: 18px;"><?php echo $user->username ?></td>
                                    <td style="line-height: 18px;"><?php echo $user->last_loggedin_datetime ?></td>
                                    <td style="line-height: 18px;"><?php echo $user->last_loggedin_ip ?></td>
                                    <td style="line-height: 18px; text-align: center;"><?php echo $user->user_status == 1 ? '<sapn class="n-btn-icon green-btn"><i class="btn-icon-only icon-ok"> </i></span>' : '<sapn class="n-btn-icon red-btn"><i class="btn-icon-only icon-remove-sign"> </i></span>'; ?></td>
                                    <td style="line-height: 18px; width: 110px;" class="td-actions ">
                                    <center>                                        
                                        <!--<a class="btn btn-small btn-warning" title="Edit" href="<?php// echo base_url() . 'users/edit/' . $user->user_id ?>"><i class="btn-icon-only icon-pencil" style="font-size: 13px;"> </i></a>-->
                                        <a class="n-btn-icon orange-btn" title="Permissions" href="<?php echo base_url() . 'users/permissions/' . $user->user_id ?>"><i class="btn-icon-only icon-key" style="font-size: 13px;"> </i></a>
                                    </center>

                                    </td>
                                    <td style="line-height: 18px; text-align: center;">
                                       <a class="n-btn-icon green-btn" title="Login Info" href="<?php echo base_url() . 'users/userlogininfo/' . $user->user_id ?>"><i class="btn-icon-only icon-search" style="font-size: 13px;"> </i></a>
                                    
                                        <a class="n-btn-icon purple-btn" title="Edit" href="<?php echo base_url() . 'users/add/' . $user->user_id ?>"><i class="btn-icon-only icon-pencil" style="font-size: 13px;"> </i></a>
                                        
                                        
                                        
                                        <a href="javascript:;" class="n-btn-icon <?= $user->user_status == 0 ? 'green-btn enable-user' : 'red-btn disable-user' ?>" data-id="<?= $user->user_id; ?>"><i class="btn-icon-only <?= $user->user_status == 0 ? 'icon-ok' : 'icon-remove' ?>"> </i></a>
                                    </td>
                                </tr>
                            <?php
                            $i++;
                        }
                    }
                    ?>   

                    </tbody>
                </table>                 
            </div>            
        </div>        
    </div>

</div>
<script>
/*********************************************************************************** */
// zones - script
(function (a) {
    a(document).ready(function (b) {
        if (a('#user-list-table').length > 0) {
            a("table#user-list-table").dataTable({
                'sPaginationType': "full_numbers",
                "bSort": true,
                "scrollY": true,
                "orderMulti": false,
                'bFilter':true,
                "lengthChange": true,
                "pageLength": 100,
                'columnDefs': [
                    {
                        'targets': [6,7,-1],
                        'orderable': false
                    },
                ]
            });
        }
    });
})(jQuery);
function closeFancy() {
	$.fancybox.close();
}
function confirm_enable() {
	$.ajax({
		type: "POST",
		url: _base_url + "users/update",
		data: {
			action: 'enable-user',
            id: enbale_user_id
		},
		dataType: "json",
		cache: false,
		success: function(data) {
            if(data.status == true){
                toast(data.type,data.message);
                location.reload();
            }
            else{
                toast(data.type,data.message);
            }
		},
		error: function() {
            toast('error',"An error occured !");
		},
        complete: function() {
            closeFancy();
		}
	});
}
function confirm_disable() {
	$.ajax({
		type: "POST",
		url: _base_url + "users/update",
		data: {
			action: 'disable-user',
            id: disable_user_id
		},
		dataType: "json",
		cache: false,
		success: function(data) {
            if(data.status == true){
                toast(data.type,data.message);
                location.reload();
            }
            else{
                toast(data.type,data.message);
            }
		},
		error: function() {
             toast('error',"An error occured !");
		},
        complete: function() {
            closeFancy();
		}
	});
}
/*********************************************************************************** */
var enbale_user_id;
var disable_user_id;
$('.enable-user').click(function(){
    fancybox_show('enable-popup');
    enbale_user_id = $(this).data("id");
});
$('.disable-user').click(function(){
    fancybox_show('disable-popup');
    disable_user_id = $(this).data("id");
});
$("#user-status").change(function(){
    if($("#user-status").val()){
        location.href = _page_url+'?status='+$("#user-status").val();
    }
    else{
        location.href = _page_url;
    }
});
</script>