<div class="row">   
    <div class="span12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form class="form-horizontal" method="post">
                <ul>
                <li>
                    <i class="icon-th-list"></i>
                    <h3>Activity</h3> 
                    </li>
                    <li>                  
                   
                        <input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="vehicle_date" name="service_date" value="<?php echo $service_date ?>">                       
                        <input type="hidden" id="formatted-date" value="<?php echo $formatted_date ?>"/>
                        </li>
                    <li>
                    <input type="submit" class="n-btn" value="Go" name="vehicle_report"> 
                    
                    </li>
                    
                    <li class="mr-0 float-right">

                    <div class="topiconnew border-0 green-btn">
                    	 <a id="synch-to-odoo" class="n-btn" target="_blank">Synchronize</a>
                    </div>
                    
                    <div class="topiconnew border-0 green-btn">
                    	 <a href="<?php echo base_url() . 'reports/activity_summary_view/' . $formatted_date; ?>" id="ActivityPrint" target="_blank"><i class="fa fa-print"></i></a>
                    </div>
                    
                    </li>
                    </ul>
                </form>   
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <table class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; width: 10px;"> Sl No.</th>
                            <th style="line-height: 18px;"> Maid</th>
                            <th style="line-height: 18px;"> Customer</th>
                            <th style="line-height: 18px;"> Working Hours</th>                            
                            
                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(!empty($dayservices))
                        {
                            $i = 0;
                            $zone_id = 0;                            
                            $total_fee = 0;
                            $collected_total_fee = 0;
                            $total_wrk_hrs = 0;
                            $collected_amount = 0;
                            $ztotal_wrk_hrs = 0;
                            foreach ($dayservices as $service)
                            {
                                
                                                                                                                               
                                if($zone_id != $service->zone_id)
                                {
                                    if($ztotal_wrk_hrs != 0)
                                    {

                                        echo '<tr>'
                                                . '<td style="line-height: 18px;"></td>'
                                                . '<td style="line-height: 18px;"></td>'
                                                . '<td style="line-height: 18px;"></td>'
                                                . '<td style="line-height: 18px; text-align : right;"><b>' . $ztotal_wrk_hrs . ' Hrs</b></td>'
                                                
                                             . '</tr>';
                                        $ztotal_wrk_hrs = 0;
                                    }
                                    
                                    $zone_id = $service->zone_id;
                                    echo '<tr>'
                                            . '<td style="line-height: 18px;"></td>'
                                            . '<td style="line-height: 18px;"><b style="color:#CB3636;">' . $service->zone_name . '</b></td>'
                                            . '<td style="line-height: 18px;"></td>'
                                            . '<td style="line-height: 18px;"></td>'
                                            
                                         . '</tr>';
                                    
                                    
                                }
                                
                                
                                $total_wrk_hrs += ((strtotime($service->end_time) - strtotime($service->start_time))/ 3600) ;
                                $ztotal_wrk_hrs += ((strtotime($service->end_time) - strtotime($service->start_time))/ 3600);
                                echo '<tr>'
                                        . '<td style="line-height: 18px;">' . ++$i . '</td>'
                                        . '<td style="line-height: 18px;">' . $service->maid_name . '</td>'
                                        . '<td style="line-height: 18px;">' . $service->customer_name . ' <b style="color:#006600;">(' . ($service->actual_zone ? substr($service->actual_zone, 2) : $service->zone_name) . '</b>)</td>'
                                        . '<td style="line-height: 18px; text-align : right;">' . $service->time_from . '-' . $service->time_to . ' [' . ((strtotime($service->end_time) - strtotime($service->start_time))/ 3600) . ']' . '</td>'
                                        /*. '<td style="line-height: 18px;">'
                                        . '<a class="btn btn-small btn-success start-stop" title="Start" href="javascript:void" ' . $play . '><i class="icon-play"> </i></a>'
                                        . '<a class="btn btn-small btn-info start-stop" title="Stop" href="#payment-modal" data-toggle="modal" ' . $stop . '><i class="icon-stop"> </i></a>'
                                        . '<a class="btn btn-small btn-warning start-stop" title="Transfer" href="#transfer-modal" data-toggle="modal" ' . $transfer . '><i class="icon-share"> </i></a>'
                                        . '<a class="btn btn-small btn-danger start-stop" title="Cancel" href="javascript:void" ' . $cancel . '><i class="icon-ban-circle"> </i></a>'
                                        . '</td>'*/
                                    .'</tr>';
                                
                                
                            }
                            echo '<tr style="font-weight:bold;">'
                                    . '<td style="line-height: 18px;"></td>'
                                    . '<td style="line-height: 18px;"></td>'
                                    . '<td style="line-height: 18px;">TOTALS</td>'
                                    . '<td style="line-height: 18px; text-align : right;">' . $total_wrk_hrs . '</td>'
                                    //. '<td style="line-height: 18px;"></td>'
                                .'</tr>';
                        }
                        
                        ?>
                    </tbody>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>



