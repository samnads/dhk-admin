<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=".$file_name."");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table border="1">
    <thead>
        <tr style="background-color:yellow">
            <th style="line-height: 18px; padding: 10px; text-align: center;">Sl. No.</th>
            <th style="line-height: 18px; padding: 10px; text-align: center;">Date of Service</th>
			<th style="line-height: 18px; padding: 10px; text-align: center;">Crews Name</th>
			<th style="line-height: 18px; padding: 10px; text-align: center;">Address</th>
			<th style="line-height: 18px; padding: 10px; text-align: center;">Booking Time</th>
			<th style="line-height: 18px; padding: 10px; text-align: center;">Hours</th>
			<th style="line-height: 18px; padding: 10px; text-align: center;">Customer Name</th>
			<th style="line-height: 18px; padding: 10px; text-align: center;">Cleaning Supplies</th>
			<th style="line-height: 18px; padding: 10px; text-align: center;">Service Amount</th>
			<th style="line-height: 18px; padding: 10px; text-align: center;">Previous Outstanding</th>
			<th style="line-height: 18px; padding: 10px; text-align: center;">Notes</th>
			<th style="line-height: 18px; padding: 10px; text-align: center;">Payment Mode</th>
        </tr>
    </thead>
    <tbody>
		<?php
		if(!empty($reports))
		{
			$i = 0;
			$totamount = 0;
			$total_hours = 0;
			foreach ($reports as $rep)
            {
            	foreach ($rep as $report)
                {
				$totamount += $report->total_amount;
		?>
			<tr>
				<td style="line-height: 18px; text-align: center;"><?php echo ++$i; ?></td>
				<td style="line-height: 18px;"><?php echo $report->sch_date; ?></td>
				<td style="line-height: 18px;"><?php echo $report->allmaidss; ?></td>
				<td style="line-height: 18px; width: 100%;">
				<?php
				$addrr = "";
				if($report->building != "")
				{
					$addrr .= $report->building.', ';
				}
				if($report->customer_address != "")
				{
					$addrr .= $report->customer_address.', ';
				}
				if($report->area_name != "")
				{
					$addrr .= $report->area_name;
				}
				echo $addrr;
									?>
				</td>
				<td style="line-height: 18px;"><?php echo $report->booking_time_from." - ".$report->booking_time_to; ?></td>
				<td style="line-height: 18px;">
					<?php
									$hours=(strtotime($report->time_to) - strtotime($report->time_from)) / 3600;
									$total_hours += $hours;
									$hrs=strtotime($report->time_to)-strtotime($report->time_from);
									?>
									<?php echo gmdate("H:i", $hrs); ?>
				</td>
				<td style="line-height: 18px;"><?php echo $report->customer_name; ?></td>
				<td style="line-height: 18px;">
					<?php echo ($report->cleaning_material == 'Y') ? 'Yes' : 'No'; ?>
				</td>
				<td style="line-height: 18px;"><?php echo $report->total_amount; ?></td>
				<td style="line-height: 18px;"><?php echo $report->balance; ?></td>
				<td style="line-height: 18px;"><?php echo $report->booking_note; ?></td>
				<td style="line-height: 18px;"><?php echo $report->pay_by; ?></td>
			</tr>
		<?php
				}
			}
		}
		
		?>
	</tbody>
	<tfoot>
		<tr>
			<td style="line-height: 18px;" colspan="4"></td>
			<td style="line-height: 18px;">Total</td>
			<td style="line-height: 18px;"><?=  $total_hours ?></td>
			<td style="line-height: 18px;" colspan="2"></td>
			<td style="line-height: 18px;"><?php echo $totamount; ?></td>
			<td style="line-height: 18px;" colspan="3"></td>
		</tr>
	</tfoot>
</table>