<style>
	
</style>

<div class="schedule_report">
	<div class="inner">
		<div class="head">Schedule - <?php echo $schedule_day; ?></div>
		<div class="table">
			<div class="row">
				<div class="cell1"><strong>Sl. No</strong></div>
				<div class="cell2"><strong>Maid</strong></div>
				<div class="cell3"><strong>Customer</strong></div>
				<div class="cell4"><strong>Time</strong></div>
                <div class="cell5"><strong>Hours</strong></div>
                <div class="cell6"><strong>Price</strong></div>
			</div>
			
			<?php
			$sln = 1;
			$tottime = 0;
			$totprice = 0;
			foreach($all_bookings as $booking)
			{
				?>
				<div class="row">
					<div class="cell1"><?php echo $sln++; ?>.</div>
					<div class="cell2"><?php echo html_escape($booking->maid_name); ?></div>
					<div class="cell3"><?php echo html_escape($booking->customer_nick_name); ?></div>
					<div class="cell4"><?php echo $booking->time_from; ?> - <?php echo $booking->time_to; ?></div>
                    	<div class="cell5" align="center">
                        <?php
						$from = $booking->time_from;
						$to = $booking->time_to;
						$d1 = strtotime($from);
						$d2 = strtotime($to);
						$diff = (($d2 - $d1)/60)/60;
						$tottime += $diff;
						echo $diff . ' Hrs';
                        ?>
                        </div>
                        
                        <div class="cell6" align="right"><?php echo $booking->total_amount; $totprice += $booking->total_amount; ?></div>
				</div>
				<?php
			}
			?>
            <div class="row">
				<div class="cell1"><strong></strong></div>
				<div class="cell2"><strong></strong></div>
				<div class="cell3"><strong></strong></div>
				<div class="cell4"><strong></strong></div>
                <div class="cell5" align="center"><strong><?php echo $tottime. ' Hrs'; ?></strong></div>
                <div class="cell6" align="right"><strong><?php echo $totprice; ?></strong></div>
			</div>
		</div>
	</div>
</div>