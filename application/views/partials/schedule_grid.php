<?php
foreach($maids as $maid)
{
	$top = 15;
	?>
        <div class="row tb-slider" id="<?php echo 'maid-' . $maid->maid_id; ?>">
		<?php
		$booked = isset($maid_bookings[$maid->maid_id]) ? $maid_bookings[$maid->maid_id] : array();
		$f_slot = FALSE;
		$b_slot = FALSE;
		foreach($times as $time_index=>$time)
		{
			if($b_slot && isset($tb_end_time) && $tb_end_time == $time->stamp)
			{
				echo '</div>';
				$b_slot = FALSE;
				$f_slot = FALSE;
			}

			if(!$b_slot && isset($booked[$time->stamp]['end_time']))
			{
				if($f_slot)
				{
					echo '</div>';
				}

				$f_slot = FALSE;
				$b_slot = TRUE;
				$tb_end_time = $booked[$time->stamp]['end_time'];
				echo '<div class="slot">';
			}

			if(!$b_slot && !$f_slot)
			{
				$f_slot = TRUE;
                                
                                if(in_array($maid->maid_id, $leave_maid_ids)) // Maid Leave
                                {
									foreach($leave_maid_types as $typevalue)
									{
										if($typevalue['maid_id'] == $maid->maid_id)
										{
											if($typevalue['maid_type'] == "leave")
											{
												echo '<div class="slot casualleave"><div class="leave">Casual Leave</div>';
											} else if($typevalue['maid_type'] == "absent"){
												echo '<div class="slot absent"><div class="leave">Absent</div>';
											} else if($typevalue['maid_type'] == "dayoff"){
												echo '<div class="slot dayoff"><div class="leave">DAY OFF</div>';
											} else if($typevalue['maid_type'] == "offset"){
												echo '<div class="slot offset"><div class="leave">Offset</div>';
											} else if($typevalue['maid_type'] == "sick leave"){
												echo '<div class="slot sickleave"><div class="leave">Sick Leave</div>';
											}else if($typevalue['maid_type'] == "sm_replacement"){
												echo '<div class="slot sm_replacement"><div class="leave">SM Replacement</div>';
											}
											break;
										}
									}	
                                }
                                else
                                {
                                    echo '<div class="' . ($booking_allowed == 1 ? 'selectable' : '') . ' slot">';
                                }
				
			}
			?>

			<div class="cell" id="<?php echo $time_index; ?>" <?php if($current_hour_index === $time_index) { echo 'style="border-right:1px solid #F00;"'; } ?>><?php // echo $time->display . '-' . $current_hour_index; ?>
				<?php
				if($b_slot && isset($booked[$time->stamp]['end_time']) && isset($tb_end_time))
				{
					//$diff = (($tb_end_time - $time->stamp) / 60) / 60;
					$diff = (($tb_end_time - $time->stamp) / 60) / 30;
					$width = ($diff * 71) - 22 + 20;
					//$width = ($diff * 100) - 22 + 20;
					//$width = ($diff * 100) - 22;
					$top = 3;
					//$top = $top == 3 ? 20 : 3;
                                        //Payment Type
                                        if($booked[$time->stamp]['payment_type'] == "D")
                                        {
                                            $paytype = "(D)";
                                        } else if($booked[$time->stamp]['payment_type'] == "W")
                                        {
                                            $paytype = "(W)";
                                        } else if($booked[$time->stamp]['payment_type'] == "M")
                                        {
                                            $paytype = "(M)";
                                        } else
                                        {
                                            $paytype = "";
                                        }
					//$top = $top == 3 ? 34 : 3;
                                        $progress_service_icon='';
                                        $scheduled_date = date("Y-m-d"); 
                                        //$job_service=$this->bookings_model->job_start_or_finish($booked[$time->stamp]['booking_id'],$scheduled_date);
                                        // if(!empty($job_service)){
                                        // if($job_service[0]->service_status==1){
                                        // $progress_service_icon='<i class="fa fa-gear fa-spin" style="font-size:18px;" title="In progress"></i>';
                                        // }
                                        // if($job_service[0]->service_status==2){
                                        // $progress_service_icon='<i class="fa fa-check" aria-hidden="true" style="font-size:18px;"></i>';
                                        // }
                                        
                                        // }
										if($booked[$time->stamp]['servicestatus'] != "")
										{
											if($booked[$time->stamp]['servicestatus'] == 1)
											{
												$progress_service_icon='<i class="fa fa-gear fa-spin" style="font-size:18px;" title="In progress"></i>';
											}
											if($booked[$time->stamp]['servicestatus'] == 2)
											{
												$progress_service_icon='<i class="fa fa-check" aria-hidden="true" style="font-size:18px;"></i>';
											}
										}

										if($booked[$time->stamp]['bookingStatuss'] == "Y")
                                        {
											$statusstyle = '<i class="fa fa-circle fa-1x" style="color:#9AE275;" aria-hidden="true"></i>';
										} else {
											$statusstyle = '<i class="fa fa-circle fa-1x" style="color:red;" aria-hidden="true"></i>';
										}

										if($booked[$time->stamp]['is_locked'] == "1")
                                        {
											$bookinglock = '<li><div class="n-icon-box button-name" onclick=""><i class="fa fa-lock fa-1x" aria-hidden="true"></i></div></li>';
										} else {
											$bookinglock = '';
										}

										if($booked[$time->stamp]['operational_note'] != "")
                                        {
											$operatnnote = '<li class="op-notes" title="'.$booked[$time->stamp]['operational_note'].'"><div class="n-icon-box button-name" onclick=""><i class="fa fa-paperclip fa-1x" aria-hidden="true"></i></div></li>';
										} else {
											$operatnnote = '';
										}
                                        
					echo '<div class="new-cont-sectn"><ul class="n-icon-section ' . strtolower($booked[$time->stamp]['type']) . '"><li><div class="n-icon-box cpyBooking" href="#" onclick="copyBookingMessage(this)" data-id="'.$booked[$time->stamp]['booking_id'].'"><i class="fa fa-clipboard" aria-hidden="true"></i></div></li><li><div class="n-icon-box toggleconfirm" onclick="bookingToggleConfirm(this)" data-bukid="'.$booked[$time->stamp]['booking_id'].'" data-status="'.$booked[$time->stamp]['bookingStatuss'].'" data-servdate="'.$booked[$time->stamp]['service_dates'].'" data-bookingtime="'.$booked[$time->stamp]['bookingtime'].'">'.$statusstyle.'</div></li>'.$bookinglock.$operatnnote.'</ul><div class="schedule_bubble ' . strtolower($booked[$time->stamp]['type']) . '" id="booking-' . $booked[$time->stamp]['booking_id'] . '" style="top:' . $top . 'px; width:' . $width . 'px;" title="' . html_escape($booked[$time->stamp]['customer'] . ' - ' . $booked[$time->stamp]['driverName'] . ' (' . date('g:i a', $booked[$time->stamp]['start_time']) . ' to ' . date('g:i a', $booked[$time->stamp]['end_time']) . ')') . '"><div>' . html_escape($booked[$time->stamp]['customer']. ' - ' . $booked[$time->stamp]['area'] . ' - ' . $booked[$time->stamp]['driverName']) . '<br /> ' . date('g:i a', $booked[$time->stamp]['start_time']) . ' to ' . date('g:i a', $booked[$time->stamp]['end_time']) . ' <span style="border-radius:1px; padding:3px;"> - ' . $booked[$time->stamp]['user'] . '<br>B.A : '.$booked[$time->stamp]['balance'].$booked[$time->stamp]['signed'].' '. $paytype .' - '.$booked[$time->stamp]['ref'] .'</span>'.$progress_service_icon.'</div></div></div>';
				}
				?>
			</div>
		<?php
		}

		if($f_slot || $b_slot)
		{
			echo '</div>';
		}
		?>						
		<div class="clear"></div>
	</div>
	<?php
        
}

?>
<!--<input type="hidden" id="all-bookings" value='<?php //echo json_encode($all_bookings); ?>' />-->
<input type="hidden" id="all-bookings" value='<?php echo htmlspecialchars(json_encode($all_bookings), ENT_QUOTES, 'UTF-8'); ?>' />