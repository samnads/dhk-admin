<div class="row m-0">   
    <div class="col-md-12">
    
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form id="edit-profile" class="form-horizontal" method="post" enctype="multipart/form-data">
                <ul>
                <li>
                    <i class="icon-th-list"></i>
                    <h3>Schedule Reports.</h3> 
                    
                    </li>
                    <li>
                
                <?php
                    if ($search['search_date'] == "") {
                        ?>
                        <input type="text" style="width: 160px;" id="schedule_date" name="schedule_date" value="<?php echo date('d/m/Y') ?>">


                        <?Php
                    } else {
                        ?>
                        <input type="text" style="width: 160px;" id="schedule_date" name="zone_date" value="<?php echo $search['search_date'] ?>">

                        <?Php
                    }
                    ?>

</li>
                    <li class="mr-2">
                    <span style="margin-left:23px;">Zone :</span>

                    <?php
                    if ($search['search_zone'] == "") {
                        ?>

                        <select style="width:160px;" id="zones" name="zones">
                            <option value="">-- Select Zone --</option>
                            <?php
                            if (count($zones) > 0) {
                                foreach ($zones as $zones_val) {
                                    ?>
                                    <option value="<?php echo $zones_val['zone_id']; ?>"><?php echo $zones_val['zone_name']; ?></option>

                                    <?php
                                }
                            }
                            ?>
                        </select>

                        <?Php
                    } else {
                        ?>
                        <select style="width:160px;" id="zones" name="zones">
                            <option value="">-- Select Zone --</option>
                            <?php
                            if (count($zones) > 0) {
                                foreach ($zones as $zones_val) {
                                    ?>
                                    <option value="<?php echo $zones_val['zone_id']; ?>" <?php echo isset($search['search_zone']) ? ($search['search_zone'] == $zones_val['zone_id'] ? 'selected="selected"' : '') : '' ?> ><?php echo $zones_val['zone_name']; ?></option>


                                    <?php
                                }
                            }
                            ?>
                        </select>
                        <?Php
                    }
                    ?>
                    </li>
                    <li>
                    <span style="margin-left:15px;"></span>
                    <input type="submit" class="n-btn" value="Go" name="schedule_report">
                    </li>
                    <li class="mr-0 float-right">

                    <div class="topiconnew border-0 green-btn">
                       <a id="printBtn" title="Print"> <i class="fa fa-download"></i></a>
                    </div> 
                    
                    </li>
                    </ul>
            </form>
            </div>
            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; width: 20px"><center> Sl.No.</center> </th>
                            <th style="line-height: 18px"> <center>Maid</center></th>
                            <th style="line-height: 18px"> <center>
                              Customer
                            </center></th>
                            <th style="line-height: 18px"> <center>
                              Time
                            </center></th>
                            <th style="line-height: 18px"> <center>Area</center></th>
                            <th style="line-height: 18px"> <center>Service Status</center></th>
                            <th style="line-height: 18px"> <center>Paid Amount</center></th>
                            <th style="line-height: 18px"> <center>Payment Status</center></th>
                        </tr>
                    </thead>
                    <tbody>
                         <?php
                        if (count($maids) != "") {
                            $i = 1;
                            foreach ($maids as $maids_val) {
                                ?> 
                                <?php
                                if (array_key_exists($maids_val['maid_id'], $schedule_report)) {
                                    foreach ($schedule_report as $schedule_report_val) {
                                        if ($maids_val['maid_id'] == $schedule_report_val['maid_id']) {
                                            $customer = $schedule_report_val['customer'];
                                            $zone = $schedule_report_val['zone'];
                                            $area = $schedule_report_val['area'];
                                            $start_time = $schedule_report_val['start_time'];
                                            $end_time = $schedule_report_val['end_time'];
                                            $service_status = $schedule_report_val['service_status'];
                                            $paid_amount = number_format($schedule_report_val['paid_amount'],2);
                                            $payment_status = $schedule_report_val['payment_status'];
                                        }
                                    }
                                }
                                ?>
                        
                        
                        <tr>
                            <td style="line-height: 18px; width: 20px"><?php echo $i++; ?> </td>
                            <td style="line-height: 18px"> <?php echo $maids_val['maid_name'] ?></td>
                            <td style="line-height: 18px"><?php
                                if (array_key_exists($maids_val['maid_id'], $schedule_report)) {
                                    echo  $customer;
                                }
                                else {

                                }
                                ?></td>
                            <td style="line-height: 18px"><?php
                                if (array_key_exists($maids_val['maid_id'], $schedule_report)) {
                                    echo  $start_time . '&nbsp; - ' . $end_time;
                                }
                                else {

                                }
                                ?></td>
                            <td style="line-height: 18px"> 
                                <?php
                                if (array_key_exists($maids_val['maid_id'], $schedule_report)) {
                                    echo  $area;
                                }
                                else {

                                }
                                ?>
                            </td>
                            <td style="line-height: 18px"> 
                                
                                <?php
                                if (array_key_exists($maids_val['maid_id'], $schedule_report)) {
                                    if($service_status == 1)
                                        echo "Service Started";
                                    else if($service_status == 2)
                                        echo "Service Finished";
                                    else if($service_status == 3)
                                        echo "Service Not Done";
                                }
                                else {
                                    
                                }
                                ?>
                            
                            </td>
                            <td style="line-height: 18px"> 
                            
                            <?php
                                if (array_key_exists($maids_val['maid_id'], $schedule_report)) {
                                    echo  $paid_amount;
                                }
                                else {
                                    
                                }
                                ?>
                            </td>
                            <td style="line-height: 18px"> 
                            
                                <?php
                                if (array_key_exists($maids_val['maid_id'], $schedule_report)) {
                                    if($payment_status == 0)
                                        echo "Not Paid";
                                    else if($payment_status == 1)
                                        echo "Paid";
                                    
                                }
                                else {
                                    
                                }
                                ?>
                            
                            </td>
                        </tr>
                        
                            <?php
                            }
                        }
                        ?>

                    </tbody>
                </table>
            </div><!-- /widget-content --> 
        </div>
        <!-- /widget --> 
    </div>
    <!-- /span12 --> 
</div>




<div id="divPrint" style="display: none">
    <div class="widget-content" style="margin-bottom:30px">
        <table border="1" width="100%" cellspacing="0" cellpadding = "0">
            <thead>
                <tr>
                    <th style="line-height: 18px; width: 20px"><center> Sl.No.</center> </th>
                    <th style="line-height: 18px"> <center>Maid</center></th>
                    <th style="line-height: 18px"> <center>Time</center></th>
                    <th style="line-height: 18px"> <center>Customer</center></th>
                    <th style="line-height: 18px"> <center>Area</center></th>
                    <th style="line-height: 18px"> <center>Service Status</center></th>
                    <th style="line-height: 18px"> <center>Paid Amount</center></th>
                    <th style="line-height: 18px"> <center>Payment Status</center></th>
                </tr>
            </thead>
            <tbody>
                 <?php
                if (count($maids) != "") {
                    $i = 1;
                    foreach ($maids as $maids_val) {
                        ?> 
                        <?php
                        if (array_key_exists($maids_val['maid_id'], $schedule_report)) {
                            foreach ($schedule_report as $schedule_report_val) {
                                if ($maids_val['maid_id'] == $schedule_report_val['maid_id']) {
                                    $customer = $schedule_report_val['customer'];
                                    $zone = $schedule_report_val['zone'];
                                    $area = $schedule_report_val['area'];
                                    $start_time = $schedule_report_val['start_time'];
                                    $end_time = $schedule_report_val['end_time'];
                                    $service_status = $schedule_report_val['service_status'];
                                    $paid_amount = number_format($schedule_report_val['paid_amount'],2);
                                    $payment_status = $schedule_report_val['payment_status'];
                                }
                            }
                        }
                        ?>


                <tr>
                    <td style="line-height: 18px; width: 20px"><?php echo $i++; ?> </td>
                    <td style="line-height: 18px"> <?php echo $maids_val['maid_name'] ?></td>
                    <input type="hidden" value="<?php echo $search['search_zone_name']?>" id="zone_name" name="zone_name">
                    <input type="hidden" value="<?php echo $search['search_day']?>" id="day" name="day">
                    <td style="line-height: 18px">                            
                        <?php
                        if (array_key_exists($maids_val['maid_id'], $schedule_report)) {
                            echo  $start_time . '&nbsp; - ' . $end_time;
                        }
                        else {

                        }
                        ?>                         
                    </td>
                    <td style="line-height: 18px"> 
                        <?php
                        if (array_key_exists($maids_val['maid_id'], $schedule_report)) {
                            echo  $customer;
                        }
                        else {

                        }
                        ?>
                    </td>
                    <td style="line-height: 18px"> 
                        <?php
                        if (array_key_exists($maids_val['maid_id'], $schedule_report)) {
                            echo  $area;
                        }
                        else {

                        }
                        ?>
                    </td>
                    <td style="line-height: 18px"> 

                        <?php
                        if (array_key_exists($maids_val['maid_id'], $schedule_report)) {
                            if($service_status == 1)
                                echo "Service Started";
                            else if($service_status == 2)
                                echo "Service Finished";
                            else if($service_status == 3)
                                echo "Service Not Done";
                        }
                        else {

                        }
                        ?>

                    </td>
                    <td style="line-height: 18px"> 

                    <?php
                        if (array_key_exists($maids_val['maid_id'], $schedule_report)) {
                            echo  $paid_amount;
                        }
                        else {

                        }
                        ?>
                    </td>
                    <td style="line-height: 18px"> 

                        <?php
                        if (array_key_exists($maids_val['maid_id'], $schedule_report)) {
                            if($payment_status == 0)
                                echo "Not Paid";
                            else if($payment_status == 1)
                                echo "Paid";

                        }
                        else {

                        }
                        ?>

                    </td>
                </tr>

                    <?php
                    }
                }
                ?>

            </tbody>
        </table>
    </div><!-- /widget-content --> 
</div>