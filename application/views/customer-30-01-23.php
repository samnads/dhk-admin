<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAzKGuULLF6eqMALYjRS7FncopnXnJI4ws&amp;libraries=places"></script> -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC_hBALevP_xk8OIkxyGQjt_oInuAW3Ivk&amp;libraries=places"></script>
<!-- this is replaced from PM -->

<link rel="stylesheet" href="https://booking.emaid.info:3443/emaid-demo/css/datepicker.css" />


<style>
    #edit-profile label, input, button, select, textarea{font-size: 12px !important;}
    
</style> 
<div class="row">
    <div class="span12">      		
        <div class="widget">
            <div class="widget-header">
                <i class="icon-user"></i>
                <h3>Customer</h3>
                <div class="topiconnew"><a href="<?php echo base_url(); ?>customers"><img src="<?php echo base_url(); ?>images/user-list-icon.png" title="Customer List"/></a></div>
            </div> <!-- /widget-header -->
            <div class="widget-content">
                <div class="tabbable">
                    <ul class="nav nav-tabs" id="mytabs">
                        <li class="active" id="firstli"><a href="#personal" data-toggle="tab">Personal Details</a></li>
                        <li id="secondli"><a href="#address-details" data-toggle="tab">Address Details</a></li>
                        <li id="thirdli"><a href="#account" data-toggle="tab">Account and Other Details</a></li>
                    </ul>
                    <br>
                    <form id="edit-profile" class="form-horizontal" method="post">      
                        <div class="tab-content">
                            <div class="tab-pane active" id="personal">
                                <!--                                                <form id="edit-profile" class="form-horizontal">                                                                                                    -->
                                
                                <?php 
                                if($message == "success")
                                {?>
                                    <div class="control-group">
                                        <label class="control-label"></label>
                                            <div class="controls">                                                            
                                                <div class="alert alert-success">
                                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                    <strong>Success!</strong> Customer added Successfully.
                                                </div>
                                            </div> <!-- /controls -->	
                                    </div> <!-- /control-group -->
                               <?php
                               }
                               ?>
                                
                                
                                
                                <fieldset>            
                                    <div class="span5" style="margin-left:0px;">
                                        <div class="widget">
                                            <div class="widget-content" style="border: 0px">
                                                <fieldset>
                                                    <div class="control-group">											
                                                        <label class="control-label" for="customer_name">Customer Name&nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
                                                            <input type="text" name="customer_name" class="span3" id="customer_name" onchang="document.getElementById('customer_nick').value = this.value">
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->				

                                                    <div class="control-group">											
                                                        <label class="control-label" for="firstname">Customer Nick Name&nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
                                                            <input type="text" class="span3" id="customer_nick" name="customer_nick">
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->
													
													<div class="control-group">											
                                                        <label class="control-label" for="firstname">Is Flagged&nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
															<label class="radio inline"><input type="radio"  name="flag" value="Y"> Yes</label>
															<label class="radio inline"><input type="radio" name="flag" checked="checked" value="N"> No</label>
															<!--<input type="text" class="span3" id="contact_person" name="contact_person">-->
														</div> <!-- /controls -->			
                                                    </div>
													
													<div class="control-group">											
                                                        <label class="control-label" for="reasonflag">Reason For Flagging</label>
                                                        <div class="controls">
                                                            <textarea class="span3" rows="1" id="reasonflag" name="reasonflag"></textarea>	
                                                        </div> <!-- /controls -->				
                                                </div> <!-- /control-group -->
                                                
                                                 <div class="control-group">											
                                                    <label class="control-label" for="basicinput">Mobile Number 1&nbsp;<font style="color: #C00">*</font></label>
                                                    <div class="controls">
                                                        <input type="text" class="span3" id="mobile_number1" name="mobile_number1">
                                                        <?php echo form_error('mobile_number1'); ?>
                                                    </div> <!-- /controls -->				
                                                </div> <!-- /control-group -->

                                                <div class="control-group">											
                                                    <label class="control-label" for="basicinput">Mobile Number 2</label>
                                                    <div class="controls">
                                                        <input type="text" class="span3" id="mobile_number2" name="mobile_number2">
                                                        <?php echo form_error('mobile_number2'); ?>
                                                    </div> <!-- /controls -->				
                                                </div> <!-- /control-group -->

                                                <div class="control-group">											
                                                    <label class="control-label" for="basicinput">Mobile Number 3</label>
                                                    <div class="controls">
                                                        <input type="text" class="span3" id="mobile_number3" name="mobile_number3">
                                                        <?php echo form_error('mobile_number3'); ?>
                                                    </div> <!-- /controls -->				
                                                </div> <!-- /control-group -->

                                                   <div class="control-group">											
                                                    <label class="control-label" for="basicinput">Phone</label>
                                                    <div class="controls">
                                                        <input type="text" class="span3" id="phone" name="phone">
                                                        <?php echo form_error('phone'); ?>
                                                    </div> <!-- /controls -->				
                                                </div> <!-- /control-group -->

                                                <div class="control-group">											
                                                    <label class="control-label" for="basicinput">Fax</label>
                                                    <div class="controls">
                                                        <input type="text" class="span3" id="fax" name="fax">
                                                    </div> <!-- /controls -->				
                                                </div> <!-- /control-group --> 
												
												<div class="control-group">											
                                                    <label class="control-label" for="basicinput">TRN#</label>
                                                    <div class="controls">
                                                        <input type="text" class="span3" id="trnnumber" name="trnnumber">
                                                    </div> <!-- /controls -->				
                                                </div>
												
												<div class="control-group">											
                                                    <label class="control-label" for="basicinput">VAT#</label>
                                                    <div class="controls">
                                                        <input type="text" class="span3" id="vatnumber" name="vatnumber">
                                                    </div> <!-- /controls -->				
                                                </div>

                                                    
                                                    

                                                    
                                                    <div class="control-group">
                                                        <label class="control-label" for="basicinput">Source&nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
                                                            <select name="customer_source" id="customer_source" class="span3" required >
                                                                <option value="">-Select-</option>
                                                                <option value="Live In">Live In</option>
                                                                <option value="Live Out">Live Out</option>
                                                                <option value="Rizek">Rizek</option>
                                                                <option value="Justmop">Justmop</option>
																<option value="Matic">Matic</option>
																<option value="ServiceMarket">ServiceMarket</option>
																<option value="Helpling">Helpling</option>
																<option value="Urban Clap">Urban Clap</option>
																<option value="Emaar">Emaar</option>
																<option value="MyHome">MyHome</option>
                                                                <option value="Facebook">Facebook</option>
                                                                <option value="Google">Google</option>
                                                                <option value="Yahoo">Yahoo</option>
                                                                <option value="Bing">Bing</option>
                                                                <option value="Direct Call" selected="selected">Direct Call</option>
                                                                <option value="Flyers">Flyers</option>
                                                                <option value="Referral">Referral</option>
                                                                <option value="Watchman/Security Guard">Watchman/Security Guard</option>
                                                                <option value="Maid">Maid</option>
                                                                <option value="Driver">Driver</option>
																<option value="By email">By email</option>
																<option value="Schedule visit">Schedule visit</option>
																<option value="Website">Website</option>
																<option value="Referred by staff">Referred by staff</option>
																<option value="Referred by customer">Referred by customer</option>
																<option value="Tekram">Tekram</option>
                                                            </select>
                                                        </div>
                                                    </div>
													<div class="control-group" id="customer_source_others" style="display: none;">
                                                        <label class="control-label" for="basicinput">Source Reference&nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
															<input type="text" class="span3" id="customer_source_others_val" name="customer_source_others_val">
                                                        </div>
                                                    </div>
                                                    <div class="control-group" id="customer_source_reference" style="display: none;">
                                                        <label class="control-label" for="basicinput">Source Reference&nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
                                                            <select name="customer_source_id" id="customer_source_id" class="span3" >
                                                                <option value="">-- Select Maids --</option>
                                                                <?php
                                                                if (!empty($maids) > 0) {
                                                                    foreach ($maids as $maid) {
                                                                        ?>
                                                                        <option value="<?php echo $maid['maid_id']; ?>"><?php echo $maid['maid_name']; ?></option>

                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </fieldset>

                                            </div> <!-- /widget-content -->
                                        </div> <!-- /widget -->
                                    </div> <!-- /span6 -->

                                    <div class="span5">
                                        <div id="target-2" class="widget">
                                            <div class="widget-content" style="border: 0px">

                                          <div class="control-group">
                                                        <label class="control-label" for="basicinput">Customer Type &nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
                                                            <select name="customer_type" class="span3" required >
                                                                <option value="HO" selected="selected">Home</option>
                                                                <option value="OF">Office</option>
                                                                <option value="WH">Warehouse</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                
                                                <!--Edited by vishnu-->
                                                <div class="control-group">											
                                                    <label class="control-label" for="basicinput">Is Company&nbsp;<font style="color: #C00">*</font></label>
                                                    <div class="controls">
                                                        <label class="radio inline"><input type="radio"  name="company" value="Y"> Yes</label>
                                                        <label class="radio inline"><input type="radio" name="company" checked="checked" value="N"> No</label>
                                                        <!--<input type="text" class="span3" id="contact_person" name="contact_person">-->
                                                    </div> <!-- /controls -->				
                                                </div> <!-- /control-group -->
                                                
                                                <div class="control-group" id="company_source" style="display: none;">											
                                                    <label class="control-label" for="basicinput">Select Company&nbsp;<font style="color: #C00">*</font></label>
                                                    <div class="controls">
                                                        <select name="company_name" id="company_name" class="span3">
                                                            <option value="">-Select-</option>
															<option value="Live In">Live In</option>
															<option value="Live Out">Live Out</option>
															<option value="Rizek">Rizek</option>
															<option value="Justmop">Justmop</option>
															<option value="Matic">Matic</option>
															<option value="ServiceMarket">ServiceMarket</option>
															<option value="Helpling">Helpling</option>
															<option value="Urban Clap">Urban Clap</option>
															<option value="Emaar">Emaar</option>
															<option value="MyHome">MyHome</option>
                                                        </select>
                                                    </div> <!-- /controls -->				
                                                </div> <!-- /control-group -->
                                                
                                                <!-- Ends -->
												
												<div class="control-group">											
                                                    <label class="control-label" for="basicinput">Contact Person&nbsp;<font style="color: #C00">*</font></label>
                                                    <div class="controls">
                                                        <input type="text" class="span3" id="contact_person" name="contact_person">
                                                    </div> <!-- /controls -->				
                                                </div> <!-- /control-group -->
												

                                                <div class="control-group">											
                                                    <label class="control-label" for="basicinput">Website URL</label>
                                                    <div class="controls">
                                                        <input type="text" class="span3" id="website" name="website">
                                                    </div> <!-- /controls -->				
                                                </div> <!-- /control-group -->

                                                <div class="control-group">											
                                                    <label class="control-label" for="basicinput">User Name</label>
                                                    <div class="controls">
                                                        <input type="text" class="span3" id="user_name" name="user_name">
                                                    </div><!-- /controls -->			
                                                </div> <!-- /control-group -->

                                                <div class="control-group">											
                                                    <label class="control-label" for="email">Email Address&nbsp;<font style="color: #C00">*</font></label>
                                                    <div class="controls">
                                                        <input type="email" class="span3" id="email" name="email" >
                                                    </div> <!-- /controls -->				
                                                </div> <!-- /control-group -->

                                                <div class="control-group">											
                                                    <label class="control-label" for="password1">Password</label>
                                                    <div class="controls">
                                                        <input type="password" class="span3" id="password1" name="password">
                                                    </div> <!-- /controls -->				
                                                </div> <!-- /control-group -->   
                                                
                                                
                                                <div class="control-group">											
                                                        <label class="control-label" for="basicinput">Notes</label>
                                                        <div class="controls">
                                                            <textarea class="span3" rows="5" id="notes" name="notes"></textarea>	
                                                        </div> <!-- /controls -->				
                                                </div> <!-- /control-group -->

                                            </div> <!-- /widget-content -->
                                        </div> <!-- /widget -->
                                    </div> <!-- /span6 -->

                                </fieldset>  
                                <button type="button" class="btn mm-btn pull-right" id="clickfirst">Next</button>
                            </div>
                            <div class="tab-pane" id="address-details">
                                <fieldset>            
                                    <div class="span7" style="margin-left:0px;">
                                        <div class="widget">
                                            <div class="widget-content" style="border: 0px">
                                                <div class="control-group">
                                                        <label class="control-label" for="basicinput">Area&nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
                                                            <select name="area[]" id="area" class="span3 area_customer" style="min-width: 270px;" required >
                                                                <option value="" selected>Select Area</option>
                                                                <?php
                                                                if (count($areas) > 0) {
                                                                    foreach ($areas as $areasVal) {
                                                                        ?>
                                                                        <option value="<?php echo $areasVal['area_id'] ?>"><?php echo $areasVal['area_name'] ?></option>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>

                                                            </select>
                                                        </div>
                                                    </div>
                                                
                                                <div class="control-group">
                                                        <label class="control-label" for="basicinput">Apartment No&nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
                                                            <input id="apartment_no" type="text" name="apartment_no[]" class="span3" style="min-width: 270px;"/>
                                                        </div>
                                                    </div>
                                               
                                                 <div class="control-group">											
                                                        <label class="control-label" for="basicinput">Address&nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
                                                            <input id="address" type="text" name="address[]" onkeyup="loadLocationField('address','1');" class="span3" style="min-width: 270px;"/>
                                                           <input type="hidden" name="lat[]" id="latitude_1" value=""/>
                                                           <input type="hidden" name="lng[]" id="longitude_1" value=""/>
<!--                                                            <textarea class="span3" rows="5" id="address" name="address[]"></textarea>	-->
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->
                                                    
                                                   
                                                    <div id="map_canvas_1"></div>
                                              
                                                    <div class="input_fields_wrap"></div><br>
                                                    
                                                    <div class="control-group">											
                                                        <div class="controls">
                                                            <a style="float:right ; margin-right:200px; cursor:pointer;" class="add_field_button">Add More Address</a>	
                                                        </div> <!-- /controls -->	
                                                    </div> <!-- /control-group -->
                                             
                                            </div>
                                        </div> 
                                     </div> 
                                  
                                 </fieldset>
                                <button type="button" class="btn mm-btn pull-right" id="clicksecond">Next</button>
                              </div>  

                            <div class="tab-pane" id="account">

                                <fieldset>            
                                    <div class="span5" style="margin-left:0px;">
                                        <div class="widget">
                                            <div class="widget-content" style="border: 0px">
                                                <fieldset>
                                                    <div class="control-group">
                                                        <label class="control-label" for="basicinput">Customer Book Type&nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
                                                            <select name="customers_type" id="customers_type" class="span3" required >
                                                                <option value="0" selected="selected">Non Regular</option>
                                                                <option value="1">Regular</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    
                                                    
                                                    <div class="control-group">
                                                        <label class="control-label" for="basicinput">Payment Type&nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
                                                            <select name="payment_type" id="payment_type" class="span3" required >
                                                                <option value="D" selected="selected">Daily Paying</option>
                                                                <option value="W">Weekly Paying</option>
                                                                <option value="M">Monthly Paying</option>
                                                            </select>
                                                        </div>
                                                    </div>


                                                    <div class="control-group">
                                                        <label class="control-label" for="basicinput">Payment Mode&nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
                                                            <select name="payment_mode" id="payment_mode" class="span3" required >
                                                                <option value="Cash" selected="selected">Cash</option>
                                                                <option value="Cheque">Cheque</option>
                                                                <option value="Online">Online</option>
																<option value="Credit Card">Credit Card</option>
                                                            </select>
                                                        </div>
                                                    </div>


                                                    <div class="control-group">											
                                                        <label class="control-label" for="basicinput">Hourly &nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
                                                            <input type="text" class="span3" id="hourly" name="hourly" value="35">

                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->


                                                    <div class="control-group">											
                                                        <label class="control-label" for="basicinput">Extra &nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
                                                            <input type="text" class="span3" id="extra" name="extra" value="35">
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->


                                                    <div class="control-group">											
                                                        <label class="control-label" for="basicinput">Weekend &nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
                                                            <input type="text" class="span3" id="weekend" name="weekend" value="35">
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group --> 

                                                </fieldset>

                                            </div> <!-- /widget-content -->
                                        </div> <!-- /widget -->
                                    </div> <!-- /span6 -->


                                    <div class="span5">
                                        <div id="target-2" class="widget">
                                            <div class="widget-content" style="border: 0px">
                                                <fieldset>
                                                    <div class="control-group">											
                                                        <label class="control-label" for="basicinput">Latitude</label>
                                                        <div class="controls">
                                                            <input type="text" class="span3" id="latitude" name="latitude">
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->


                                                    <div class="control-group">											
                                                        <label class="control-label" for="basicinput">Longitude</label>
                                                        <div class="controls">
                                                            <input type="text" class="span3" id="longitude" name="longitude">
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->


                                                    <div class="control-group">											
                                                        <label class="control-label">Key</label>
                                                        <div class="controls">
                                                            <label class="radio inline"><input type="radio"  name="key" value="Y"> Yes</label>
                                                            <label class="radio inline"><input type="radio" name="key" checked="checked" value="N"> No</label>
                                                        </div>	<!-- /controls -->			
                                                    </div> <!-- /control-group -->
                                                    
                                                    <div class="control-group">                                         
                                                        <label class="control-label">Rating Mail</label>
                                                        <div class="controls">
                                                            <label class="radio inline"><input type="radio"  name="rating_mail_stat" checked="checked" value="Y"> Yes</label>
                                                            <label class="radio inline"><input type="radio" name="rating_mail_stat"  value="N"> No</label>
                                                        </div>  <!-- /controls -->          
                                                    </div> <!-- /control-group -->

                                                    <div class="control-group">
                                                        <label class="control-label" for="basicinput">Upload Photo</label>
                                                        <div class="controls">
                                                            <div id="me" class="styleall" style=" cursor:pointer;"> 
                                                                <span style=" cursor:pointer;">                             
                                                                    <img src="<?php echo base_url();?>img/profile_pic.jpg" style="height: 100px; width: 100px"/> 
                                                                </span> 
                                                            </div> 
                                                        </div>
                                                    </div>

                                                    
                                                    <br />
                                                </fieldset>
                                            </div> <!-- /widget-content -->
                                        </div> <!-- /widget -->
                                    </div> <!-- /span6 -->
                                </fieldset>            

                                <div class="form-actions" style="padding-left: 211px;">
                                    <input type="hidden" name="call_method" id="call_method" value="customer/customerimgupload"/>
                                    <input type="hidden" name="img_fold" id="img_fold" value="customer_img"/>
                                    <input type="hidden" name="img_name_resp" id="img_name_resp"/>
                                    <input type="submit" class="btn mm-btn pull-right" value="Submit" name="customer_sub" onclick="return validate_customer();">
                                </div>   
                            </div>
                        </div>	
                    </form>
                </div>
            </div> <!-- /widget-content -->						
        </div> <!-- /widget -->	      		
    </div> <!-- /span8 -->	      		      		      		      	
</div> <!-- /row -->