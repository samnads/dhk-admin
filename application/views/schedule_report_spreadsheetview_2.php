<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=".$file_name."");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table border="1">
    <thead>
        <tr style="background-color:yellow">
            <th style="line-height: 18px; padding: 10px;">Sl. No.</th>
			<th style="line-height: 18px; padding: 10px;">Name</th>
			<th style="line-height: 18px; padding: 10px;">Phone</th>
			<th style="line-height: 18px; padding: 10px;">Email</th>
			<th style="line-height: 18px; padding: 10px;">Date of Service</th>
			<th style="line-height: 18px; padding: 10px;">Booking Type</th>
			<th style="line-height: 18px; padding: 10px;">Booking Source</th>
			<th style="line-height: 18px; padding: 10px;">Cleaner</th>
			<th style="line-height: 18px; padding: 10px;">Hours</th>
			<th style="line-height: 18px; padding: 10px;">Amount</th>
			<th style="line-height: 18px; padding: 10px;">Status</th>
			<th style="line-height: 18px; padding: 10px;">Started By</th>
			<th style="line-height: 18px; padding: 10px;">Service Name</th>
			<th style="line-height: 18px; padding: 10px;">Customer Added Date</th>
			<th style="line-height: 18px; padding: 10px;">Star Rating</th>
			<th style="line-height: 18px; padding: 10px;">Comments</th>
			<th style="line-height: 18px; padding: 10px;">Customer Area-Zone</th>
        </tr>
    </thead>
    <tbody>
		<?php
		if(!empty($reports))
		{
			$i = 0;
			$totamount = 0;
			$total_hours = 0;
			foreach ($reports as $rep)
            {
            	foreach ($rep as $report)
                {
				$totamount += $report->total_amount;
		?>
			<tr>
				<td style="line-height: 18px;"><?php echo ++$i; ?></td>
				<td style="line-height: 18px;"><?php echo $report->customer_name; ?></td>
				<td style="line-height: 18px;"><?php echo $report->mobile_number_1 ?: '-'; ?></td>
				<td style="line-height: 18px;"><?php echo $report->email_address ?: '-'; ?></td>
				<td style="line-height: 18px;"><?php echo $report->sch_date; ?></td>
				<td style="line-height: 18px;">
					<?php
					if($report->booking_type == "OD")
					{
						echo "One Day";
					} else if($report->booking_type == "WE"){
						echo "Weekly";
					} else if($report->booking_type == "BW"){
						echo "Bi-Weekly";
					}
					$hrs=strtotime($report->time_to)-strtotime($report->time_from);
					$total_hours += (strtotime($report->time_to) - strtotime($report->time_from))/3600;
					?>
				</td>
				<td style="line-height: 18px;"><?php echo $report->customer_source ?: '-'; ?></td>
				<td style="line-height: 18px;"><?php echo $report->maid_name ?: '-'; ?></td>
				<td style="line-height: 18px;"><?php echo gmdate("H:i", $hrs); ?></td>
				<td style="line-height: 18px;"><?php echo $report->total_amount ?: '-'; ?></td>
				<td style="line-height: 18px;">
				<?php
				if($report->service_status == 1)
				{
					echo "In Progress";
				} else if($report->service_status == 2){
					echo "Finished";
				} else if($report->service_status == 3){
					echo "Cancelled";
				} else {
					echo "Not Started";
				}
				?>
				</td>
				<td style="line-height: 18px;"><?php echo $report->drivername; ?></td>
				<td style="line-height: 18px;"><?php echo $report->service_type_name; ?></td>
				<td style="line-height: 18px;"><?php echo date('d/m/Y',strtotime($report->customer_added_datetime)); ?></td>
				<td style="line-height: 18px; text-align: center;">
					<?php
					if($report->rating > 0)
					{
						echo $report->rating;
					}
					else{
						echo '-';
					}
					?>
				</td>
				<td style="line-height: 18px;"><?php echo $report->comments ?: '-'; ?></td>
				<td style="line-height: 18px;"><?php echo $report->area_name;?>-<?php echo $report->zone_name; ?></td>
			</tr>
		<?php
				}
			}
		}
		
		?>
	</tbody>
	<tfoot>
		<tr>
			<td style="line-height: 18px;" colspan="7"></td>
			<td style="line-height: 18px;">Total</td>
			<td style="line-height: 18px;"><?=  $total_hours ?></td>
			<td style="line-height: 18px;"><?php echo $totamount; ?></td>
			<td style="line-height: 18px;" colspan="6"></td>
		</tr>
	</tfoot>
</table>