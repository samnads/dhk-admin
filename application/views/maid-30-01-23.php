<div class="row m-0">
    <div class="col-md-12">      		
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-user"></i>
                <h3>Maid</h3>
                
                <!--<a style="float:right ; margin-right:20px; cursor:pointer;" href="<?php// echo base_url();?>maids"><img src="<?php// echo base_url(); ?>img/list_female_user.png" title="Maid List"/></a>-->
                <div class="topiconnew"><a href="<?php echo base_url(); ?>maids"><img src="<?php echo base_url(); ?>images/maid-list-icon.png" title="Maid List"/></a></div>
            </div> <!-- /widget-header -->
            <div class="widget-content">
                <div class="tabbable">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#personal" data-toggle="tab">Personal Details</a></li>
                        <!--<li><a href="#attachments" data-toggle="tab">Attachments</a></li>-->
                        
                    </ul>
                    <br>
                    <form id="edit-profile" class="form-horizontal" method="post" enctype="multipart/form-data">      
                        <div class="tab-content">
                            <div class="tab-pane active" id="personal">
                                <!--                                                <form id="edit-profile" class="form-horizontal">                                                                                                    -->
                                
                                <?php 
                                if($message == "success")
                                {?>
                                    <div class="control-group">
                                        <label class="control-label"></label>
                                            <div class="controls">                                                            
                                                <div class="alert alert-success">
                                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                    <strong>Success!</strong> Maid added Successfully.
                                                </div>
                                            </div> <!-- /controls -->	
                                    </div> <!-- /control-group -->
                               <?php
                               }
                               ?>
                                
                                
                                <fieldset>            
                                    <div class="span5">
                                        <div class="widget">
                                            <div class="widget-content" style="border: 0px">
                                                <fieldset>
                                                    <div class="control-group">											
                                                        <label class="control-label" for="firstname">Maid Name&nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
                                                            <input type="text" class="span3" id="maid_name" name="maid_name">
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->				

                                                    <div class="control-group">											
                                                        <label class="control-label">Gender&nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
                                                            <label class="radio inline">
                                                                <input type="radio" name="gender" value="M" checked="checked"> Male
                                                            </label>
                                                            <label class="radio inline">
                                                                <input type="radio" name="gender" value="F"> Female
                                                            </label>
                                                        </div>	<!-- /controls -->			
                                                    </div> <!-- /control-group -->


                                                    <div class="control-group">											
                                                        <label class="control-label" for="firstname">Nationality&nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
                                                            <input type="text" class="span3" id="nationality" name="nationality">
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->		

                                                    <div class="control-group">
                                                        <label class="control-label" for="basicinput">Upload Photo</label>
                                                        <div class="controls">
                                                            <div id="me" class="styleall" style=" cursor:pointer;"> 
                                                                <span style=" cursor:pointer;">                             
                                                                    <img src="<?php echo base_url(); ?>img/profile_pic.jpg" style="height: 100px; width: 100px"/> 
                                                                </span> 
                                                            </div> 
                                                        </div>
                                                    </div>    



                                                    <div class="control-group">											
                                                        <label class="control-label" for="firstname">Present Address&nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
                                                            <textarea class="span3" rows="5" id="present_address" name="present_address"></textarea>	
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->



                                                    <div class="control-group">											
                                                        <label class="control-label" for="firstname">Permanent Address&nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
                                                            <textarea class="span3" rows="5" id="permanent_address" name="permanent_address"></textarea>	
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->



                                                </fieldset>

                                            </div> <!-- /widget-content -->
                                        </div> <!-- /widget -->
                                    </div> <!-- /span6 -->




                                    <div class="span5">
                                        <div id="target-2" class="widget">
                                            <div class="widget-content" style="border: 0px">


                                                <div class="control-group">											
                                                    <label class="control-label" for="firstname">Date of Joining &nbsp;<font style="color: #C00">*</font></label>
                                                    <div class="controls">
                                                        <input type="text" class="span3" id="doj" name="doj">
                                                    </div> <!-- /controls -->				
                                                </div> <!-- /control-group -->
                                                <div class="control-group">											
                                                    <label class="control-label" for="firstname">Mobile Number 1&nbsp;<font style="color: #C00">*</font></label>
                                                    <div class="controls">
                                                        <input type="text" class="span3" id="mobile1" name="mobile1">
                                                    </div> <!-- /controls -->				
                                                </div> <!-- /control-group -->


                                                <div class="control-group">											
                                                    <label class="control-label" for="firstname">Mobile Number 2</label>
                                                    <div class="controls">
                                                        <input type="text" class="span3" id="mobile2" name="mobile2">
                                                    </div> <!-- /controls -->				
                                                </div> <!-- /control-group -->


                                                <div class="control-group">
                                                    <label class="control-label" for="basicinput">Flat&nbsp;<font style="color: #C00">*</font></label>
                                                    <div class="controls">
                                                        <select name="flat" id="flat" class="span3" required >
                                                            <?php
                                                            if(count($flats)>0)
                                                            {
                                                                foreach ($flats as $flatsVal)
                                                                {
                                                                ?>
                                                                        <option value="<?php echo $flatsVal['flat_id']?>"><?php echo $flatsVal['flat_name']?></option>
                                                                <?php
                                                                }
                                                            }
                                                            ?>
                                                            
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                                <div class="control-group">
                                                    <label class="control-label" for="basicinput">Team&nbsp;<!--<font style="color: #C00">*</font>--></label>
                                                    <div class="controls">
                                                        <select name="team" id="team" class="span3">
                                                            <option value="">Select Team</option>
                                                            <?php
                                                            if(count($teams)>0)
                                                            {
                                                                foreach ($teams as $teamsVal)
                                                                {
                                                                ?>
                                                                        <option value="<?php echo $teamsVal->team_id; ?>"><?php echo $teamsVal->team_name; ?></option>
                                                                <?php
                                                                }
                                                            }
                                                            ?>
                                                            
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="control-group">											
                                                    <label class="control-label">Services &nbsp;<font style="color: #C00">*</font></label>
                                                    <div class="controls">
                                                        
                                                        <?php
                                                        if( count($services)>0 )
                                                        {
                                                            foreach ($services as $serviceVal)
                                                            {
                                                                ?>
                                                        <label class="checkbox"><input type="checkbox" class="services" name="services[]" value="<?php echo $serviceVal['service_type_id']?>"> <?php echo $serviceVal['service_type_name']?></label>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                       
                                                    </div><!-- /controls -->		
                                                </div> <!-- /control-group -->


                                                <div class="control-group">											
                                                    <label class="control-label" for="firstname">Notes</label>
                                                    <div class="controls">
                                                        <textarea class="span3" rows="5" id="notes" name="notes"></textarea>	
                                                    </div> <!-- /controls -->				
                                                </div> <!-- /control-group -->
												
												<div class="control-group">											
                                                            <label class="control-label" for="username">Username</label>
                                                            <div class="controls">
                                                                <input type="text" class="span3" id="username" name="username">
                                                            </div> <!-- /controls -->				
                                                        </div> <!-- /control-group -->
														<div class="control-group">											
                                                            <label class="control-label" for="password">Password</label>
                                                            <div class="controls">
                                                                <input type="text" class="span3" id="password" name="password">
                                                            </div> <!-- /controls -->				
                                                        </div> <!-- /control-group -->

                                            </div> <!-- /widget-content -->
                                        </div> <!-- /widget -->
                                    </div> <!-- /span5 -->

                                </fieldset>   
                                    <div class="form-actions" style="padding-left: 211px;">
                                    
                                    <input type="hidden" name="call_method" id="call_method" value="maid/maidimgupload"/>
                                    <input type="hidden" name="img_fold" id="img_fold" value="maidimg"/>
                                    <input type="hidden" name="img_name_resp" id="img_name_resp"/>
                                    <input type="submit" class="btn mm-btn pull-right" value="Submit" name="maid_sub" onclick="return validate_maid();">
                                    
                                    
                                </div> 
                            </div>

                            <div class="tab-pane" id="attachments">
                                <fieldset>            

                                    <div class="span5">
                                        <div class="widget">
                                            <div class="widget-content" style="border: 0px">
                                                <fieldset>
                                                    <div class="control-group">											
                                                        <label class="control-label" for="firstname">Passport Number</label>
                                                        <div class="controls">
                                                            <input type="text" class="span3" id="passport_number" name="passport_number">
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->

                                                    <div class="control-group">											
                                                        <label class="control-label" for="firstname">Passport Expiry</label>
                                                        <div class="controls">
                                                            <input type="text" class="span3" id="passport_expiry" name="passport_expiry">
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->


                                                    <div class="control-group">											
                                                        <label class="control-label" for="firstname">Attach Passport</label>
                                                        <div class="controls">
                                                            <input type="file" class="span3" id="attach_passport" name="attach_passport">
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->

                                                    <div class="control-group">											
                                                        <label class="control-label" for="email">Visa Number</label>
                                                        <div class="controls">
                                                            <input type="text" class="span3" id="visa_number" name="visa_number">
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->


                                                    <div class="control-group">											
                                                        <label class="control-label" for="firstname">Visa Expiry</label>
                                                        <div class="controls">
                                                            <input type="text" class="span3" id="visa_expiry" name="visa_expiry">
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->


                                                    <div class="control-group">											
                                                        <label class="control-label" for="firstname">Attach Visa</label>
                                                        <div class="controls">
                                                            <input type="file" class="span3" id="attach_visa" name="attach_visa">
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->


                                                </fieldset>

                                            </div> <!-- /widget-content -->
                                        </div> <!-- /widget -->
                                    </div> <!-- /span6 -->


                                    <div class="span5">
                                        <div id="target-2" class="widget">
                                            <div class="widget-content" style="border: 0px">
                                                <fieldset>
                                                    <div class="control-group">											
                                                        <label class="control-label" for="email">Labour Card Number</label>
                                                        <div class="controls">
                                                            <input type="text" class="span3" id="labour_number" name="labour_number">
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->


                                                    <div class="control-group">											
                                                        <label class="control-label" for="firstname">Labour Card Expiry</label>
                                                        <div class="controls">
                                                            <input type="text" class="span3" id="labour_expiry" name="labour_expiry">
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->


                                                    <div class="control-group">											
                                                        <label class="control-label" for="firstname">Attach Labour Card</label>
                                                        <div class="controls">
                                                            <input type="file" class="span3" id="attach_labour" name="attach_labour">
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->

                                                    <div class="control-group">											
                                                        <label class="control-label" for="email">Emirates Id</label>
                                                        <div class="controls">
                                                            <input type="text" class="span3" id="emirates_id" name="emirates_id">
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->


                                                    <div class="control-group">											
                                                        <label class="control-label" for="firstname">Emirates Id Expiry</label>
                                                        <div class="controls">
                                                            <input type="text" class="span3" id="emirates_expiry" name="emirates_expiry">
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->


                                                    <div class="control-group">											
                                                        <label class="control-label" for="firstname">Attach Emirates Card</label>
                                                        <div class="controls">
                                                            <input type="file" class="span3" id="attach_emirates" name="attach_emirates">
                                                        </div> <!-- /controls -->				
                                                    </div> <!-- /control-group -->

                                                    <br />
                                                </fieldset>
                                            </div> <!-- /widget-content -->
                                        </div> <!-- /widget -->
                                    </div> <!-- /span5 -->

                                </fieldset>            

<!--                                <div class="form-actions" style="padding-left: 211px;">
                                    
                                    <input type="hidden" name="call_method" id="call_method" value="maid/maidimgupload"/>
                                    <input type="hidden" name="img_fold" id="img_fold" value="maidimg"/>
                                    <input type="hidden" name="img_name_resp" id="img_name_resp"/>
                                    <input type="submit" class="btn btn-primary pull-right" value="Submit" name="maid_sub" onclick="return validate_maid();">
                                    
                                    
                                </div>   -->

                                
                            </div>
                        </div>	
                    </form>
                </div>
            </div> <!-- /widget-content -->					
        </div> <!-- /widget -->	      		
    </div> <!-- /span8 -->	      		      		      		      	
</div> <!-- /row -->
