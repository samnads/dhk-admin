<div class="row m-0">   
    <div class="col-sm-12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
        
        <div class="widget-header">
        <form class="form-horizontal" method="post">
     <div class="book-nav-top">
            <ul>
              <li><i class="icon-th-list"></i><h3>Activity</h3></li>
              
              <li class="mr-2">
                  <input type="text" style="width: 160px;" id="vehicle_date" name="service_date" value="<?php echo $service_date ?>"> 
              </li>
      
      
      
              <li class="mr-2">
                  <input type="hidden" id="formatted-date" value="<?php echo $formatted_date ?>"/>
              </li>
      
      
      
              <li>
                  <input type="submit" class="n-btn" value="Go" name="vehicle_report"> 
              </li>
      
      
      
              <li class="mr-0 float-right">
                  <div class="topiconnew border-0 green-btn">
                    	<a href="<?php echo base_url() . 'reports/activity_summary_view/' . $formatted_date; ?>" target="_blank" title="Download to Excel"> <i class="fa fa-print"></i></a>
                    </div>
              </li>
      
      
      
              <div class="clear"></div>
            </ul>
     </div>
     </form>
</div>



            <div class="widget-content" style="margin-bottom:30px">
            

                
                <!--New table starts-->
                <div class="Table table-top-style-box">
                    
               
                <div class="Heading table-head">

                        <div class="Cell">
                            <p><strong>NO.</strong></p>
                        </div>

                        <div class="Cell">
                            <p><strong> MAID</strong></p>
                        </div>

                        <div class="Cell">
                            <p><strong>CUSTOMER</strong></p>
                        </div>
                        <div class="Cell">
                            <p><strong>WORKING HOURS</strong></p>
                        </div>

                        <div class="Cell">
                            <p><strong>BILLED AMOUNT</strong></p>
                        </div>

                        <div class="Cell">
                            <p><strong>COLLECTED AMOUNT</strong></p>
                        </div>

                        <div class="Cell">
                            <p><strong> BALANCE AMOUNT</strong></p>
                        </div>

                        <div class="Cell">
                            <p><strong>PS NO</strong></p>
                        </div>
                        <div class="Cell">
                            <p><strong>STATUS</strong></p>
                        </div>

                    </div>
                    
                    
                    <?php
                        if(!empty($dayservices))
                        {
                            $i = 0;
                            $zone_id = 0;                            
                            $total_fee = 0;
                            $material_fee = 0;
                            $collected_total_fee = 0;
                            $total_wrk_hrs = 0;
                            $collected_amount = 0;
                            $ztotal_wrk_hrs = 0;
                     
                            foreach ($dayservices as $service)
                            {
                                //Payment Type
                                    if($service->payment_type == "D")
                                    {
                                        $paytype = "(D)";
                                    } else if($service->payment_type == "W")
                                    {
                                        $paytype = "(W)";
                                    } else if($service->payment_type == "M")
                                    {
                                        $paytype = "(M)";
                                    } else
                                    {
                                        $paytype = "";
                                    }
                                
                                                                                                                               
                                $service_status = $service->service_status == 1 ? 'ON GOING' : ($service->service_status == 2 ? 'FINISHED' : ($service->service_status == 3 ? 'CANCELLED' : 'NOT STARTED'));                                
                                $class = $service->service_status == 1 ? 'on-going' : ($service->service_status == 2 ? 'finished' : ($service->service_status == 3 ? 'cancelled' : 'not-started'));
                                //$service_status .=  $service->payment_status == 1 ? (' ' . $service->total_fee) : ($service->payment_status == 0  && $service->service_status == 2 ? ' NP': '');
                                
                                $activity_status = 0;
                                //$activity_status = $service->service_status ? $service->service_status : 0;                                                                
                                $activity_status = $service->service_status == 1 ? 1 : ($service->service_status == 2 ? 2 : ($service->service_status == 3 ? 3 : 0));                                                                
                                $activity_status =  $service->payment_status == 1 ? 5 : ($service->payment_status == 0  && $service->service_status == 2 ? 6: ($service->service_status == 1 && $service->payment_status != NULL  ? 1 : ( $service->service_status == 3 ? 3 : 0)));
                                $service->total_fee = $service->service_status == 2 ? $service->total_fee : '';
                                $service->material_fee = $service->service_status == 2 ? $service->material_fee : '';
                                if($zone_id != $service->zone_id)
                                {
                                    if($ztotal_wrk_hrs != 0)
                                    {

                                     echo '<div class="Row">

                                <div class="Cell">
                                    <p></p>
                                </div>
                                <div class="Cell">
                                    <p></p>
                                </div>
                                <div class="Cell">
                                    <p></p>
                                </div>
                                <div class="Cell">
                                    <p><b>' . $ztotal_wrk_hrs . ' Hrs</b></p>
                                </div>
                                <div class="Cell">
                                    <p></p>
                                </div>
                                <div class="Cell">
                                    <p></p>
                                </div>
                                <div class="Cell">
                                    <p></p>
                                </div>
                                <div class="Cell">
                                    <p></p>
                                </div>
                                <div class="Cell">
                                    <p></p>
                                </div>
                                
</div>
';   
                                        
                                        
//                                        echo '<tr>'
//                                                . '<td style="line-height: 18px;"></td>'
//                                                . '<td style="line-height: 18px;"></td>'
//                                                . '<td style="line-height: 18px;"></td>'
//                                                . '<td style="line-height: 18px; text-align : right;"><b>' . $ztotal_wrk_hrs . ' Hrs</b></td>'
//                                                . '<td style="line-height: 18px;"></td>'
//                                                . '<td style="line-height: 18px;"></td>'
//                                                . '<td style="line-height: 18px;"></td>'
//                                                . '<td style="line-height: 18px;"></td>'
//                                                . '<td style="line-height: 18px;"></td>'
//                                                //. '<td style="line-height: 18px;"></td>'
//                                             . '</tr>';
                                        $ztotal_wrk_hrs = 0;
                                    }
                                    
                                    $zone_id = $service->zone_id;
                                    
                                  
                                    
                                            echo '<div class="Row">

                                <div class="Cell">
                                    <p></p>
                                </div>
                                <div class="Cell">
                                    <p><b style="color:#CB3636;">' . $service->zone_name . '</b></p>
                                </div>
                                <div class="Cell">
                                    <p></p>
                                </div>
                                <div class="Cell">
                                    <p></p>
                                </div>
                                <div class="Cell">
                                    <p></p>
                                </div>
                                <div class="Cell">
                                    <p></p>
                                </div>
                                <div class="Cell">
                                    <p></p>
                                </div>
                                <div class="Cell">
                                    <p></p>
                                </div>
                                <div class="Cell">
                                    <p></p>
                                </div>
                                
</div>
';  
//                                    echo '<tr>'
//                                            . '<td style="line-height: 18px;"></td>'
//                                            . '<td style="line-height: 18px;"><b style="color:#CB3636;">' . $service->zone_name . '</b></td>'
//                                            . '<td style="line-height: 18px;"></td>'
//                                            . '<td style="line-height: 18px;"></td>'
//                                            . '<td style="line-height: 18px;"></td>'
//                                            . '<td style="line-height: 18px;"></td>'
//                                            . '<td style="line-height: 18px;"></td>'
//                                            . '<td style="line-height: 18px;"></td>'
//                                            . '<td style="line-height: 18px;"></td>'
//                                         . '</tr>';
                                    
                                    
                                }
                                
                                $total_fee += $service->total_fee;
                                $material_fee += $service->material_fee;
                                $collected_total_fee += $service->collected_amount;
                                $collected_amount += ($service->payment_status == 1 ? $service->total_fee : 0);
                                $total_wrk_hrs += ($service->service_status == 2 ? ((strtotime($service->end_time) - strtotime($service->start_time))/ 3600) : 0);
                                $ztotal_wrk_hrs += ($service->service_status == 2 ? ((strtotime($service->end_time) - strtotime($service->start_time))/ 3600) : 0);
                                
                                
                                  echo '<div class="Row">

                                <div class="Cell book_type_'.$service->booking_type.'">
                                    <p>' . ++$i . '</p>
                                </div>
                                <div class="Cell book_type_'.$service->booking_type.'">
                                    <p>' . $service->maid_name . '</p>
                                </div>
                                <div class="Cell book_type_'.$service->booking_type.'">
                                    <p>' . $service->customer_name . ' <b style="color:#006600;">(' . ($service->actual_zone ? substr($service->actual_zone, 2) : $service->zone_name) . '</b>) '.$paytype.'</p>
                                </div>
                                <div class="Cell book_type_'.$service->booking_type.'">
                                    <p>' . $service->time_from . '-' . $service->time_to . ' [' . ((strtotime($service->end_time) - strtotime($service->start_time))/ 3600) . ']' . '</p>
                                </div>
                                <div class="Cell book_type_'.$service->booking_type.'">
                                    <p>' . $service->total_fee . '('.$service->material_fee.')</p>
                                </div>
                                <div class="Cell book_type_'.$service->booking_type.'">
                                    <p>' . $service->collected_amount . '</p>
                                </div>
                                <div class="Cell book_type_'.$service->booking_type.'">
                                    <p>' . $service->balance.$service->signed. '</p>
                                </div>
                                <div class="Cell book_type_'.$service->booking_type.'">
                                    <p>' . $service->ps_no . '</p>
                                </div>
                                <div class="Cell book_type_'.$service->booking_type.'">
                                    <p>' . $service_status . '</p>
                                </div>
                                
</div>
';  
                                
                                
//                                echo '<tr>'
//                                        . '<td style="line-height: 18px;" class="book_type_'.$service->booking_type.'">' . ++$i . '</td>'
//                                        . '<td style="line-height: 18px;" class="book_type_'.$service->booking_type.'">' . $service->maid_name . '</td>'
//                                        . '<td style="line-height: 18px;" class="book_type_'.$service->booking_type.'">' . $service->customer_name . ' <b style="color:#006600;">(' . ($service->actual_zone ? substr($service->actual_zone, 2) : $service->zone_name) . '</b>) '.$paytype.'</td>'
//                                        . '<td style="line-height: 18px; text-align : right;" class="book_type_'.$service->booking_type.'">' . $service->time_from . '-' . $service->time_to . ' [' . ((strtotime($service->end_time) - strtotime($service->start_time))/ 3600) . ']' . '</td>'
//                                        . '<td style="line-height: 18px; text-align : right;" class="book_type_'.$service->booking_type.'">' . $service->total_fee . '('.$service->material_fee.')</td>'
//                                        . '<td style="line-height: 18px; text-align : right;" class="book_type_'.$service->booking_type.'">' . $service->collected_amount . '</td>'
//                                        . '<td style="line-height: 18px; text-align : right;" class="book_type_'.$service->booking_type.'">' . $service->balance.$service->signed. '</td>'
//                                        . '<td style="line-height: 18px; text-align : right;" class="book_type_'.$service->booking_type.'">' . $service->ps_no . '</td>'
//                                       
//                                        . '<td style="line-height: 18px;" class="book_type_'.$service->booking_type.'">' . $service_status . '</td>' //title="' . $service->service_status . '-' . $service->payment_status . '"
//                                      
//                                    .'</tr>';
                                
                                
                            }
                            
                 echo '<div class="Row">

                                <div class="Cell">
                                    <p></p>
                                </div>
                                <div class="Cell">
                                    <p></p>
                                </div>
                                <div class="Cell">
                                    <p>TOTALS</p>
                                </div>
                                <div class="Cell">
                                    <p>' . $total_wrk_hrs . '</p>
                                </div>
                                <div class="Cell">
                                    <p>' . number_format($total_fee, 2) . '('. number_format($material_fee, 2) .')</p>
                                </div>
                                <div class="Cell">
                                    <p>' . number_format($collected_total_fee, 2) . '</p>
                                </div>
                                <div class="Cell">
                                    <p></p>
                                </div>
                                <div class="Cell">
                                    <p></p>
                                </div>
                                <div class="Cell">
                                    <p></p>
                                </div>
                                
</div>
';               
                            
//                            echo '<tr style="font-weight:bold;">'
//                                    . '<td style="line-height: 18px;"></td>'
//                                    . '<td style="line-height: 18px;"></td>'
//                                    . '<td style="line-height: 18px;">TOTALS</td>'
//                                    . '<td style="line-height: 18px; text-align : right;">' . $total_wrk_hrs . '</td>'
//                                    . '<td style="line-height: 18px; text-align : right;">' . number_format($total_fee, 2) . '('. number_format($material_fee, 2) .')</td>'
//                                    . '<td style="line-height: 18px; text-align : right;">' . number_format($collected_total_fee, 2) . '</td>'
//                                    //. '<td style="line-height: 18px; text-align : right;">' . /*number_format($collected_amount, 2) . */'</td>'
//                                    . '<td style="line-height: 18px;"></td>'
//                                .'</tr>';
                        }
                        
                        ?>
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                     </div>
                <!--New table ends-->
                
<!--                <table class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; width: 10px;"> Sl No.</th>
                            <th style="line-height: 18px;"> Maid</th>
                            <th style="line-height: 18px;"> Customer</th>
                            <th style="line-height: 18px;"> Working Hours</th>                            
                            <th style="line-height: 18px;"> Billed Amount</th>     
                            <th style="line-height: 18px;"> Collected Amount</th>
                            <th style="line-height: 18px;"> Balance Amount</th>
                            <th style="line-height: 18px;"> PS No</th>
                           
                            <th style="line-height: 18px;"> Status</th> 
                            <th style="line-height: 18px; width: 123px;"> Actions</th> 
                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(!empty($dayservices))
                        {
                            $i = 0;
                            $zone_id = 0;                            
                            $total_fee = 0;
                            $material_fee = 0;
                            $collected_total_fee = 0;
                            $total_wrk_hrs = 0;
                            $collected_amount = 0;
                            $ztotal_wrk_hrs = 0;
                     
                            foreach ($dayservices as $service)
                            {
                                //Payment Type
                                    if($service->payment_type == "D")
                                    {
                                        $paytype = "(D)";
                                    } else if($service->payment_type == "W")
                                    {
                                        $paytype = "(W)";
                                    } else if($service->payment_type == "M")
                                    {
                                        $paytype = "(M)";
                                    } else
                                    {
                                        $paytype = "";
                                    }
                                
                                                                                                                               
                                $service_status = $service->service_status == 1 ? 'ON GOING' : ($service->service_status == 2 ? 'FINISHED' : ($service->service_status == 3 ? 'CANCELLED' : 'NOT STARTED'));                                
                                $class = $service->service_status == 1 ? 'on-going' : ($service->service_status == 2 ? 'finished' : ($service->service_status == 3 ? 'cancelled' : 'not-started'));
                                //$service_status .=  $service->payment_status == 1 ? (' ' . $service->total_fee) : ($service->payment_status == 0  && $service->service_status == 2 ? ' NP': '');
                                
                                $activity_status = 0;
                                //$activity_status = $service->service_status ? $service->service_status : 0;                                                                
                                $activity_status = $service->service_status == 1 ? 1 : ($service->service_status == 2 ? 2 : ($service->service_status == 3 ? 3 : 0));                                                                
                                $activity_status =  $service->payment_status == 1 ? 5 : ($service->payment_status == 0  && $service->service_status == 2 ? 6: ($service->service_status == 1 && $service->payment_status != NULL  ? 1 : ( $service->service_status == 3 ? 3 : 0)));
                                $service->total_fee = $service->service_status == 2 ? $service->total_fee : '';
                                $service->material_fee = $service->service_status == 2 ? $service->material_fee : '';
                                if($zone_id != $service->zone_id)
                                {
                                    if($ztotal_wrk_hrs != 0)
                                    {

                                        echo '<tr>'
                                                . '<td style="line-height: 18px;"></td>'
                                                . '<td style="line-height: 18px;"></td>'
                                                . '<td style="line-height: 18px;"></td>'
                                                . '<td style="line-height: 18px; text-align : right;"><b>' . $ztotal_wrk_hrs . ' Hrs</b></td>'
                                                . '<td style="line-height: 18px;"></td>'
                                                . '<td style="line-height: 18px;"></td>'
                                                . '<td style="line-height: 18px;"></td>'
                                                . '<td style="line-height: 18px;"></td>'
                                                . '<td style="line-height: 18px;"></td>'
                                                //. '<td style="line-height: 18px;"></td>'
                                             . '</tr>';
                                        $ztotal_wrk_hrs = 0;
                                    }
                                    
                                    $zone_id = $service->zone_id;
                                    echo '<tr>'
                                            . '<td style="line-height: 18px;"></td>'
                                            . '<td style="line-height: 18px;"><b style="color:#CB3636;">' . $service->zone_name . '</b></td>'
                                            . '<td style="line-height: 18px;"></td>'
                                            . '<td style="line-height: 18px;"></td>'
                                            . '<td style="line-height: 18px;"></td>'
                                            . '<td style="line-height: 18px;"></td>'
                                            . '<td style="line-height: 18px;"></td>'
                                            . '<td style="line-height: 18px;"></td>'
                                            . '<td style="line-height: 18px;"></td>'
                                         . '</tr>';
                                    
                                    
                                }
                                
                                $total_fee += $service->total_fee;
                                $material_fee += $service->material_fee;
                                $collected_total_fee += $service->collected_amount;
                                $collected_amount += ($service->payment_status == 1 ? $service->total_fee : 0);
                                $total_wrk_hrs += ($service->service_status == 2 ? ((strtotime($service->end_time) - strtotime($service->start_time))/ 3600) : 0);
                                $ztotal_wrk_hrs += ($service->service_status == 2 ? ((strtotime($service->end_time) - strtotime($service->start_time))/ 3600) : 0);
                                echo '<tr>'
                                        . '<td style="line-height: 18px;" class="book_type_'.$service->booking_type.'">' . ++$i . '</td>'
                                        . '<td style="line-height: 18px;" class="book_type_'.$service->booking_type.'">' . $service->maid_name . '</td>'
                                        . '<td style="line-height: 18px;" class="book_type_'.$service->booking_type.'">' . $service->customer_name . ' <b style="color:#006600;">(' . ($service->actual_zone ? substr($service->actual_zone, 2) : $service->zone_name) . '</b>) '.$paytype.'</td>'
                                        . '<td style="line-height: 18px; text-align : right;" class="book_type_'.$service->booking_type.'">' . $service->time_from . '-' . $service->time_to . ' [' . ((strtotime($service->end_time) - strtotime($service->start_time))/ 3600) . ']' . '</td>'
                                        . '<td style="line-height: 18px; text-align : right;" class="book_type_'.$service->booking_type.'">' . $service->total_fee . '('.$service->material_fee.')</td>'
                                        . '<td style="line-height: 18px; text-align : right;" class="book_type_'.$service->booking_type.'">' . $service->collected_amount . '</td>'
                                        . '<td style="line-height: 18px; text-align : right;" class="book_type_'.$service->booking_type.'">' . $service->balance.$service->signed. '</td>'
                                        . '<td style="line-height: 18px; text-align : right;" class="book_type_'.$service->booking_type.'">' . $service->ps_no . '</td>'
                                       
                                        . '<td style="line-height: 18px;" class="book_type_'.$service->booking_type.'">' . $service_status . '</td>' //title="' . $service->service_status . '-' . $service->payment_status . '"
                                        /*. '<td style="line-height: 18px;">'
                                        . '<a class="btn btn-small btn-success start-stop" title="Start" href="javascript:void" ' . $play . '><i class="icon-play"> </i></a>'
                                        . '<a class="btn btn-small btn-info start-stop" title="Stop" href="#payment-modal" data-toggle="modal" ' . $stop . '><i class="icon-stop"> </i></a>'
                                        . '<a class="btn btn-small btn-warning start-stop" title="Transfer" href="#transfer-modal" data-toggle="modal" ' . $transfer . '><i class="icon-share"> </i></a>'
                                        . '<a class="btn btn-small btn-danger start-stop" title="Cancel" href="javascript:void" ' . $cancel . '><i class="icon-ban-circle"> </i></a>'
                                        . '</td>'*/
                                    .'</tr>';
                                
                                
                            }
//                            echo '<tr style="font-weight:bold;">'
//                                    . '<td style="line-height: 18px;"></td>'
//                                    . '<td style="line-height: 18px;"></td>'
//                                    . '<td style="line-height: 18px;">TOTALS</td>'
//                                    . '<td style="line-height: 18px; text-align : right;">' . $total_wrk_hrs . '</td>'
//                                    . '<td style="line-height: 18px; text-align : right;">' . number_format($total_fee, 2) . '('. number_format($material_fee, 2) .')</td>'
//                                    . '<td style="line-height: 18px; text-align : right;">' . number_format($collected_total_fee, 2) . '</td>'
//                                    //. '<td style="line-height: 18px; text-align : right;">' . /*number_format($collected_amount, 2) . */'</td>'
//                                    . '<td style="line-height: 18px;"></td>'
//                                .'</tr>';
                        }
                        
                        ?>
                    </tbody>
                </table>-->
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>


<div id="activity-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 style="color:white; padding-bottom: 0px !important;">Activity</h3>
    </div>
    <div class="modal-body">
        <p> 
            <a class="btn btn-medium btn-success" title="Start" href="javascript:void" ><i class="icon-play"> </i>START</a>
            <a class="btn btn-medium btn-warning" title="Stop" href="javascript:void" ><i class="icon-stop"> </i>STOP</a>
            <a class="btn btn-medium btn-info" title="Transfer" href="javascript:void" ><i class="icon-share"> </i>TRANSFER</a>
            <!--<a class="btn btn-medium btn-danger" title="Cancel" href="javascript:void" ><i class="icon-ban-circle"> </i>CANCEL</a>-->
        </p>
        
        <div class="controls" id="payment-details">
            <label class="radio inline">
                <input type="radio" name="payment_type" value="1" checked="checked"> Payment
            </label>

            <label class="radio inline">
                <input type="radio" name="payment_type" value="0"> Payment Not Received
            </label>
            <label class="radio inline">
                <input type="radio" name="payment_type" value="3"> Service Not Done
            </label>
        </div>
        <p>
            <input type="text" id="paid-amount" style="width: 50px;"/>
        </p>
       
        <p id="frm-transfer"> 
            <!--<form id="frm-transfer">-->
                <select id="transfer-zone-id">
                    <option value="">Select Zone</option>
                    <?php
                    foreach ($zones as $zone)
                    {
                        echo '<option value="' . $zone->zone_id . '">' . $zone->zone_name . '</option>';
                    }

                    ?>
                </select>
            <!--</form>-->
        </p>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <a class="save-but" style="text-decoration: none;" href="javascript:void" onclick="add_activity();">Submit</a>
    </div>
</div>
