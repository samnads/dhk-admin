<style>
	.btn-mm-success {
		background-color: #7eb216;
		background-image: linear-gradient(rgba(126, 178, 22, 0), rgba(0, 0, 0, 0.1));
		border-color: rgba(0, 0, 0, 0.05) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.15);
		color: white;
		text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.15);
	}

	.btn-mm-success:hover {
		background-color: #7eb216;
		background-image: linear-gradient(rgba(126, 178, 22, 0), rgba(0, 0, 0, 0.1));
		border-color: rgba(0, 0, 0, 0.05) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.15);
		color: white;
		text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.15);
	}
</style>
<div id="avatar-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name" class="avatar-popup-name"></span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div id="" class="col-12 p-0">
      <div class="modal-body">
        <div class="img-container">
          <img id="avatar-popup-image" src="#">
        </div>
      </div>
    </div>
  </div>
</div>
<div class="main-inner">
	<div class="ml-3 mr-3">
		<div class="widget widget-table action-table" style="margin-bottom:30px">
			<div class="widget-header">

            <ul>
            <li>
            <i class="icon-th-list"></i>
            <h3>Customer Payments</h3>
            </li>
			<!--<li>
				<input type="text"  id="keyword-search" Placeholder="Search..." style="width: 160px;">
			</li>-->
			<li>
				<input type="text"  id="payment-date" Placeholder="From Date" style="width: 160px;">
			</li>
			
			<li>
				<input type="text"  id="payment-date-to" Placeholder="To Date" style="width: 160px;">
			</li>
			<li>
				<select class="sel2" style="margin-top: 5px; width:160px; margin-bottom: 9px;" id="customers_vh_rep_new"  Placeholder="Select Customer" name="customers_vh_rep">
						<option value="">-- Select Customer --</option>
						<?php
						$url_custid='nil';
						foreach($customerlist as $c_val)
						{
							if($c_val->customer_id == $customer_id)
							{
								$selected = 'selected="selected"';
								$url_custid=$customer_id;
							} else {
								$selected = '';
							}
						?>
						<option value="<?php echo $c_val->customer_id; ?>" <?php echo $selected; ?>><?php echo $c_val->customer_name; ?></option>
						<?php
						}
						?>  
				</select>
			</li>
			<li>
				<select class="sel2" style="margin-top: 5px; width:160px; margin-bottom: 9px;" id="pay_status" name="pay_status"  Placeholder="Select Status">
						<option value="">-- Select Status --</option>
						<option value="D">Draft</option>
						<option value="P">Posted</option>
				</select>
			</li>
			<li>
				<select class="sel2" style="margin-top: 5px; width:160px; margin-bottom: 9px;" id="pay_type" name="pay_type" Placeholder="Select Type">
						<option value="">-- Select Type --</option>
						<?php
						$type=[['id'=>'22','label'=>'Cash'],['id'=>'1','label'=>'Card'],['id'=>'2','label'=>'Cheque'],['id'=>'3','label'=>'Bank'],['id'=>'4','label'=>'Credit Card']];
						foreach($type as $c_val)
						{
						?>
						<option value="<?php echo $c_val['id']; ?>"><?php echo $c_val['label']; ?></option>
						<?php
						}
						?>  
				</select>
			</li>
			
			<li class="mr-0 float-right">
				<div class="topiconnew border-0 green-btn"> <a href="javascript:void(0);" id="paymentexceldownload"> <i class="fa fa-file-excel-o"></i></a> </div>
			</li>
			
			<li class="mr-0 float-right">
				<div class="topiconnew border-0 green-btn"> <a href="<?php echo base_url(); ?>customer/add_payment" id=""> <i class="fa fa-plus-circle""></i></a> </div>
			</li>

            
			</ul>
			</div>
			<div class="widget-content" style="margin-bottom:30px">
				<table id="paymentlisttable" class="table da-table" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th style="width:50px;"><center>Sl.No.</center></th>
							<th style="">Date</th>
							<th style="">Customer</th>
							<th style="">Name</th>
							<th style="">Memo</th>
							<th style="">Receipt No</th>
							<th style="">Type</th>
							<th style="">Amount</th>
							<th style="">Paid Amount</th>
							<th style="">Balance Amount</th>
							<th style="">Payment From</th>
							<th style="">Status</th>
							<th style="">Action</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
					<tfoot>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td>Total</td>
							<td id="totnetamt"></td>
							<td id="paidamt"></td>
							<td id="balamt"></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					</tfoot>
				</table>
			</div>

		</div>
		<!-- /widget -->
	</div>
	<!-- /span6 -->
</div>