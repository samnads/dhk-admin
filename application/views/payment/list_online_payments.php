<style>
	.btn-mm-success {
		background-color: #7eb216;
		background-image: linear-gradient(rgba(126, 178, 22, 0), rgba(0, 0, 0, 0.1));
		border-color: rgba(0, 0, 0, 0.05) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.15);
		color: white;
		text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.15);
	}

	.btn-mm-success:hover {
		background-color: #7eb216;
		background-image: linear-gradient(rgba(126, 178, 22, 0), rgba(0, 0, 0, 0.1));
		border-color: rgba(0, 0, 0, 0.05) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.15);
		color: white;
		text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.15);
	}
</style>
<div id="edit-popup" style="display:none;">
    <div class="popup-main-box">
        <div class="col-md-12 col-sm-12 green-popup-head">
            <span id="b-maid-name">Update Payment</span>
            <span id="b-time-slot"></span>
            <span class="pop_close n-close-btn">&nbsp;</span>
        </div>
        <div id="popup-booking" class="col-12 p-0">
            <form class="form-horizontal" method="post" id="update-payment-form">
                <div class="modal-body">
                    <div class="row m-0 n-field-main">
                        <p>Customer</p>
                        <div class="col-sm-12 p-0 n-field-box">
                            <input type="text" readonly name="edit_customername" id="edit_customername" autocomplete="off">
                            <input type="hidden" name="edit_paymentid" id="edit_paymentid">
                            <input type="hidden" name="edit_customerid" id="edit_customerid">
                            <input type="hidden" name="edit_paiddatetime" id="edit_paiddatetime">
                        </div>
                    </div>

                    <div class="row m-0 n-field-main">
                        <p>Ref No</p>
                        <div class="col-sm-12 p-0 n-field-box">
                            <input type="text" name="edit_refno" id="edit_refno" autocomplete="off" required>
                        </div>
                    </div>

                    <div class="row m-0 n-field-main">
                        <p>Transaction ID</p>
                        <div class="col-sm-12 p-0 n-field-box">
                            <input type="text" name="edit_transactionid" id="edit_transactionid" autocomplete="off" required>
                        </div>
                    </div>

                    <div class="row m-0 n-field-main">
                        <p>Amount</p>
                        <div class="col-sm-12 p-0 n-field-box">
                            <input type="text" readonly name="edit_amount" id="edit_amount" autocomplete="off" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
                    <button type="submit" class="n-btn mb-0" value="Submit" name="zone_edit">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="main-inner">
	<div class="ml-3 mr-3">
		<div class="widget widget-table action-table" style="margin-bottom:30px">
			<div class="widget-header">

            <ul>
            <li>
            <i class="icon-th-list"></i>
            <h3>Online Payments</h3>
            </li>
			<!--<li>
				<input type="text"  id="keyword-search" Placeholder="Search..." style="width: 160px;">
			</li>-->
			<li>
				<input type="text"  id="payment-date" Placeholder="From Date" style="width: 160px;">
			</li>
			
			<li>
				<input type="text"  id="payment-date-to" Placeholder="To Date" style="width: 160px;">
			</li>
			<li>
				<select class="sel2" style="margin-top: 5px; width:160px; margin-bottom: 9px;" id="customers_vh_rep_new"  Placeholder="Select Customer" name="customers_vh_rep">
						<option value="">-- Select Customer --</option>
						<?php
						$url_custid='nil';
						foreach($customerlist as $c_val)
						{
							if($c_val->customer_id == $customer_id)
							{
								$selected = 'selected="selected"';
								$url_custid=$customer_id;
							} else {
								$selected = '';
							}
						?>
						<option value="<?php echo $c_val->customer_id; ?>" <?php echo $selected; ?>><?php echo $c_val->customer_name; ?></option>
						<?php
						}
						?>  
				</select>
			</li>
			<li>
				<select class="sel2" style="margin-top: 5px; width:160px; margin-bottom: 9px;" id="pay_status" name="pay_status"  Placeholder="Select Status">
						<option value="">-- Select Status --</option>
						<option value="success">Success</option>
						<option value="failed">Failed</option>
						<option value="initiated">Initiated</option>
				</select>
			</li>
			
			
			<li class="mr-0 float-right">
				<div class="topiconnew border-0 green-btn"> <a href="javascript:void(0);" id="onlinepaymentexceldownload"> <i class="fa fa-file-excel-o"></i></a> </div>
			</li>
            
			</ul>
			</div>
			<div class="widget-content" style="margin-bottom:30px">
				<table id="onlinepaymentlisttable" class="table da-table" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th style="width:50px;"><center>Sl.No.</center></th>
							<th style="">Customer</th>
							<th style="">Transaction Id</th>
							<th style="">Reference No</th>
							<th style="">Amount</th>
							<th style="">Convenience Fee</th>
							<th style="">Total Amount</th>
							<th style="">Description</th>
							<th style="">Status</th>
							<th style="">Paid Date Time</th>
							<th style="">Action</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
					<tfoot>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td>Total</td>
							<td id="totamt"></td>
							<td id="convamt"></td>
							<td id="netamt"></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					</tfoot>
				</table>
			</div>

		</div>
		<!-- /widget -->
	</div>
	<!-- /span6 -->
</div>