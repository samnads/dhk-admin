<!--<script src="<?php// echo base_url(); ?>js/jquery-1.7.2.min.js"></script>-->
<script src="//maps.googleapis.com/maps/api/js?key=<?= $settings->google_map_api_key; ?>&libraries=places&callback=Function.prototype" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/gh/geocodezip/v3-utility-library@master/archive/maplabel/src/maplabel.js"></script>
<section>
    <div class="row no-left-right-margin">

        <div class="col-md-12 col-sm-12 no-left-right-padding">
            <!--<div class="widget widget-table action-table" style="margin-bottom:30px">-->
            <div class="widget-header">
                <form class="form-horizontal" method="POST" action="<?php echo base_url(); ?>booking/locations">
                    <i class="icon-th-list"></i>
                    <h3>Driver Locations</h3>
                    <!--<input type="text" style="margin-top: 6px; width: 160px; margin-bottom: 9px;" id="vehicle_date"
                        name="service_date" value="<?php echo $service_date ?>">
                    <input type="hidden" id="formatted-date-job" value="<?php echo $formatted_date ?>" />
                    <input type="submit" class="btn" value="Go" name="map_button" style="margin-bottom: 4px;">-->
                    <!--<a id="synch-to-odoo" href="#" style="cursor: pointer;" class="btn btn btn-primary">Synchronize</a>-->
                    <!--<a style="float:right ; margin-right:20px; cursor:pointer;" href="<?php// echo base_url() . 'reports/activity_summary_view/'; ?>" target="_blank"><img src="<?php// echo base_url(); ?>img/printer.png"/></a>-->
                </form>
            </div>
            <!--<div class="widget-content" style="margin-bottom:30px">-->
            <div id="driver-map" style="height: 800px; width:100%;"></div>
        </div><!--welcome-text-main end-->

    </div><!--row content-wrapper end-->
</section><!--welcome-text end-->
<script type="text/javascript">
var map;
var mapOptions = {
	zoom: 12,
	center: {
		lat: 25.2048,
		lng: 55.2708
	}
}; // map options
var markers = [];
var dataarray = <?= json_encode($dataarray); ?> ;

function initialize() {
	map = new google.maps.Map(document.getElementById('driver-map'), mapOptions);
	const image = "https://cdn-icons-png.flaticon.com/64/6395/6395395.png";
	var infowindow = new google.maps.InfoWindow();
	for (var i = 0; i < dataarray.length; i++) {
		var mapLabel = new MapLabel({
			text:dataarray[i].zone_name,
			position: new google.maps.LatLng(Number(dataarray[i].latitude), Number(dataarray[i].longitude)),
			map: map,
			fontSize: 15,
            fontColor: 'red',
			align: 'center'
		});
		marker = new google.maps.Marker({
			position: {
				lat: Number(dataarray[i].latitude),
				lng: Number(dataarray[i].longitude)
			},
			map: map,
			icon: image,
			animation: google.maps.Animation.DROP,
			html: '<p><b><i class="fa fa-user"></i>&nbsp;&nbsp;&nbsp;' + dataarray[i].tablet_driver_name + '</b></p><p><b>' + dataarray[i].zone_name + '</b></p>'
		});
		markers.push(marker);
		google.maps.event.addListener(marker, 'click', (function(marker, i) {
				return function() {
					infowindow.setContent(marker.html);
					//prov.value = data[i][5];
					infowindow.open(map, marker);
				}
			})
			(marker, i));
	}
}
window.addEventListener('load', initialize)
$(function() {
	let current = window.location.href;
	$('#primary_nav_wrap li a').each(function() {
		var $this = $(this);
		// if the current path is like this link, make it active
		if ($this.attr('href') === '<?php echo base_url('location'); ?>') {
			$this.addClass('active');
		}
	})
})
</script>