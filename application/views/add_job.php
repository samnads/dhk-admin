<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.flexdatalist.css" type="text/css" />
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.flexdatalist.js"></script>
<script type="text/javascript">
    $('.flexdatalist-json').flexdatalist({
        searchContain: false,
        textProperty: '{capital}, {name}, {continent}',
        valueProperty: 'iso2',
        minLength: 0,
        focusFirstResult: true,
        selectionRequired: true,
        groupBy: 'continent',
        visibleProperties: ["name", "continent", "capital", "capital_timezone"],
        searchIn: ["name", "continent", "capital"],
        data: 'countries.json'
    });
</script>
<style>
     .field-error.text-danger{
        color: #f00;
    }
    .select2-arrow {
        visibility: hidden;
    }

    .select2-container .select2-choice {
        -moz-appearance: none;
        background: #fff url("http://demo.azinova.info/php/mymaid/css/../images/ash-arrow.png") no-repeat scroll right 7px top 11px;
        border: 1px solid #eae8e8;
        border-radius: 3px;
        cursor: pointer;
        font-size: 14px;
        height: 36px;
        line-height: 30px;
        padding: 3px 10px 3px 25px;

        text-indent: 0.01px;
    }

    .text-field-main div.time-box .select2-container a.select2-choice {
        background: transparent;
        overflow: hidden;
    }

    .text-field-details {
        padding: 10px;
    }

    .text-field-details p {
        line-height: 20px;
        padding-bottom: 5px;
    }




    .job-customer {
        width: 100% !important;
    }

    #job-b-error {
        color: #f00;
        font-weight: bold;
    }
    .flexdatalist-multiple.flex0 { margin-bottom: 0px;}
.flexdatalist-results li { margin: 0px !important; height: auto !important;overflow-y: hidden;}
</style>
<div id="alert-popup" style="background:none;display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="alert-title"></span>
    </div>
    <div class="modal-body">
      <h3 id="alert-message"></h3>
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn mb-0 pop_close_success">OK</button>
    </div>
  </div>
</div>







<div class="row m-0">
    <div class="col-sm-12">
        <div class="widget">

             <!-- /widget-header -->





            <div class="widget-header">
     <div class="book-nav-top">
            <ul>
              <li><i class="icon-th-list"></i><h3>New Job</h3></li>
              <div class="clear"></div>
            </ul>
     </div>
</div>
















            <div class="widget-content">
                <form name="job_add_form" id="job_add_form">

         
         
<div class="col-sm-4 mb-3">

                
<div class="row m-0 n-field-main">
    <p>Customer</p>
    <div class="col-sm-12 p-0 n-field-box">
         <select name="customer_id" id="b-customer-id" data-placeholder="Select customer">
      
</select>
                        
         <div class="text-field-details" id="selected-address" style="display:none;">
                        <p><b>Selected Address</b><br>
                            <i class="fa fa-home"></i> &nbsp; <span id="job-picked-address" style=" font-family:Arial, Helvetica, sans-serif;">uiiujioujo joijoioojdsfsdfdsa sdfs</span>
                        </p>
                        <div class="clear"></div>
                    </div>
                    
         <div id="job-customer-address-panel">
                        <div class="head">Pick One Address <span class="close">Close</span></div>

                        <div class="inner">
                            Loading<span class="dots_loader"></span>
                        </div>
                    </div>
    </div>
</div>
       
                

<div class="row m-0 n-field-main">
    <p>Select Maids</p>
    <div class="col-sm-12 p-0 n-field-box">
         <input type='text' placeholder='Search maid...' class='flexdatalist form-control' data-min-length='0' data-searchContain='true' multiple='multiple' list='skills' data-value-property='value' name='skill' id='jobmaidval'>

          <datalist id="skills">
              <?php
              foreach ($maids as $maid) {
                  if (!in_array($maid->maid_id, $leave_maid_ids)) // Excluding maids on leave
                  {
              ?>
                      <option value="<?php echo $maid->maid_id; ?>"><?php echo $maid->maid_name; ?></option>
  
              <?php }
              } ?>
          </datalist>
    </div>
</div>




<div class="row m-0 n-field-main">
    <p>Service Type</p>
    <div class="col-sm-12 p-0 n-field-box">
         <select name="service_type_id" id="b-service-type-id" data-placeholder="Select service type" class="job-customer">
                            <option></option>
                            <?php
                            foreach ($service_types as $service_type) {
                                $selected = $service_type->service_type_id == 1 ? 'selected="selected"' : '';
                                echo '<option value="' . $service_type->service_type_id . '" ' . $selected . '>' . html_escape($service_type->service_type_name) . '</option>';
                            }
                            ?>
                        </select>
    </div>
</div>






                <!--<div class="col-sm-12 week-type-box newjobbox no-right-padding">
                    <div class="text-field-main">
                        <p>Cleaning Materials :</p>

                        <div class="col-md-4 col-sm-2 col-xs-3 week-date no-left-right-padding">
                            <input type="radio" value="Y" id="b-cleaning-materials-1" name="cleaning_materials" class="">
                            <label for="b-cleaning-materials-1"> <span></span> Yes</label>
                        </div>


                        <div class="col-md-4 col-sm-2 col-xs-3 week-date no-left-right-padding">
                            <input type="radio" value="N" id="b-cleaning-materials-2" name="cleaning_materials" class="">
                            <label for="b-cleaning-materials-2"> <span></span> No</label>
                        </div>


                        <div class="clear"></div>
                    </div>
                </div>-->





      
        
        
        



<div class="row m-0 n-field-main">
          
          <div class="col-sm-6 n-field-box pl-0">
               <p>From Time</p> 
               <div class="col-sm-12 p-0 n-field-box">
                    <select name="time_from" id="b-from-time" data-placeholder="From" class="job-customer">
                                        <option></option>
                                        <?php
                                        foreach ($times as $time_index => $time) {
                                            echo '<option value="' . $time->stamp . '">' . $time->display . '</option>';
                                        }
                                        ?>
                                        <!--                                        <option value="">09:00am</option> 
                                        <option value="">09:30am</option> 
                                        <option value="">10:00am</option> 
                                        <option value="">10:30am</option> 
                                        <option value="">11:00am</option> -->
                                    </select>
               </div>
          </div>
          
          <div class="col-sm-6 n-field-box pr-0">
               <p>To Time</p>
               <div class="col-sm-12 p-0 n-field-box">
                    <select name="time_to" id="b-to-time" data-placeholder="To" class="job-customer">
                                        <option></option>
                                        <?php
                                        foreach ($times as $time_index => $time) {
                                            if ($time_index == 't-0') {
                                                continue;
                                            }

                                            echo '<option value="' . $time->stamp . '">' . $time->display . '</option>';
                                        }
                                        ?>
                                        <!--                                        <option value="">09:00am</option> 
                                        <option value="">09:30am</option> 
                                        <option value="">10:00am</option> 
                                        <option value="">10:30am</option> 
                                        <option value="">11:00am</option> -->
                                    </select>
               </div>

          </div>
</div>







        <div class="row m-0 n-field-main">
          <p>Repeats</p>
          <div class="n-field-box">
               <select name="booking_type" id="b-booking-type" data-placeholder="Select repeat type" class="job-customer">
                    <option></option>
                    <option value="OD"><?= $settings->od_booking_text; ?></option>
                    <option value="WE"><?= $settings->we_booking_text; ?></option>
                </select>
          </div>
        </div>
        
      
     
        
        <div class="row m-0 n-field-main"  id="jobweekday" style="display:none;">
          <p>Repeat On</p>
          <div class="col-sm-12 pl-0 pr-0 n-field-box n-field-day">
               
               <div class="n-days">
                	<input value="0" name="w_day[]" id="repeat-on-0" <?php if ($day_number == 0){ echo 'checked="checked"'; } ?> type="checkbox">
                    <label for="repeat-on-0"> <span class="border-radius-3"></span><p>Sun</p></label>
               </div>
               
               <div class="n-days">
                	<input value="1" name="w_day[]" id="repeat-on-1" <?php if ($day_number == 1){ echo 'checked="checked"'; } ?> type="checkbox">
                    <label for="repeat-on-1"> <span class="border-radius-3"></span><p>Mon</p></label>
               </div>
               
               <div class="n-days">
                	<input value="2" name="w_day[]" id="repeat-on-2" <?php if ($day_number == 2) { echo 'checked="checked"'; } ?> type="checkbox">
                    <label for="repeat-on-2"> <span class="border-radius-3"></span><p>Tue</p></label>
               </div>
               
               <div class="n-days">
                	<input value="3" name="w_day[]" id="repeat-on-3" <?php if ($day_number == 3) {  echo 'checked="checked"'; } ?> type="checkbox">
                    <label for="repeat-on-3"> <span class="border-radius-3"></span><p>Wed</p></label>
               </div>
               
               <div class="n-days">
                	<input value="4" name="w_day[]" id="repeat-on-4" <?php if ($day_number == 4) { echo 'checked="checked"'; } ?> type="checkbox">
                    <label for="repeat-on-4"> <span class="border-radius-3"></span><p>Thu</p></label>
               </div>
               
               <div class="n-days">
                	<input value="5" name="w_day[]" id="repeat-on-5" <?php if ($day_number == 5) { echo 'checked="checked"'; } ?> type="checkbox">
                    <label for="repeat-on-5"> <span class="border-radius-3"></span><p>Fri</p></label>
               </div>
               
               
               <div class="n-days pr-0">
               	   <input value="6" name="w_day[]" id="repeat-on-6" <?php if ($day_number == 6) { echo 'checked="checked"'; } ?> type="checkbox">
                   <label for="repeat-on-6"> <span class="border-radius-3"></span><p>Sat</p></label>
               </div>
          
              <div class="clear"></div>
          </div>
        </div>
        
        
        
        <div class="row m-0 n-field-main" id="jobneverweekday" style="display:none;">
          
          <div class="col-sm-6 pl-0 n-field-box n-end-main">
          <p>Never Ends</p>
          
          <div class="n-end">
               <input value="never" id="job-repeat-end-never" name="repeat_end" class="" type="radio" checked="checked">
               <label for="job-repeat-end-never"> <span class="border-radius-3"></span></label>
          </div>
          </div>
          
          <div class="col-sm-6 pr-0 n-field-box"> 
          <p>Ends</p> 
          <div class="n-end">
               <input value="ondate" id="job-repeat-end-ondate" name="repeat_end" class="" type="radio">
               <label for="job-repeat-end-ondate"> <span class="border-radius-3"></span></label>
          </div>  
          <div class="n-end-field">
            <input type="text" class="end_datepicker" id="job-repeat-end-date" data-date-format="dd/mm/yyyy" readonly="readonly" disabled="disabled"  style="height: 26px; position: relative; top: -3px; background: #FFF;"/>
          </div>

          </div>
        </div>
        
        
        
        



<div class="row m-0 n-field-main">
          
          <div class="col-sm-6 n-field-box pl-0">
            <p>Cleaning Materials</p> 
            <div class="switch-main">         
                  <label class="switch">
                     <input type="checkbox" id="b-cleaning-materials-1" name="cleaning_materials" value="Y">
                     <span class="slider round"></span>
                  </label>
              </div>
          </div>
          
          <div class="col-sm-6 n-field-box pr-0">
               <p>Lock Booking</p>
              <div class="switch-main">         
                  <label class="switch">
                     <input type="checkbox" name="lock_booking" value="Y">
                     <span class="slider round"></span>
                  </label>
              </div>

          </div>
        </div>
        


</div>
<div class="col-sm-4">





<div class="row m-0 n-field-main">
    <p>Contact Number</p>
    <div class="col-sm-12 p-0 n-field-box">
         <input name="jobcontact" class="text-field" type="text" id="job-contact" readonly>
    </div>
</div>









<!--<div class="col-sm-12 week-type-box newjobbox no-right-padding">
    <div class="text-field-main">
        <p>Notification</p>

        <div class="col-md-4 col-sm-2 col-xs-3 week-date no-left-right-padding">
            <input value="" name="notification_email_booking" id="job-notification-email-booking" class="" type="checkbox">
            <label for="job-notification-email-booking"> <span></span> Email</label>
        </div>


        <div class="col-md-4 col-sm-2 col-xs-3 week-date no-left-right-padding">
            <input value="" name="notification_sms_booking" id="job-notification-sms-booking" class="" type="checkbox">
            <label for="job-notification-sms-booking"> <span></span> SMS</label>
        </div>


        <div class="clear"></div>
    </div>
</div>-->





<div class="row m-0 n-field-main">
    <p>Email ID</p>
    <div class="col-sm-12 p-0 n-field-box">
         <input name="jobemail" id="jobemail" class="text-field" type="text" readonly>
    </div>
</div>




<div class="row m-0 n-field-main">
          <div class="col-sm-6 n-field-box pl-0">
          	<p>Email Notification</p>
            <div class="switch-main">         
                  <label class="switch">
                     <input type="checkbox">
                     <span class="slider round"></span>
                  </label>
              </div>
          </div>
          
          <div class="col-sm-6 n-field-box pr-0">
          	<p>SMS Notification</p>
            <div class="switch-main">         
                  <label class="switch">
                     <input type="checkbox">
                     <span class="slider round"></span>
                  </label>
              </div>
          </div>
        </div>
                
                
                
<div class="row m-0 n-field-main">
    <p>Total Amount</p>
    <div class="col-sm-12 p-0 n-field-box">
         <input name="n_totamt" id="n_totamt" class="text-field" type="number">
    </div>
</div>




                
                

<div class="row m-0 n-field-main">

    <div class="col-sm-12 p-0 mt-3">
         <input type="hidden" id="job-customer-address-id" name="job-customer-address-id" />
         <input type="button" class="n-btn mb-0" id="save-booking" value="Save">
         <p id="job-b-error"></p>
    </div>
</div>







</div>
            </form>
            </div> <!-- /widget-content -->
        </div> <!-- /widget -->
    </div> <!-- /span8 -->
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#jobmaidval-flexdatalist').focus(function() {
            $(this).attr('placeholder', 'Type maid name...')
        }).blur(function() {
            $(this).attr('placeholder', 'Search maid...')
        })
    });
    $(function() {
	let current = window.location.href;
	$('#primary_nav_wrap li a').each(function() {
		var $this = $(this);
		// if the current path is like this link, make it active
		if ($this.attr('href') == '<?php echo base_url('activity/jobs'); ?>') {
			$this.addClass('active');
		}
	})
})
</script>