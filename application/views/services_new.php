<link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
<script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
<link rel="stylesheet" href="https://fengyuanchen.github.io/cropperjs/css/cropper.css">
<script src="https://fengyuanchen.github.io/cropperjs/js/cropper.js"></script>
<script type="text/javascript">
    $('.flexdatalist-json').flexdatalist({
        searchContain: false,
        textProperty: '{capital}, {name}, {continent}',
        valueProperty: 'iso2',
        minLength: 0,
        focusFirstResult: true,
        selectionRequired: true,
        groupBy: 'continent',
        visibleProperties: ["name", "continent", "capital", "capital_timezone"],
        searchIn: ["name", "continent", "capital"],
        data: 'countries.json'
    });
</script>
<style>
    .field-error.text-danger {
        color: #f00;
    }

    .select2-arrow {
        visibility: hidden;
    }

    .select2-container .select2-choice {
        -moz-appearance: none;
        background: #fff url("http://demo.azinova.info/php/mymaid/css/../images/ash-arrow.png") no-repeat scroll right 7px top 11px;
        border: 1px solid #eae8e8;
        border-radius: 3px;
        cursor: pointer;
        font-size: 14px;
        height: 36px;
        line-height: 30px;
        padding: 3px 10px 3px 25px;

        text-indent: 0.01px;
    }

    .text-field-main div.time-box .select2-container a.select2-choice {
        background: transparent;
        overflow: hidden;
    }

    .text-field-details {
        padding: 10px;
    }

    .text-field-details p {
        line-height: 20px;
        padding-bottom: 5px;
    }




    .job-customer {
        width: 100% !important;
    }

    #job-b-error {
        color: #f00;
        font-weight: bold;
    }

    .flexdatalist-multiple.flex0 {
        margin-bottom: 0px;
    }

    .flexdatalist-results li {
        margin: 0px !important;
        height: auto !important;
        overflow-y: hidden;
    }

    input[type=checkbox] {
    margin: 0px !important;
    float: left;
    width: 20px!important;
    }

   
</style>
<div id="crop-popup" style="display:none;">
    <div class="popup-main-box">
        <div class="col-md-12 col-sm-12 green-popup-head">
            <span id="b-maid-name">Image Cropper</span>
            <span id="b-time-slot"></span>
            <span class="pop_close n-close-btn" onclick="closeCropper()">&nbsp;</span>
        </div>
        <div id="" class="col-12 p-0">
            <div class="modal-body">
                <div class="img-container">
                    <img id="image" src="#">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="n-btn red-btn mb-0" onclick="closeCropper()">Cancel</button>
                <button type="button" class="n-btn mb-0" id="crop">Crop</button>
            </div>
        </div>
    </div>
</div>
<div id="alert-popup" style="background:none;display:none;">
    <div class="popup-main-box">
        <div class="col-md-12 col-sm-12 green-popup-head">
            <span id="alert-title"></span>
        </div>
        <div class="modal-body">
            <h3 id="alert-message"></h3>
        </div>
        <div class="modal-footer">
            <button type="button" class="n-btn mb-0 pop_close_success">OK</button>
        </div>
    </div>
</div>
<div class="row m-0">
    <div class="col-sm-12">
        <div class="widget">
            <div class="widget-header">
                <div class="book-nav-top">
                    <ul>
                        <li><i class="icon-th-list"></i>
                            <h3>Services New</h3>
                        </li>
                        <li class="mr-0 float-right">
                            <div class="topiconnew border-0 green-btn">
                                <a href="<?php echo base_url(); ?>services" title="Services List"> <i class="fa fa-users"></i></a>
                            </div>
                        </li>
                        <div class="clear"></div>

                    </ul>
                </div>
            </div>
            <div class="widget-content">
                <form name="services_new_form" id="services_new_form" class="form-horizontal" method="post" enctype="multipart/form-data">
                    <div class="col-sm-4 mb-3">
                        <div class="row m-0 n-field-main">
                            <p>Service Type</p>
                            <div class="col-sm-12 p-0 n-field-box">
                                <input type="text" id="service_type_name" name="service_type_name">
                            </div>
                        </div>

                        <div class="row m-0 n-field-main">
                            <p>Service Name(App)</p>
                            <div class="col-sm-12 p-0 n-field-box">
                                <input type="text" id="customer_app_service_type_name" name="customer_app_service_type_name">
                            </div>
                        </div>
                        <div class="row m-0 n-field-main">
                            <p>Service Rate</p>
                            <div class="col-sm-12 p-0 n-field-box">
                                <input type="number" id="service_rate" name="service_rate">
                            </div>
                        </div>
                        <div class="row m-0 n-field-main">
                            <p><?php echo form_label('Thumbnail  Image(500x500)', '', array('class' => '')); ?></p>
                            <div class="col-sm-12 p-0 n-field-box">
                                <label>
                                    <img class="img-thumbnail" id="thumbnail_image_show" style="width:150px;" src="<?php echo $data['promotion_image'] ? base_url(PROMO_IMG_PATH . $data['promotion_image']) : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAY0AAAB/CAMAAAAkVG5FAAABCFBMVEX80k8fE0f////9008dEUf/1k8bD0cTBUf0y0+7mkz812FtWEkAAEf/108YC0cZDUflvk4OAEdqVUneuE5SQEhGNUjPq02BaEqWe0sxI0juxk4WCUf80Ujpwk5XRUgSAkcJAD4AADk1J0ioikyKcErIpU0kFkfEok2Weks5Kkifgkv82Wv+8s2wkUwtH0gAADV0X0mloK1ENEj++u397L3u7e+3s78AACRjT0k+L0iSi5pLO0j93X3+9t796rP95Z/e3eDw7/FgWHCKgpJ0bIFFP2HEwMaBeIpVSmKEbEr94I9KPVVyZWwJACxpX3PU0diilYyoj17cxIPawXj/5ZT8zjj82nb+8Mgwv0ooAAARiUlEQVR4nO2de0PbONbGFSRfArKNMDgQOxeSkJCEpOE6gRQKtJ0WZt/l3dmBfv9vsrYkO74lXCPYHT39p62j2NHP5xxdjiSw9CwdX16e7F///ON02C5IzRAaDod//Pyxf3J5efy86gVP/+jV7/tn18NP2267YKnovX/yRxZCyCogd/vT1vXZ/u8LoHF88u37sF53fQxIkniKUFBTVr2+9f0fJ1dvSePq5Nv/bWvSIF4ipJra9vDbyeUb0bg6uzY1CeI1Uuvm97MnGMijNO7+Wa9LFq+W77PMf969ksbdjy0zgwJJPUWZatPUH4/wmE/j61Y9GSx8L1gjNdc1LVVqtizTdf16MlORFtWHX19K42p/u64mvswl5LQ6avWKjl0p61KzVK7YnWKvNao2MLESQNT69v6LaNxdmzEWSHUxXt/t2KvAMBRFgVLz5NeQYYBVp9g6wNiNv9OqNsddzaRxMozFbmSSrWqzDGCAAUg9TQEUACq96pCYCXe1P6t5NYPG5Y9PMaAmmfQcw5AgXiAIDWNncwNbMfP49HXGiEk+jeOf9VhTAHeLFai898/6L5YCK4MGdtHUPH7mdwZzaZz85k7twh0XDUVaxevkG0ixSqb24f528lQa++3ISyF8OtCN9/4t/xMyyoMGicxDHea1rXJofJ22azWzVZF28UaCSqXkRk5Hred0PbI0zrZDgIiMdwzJ4u0EjeI6idzO9tnjNM6i/qOFS2XppN5Wvnng0PMgNYMjTeNrZBlatyibtG8uCAcNN7KOtLNK0divhzDIqSMNYwGC0BmG3grV9+fROIlaU6RqSxiLkWFXw7aVOjyZTeP4txAGXinL7t6ipFTWQutQfzueReP4WpMwREipjHEYna+P82lcfQ2HQ4iEsVjFrKP+9SqXxv6nKGZIGAuWUlmv8Uj+aT+Pxt2QBw3t1JYwFi3FHrphJL/L0rgKg4bVlU1bATKcBn/76z+yNPbDbARclDBECA54JEfafoZG1+RjUyXZAxciCEc8kqvbaRpft9kVt1qWMMQIVtbdsF2VpHE3ZH4KmR0JQ5RghyeUWI3LBI0z3tXALRk0xMko8dBR/xancccHC83TijQNcYKVLTY5i9y7GA3eC0dkIHsaIqUM+PghjxyUxh2f1NDGujQNkYLlKgvkaPsqorEfRg1xXQ2akSfqZh/m1hkZxTBynIU0wm641RAEAypQ18s6yEuACJImk4LZqzMqc35ZJkXRy+WyDjOXsoUVZfF+22hYfCz3itM44XlXeCCGhqJ3mqNxdaU1sLN1YhfTijUsoL6T/q94WT1T1k59EEKnuVbdWB/3iiBZ1bDcyZQudhaOw+A9cuSecBrfmKMyJ2IaVIa90iZEczXibmymc1KMzSipgo8btHemVQJX14MhA+zk1hJcxcmyBbKb/KBSXmlgzbVMjajj5HActCdWqrRfIwt/PeHqqTVt5Po0rtjgLar1xMDotAkfL0MWTrcbfBrJNSlqiobr/99sGqkFLaSV+CB0tkiUsVHDzYQPtE/N9HoYd/E0AOSvnzq8ojRO2KCIupX/G99Yio3iCdukBBI4jM3aW9pGgoZiH2ixiyrpxCob2hvvYRtA2eEzGdsnlMY39ohuVUTUgPoKfxfYqABKteMW6amMCftuxBemmAexsALtg3ehAQzeyNW+URrf2buKeyJoKB26IA7hxqRA68bsJmqW01BJJO25NIIlWFw4bhtKB9Nba+1Jl/6tgFtxGl1Mb8eAabT4gQgaPfYOmd8DGr+3+UsqJobvEnqzgb3qrLn0RU1ULaOhNm5KoW7iL/ATaKD22rRwMfabjBK9NSnZq/aAxg9ra+omYWWXllgJcKDhiP5jVwANWOGtqvbvPg3evjUPgAAaUJ8E1Ul6fldA0Tfo3xM/mcUN30VMFXusp9BQu860rJIpS0qGAqFRLKDgpYiTVmgJPfgOa2M1XXxhgoAFrKCNC8LhW7IrwjSgHqT5oiGtTqMVvKzkJkGjV5sTw55EozHjKgsMeDX4nbBSpV+0k/4otBmNdEdlkWqxcOZ3x8HxNQsiePE9HRDQoD/2lP5YZSf4BxnFjZJ5qlnh8zU0FGdLDcyB3c0YBdRxZpSU+Q2hNJQio+FeH4NLFjZm/YY3VkQjuJn/Hpo1gtf0DA13fVE01HZII7gPbqbvA8viafB8BXV4CS5ZFpVZXRXjqYLWDGrTCoMVcjC66dnxDyyaBiLcEmm0zqEh3lPBVeo1C+jTJeB9v9pIRBD3aXStaZCCdEA1p4X7Kk/VnUsDfTjbgGDEerzbJ4CPphMxU7AQsGamxhLosiPbPIq/xja2MqGZinuqQkgDu677IWjw1owfxvfBD9rdQTUhfb8gZNGOV23WvBa3jdfQmG8b3FMZu9XxeFwtfoQ2VTj+oP0AP1ljV8s812LE69NvSeXjWHgU57YBMv0R/h3vYBtKkXX5rJ8hDVdIA9cX7LHxCbeVu8AzohHu2JEs/EQaeWVTNGY93nvQ6EQ0tti4iCsqExoaVcJGYnbz5kMZDWsSTNBRJQs/KW4Uc8um2lSzHu8dPJViMxpoC7C0NmQJS91R7FOXvQB58yksiqM235LMrSae62mjhmHh5Mc+rm3ACh/PHgK2ry2yxCV8wk6beke1kDPzyyPadL7n+TQKbfRfRqPMcw7boMBpCMzdMYpsBk4lzYyzSs1vmOsvoBEqj8ZH9FRQD7cUA0g8Dd8dqRSHNcykDC2axoe0jZAGEh83gtvDJpt+s9R0vYaeymUiL/FUFi+caxvoERriRw3jceM9aPiV3kQUh3uQ+tlGj832VJnWR+Xn0kDtCS88ceJlP7Cnss2QRpf9xRR5exB1OzLJpmELt7zKVEmWelJ/o1PhhfUsjY/oqRSHt3C7ovvioaDRZL1AfJM3avi63l9uYW4bFqcxIxfxPTxVrPf3k8UPTXh2urFL87NVK9HvFDWirjjFjv8nU+nv0vsbsA09rT/AGssYIYJGDaeC+g19iNrNG8/9zcgLS43hlsiwMCTNzKjhe4zh9vio4Qj8P0tZSc5OCxHU6bo3KxHIWRR/1bz4/DFcN5rfCPqH2RH11XewjRKjgf8F/mTzG4Jmm4JM8Gjo1KCj64jE5yMWPi+uPTL3J54G1FeYRdT/9GkwnyXm/rDsFHs3VRak+EQg7r2Hp/pAc3/2hM3Eav8Gf3ZZo6otJEvBaGGMCd6klcAnAkkr/gEWxReQwcNto/YIDfG2oTgsUcTq/ht8WWNmkk1mWYQMmmoYTTRuzqCxQE/FWriQe6psBs870OBLOLTxLfhrl4UQUhJxZ2OXplCVmKfKs42FRXFoByslEKZdQqiP3Xwawj0VBHxPBbL7Gfw1YN1yqyEijCtFnlipBAG9HLytySphxuPbxnS//viTRzTyLk/jRv5V6p7xIJhzNBx26ww48f0NCNj2hUgdfAYX9hbLn8ciHsB/9QL22pruP0WZvhTJiBV6KjA9zSJenNPYiR12Ec+ZZrYx46pOfbJ16gS3pmn63FASDyi8L644zFH5b9Ff4MJb5/2/koiEbMA2LSPDVnPUoH9NTu/xMVwLTxWbsec0EJlejcVhPoarxsrGVwzwTpbbHvVa7NZkJfuAwj1VmL9jrntfwJFxw/81ETHFoXRUPtOE2ZQTIokFXwYPYzNXmpmJqyi+cjSa35iuNIuvpoE65rcmbH95RLKpGcI9Fc/ap9ZwAfa8nXD9gIiBQwh2p1u70xoZJ9c29eKLwQr5a5tiyqURKbW2aVCLnxKDSCv7/gm3DaXY5iuS7fs9cK4A1uMI2jYiIgdYw2qsRsbJiZXQNt6IRmKA2H8TYjhQZgUo/YxgGlDnrsnqAu8cHC4DPkxiNQR1x1suMdm5nQSXUrNcfhNYS4hYCRoTkrycihta6mpqTSzo4Ro7XNUi5CZvL03fUwU3PRVGw+Yb6pFdCH0aD4rDV2ViEQurgqkNpzRp+11ydDrqpFPclMF4LamV2AwerNykro7jMb6cLjtuJr8eGnar2vbbAO2DkZObXQfL1aBgSUjGPqCDE7Tu1aGj9A/B0q1XHjNfraqC9t9QgO10Ojs7Nsh5OzPntc2/mmgAz73Kb13xb91x7JlHH+WXW5CgztfnauOy99mnceEZ4b48uCcq4ZBt6pELP3Na22uu5twAztupJPqOl/yqF0jhKTKIDAzvyxJYOoJQ567KkjvhipVi830t1KEO4ZFP43wZhs7LDyXv/Xx/M4UtSBzkiO8Fq/f7EFZ4IxdhaRwCpTg8RFjdCoR9upfCrceHtgvBKpcPs5HW/74grIY7N/QA8D5TGr/ug/1QuHFomXl7qUVJafKNwWhqgHfE9qfye0XhplUFS8xOPFJ0+ovvMxPsfAPBIaPhuyoIws2CyLo8VE6IoBGeNucGm4p4D3zvtj0PAqUTHviAS2KyR/7mgmCFuyO1ELgjb4/TOOxD/2KrxtPWUXZVhdRbC8Jmgdc3Ha2F/fNwB9YvXjAE1OV7rFvtjvBMt7+dDL7Ay/dTp4Ev8m6j/XDPvWCqeCc8Xs5sO9I4FivohAcqq4Wgiwe98+nO3X0v+MQmRmFYkYf9LVSKc8DnaRChuX3ecmwf9T1KQx+FszXuULZzFyeoONHppLhEp7xoDA9pHH6mOMqn4TSoOyMpSeoNBHe6YT3XNhiMh8P4+RtHtBmlOGEkL5jdgex3LETQ6DTM5EsfDN8mTgqixgEMZxhuQ2rhnux3LEBQ7+Gwjs0G2686NI2Ixvk9/azRiVIIVFQqz5mUkXqJoFEeadEB7hZPsbjfS59pdkuNAyiDQkgOkYkjzeNNBeHORpTAZLUHrHZZXyNB45C2cv3Pd6Ktxguu2irLpu7bySi3UBiYCxbiWxfA5fMMjaULjxcpkqiEisc7hnRXbyKoKJ3xNJXMJeHpcd6XpSwNHsiDUL4xzTBz8djJPbVE6nlSgDPG0WteIBvhgRPBLFMOjcMwSBh2FUfZmYi0b5zsSS5Sz5Fff05rOH3HEamGox0QnOfSWDryeJ0rlRGZbriv4sbIARLISxXUnDNqTMNxwZzmOULvaCmfBh3LZZ8BzcbUqoKE8vWBoxtypP3ZgtAA9mAdx1gUSLcZ9axjQSNNg40esi9xJjie0O3ixkrPhobygQ4E+9gKKkoxoN0bNWLxIuhWV6ejTny0MJ/G4UOIAyjKbiOR269qNXVrNLArwWFkRt4BYFKRDAXo5Yo9WDtVaySxTKHW3Zz6fK9/OIfG0vlyhAMqzojEoQZL0wjGW+uj3eagOMieACbF5ddOb3O0voWxZqFEDbpkFDvHDernS/No0DnyyDz0zilxk1/n24hZI4S4bkFqllyXYFIz1dR/IxdvdPRYTr23tzSfxtLecixBXzEG1ZqW5kG/WWqe8mrMJeNmvCsNl9MwsjR864icVWAflU4V19S8r5d6ugIfP+kkhpngfQZGDg3fOuI4oN+JXGuYJG12Uk+XSky/y6YnemweyMLIo7F03o/jCPzV6mC0hWsSyEuk1vze82A1Ndzn9c9zaj6PRjCem16hpdvFFYQx0Uw11ytKpYWQamp+GxStFG09NY8KveXDvIrPpRH0ylNdvKAnY9jNm/GkEaybCw7Gk5oh4jc6/SpqNybjm6ZtGJk1XDDZA3+UxtIR8EBG0DDgqtPxW9O7pdKK1AyNVkq7vUGx46xCIy+5wAMXM2p9Fo2l889p8wijiBKsmsuud5SKKaymvAr0HnLi9yM0fG+1nGMekeOSmqeZFecbxgwv9QiNpfOH+9k8pF4i7/7zTMN4hIbf9XjwJI+3k+f1f82t7/k0lg4v0o1dqZcKev2L3Hbtk2n4PI6WPQnk1YK+kzl6hMUTaPjau12WDutV8nt7c+PFc2j48fyo7/kWIk3k+YK+VXj9L3njIC+lsUQjyDKQRJ6lgARY7l88DcVzaARAfl3c9sG9R81Eao6CMOF596B/e/Hr0WDxQhqUyOH5r4u/Pj/0l6Vmqt9/uPU5nB8+h0Sg/wAZcxAF+FtXUwAAAABJRU5ErkJggg==
                                    '; ?>" style="width: 100%;">
                                    <?php if ($this->uri->segment(2) != 'edit') : ?>
                                        <input type="file" class="input-image" accept="image/*" data-selector="thumbnail_image" data-crop-width="500" data-crop-height="500">
                                    <?php endif; ?>
                                </label>
                                <input type="hidden" name="thumbnail_image_base64" id="thumbnail_image_base64">

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="row m-0 n-field-main">
                            <p>Info HTML</p>
                            <div class="col-sm-12 p-0 n-field-box">
                                <textarea class="hidden" id="info_html" name="info_html"></textarea>
                                <div class="control-group">
                                    <div>
                                        <div id="quill_editor"></div>
                                    </div>
                                </div>
                                <div>
                                </div>
                                <div class="row m-0 n-field-main">
                                    <div class="col-sm-6 n-field-box pl-0">
                                        <p>App/Web Status</p>

                                        <div class="switch-main">
                                            <label class="switch">
                                                <input type="checkbox" name="customer_app_status" value="1">
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4 mb-3">
                        <div class="row m-0 n-field-main">
                            <p>Service Category</p>
                            <div class="col-sm-12 p-0 n-field-box">
                                <?php foreach ($services as $service) { ?>
                                    <div class="">
                                        <input type="checkbox" id="service_type_category_<?php echo $service->id; ?>" name="service_type_category[]" value="<?php echo $service->id; ?>">
                                        <label class="pl-3" for="service_type_category_<?php echo $service->id; ?>"><?php echo $service->service_category_name; ?></label>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 p-0 mt-3 text-center">
                            <button type="submit" class="btn mm-btn" value="Save" id="services_sub">Save</button>
                        </div>
                    </div>

                </form>
            </div> <!-- /widget-content -->
        </div> <!-- /widget -->
    </div> <!-- /span8 -->
</div>

<script type="text/javascript">
    var cropper;

    function showCropper(cropWidth, cropHeight) {
        cropper = new Cropper(image, {
            aspectRatio: cropWidth / cropHeight,
            viewMode: 0,
        });
        $.fancybox.open({
            autoCenter: true,
            fitToView: false,
            scrolling: false,
            openEffect: 'none',
            openSpeed: 1,
            autoSize: false,
            width: 600,
            height: 'auto',
            helpers: {
                overlay: {
                    css: {
                        'background': 'rgba(0, 0, 0, 0.3)'
                    },
                    closeClick: false
                }
            },
            padding: 0,
            closeBtn: false,
            content: $('#crop-popup'),
        });
    }
    window.addEventListener('DOMContentLoaded', function() {
        var selector = null;
        var selectorWidth = null;
        var selectorHeight = null;
        var image = document.getElementById('image');
        [...document.querySelectorAll('.input-image')].forEach(function(item) {
            item.addEventListener('change', function(e) {
                selector = e.target.getAttribute('data-selector');
                selectorWidth = e.target.getAttribute('data-crop-width');
                selectorHeight = e.target.getAttribute('data-crop-height');
                var files = e.target.files;
                var done = function(url) {
                    item.value = '';
                    image.src = url;
                    showCropper(selectorWidth, selectorHeight);
                };
                var reader;
                var file;
                var url;
                if (files && files.length > 0) {
                    file = files[0];
                    if (URL) {
                        done(URL.createObjectURL(file));
                    } else if (FileReader) {
                        reader = new FileReader();
                        reader.onload = function(e) {
                            done(reader.result);
                        };
                        reader.readAsDataURL(file);
                    }
                }
            });
        });
        document.getElementById('crop').addEventListener('click', function() {
            var canvas;
            if (cropper) {
                canvas = cropper.getCroppedCanvas({
                    width: selectorWidth,
                    height: selectorHeight,
                });
                var src = canvas.toDataURL();
                document.getElementById(selector + "_base64").value = src; // reflect changed value
                document.getElementById(selector + "_show").src = src; // show changed image
            }
            closeCropper();
        });
    });

    function closeCropper() {
        cropper.destroy();
        cropper = null;
        $.fancybox.close();
    }
</script>