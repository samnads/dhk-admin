<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title><?php echo isset($page_title) ? html_escape($page_title) : '-: Emaid :-'; ?></title>
    <!--<link rel="manifest" href="img/fav/manifest.json">-->
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="img/fav/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link href="<?php echo base_url(); ?>css/newstyle.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/bootstrap-responsive.min.css" rel="stylesheet">
    <!--<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">-->
    <link href="<?php echo base_url(); ?>css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/slider.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/animation.css" type="text/css">
    <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/pages/dashboard.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/hm.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>css/select2.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-2.1.0.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <?php
    if (isset($css_files) && is_array($css_files)) {
        foreach ($css_files as $css_file) {
            echo '<link rel="stylesheet" href="' . base_url() . 'css/' . $css_file . '" />';
        }
    }
    $extra_navbar_inner_style = '';
    $extra_subnavbar_inner_style = '';
    $body_background = '';
    if (strtolower($page_title) == 'bookings' || strtolower($page_title) == 'schedule') {
        $extra_navbar_inner_style = 'style="width: 104%;margin-left: -20px;"';
        $extra_subnavbar_inner_style = 'style="margin-top:54px;"';
        $body_background = 'style="background-color:#FFF;"';
    }
    ?>
</head>

<body <?php echo $body_background; ?>>
    <script language="javascript">
        var _base_url = '<?php echo base_url(); ?>';
        var _page_url = '<?php echo current_url(); ?>';
    </script>
    <?php
    if (is_user_loggedin()) {
        ?>
        <header>
            <div class="row header-wrapper no-left-right-margin">

                <div class="col-md-12 col-sm-12 search-wrapper">


                    <div class="col-md-3 col-sm-12 no-left-right-padding logo-section">
                        <div class="logo"><a href="<?php echo base_url(); ?>"><img
                                    src="<?php echo base_url(); ?>images/maidlogo.png" alt=""></a></div>
                        <form class="navbar-search mob-menu-view-only">
                            <select style="margin-top: 6px; width:220px; margin-bottom: 9px;"
                                class="search-query customers_vh_rep_new" id="" name="customers_vh_rep_new">
                                <option value="0">-- Select Customer --</option>
                            </select>
                            <!--<input class="search-query" id="head-search-f" placeholder="Search Customer" autocomplete="off" type="text">-->
                        </form>
                        <div class="clear"></div>
                    </div>

                    <div class="col-md-9 col-sm-6 no-left-right-padding search-section">

                        <div class="admin-drp-main">
                            <nav id="primary_nav_wrap">
                                <ul>
                                    <li><a href="#" class="e-hed-icon"><i class="fa fa-user"></i>
                                            <?php echo user_authenticate_name() ?>
                                        </a>

                                        <ul class="last-ul">
                                            <li><a href="<?php echo base_url() . 'dashboard/changepassword' ?>">Change
                                                    Password</a></li>
                                            <li><a href="<?php echo base_url() . 'logout' ?>">Logout</a></li>
                                        </ul>

                                    </li>



                                    <?php if (user_authenticate() == 1) { ?>
                                        <li><a href="#" class="e-hed-icon"><i class="fa fa-cogs"></i></a>

                                            <ul>
                                                <!--<li><a href="<?php // echo base_url() . 'sms-settings' 
                                                        ?>">SMS Configuration</a></li>
                                                <li><a href="<?php // echo base_url() . 'email-settings' 
                                                        ?>">Email Configuration</a></li>
                                                <li><a href="<?php // echo base_url() . 'tax-settings' 
                                                        ?>">Tax Configuration</a></li>-->

                                                <?php if (user_permission(user_authenticate(), 22)) { ?>
                                                    <li><a href="<?php echo base_url() . 'users'; ?>">Users</a></li>
                                                <?php } ?>
                                                <!--<li><a href="javascript:;">Settings</a></li>
                                                <li><a href="javascript:;">Help</a></li>-->

                                            </ul>

                                        </li>
                                    <?php } ?>
                                    <li style="width: 25px; padding: 0px;">
                                        <div class="mob-menu-icon mob-menu-view-only"><img
                                                src="<?php echo base_url(); ?>images/menu.png"></div>
                                        <!--logo end-->
                                    </li>
                                </ul>
                            </nav>
                        </div>

                        <form class="navbar-search pc-menu-view-only">
                            <select style="margin-top: 6px; width:220px; margin-bottom: 9px;"
                                class="search-query customers_vh_rep_new" id="" name="customers_vh_rep_new">
                                <option value="0">-- Select Customer --</option>
                            </select>
                            <!--<input class="search-query" id="head-search-m" placeholder="Search Customer" autocomplete="off" type="text">-->
                        </form>

                        <div class="clear"></div>
                    </div>

                </div>




                <div class="col-md-12 col-sm-12 no-left-right-padding menu">
                    <?php $this->load->view('layouts/primary_navigation'); ?>
                </div>

            </div>
            <!--row header-wrapper end-->
        </header>
        <!--header section end-->

        <div class="main fortoppadding" style="min-height: 572px;">
            <div class="main-inner">
                <div class="booking-fl-box full-width"><!--container-->

                    <?php
                    echo $content_body;
                    ?>
                </div>

            </div>

        </div>
        <?php
    } else {
        echo $content_body;
    }
    ?>
    <?php
    if (is_user_loggedin()) {
        ?>
        <div class="footer">
            <div class="footer-inner">
                <div class="container">
                    <div class="row">
                        <div class="span12"> &copy; <?php echo date('Y'); ?> <?= $settings->site_name; ?>. All rights reserved.</div>

                    </div>

                </div>

            </div>

        </div>
    <?php } ?>


    <!-- Le javascript
        ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->




    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.validate.min.js"></script>




    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/select2.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/hm.js"></script>
    <script type="text/javascript" src="https://momentjs.com/downloads/moment.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/ckeditor/js/sample.js"></script>

    <script>
        if ($("#editor").length > 0) {
            initSample();

        }
    </script>
    <!--------------------------- SHOW ACTIVE LINK IN MENU -------------------------- -->
    <script>
        $(function () {
            var current = window.location.href;
            $('#primary_nav_wrap li a').each(function () {
                var $this = $(this);
                // if the current path is like this link, make it active
                if ($this.attr('href') === current) {
                    $this.addClass('active');
                }
            })
        })
    </script>
    <style>
        #primary_nav_wrap li:has(ul li a.active) > a {
            background: #fff;
            color: #333 !important;
        }
        #primary_nav_wrap ul li ul li .active {
            background: #4c656e;
            color: #fff !important;
        }
    </style>
    <!----------------------------------------------------------------------------- -->
    <?php
    if (isset($external_js_files) && is_array($external_js_files)) {
        foreach ($external_js_files as $external_js_file) {
            echo '<script src="' . $external_js_file . '"></script>' . "\n";
        }
    }
    if (isset($js_files) && is_array($js_files)) {
        foreach ($js_files as $js_file) {
            echo '<script type="text/javascript" src="' . base_url() . 'js/' . $js_file . '"></script>' . "\n";
        }
    }
    ?>
    <div class="mm-loader"></div>
</body>

</html>