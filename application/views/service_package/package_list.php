<style type="text/css">
  .widget .widget-header {
    margin-bottom: 0px;
  }
  

  table.da-table tr td {
    padding: 0px 6px;
  }
</style>
<div id="disable-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Disable ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to disable this package ?</h3>
      <input type="hidden" id="disable_id">
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn red-btn mb-0" onclick="confirm_disable()">Disable</button>
    </div>
  </div>
</div>
<div id="enable-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Enable ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to enable this package ?</h3>
      <input type="hidden" id="enable_id">
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn mb-0" onclick="confirm_enable()">Enable</button>
    </div>
  </div>
</div>
<div class="row m-0">
  <div class="col-md-12">
    <div class="widget widget-table action-table">
      <div class="widget-header">
      <ul>
      <li><i class="icon-th-list"></i>
        <h3>Service Packages</h3></li>
        
        <li><select style="width:160px;" id="all-maids" onchange="statusFilter(this.value)">
          <option value="" <?php echo $status == null ? 'selected' : ''; ?>>Active</option>
          <option value="1" <?php echo $status === '1' ? 'selected' : ''; ?>>Disabled</option>
        </select></li>
        
        <li class="mr-0 float-right">
        
        <div class="topiconnew border-0 green-btn">
        	<a href="<?php echo base_url('service-package/new'); ?>" title="Add"> <i class="fa fa-plus"></i></a>
        </div>
        
        </li>
        </ul>
      </div>
      <!-- /widget-header -->

      <div class="widget-content">
        <table id="package-list-table" class="table table-hover da-table" width="100%">
            <thead>
                <tr style="line-height: 18px; width: 20px; background:#395C6A;color: #fff">
                        <th style="line-height: 18px; width: 150px">Category</th>
                        <th style="line-height: 18px; width: 150px">Sub Category</th>
                        <th style="line-height: 18px;width: 130px">Custom Title</th>
                        <th style="line-height: 18px;width: 70px;">Offer ?</th>
                        <th style="line-height: 18px;width: 70px;">Hours</th>
                        <th style="line-height: 18px;width: 70px;">No. of Maids</th>
                        <th style="line-height: 18px;width: 100px">Actual Amount</th>
                        <th style="line-height: 18px;width: 100px">Sale Amount</th>
                        <th style="line-height: 18px;width: 70px">Sort Order</th>
                        <th style="line-height: 18px;width: 100px"><center>Status</center></th>
                        <th style="line-height: 18px;width: 90px;">Image</th>
                        <th style="line-height: 18px; width: 100px" class="td-actions"><center>Actions</center></th>
                    </tr>
            </thead>
            <tbody>
                <?php foreach ($packages as $building_type => $packages): ?>
                    <thead>
                        <tr>
                            <th style="line-height: 18px; width: 20px; background:#c7f0b5;color:#000" colspan="12"><?=$building_type?></th>
                        </tr>
                    </thead>
                    <?php foreach ($packages as $key => $package): ?>
                        <tr>
                            <td style="line-height: 18px;"><?=$package->building_type?></td>
                            <td style="line-height: 18px;"><?=$package->room?></td>
                            <td style="line-height: 18px"><p><?=$package->title?></p></td>
                            <td style="line-height: 18px"><p><b><?=$package->is_offer == 1 ? '<span style="color:rgb(35, 169, 35); border-radius:1px; font-size:18px;"><i class="btn-icon-only icon-ok-sign"> </i></span>' : ""?></b></p></td>
                             <td style="line-height: 18px;"><?= $package->service_time ?></td>
                             <td style="line-height: 18px;"><?= $package->no_of_maids ?></td>
                                <td style="line-height: 18px;"><span class="strikethrough"><?=$package->actual_total_amount?></span></td>
                            <td style="line-height: 18px;"><?=$package->total_amount?></td>
                            <td style="line-height: 18px;"><?=$package->sort_order?></td>
                            <td style="line-height: 18px;"><center><?php echo !$package->deleted_at ? '<span style="color:rgb(35, 169, 35); border-radius:1px; font-size:18px;"><i class="btn-icon-only icon-ok-sign"> </i></span>' : '<span style="color:rgb(244, 55, 55); border-radius:1px; font-size:18px;"><i class="btn-icon-only icon-remove-sign"> </i></span>'; ?></center></td>
                             <td style="line-height: 18px">
                             <?php echo $package->customer_app_thumbnail ? '<img src="'.base_url().'./customer-app-assets/service-package-thumbnails/'. $package->customer_app_thumbnail. '" style="height:50px; width:auto; margin: 0 auto; border-radius: 5px;">': '-'; ?>

                                    </td>
                            <td style="line-height: 18px;" class="td-actions"><center>
                                <!--<a class="btn btn-small btn-warning" href="<?php echo base_url('package/edit/' . $package->id); ?>" title="Edit Offer"><i class="btn-icon-only icon-pencil"> </i></a>-->
                                <a href="<?=base_url('service-package/edit/' . $package->id);?>" class="n-btn-icon purple-btn" data-id="<?=$package->id;?>" style="" title="Edit"><i class="btn-icon-only fa fa-pencil"> </i></a>
                                <?php if (!$package->deleted_at): ?>
                                    <a href="#" class="n-btn-icon green-btn toggle-status" data-id="<?=$package->id;?>" style="background: #7eb216; color: #fff" title="Disable"><i class="btn-icon-only fa fa-toggle-on"> </i></a>
                                <?php else: ?>
                                    <a href="#" class="n-btn-icon red-btn toggle-status" data-id="<?=$package->id;?>" style="background: #db3325; color: #fff" title="Enable"><i class="btn-icon-only fa fa-toggle-off"> </i></a>
                                <?php endif;?>
                            </center></td>
                        </tr>
                    <?php endforeach;?>
                <?php endforeach;?>
          </tbody>
        </table>
      </div>
      <!-- /widget-content -->
    </div>
    <!-- /widget -->
  </div>
  <!-- /span6 -->
</div>
<!-- Modal -->
<script>
$(function() {
	let current = window.location.href;
	$('#primary_nav_wrap li a').each(function() {
		var $this = $(this);
		// if the current path is like this link, make it active
		if ($this.attr('href') === '<?php echo base_url('service_package'); ?>') {
        $this.addClass('active');
      }
    })
});
function closeFancy() {
  $.fancybox.close();
}
function statusFilter(status) {
    if (status)
      window.open("service-packages?status=" + status, "_self")
    else
      window.open("service-packages", "_self")
  }
// (function(a) {
// 	a(document).ready(function(b) {
// 		if (a('#package-list-table').length > 0) {
// 			a("table#package-list-table").dataTable({
// 				'sPaginationType': "full_numbers",
// 				"bSort": true,
// 				"iDisplayLength": 100,
// 				"scrollY": true,
// 				"orderMulti": false,
// 				"scrollX": true,
// 				'columnDefs': [{
// 					'targets': [-1],
// 					'orderable': false
// 				}, ]
// 			});
// 		}
// 	});
// })(jQuery);
</script>