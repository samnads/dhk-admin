<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
</style>
<div class="row m-0">  
    <div class="col-md-12">
        <div class="widget widget-table action-table">
            
            
            <!-- /widget-header -->
            
            
            
            
            <div class="widget-header">
     <div class="book-nav-top">
            <ul>
              <li><i class="icon-th-list"></i><h3>Area</h3></li>
              
              <li>
                  <button type="button" id="button_enable" class="btn btn-info" >Enable All Area</button>
              </li>
      
      
      
              <li>
                  <button type="button" id="button" class="btn btn-info" >Block All Area</button>
              </li>
      
      
      
      
              <li class="mr-0 float-right">
                  
                  <div class="topiconnew border-0 green-btn">
                       <a onclick="add_area();" title="Add Area"> <i class="fa fa-plus"></i></a>
                  </div>

              </li>
      
      
      
              <div class="clear"></div>
            </ul>
     </div>
</div>





            <div class="widget-content">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
<!--                <table class="table table-striped table-bordered">-->
                    <thead>
                        <tr>
                            <th style="line-height: 18px; width: 20px"> Sl.No. </th>
                            <th style="line-height: 18px"> Area </th>
                            <th style="line-height: 18px"> Zone</th>
                            <th style="line-height: 18px"> Driver</th>
                            <th style="line-height: 18px"> Web Status</th>
                            <th style="line-height: 18px" class="td-actions">Actions</th>
                        
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($areas) > 0) {
                            $i = 1;
                            foreach ($areas as $areas_val) {
                                ?>  
                                <tr>
                                    <td style="line-height: 18px; width: 20px"><?php echo $i; ?></td>
                                    <td style="line-height: 18px"><?php echo $areas_val['area_name'] ?></td>
                                    <td style="line-height: 18px"><?php echo $areas_val['zone_name'] ?></td>
                                    <td style="line-height: 18px"><?php echo $areas_val['driver_name'] ?></td>
                                    <?php   if( $areas_val['web_status']==1){?>
                                    <td style="line-height: 18px"><?php echo 'Active'; ?></td>
                                   <?php } ?>
                                   <?php   if( $areas_val['web_status']==0){?>
                                    <td style="line-height: 18px"><?php echo 'InActive'; ?></td>
                                   <?php } ?>
                                    <td style="line-height: 18px" class="td-actions">
										<a href="javascript:;" class="btn btn-small btn-warning" onclick="edit_area(<?php echo $areas_val['area_id'] ?>);"><i class="btn-icon-only icon-pencil"> </i></a>
										<?php if(user_authenticate() == 1) {?>
											<a href="javascript:;" class="btn btn-danger btn-small" onclick="delete_area(<?php echo $areas_val['area_id'] ?>);"><i class="btn-icon-only icon-remove"> </i></a>
										<?php } ?>
                                         <!-- <a href="javascript:void(0)"  style="background-color: #7eb216;background-image: linear-gradient(rgba(126, 178, 22, 0), rgba(0, 0, 0, 0.1));border-color: rgba(0, 0, 0, 0.05) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.15); color: white;text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.15);border-radius: 15px;" title="Disable Web Status" onclick="disable_webstatus(this, <?php echo $areas_val['area_id'] ?>, <?php echo $areas_val['web_status'] ?>);"><?php echo $areas_val['web_status'] == 1 ? '<i class="btn-icon-only  fa fa-toggle-on "> </i>' : '<i class="btn-icon-only  fa fa-toggle-off"> </i>' ?></a> -->
                                         <a href="javascript:void(0)" class="" style="background-color: #7eb216;background-image: linear-gradient(rgba(126, 178, 22, 0), rgba(0, 0, 0, 0.1));border-color: rgba(0, 0, 0, 0.05) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.15); color: white;text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.15);" title="" onclick="areaweb_status(<?php echo $areas_val['area_id']?>,<?php echo  $areas_val['web_status']?>);"><?php echo $areas_val['web_status'] == 1 ? '<i class="btn-icon-only  fa fa-toggle-on "> </i>' : '<i class="btn-icon-only  fa fa-toggle-off"> </i>' ?></a> 
                                         <!-- <?php if ($areas_val['web_status'] == 1) 
                                            {                                         
                                            ?>
                                                <a href="javascript:;" class="btn btn-success btn-small" onclick="areaweb_status(<?php echo $areas_val['area_id']?>,<?php echo  $areas_val['web_status']?>);"><i class="btn-icon-only icon-ok"> </i></a>
                                            <?php  
                                            }
                                            else     
                                            {
                                            ?>
                                                <a href="javascript:;" class="btn btn-danger btn-small" onclick="areaweb_status(<?php echo $areas_val['area_id']?>,<?php echo  $areas_val['web_status']?>);"><i class="btn-icon-only icon-remove"> </i></a>
                                            <?php
                                            }
                                             ?> -->
                                
                                </td>
                                 
                                </tr>
                                <?php
                                $i++;
                            }
                        }
                        ?>   

                    </tbody>
                </table>
            </div>
            <!-- /widget-content --> 
        </div>
        <!-- /widget --> 
    </div><!-- /span5 --> 
    <div class="span5" id="add_area" style="display: none;">      		
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-map-marker"></i>
                <h3>Add Area</h3>
                <a style="float:right ; margin-right:15px; cursor:pointer;" onclick="hideadd_area();"><img src="<?php echo base_url();?>img/minus.png" title="Hide"/></a>
            </div> <!-- /widget-header -->

            <div class="widget-content">
                <div class="tabbable">
                    <div class="tab-content">
<!--                        <form id="zone" class="form-horizontal" method="post" action="<?php //echo base_url(); ?>zones/add_zone">-->
                        <form id="area" class="form-horizontal" method="post">
                            <fieldset>
                                <div class="control-group">											
                                    <label class="control-label" for="zonename">Area Name</label>
                                    <div class="controls">
                                        <input type="text" class="span3" id="areaname" name="areaname" required="required">
                                    </div> <!-- /controls -->				
                                </div> <!-- /control-group -->

                                <div class="control-group">											
                                    <label class="control-label" for="zone_id">Zone Name</label>
                                    <div class="controls">
                                        <select name="zone_id" id="zone_id" class="span3" required >
                                            <option value="">-- Select Zone --</option>
                                            <?php
                                            if (count($zones) > 0) {
                                                foreach ($zones as $zones_val) {
                                                    ?>
                                                    <option value="<?php echo $zones_val['zone_id']; ?>"><?php echo $zones_val['zone_name']; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div> <!-- /controls -->				
                                </div> <!-- /control-group -->

                                <br />

                                <div class="form-actions">
                                    <input type="submit" class="btn mm-btn pull-right" value="Submit" name="area_sub">
<!--                                    <button class="btn">Cancel</button>-->
                                </div> <!-- /form-actions -->
                            </fieldset>
                        </form>

                    </div>
                </div>
            </div> <!-- /widget-content -->
        </div> <!-- /widget -->
    </div> <!-- /span5 -->
    <div class="span5" id="edit_area" style="display: none;">      		
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-map-marker"></i>
                <h3>Edit Area</h3>
                <a style="float:right ; margin-right:15px; cursor:pointer;" onclick="hideedit_area();"><img src="<?php echo base_url();?>img/minus.png" title="Hide"/></a>
            </div> <!-- /widget-header -->

            <div class="widget-content">
                <div class="tabbable">
                    <div class="tab-content">
                        <form id="area" class="form-horizontal" method="post">
                            <fieldset>
                                <div class="control-group">											
                                    <label class="control-label" for="zonename">Area Name</label>
                                    <div class="controls">
                                        <input type="text" class="span3" id="edit_areaname" name="edit_areaname" required="required">
                                        <input type="hidden" class="span3" id="edit_areaid" name="edit_areaid">
                                    </div> <!-- /controls -->				
                                </div> <!-- /control-group -->

                                <div class="control-group">											
                                    <label class="control-label" for="zone_id">Zone Name</label>
                                    <div class="controls">
                                        <select name="edit_zone_id" id="edit_zone_id" class="span3" required >
                                            <option value="">-- Select Zone --</option>
                                            <?php
                                            if (count($zones) > 0) {
                                                foreach ($zones as $zones_val) {
                                                    ?>
                                                    <option value="<?php echo $zones_val['zone_id']; ?>"><?php echo $zones_val['zone_name']; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div> <!-- /controls -->				
                                </div> <!-- /control-group -->

                                <br />

                                <div class="form-actions">
                                    <input type="submit" class="btn mm-btn pull-right" value="Submit" name="area_edit">
<!--                                    <button class="btn">Cancel</button>-->
                                </div> <!-- /form-actions -->
                            </fieldset>
                        </form>

                    </div>
                </div>
            </div> <!-- /widget-content -->
        </div> <!-- /widget -->
    </div> <!-- /span5 -->
</div>



<script>

$('#button').click(function() {
    // alert('God Is Love');
    // var candidate_id = $('input[name="candidate_id"]').val();
    var url = 'settings/change_status/';
    $.ajax({
        url: 'https://booking.emaid.info:3443/elitemaids/' + url,
        type: 'POST',
        // data: {'$candidate_id': $candidate_id},
        dataType: 'JSON',
        success: function(data) {    
            alert('All Areas are Blocked');
            window.location.reload();
        }
    });
});



$('#button_enable').click(function() {
    // alert('God Is Love');
    // var candidate_id = $('input[name="candidate_id"]').val();
    var url = 'settings/change_status_enable/';
    $.ajax({
        url: 'https://booking.emaid.info:3443/elitemaids/' + url,
        type: 'POST',
        // data: {'$candidate_id': $candidate_id},
        dataType: 'JSON',
        success: function(data) {    
            alert('All Areas are Enabled');
            window.location.reload();
        }
    });
});

function disable_webstatus($this, area_id, web_status)
{
    // alert(area_id);
    // alert(web_status); die;
    var _lblstatus = web_status == 1 ? 'disable' : 'enable';
    if (confirm('Are you sure you want to ' + _lblstatus + ' the Web status'))
    {
        $.ajax({
            type: "POST",
            url: _base_url + "settings/remove_area_click",
            data: {area_id: area_id, web_status : web_status},
            dataType: "text",
            cache: false,
            success: function (result) {
                //window.location.assign(_base_url + 'customers');
                window.location.reload();
                if(result == 1)
                {
                    $($this).attr('class', 'btn btn-success btn-small');
                    $($this).html('<i class="btn-icon-only icon-ok"></i>');
                 
                   
                }
                else
                {
                    if(result == 'exist_bookings') 
                    {
                        alert('Warning! Can\'t deactivate this customer, have some active bookings.');
                        result = 0;
                    }
                    else
                    {
                        $($this).attr('class', 'btn btn-danger btn-small');
                        $($this).html('<i class="btn-icon-only icon-remove"> </i>');
                       
                       
                    }
                    
                }
                $($this).attr('onclick', 'delete_customer(this, ' + area_id +', ' + result +')');
                 
            }
        });
    }
}

function areaweb_status(area_id, web_status)
{
    if (web_status === 1)
    {
        if (confirm('Are you sure you want to Diasble this Area'))
        {
            $.ajax({
                type: "POST",
                url: _base_url + "settings/areaweb_status",
                data: {area_id: area_id, web_status: web_status},
                dataType: "text",
                cache: false,
                success: function (result) {
                    window.location.assign(_base_url + 'areas');
                }
            });
        }
    }
    else if (web_status === 0)
    {
        if (confirm('Are you sure you want to Activate this Area'))
        {
            $.ajax({
                type: "POST",
                url: _base_url + "settings/areaweb_status",
                data: {area_id: area_id, web_status: web_status},
                dataType: "text",
                cache: false,
                success: function (result) {
                    window.location.assign(_base_url + 'areas');
                }
            });
        }
    }

}
</script>