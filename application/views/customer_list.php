<style>
    .btn-mm-success {
    background-color: #7eb216;
    background-image: linear-gradient(rgba(126, 178, 22, 0), rgba(0, 0, 0, 0.1));
    border-color: rgba(0, 0, 0, 0.05) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.15);
    color: white;
    text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.15);
}
.btn-mm-success:hover {
    background-color: #7eb216;
    background-image: linear-gradient(rgba(126, 178, 22, 0), rgba(0, 0, 0, 0.1));
    border-color: rgba(0, 0, 0, 0.05) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.15);
    color: white;
    text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.15);
}
    
</style>
<div class="row m-0">      
    <div class="col-sm-12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header">
            
            <ul>
            <li>
            <i class="icon-th-list"></i>
            <h3>Customer List</h3>
            
            </li>
            <li>
            
                <span style="margin-left:5px;"></span>
                <div class="mm-drop">
                <select style="width:120px;" id="per-page">
                    <option value="50" <?php echo $per_page == 50 ? 'selected="selected"' : ''; ?>>50 records</option>
                    <option value="100" <?php echo $per_page == 100 ? 'selected="selected"' : ''; ?>>100 records</option>
                </select>
            </div>
            
            </li>
            <li>
            
                <div class="mm-drop">
                <select style="width:80px;" id="all-customers">
                    <option value="1" <?php echo $active == 1 ? 'selected="selected"' : ''; ?>>All</option>
                    <option value="2" <?php echo $active == 2 ? 'selected="selected"' : ''; ?>>Active</option>
                    <option value="3" <?php echo $active == 3 ? 'selected="selected"' : ''; ?>>Inactive</option>
                </select>
                    </div>
             
            </li>
            <li>
            
                <input type="checkbox" name="checkdate" id="checkdate" value="1" />
                <span style="margin-left:15px;">From :</span>
                <input type="text" style="width: 100px;" id="cust_from_date" />
                <span style="margin-left:15px;">To :</span>
                <input type="text" style="width: 100px; margin-right: 10px;" id="cust_to_date" />
<!--                <span style="margin-left:15px;">Pay Type :</span>-->
            
            </li>
            <li> 
                <div class="mm-drop">
                <select style="width: 120px; margin-right: 10px;" id="payment_type">
                    <option value="" selected="selected">All Pay Type</option>
                    <option value="D">Daily</option>
                    <option value="W">Weekly</option>
                    <option value="M">Monthly</option>
                </select>
                </div>
                
                
                </li>
            <li>
            
                <div class="mm-drop">    
                <select style="width: 80px; margin-right: 10px;" id="sort_custmer">
                    <option value="1" selected="selected">All</option>
                    <option value="2">Today Booking</option>
                    <option value="3">Today First Time Booking</option>
                </select>
                </div>
                
                
                </li>
            <li>
            
                <div class="mm-drop">    
                <select style="width: 90px;" id="sort_source">
                    <option value="">-Select-</option>          
                    <option value="Rizek">Rizek</option>
                    <option value="Facebook">Facebook</option>
                    <option value="Google">Google</option>
                    <option value="Yahoo">Yahoo</option>
                    <option value="Bing">Bing</option>
					<option value="Direct Call">Direct Call</option>
					<option value="Flyers">Flyers</option>
					<option value="Referral">Referral</option>
					<option value="Watchman/Security Guard">Watchman/Security Guard</option>
					<option value="Maid">Maid</option>
                    <option value="Driver">Driver</option>
					<option value="By email">By email</option>
                    <option value="Schedule visit">Schedule visit</option>
                    <option value="Website">Website</option>
					<option value="Referred by staff">Referred by staff</option>
                    <option value="Referred by customer">Referred by customer</option>
					<option value="Justmop">Justmop</option>
					<option value="Matic">Matic</option>
					<option value="ServiceMarket">ServiceMarket</option>
					<option value="Emaar">Emaar</option>
					<option value="MyHome">MyHome</option>
                </select>
                    <select style="width: 90px;" id="sort_cust_type">
                    <option value="">-Select-</option>          
                    <option value="1">Regular</option>
                    <option value="0">Non-Regular</option>
                    </select>
              </div>
              
              
              </li>
            <li>
                <input type="text" style="margin-left :8px;width:115px;" id="keyword-search">
                
                
            </li>
            <li class="mr-0 float-right">
            
                <div class="topiconnew"><a href="<?php echo base_url(); ?>customer/add"><img src="<?php echo base_url(); ?>images/user-plus-icon.png" title="Add Customer"/></a></div>
                
                
                
                <?php
				if (user_authenticate() == 1) 
				{
				?>
				<div class="topiconnew"><a href="<?php echo base_url(); ?>customer/toExcel/<?php echo $active; ?>"><img src="<?php echo base_url(); ?>images/excel-icon.png" title="Add Customer"/></a></div>
				<?php
				}
				?>
                
                </li>
            </ul>
			</div>
            <!--<div id="LoadingImage" style="text-align:center;display:none;position:absolute;top:133px;right:0px;width:100%;height:100%;background-position:center;"><img src="<?php echo base_url() ?>img/loader.gif"></div>-->
            <div class="widget-content" style="margin-bottom:30px">
<!--                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">-->



                <!--New table using div responsive starts-->
                <div class="Table table-top-style-box" id="customer-list">

                    <div class="Heading table-head">

                        <div class="Cell">
                            <p><strong>NO.</strong></p>
                        </div>

                        <div class="Cell">
                            <p><strong> CUSTOMER NAME</strong></p>
                        </div>
						<?php
						if (user_authenticate() == 1) 
						{
						?>
                        <div class="Cell">
                            <p><strong>MOBILE</strong></p>
                        </div>
                        <?php
						}
						?>
                        <div class="Cell">
                            <p><strong>AREA</strong></p>
                        </div>
                        
                        
                        <div class="Cell">
                            <p><strong>ADDRESS</strong></p>
                        </div>

                        

                        <div class="Cell">
                            <p><strong>SOURCE</strong></p>
                        </div>
                        
                        <div class="Cell">
                            <p><strong>LAST JOB</strong></p>
                        </div>

                        <div class="Cell">
                            <p><strong> ADDED DATETIME</strong></p>
                        </div>

                        <div class="Cell">
                            <p><strong>ACTIONS</strong></p>
                        </div>

                    </div>

                    <?php
                    if (!empty($customers)) {
                        $i = $starting;
                        foreach ($customers as $customers_val) {
                            if($customers_val->building != "")
                            {
                                $apartmnt_no = 'Apartment No:'.$customers_val->building.'<br/>';
                            } else {
                                $apartmnt_no = "";
                            }
                            //Payment Type
                            if ($customers_val->payment_type == "D") {
                                $paytype = "(D)";
                                $paytext = "Daily";
                            } else if ($customers_val->payment_type == "W") {
                                $paytype = "(W)";
                                $paytext = "Weekly";
                            } else if ($customers_val->payment_type == "M") {
                                $paytype = "(M)";
                                $paytext = "Monthly";
                            } else {
                                $paytype = "";
                                $paytext = "";
                            }
                            $last_jobdate = $this->customers_model->get_last_job_date_by_customerid($customers_val->customer_id);
                            if(empty($last_jobdate))
                            {
                                $last_date = "";
                            } else {
                                $last_date = $last_jobdate->service_date;
                            }
							
							if($customers_val->customer_address == "")
							{
								$a_address = 'Building - '.$customers_val->building.', '.$customers_val->unit_no.''.$customers_val->street;
							} else {
								$a_address = $customers_val->customer_address;
							}
							
							if($customers_val->is_flag == "Y") {
								$isflag = " -- Flagged (".$customers_val->flag_reason.")";
							} else if($customers_val->is_flag == "N") {
								$isflag = "";
							}
                            ?>  
                            <div class="Row">

                                <div class="Cell">
                                    <p><?php echo $i++; ?></p>
                                </div>

                                <div class="Cell">
                                    <p><a href="<?php echo base_url(); ?>customer/view/<?php echo $customers_val->customer_id ?>" style="text-decoration: none;color:#333;" data-toggle="tooltip" title="<?php echo $paytext; ?>"><?php echo $customers_val->customer_name ?> <?php echo $paytype; ?><br/><span style="color: red;"><?php echo $isflag; ?></span></a></p>
                                </div>
								<?php
								if (user_authenticate() == 1) 
								{
								?>
                                <div class="Cell">
                                    <p><?php echo $customers_val->mobile_number_1 ?></p>
                                </div>
								<?php
								}
								?>
                                
                                <div class="Cell">
                                    <p><?php echo $customers_val->zone_name . '-' . $customers_val->area_name ?></p>
                                </div>
                                
                                <div class="Cell">
                                    <p><?php echo $apartmnt_no; ?><?php echo wordwrap($a_address, 25, "<br>"); ?></p>
                                </div>

                                

                                <div class="Cell">
                                    <p>
									<?php echo $customers_val->customer_source;
										if($customers_val->customer_source =="Others"){ echo "(".$customers_val->customer_source_val.")"; } ?>
									</p>
                                </div>
                                
                                <div class="Cell">
                                    <p><?php echo $last_date; ?></p>
                                </div>


                                <div class="Cell">
                                    <p><?php echo ($customers_val->customer_added_datetime) ? date('d/m/Y H:i:s', strtotime($customers_val->customer_added_datetime)) : ""; ?></p>
                                </div>

                                <div class="Cell">
                                    <p><a class="btn btn-small btn-info" href="<?php echo base_url(); ?>customer/view/<?php echo $customers_val->customer_id ?>" title="View Customer"><i class="btn-icon-only fa fa-eye "> </i></a>
                                        <a class="btn btn-small btn-warning" href="<?php echo base_url(); ?>customer/edit/<?php echo $customers_val->customer_id ?>" title="Edit Customer"><i class="btn-icon-only icon-pencil"> </i></a>
        <?php $btn_class = $customers_val->customer_status == 1 ? 'btn btn-mm-success btn-small' : 'btn btn-danger btn-small'; ?>
        <?php
        if (user_authenticate() == 1) {
            ?>
                                        <a href="javascript:void(0)" class="<?php echo $btn_class ?>" title="Disable Customer" onclick="delete_customer(this, <?php echo $customers_val->customer_id ?>, <?php echo $customers_val->customer_status ?>);"><?php echo $customers_val->customer_status == 1 ? '<i class="btn-icon-only  fa fa-toggle-on "> </i>' : '<i class="btn-icon-only  fa fa-toggle-off"> </i>' ?></a>
                                        <?php } ?></p>
                                </div>


                            </div>

        <?php
    }
} else {
    echo '<div class="Cell"> <p>No records found</p>  </div>';
}
?>




                </div><!--Table table-top-style-box end--> 
                <!--New table using div responsive ends-->



            </div>
            <!-- /widget-content --> 

            <p style="text-align : center" class="pagination-bot"><?php echo $links; ?></p>

        </div>
        <!-- /widget --> 
    </div>
    <!-- /span6 --> 
</div>




<!-- Modal -->
<div id="myModal" class="modal hide fade" style="width: 700px; left: 45%" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel"><span id="customer_name">Customer</span></h3>
    </div>
    <div class="modal-body">
        <a style="float:right ; margin-right:20px; cursor:pointer;"><img id="customer_photo" src="<?php echo base_url(); ?>img/profile_pic.jpg" style="height: 150px; width: 150px"/> </a>
        <table>
            <tbody>
                <tr><td style="line-height: 30px; width: 200px"><b>Customer Nick Name</b></td><td>:&nbsp;&nbsp;</td><td style="line-height: 30px;"><span id="customer_nick_name"></span></td></tr>
                <tr><td style="line-height: 30px; width: 200px"><b>Customer Addresses</b></td><td>:&nbsp;&nbsp;</td><td style="line-height: 30px;" id="cust_add"><span></span></td></tr>

                <tr><td style="line-height: 30px; width: 200px"><b>Mobile Number 1 </b></td><td>:&nbsp;&nbsp;</td><td><span id="mobile_number_1"></span></td></tr>
                <tr><td style="line-height: 30px; width: 200px"><b>Mobile Number 2</b></td><td>:&nbsp;&nbsp;</td><td><span id="mobile_number_2"></span></td></tr>
                <tr><td style="line-height: 30px; width: 200px"><b>Mobile Number 3</b></td><td>:&nbsp;&nbsp;</td><td> <span id="mobile_number_3"></span></td></tr>
                <tr><td style="line-height: 30px; width: 200px"><b>Phone Number </b></td><td>:&nbsp;&nbsp;</td><td> <span id="phone_number"></span></td></tr>
                <tr><td style="line-height: 30px; width: 200px"><b>Fax Number </b></td><td>:&nbsp;&nbsp;</td><td> <span id="fax_number"></span></td></tr>
                <tr><td style="line-height: 30px; width: 200px"><b>Email Address </b></td><td>:&nbsp;&nbsp;</td><td> <span id="email_address"></span></td></tr>
                <tr><td style="line-height: 30px; width: 200px"><b>Website URL </b></td><td>:&nbsp;&nbsp;</td><td> <span id="website_url"></span></td></tr>
                <tr><td style="line-height: 30px; width: 200px"><b>Customer Type </b></td><td>:&nbsp;&nbsp;</td><td> <span id="customer_type"></span></td></tr>
                <tr><td style="line-height: 30px; width: 200px"><b>Contact Person </b></td><td>:&nbsp;&nbsp;</td><td> <span id="contact_person"></span></td></tr>
                <tr><td style="line-height: 30px; width: 200px"><b>Payment Type </b></td><td>:&nbsp;&nbsp;</td><td> <span id="payment_type"></span></td></tr>
                <tr><td style="line-height: 30px; width: 200px"><b>Payment Mode</b></td><td>:&nbsp;&nbsp;</td><td> <span id="payment_mode"></span></td></tr>
                <tr><td style="line-height: 30px; width: 200px"><b>Price Hourly</b></td><td>:&nbsp;&nbsp;</td><td><span id="price_hourly"></span></td></tr>
                <tr><td style="line-height: 30px; width: 200px"><b>Price extra</b></td><td>:&nbsp;&nbsp;</td><td> <span id="price_extra"></span></td></tr>
                <tr><td style="line-height: 30px; width: 200px"><b>Price Weekend</b></td><td>:&nbsp;&nbsp;</td><td> <span id="price_weekend"></span></td></tr>
                <tr><td style="line-height: 30px; width: 200px"><b>Notes </b></td><td>:&nbsp;&nbsp;</td><td> <span id="customer_notes"></span></td></tr>
            </tbody>
        </table>
    </div>
    <div class="modal-footer">

    </div>
</div>



<!-- customer_address -->
<div id="cust_address" class="modal hide fade" style="width: 700px; left: 45%" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel"><span id="cust_name">Customer</span></h3>
    </div>
    <div class="modal-body">
        <table>
            <tbody>
                <tr>
                    <td style="line-height: 30px; width: 200px"><b>Address</b></td>
                    <td>:&nbsp;&nbsp;</td>
                    <td style="line-height: 30px;"><span id="customer_add"></span></td>
                </tr>

            </tbody>
        </table>
    </div>
    <div class="modal-footer">

    </div>
</div>

<!--<style>

    .imge{
        display: none
    }

    td.dispimage:hover img{
        display: block;
    }
</style>-->