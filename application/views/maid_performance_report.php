<style type="text/css">
	.widget .widget-header {
		margin-bottom: 0px;
	}

	.topiconnew {
		cursor: pointer;
	}

	td {
		line-height: 24px !important;
	}
</style>

<div class="row m-0">
	<div class="col-md-12">
		<div class="widget widget-table action-table" style="margin-bottom:30px">
			<div class="widget-header">
				<form name="zone-wise-search" class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo base_url() ?>booking/booking_reports">
					<ul>
						<li>
							<i class="icon-th-list"></i>
							<h3>Maid Performance Report</h3>
						</li>
						<li>
							<input type="text" style="width: 160px;" readonly="true" name="schedule_date_from" id="vehicle_date" value="<?php echo $schedule_date_from ?>" />
						</li>
						<li>
							<input type="text" style=" width: 160px;" readonly="true" name="schedule_date_to" id="vehicle_date_to" value="<?php echo $schedule_date_to ?>" />
						</li>
						<li>

							<span style="margin-left:23px;">Zone</span>

							<select style=" width:160px; " id="zones" name="zones">
								<option value="">-- Select Zone --</option>
								<?php
								if (count($zones) > 0) {
									foreach ($zones as $zones_val) {
								?>
										<option value="<?php echo $zones_val->zone_id; ?>" <?php if ($zone_id == $zones_val->zone_id) {
																								echo 'selected="selected"';
																							} else {
																								echo '';
																							} ?>><?php echo $zones_val->zone_name; ?></option>
								<?php
									}
								}
								?>
							</select>
						</li>
						<li>

							<span style="margin-left:15px;"></span>

							<input type="submit" class="n-btn" value="Go" name="vehicle_report">

						</li>
						<li class="mr-0 float-right">
							<div class="topiconnew border-0 green-btn">
								<!--<a onclick="exportF(this)" title="Download to Excel"> <i class="fa fa-download"></i></a>-->
								<a href="javascript:void(0);" id="performanceexcelbtn" title="Download to Excel"> <i class="fa fa-file-excel-o"></i></a>
							</div>
						</li>
					</ul>

				</form>
			</div>

			<div class="widget-content" style="margin-bottom:30px">
				<table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>
								<center>Sl. No<center>
							</th>
							<th> Cleaner</th>
							<!--<th > Total Timings</th>-->
							<th> Client Name</th>
							<th> Date</th>
							<th> Phone </th>
							<th> Booking Type </th>
							<th> Material </th>
							<!--<th > Client Location</th>-->
							<th> Source</th>
							<th> Timings</th>
							<!--<th > Zone</th>-->
							<th> Driver</th>
							<th style="  padding: 5px 10px; text-align: right;"> Payment</th>
							<th> Total</th>
							<th> Hours</th>
							<!--<th > Pay Mode</th>-->
						</tr>
					</thead>
					<tbody style="line-height: 22px;">
						<?php
						$sln = 1;
						foreach ($performance_report as $performance) {
							foreach ($performance as $key) {
								$booingdate = $key->date;
								$booingdate = str_replace('/', '-', $booingdate);
								$booingdate = date('Y-m-d', strtotime($booingdate));
								$maid_disable_date = date('Y-m-d', strtotime($key->maid_disabled_datetime));
								//if($key->maid_status==0&& ($booingdate>=$curdate))// to allow only past bookings for di
								//lat active hide
								$cursdate = date('Y-m-d');
								if ($key->maid_status == '0' && ($booingdate >= $cursdate)) {
									continue;
								}
						?>
								<tr <?php if ($key->maid_status == 0) { ?> style="background-color: #fdf1f1;" <?php } ?>>
									<td style=" ">
										<center><?php echo $sln++; ?></center>
									</td>
									<td style=" ">
										<?php //echo html_escape($key->maid_name); 
										?>
										<?php
										$maid_names = explode(',', $key->maid_names);
										foreach ($maid_names as $maid_name) {
											// output each hobby and decorate/separate them however you'd like
											echo $maid_name . '<br />';
										}
										//echo html_escape($performance->customer_name);
										?>
									</td>
									<td style=" ">
										<?php
										$customers = explode(',', $key->customer_names);
										foreach ($customers as $customer) {
											// output each hobby and decorate/separate them however you'd like
											echo $customer . '<br />';
										}
										//echo html_escape($performance->customer_name);
										?>
									</td>
									<td style=" ">
									<?php echo date('d-m-y', strtotime($booingdate)) . '<br />'; ?>
									</td>
									<td style=" ">
										<?php
										$mobile_numbers = explode(',', $key->mobile_number);
										foreach ($mobile_numbers as $mobile_number) {
											// output each hobby and decorate/separate them however you'd like
											echo $mobile_number . '<br />';
										}
										//echo html_escape($performance->customer_name);
										?>
									</td>
									<td style=" ">
										<?php
										$booking_typ = explode(',', $key->booking_type);
										foreach ($booking_typ as $b_type) {
											// output each hobby and decorate/separate them however you'd like
											if ($b_type == "OD") {
												$booktype = "One Time";
											} else if ($b_type == "WE") {
												$booktype = "Weekly";
											} else if ($b_type == "BW") {
												$booktype = "Bi-Weekly";
											} else {
												$booktype = "";
											}
											echo $booktype . '<br />';
										}
										//echo html_escape($performance->customer_name);
										?>
									</td>
									<td style=" ">
										<?php
										$materials = explode(',', $key->cleaning_materials);
										foreach ($materials as $material) {
											if ($material == "Y") {
												$mat = "Yes";
											} else {
												$mat = "No";
											}
											echo $mat . '<br />';
										}
										//echo html_escape($performance->customer_name);
										?>
									</td>
									<td style=" ">
										<?php
										$sources = explode(',', $key->sources);
										foreach ($sources as $source) {
											// output each hobby and decorate/separate them however you'd like
											echo $source . '<br />';
										}
										//echo html_escape($performance->customer_name);
										?>
									</td>
									<td style=" ">
										<?php
										$shifts = explode(',', $key->time_from);
										$shifts2 = explode(',', $key->time_to);
										$i = 0;
										foreach ($shifts as $shift) {
											// output each hobby and decorate/separate them however you'd like
											echo $shift . ' - ' . $shifts2[$i] . '<br />';
											$i++;
										}
										//echo html_escape($performance->customer_name);
										?>
									</td>

									<td style=" ">
										<?php
										$drivers = explode(',', $key->drivers);
										foreach ($drivers as $driver) {
											// output each hobby and decorate/separate them however you'd like
											echo $driver . '<br />';
										}
										//echo html_escape($performance->customer_name);
										?>
									</td>

									<td style="  text-align: right; font-family:Arial, Helvetica, sans-serif;">
										<?php
										$totamt = explode(',', $key->totamt);
										$total = 0;
										$total = array_sum($totamt);
										foreach ($totamt as $totam) {
											// output each hobby and decorate/separate them however you'd like
											echo $totam . '<br />';
										}
										// echo "<br /><span>Total : <strong>".$total."</strong></span>";
										?>
									</td>
									<td>
										<?php
										$totamts = explode(',', $key->totamt);
										$totals = 0;
										$totals = array_sum($totamts);
										echo $totals;
										?>
									</td>
									<td style=" ">
										<?php
										$shiftss = explode(',', $key->new_time_from);
										$shiftss2 = explode(',', $key->new_time_to);
										$i = 0;
										$shours = 0;
										foreach ($shiftss as $shiftt) {
											$t_shrt = $shiftss2[$i];
											$cur_shrt = $shiftt;
											$hours = ((strtotime($t_shrt) - strtotime($cur_shrt)) / 3600);
											$shours += $hours;
											$i++;
										}
										echo $shours;
										?>
									</td>
							<?php


							}
						}

							?>



					</tbody>
				</table>
			</div><!-- /widget-content -->

		</div><!-- /widget -->
	</div><!-- /span12 -->
</div>

<div id="divToPrint" style="display: none">
	<div class="widget-content" style="margin-bottom:30px">
		<table border="1" width="100%" cellspacing="0" cellpadding="10" style="font-size:11px; border-color: #ccc;" class="ptable">

			<thead>
				<tr>
					<th> Sl No.</th>
					<th> Cleaner</th>
					<th> Client Name</th>
					<th> Date</th>
					<th> Phone</th>
					<th> Booking Type</th>
					<th> Material</th>
					<th> Source</th>
					<th> Timings</th>
					<th> Driver</th>
					<th> Payment</th>
					<th> Total</th>
					<th> Hours</th>
				</tr>
			</thead>
			<tbody style="line-height: 22px;">
				<?php
				$sln = 1;
				foreach ($performance_report as $performance) {
					foreach ($performance as $key) {
						$booingdate = $key->date;
						$booingdate = str_replace('/', '-', $booingdate);
						$booingdate = date('Y-m-d', strtotime($booingdate));
						$maid_disable_date = date('Y-m-d', strtotime($key->maid_disabled_datetime));
						$cursdate = date('Y-m-d');
						if ($key->maid_status == '0' && ($booingdate >= $cursdate)) {
							continue;
						}
				?>
						<tr <?php if ($key->maid_status == 0) { ?> style="background-color: #fdf1f1;" <?php } ?>>
							<td style=" ">
								<center><?php echo $sln++; ?></center>
							</td>
							<td style=" ">
								<?php
								$maid_names = explode(',', $key->maid_names);
								foreach ($maid_names as $maid_name) {
									echo $maid_name . '<br />';
								}
								?>
							</td>
							<td style=" ">
								<?php
								$customers = explode(',', $key->customer_names);
								foreach ($customers as $customer) {
									echo $customer . '<br />';
								}
								?>
							</td>
							<td style=" ">
							<?php echo date('d-m-y', strtotime($booingdate)) . '<br />'; ?>
							</td>
							<td style=" ">
								<?php
								$mobile_numbers = explode(',', $key->mobile_number);
								foreach ($mobile_numbers as $mobile_number) {
									echo $mobile_number . '<br />';
								}
								?>
							</td>
							<td style=" ">
								<?php
								$booking_typ = explode(',', $key->booking_type);
								foreach ($booking_typ as $b_type) {
									if ($b_type == "OD") {
										$booktype = "One Time";
									} else if ($b_type == "WE") {
										$booktype = "Weekly";
									} else if ($b_type == "BW") {
										$booktype = "Bi-Weekly";
									} else {
										$booktype = "";
									}
									echo $booktype . '<br />';
								}
								?>
							</td>
							<td style=" ">
								<?php
								$materials = explode(',', $key->cleaning_materials);
								foreach ($materials as $material) {
									if ($material == "Y") {
										$mat = "Yes";
									} else {
										$mat = "No";
									}
									echo $mat . '<br />';
								}
								?>
							</td>
							<td style=" ">
								<?php
								$sources = explode(',', $key->sources);
								foreach ($sources as $source) {
									echo $source . '<br />';
								}
								?>
							</td>
							<td style=" ">
								<?php
								$shifts = explode(',', $key->time_from);
								$shifts2 = explode(',', $key->time_to);
								$i = 0;
								foreach ($shifts as $shift) {
									echo $shift . ' - ' . $shifts2[$i] . '<br />';
									$i++;
								}
								?>
							</td>

							<td style=" ">
								<?php
								$drivers = explode(',', $key->drivers);
								foreach ($drivers as $driver) {
									echo $driver . '<br />';
								}
								?>
							</td>

							<td style="  text-align: right; font-family:Arial, Helvetica, sans-serif;">
								<?php
								$totamt = explode(',', $key->totamt);
								$total = 0;
								$total = array_sum($totamt);
								foreach ($totamt as $totam) {
									echo $totam . '<br />';
								}
								// echo "<br /><span>Total : <strong>".$total."</strong></span>";
								?>
							</td>

							<td>
								<?php
								$totamts = explode(',', $key->totamt);
								$totals = 0;
								$totals = array_sum($totamts);
								echo $totals;
								?>
							</td>

							<td style=" ">
								<?php
								$shiftss = explode(',', $key->new_time_from);
								$shiftss2 = explode(',', $key->new_time_to);
								$i = 0;
								$shours = 0;
								foreach ($shiftss as $shiftt) {
									$t_shrt = $shiftss2[$i];
									$cur_shrt = $shiftt;
									$hours = ((strtotime($t_shrt) - strtotime($cur_shrt)) / 3600);
									$shours += $hours;
									$i++;
								}
								echo $shours;
								?>
							</td>
					<?php


					}
				}

					?>



			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
	function exportF(elem) {
		var table = document.getElementById("divToPrint");
		var html = table.outerHTML;
		var url = 'data:application/vnd.ms-excel,' + escape(html); // Set your html table into url 
		elem.setAttribute("href", url);
		elem.setAttribute("download", "Booking Report <?php echo $schedule_date_from ?> - <?php echo $schedule_date_to ?>.xls"); // Choose the file name
		return false;
	}
</script>