<?php
//        echo '<pre>';
//        print_r($reports);
//        exit();
?>
<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
    .topiconnew{cursor: pointer;}
</style>

<div class="row m-0">   
    <div class="col-md-12">
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form id="edit-profile" class="form-horizontal" method="post" >
                    <ul>
                    <li>
                    <i class="icon-th-list"></i>
                    <h3>Zone Wise Booking Report</h3>
                    </li>
                    <li>
                    <span style="margin-left:23px;">From :</span>
                    <input type="text" style="width: 160px;" id="ActFromDate" name="from_date" value="<?php echo isset($from_date) ? $from_date : date('d/m/Y'); ?>">
                    
                    </li>
                    <li>
                    <span style="margin-left:23px;">To :</span>
                    <input type="text" style="width: 160px;" id="ActToDate" name="to_date" value="<?php echo isset($to_date) ? $to_date : date('d/m/Y'); ?>">
                    <input type="submit" class="n-btn" value="Go" name="activity_report">
                    
                    </li>
                    <li class="mr-0 float-right">
                    
                    <div class="topiconnew border-0 green-btn"> <a onclick="exportF(this)" target="_blank" title="Download to Excel"> <i class="fa fa-file-excel-o"></i></a> </div>
                    
                   <div class="topiconnew border-0 green-btn"> <a href="" id="ActivityPrint" target="_blank"><i class="fa fa-print"></i></a> </div>
                    
                    </li>
                    </ul>
                </form>   
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px;">Sl No</th>
                            <th style="line-height: 18px">Zone</th>
                            <th style="line-height: 18px">Total Booking Hours(s)</th>
                            <th style="line-height: 18px" colspan="2">One Day Booking(s)</th>
                            <th style="line-height: 18px" colspan="2">Long Term Booking(s)</th>
                            <th style="line-height: 18px">Total Payment</th>
                            <th style="line-height: 18px">Total Invoice</th>
                        </tr>
                        <tr>
                            <th style="line-height: 18px;"></th>
                            <th style="line-height: 18px"></th>
                            <th style="line-height: 18px"></th>
                            <th style="line-height: 18px">Booking(s)</th>
                            <th style="line-height: 18px">Booking Hrs</th>
                            <th style="line-height: 18px">Booking(s)</th>
                            <th style="line-height: 18px">Booking Hrs</th>
                            <th style="line-height: 18px"></th>
                            <th style="line-height: 18px"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($reports)) {
                            $i = 0;
                            foreach ($reports as $key => $report) {
                                $i++;
                                ?>
                                <tr>
                                    <td style="line-height: 18px;text-align: center;">
                                        <?php echo $i; ?>
                                    </td>
                                    <td style="line-height: 18px;text-align: center;">
                                        <?php echo $report['zone']; ?>
                                    </td>
                                    <td style="line-height: 18px;text-align: center;">
                                        <?php echo $report['hours']; ?>
                                    </td>
                                    <td style="line-height: 18px;text-align: center;">
                                        <?php echo $report['OD']; ?>
                                    </td>
                                    <td style="line-height: 18px;text-align: center;">
                                        <?php echo $report['OD_hrs']; ?>
                                    </td>
                                    <td style="line-height: 18px;text-align: center;">
                                        <?php echo $report['WE']; ?>
                                    </td>
                                    <td style="line-height: 18px;text-align: center;">
                                        <?php echo $report['WE_hrs']; ?>
                                    </td>
                                    <td style="line-height: 18px;text-align: center;">
                                        <?php echo $report['payment']; ?>
                                    </td>
                                    <td style="line-height: 18px;text-align: center;">

                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>
<div style="display: none;" id="ActivityReportPrint"> 
    <table cellpadding="0" border="1" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th style="line-height: 18px;">Sl No</th>
                <th style="line-height: 18px">Zone</th>
                <th style="line-height: 18px">Total Booking Hours(s)</th>
                <th style="line-height: 18px" colspan="2">One Day Booking(s)</th>
                <th style="line-height: 18px" colspan="2">Long Term Booking(s)</th>
                <th style="line-height: 18px">Total Payment</th>
                <th style="line-height: 18px">Total Invoice</th>
            </tr>
            <tr>
                <th style="line-height: 18px;"></th>
                <th style="line-height: 18px"></th>
                <th style="line-height: 18px"></th>
                <th style="line-height: 18px">Booking(s)</th>
                <th style="line-height: 18px">Booking Hrs</th>
                <th style="line-height: 18px">Booking(s)</th>
                <th style="line-height: 18px">Booking Hrs</th>
                <th style="line-height: 18px"></th>
                <th style="line-height: 18px"></th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (!empty($reports)) {
                $i = 0;
                foreach ($reports as $key => $report) {
                    $i++;
                    ?>
                    <tr>
                        <td style="line-height: 18px;text-align: center;">
                            <?php echo $i; ?>
                        </td>
                        <td style="line-height: 18px;text-align: center;">
                            <?php echo $report['zone']; ?>
                        </td>
                        <td style="line-height: 18px;text-align: center;">
                            <?php echo $report['hours']; ?>
                        </td>
                        <td style="line-height: 18px;text-align: center;">
                            <?php echo $report['OD']; ?>
                        </td>
                        <td style="line-height: 18px;text-align: center;">
                            <?php echo $report['OD_hrs']; ?>
                        </td>
                        <td style="line-height: 18px;text-align: center;">
                            <?php echo $report['WE']; ?>
                        </td>
                        <td style="line-height: 18px;text-align: center;">
                            <?php echo $report['WE_hrs']; ?>
                        </td>
                        <td style="line-height: 18px;text-align: center;">
                            <?php echo $report['payment']; ?>
                        </td>
                        <td style="line-height: 18px;text-align: center;">

                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>
</div>



<script type="text/javascript">
  function exportF(elem) 
  {
      var table = document.getElementById("ActivityReportPrint");
      var html = table.outerHTML;
      var url = 'data:application/vnd.ms-excel,' + escape(html); // Set your html table into url 
      elem.setAttribute("href", url);
      elem.setAttribute("download", "ZoneWiseBookingReport.xls"); // Choose the file name
      return false;
  }  
</script>

