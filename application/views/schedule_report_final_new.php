<style type="text/css">
	.widget .widget-header {
		margin-bottom: 0px;
	}

	.container.booking-fl-box {
		width: 100% !important;
	}
</style>
<div class="row m-0">
	<div class="col-md-12">

		<div class="widget widget-table action-table">
			<div class="widget-header">
				<form class="form-horizontal" method="post" action="<?php echo base_url() . 'reports/schedule-report-new' ?>">
					<ul>
						<li>
							<i class="icon-th-list"></i>
							<h3>Schedule Report</h3>
						</li>

						<li>
							<input type="text" readonly="readonly" style="width: 160px;" id="OneDayDate" name="search_date" value="<?php echo $payment_date ?>">
						</li>

						<!--<li>
							<input type="text" readonly="readonly" style="width: 160px;" id="search_date_to" name="search_date_to" value="<?php// echo $payment_date_to ?>">
						</li>-->

						<li class="mr-2">
							<span style="margin-left:23px;">Driver :</span>
							<select style="width:160px;" id="zones" name="zones" class="sel2">
								<option value="">-- Select Driver --</option>
								<?php
								if (count($zones) > 0) {
									foreach ($zones as $zones_val) {
								?>
										<option value="<?php echo $zones_val->tablet_id; ?>" <?php if ($zone_id == $zones_val->tablet_id) {
																									echo 'selected="selected"';
																								} else {
																									echo '';
																								} ?>><?php echo $zones_val->tablet_driver_name; ?></option>
								<?php
									}
								}
								?>
							</select>
						</li>
						<li>



							<input type="submit" class="n-btn" value="Go" name="add_payment">
							<input type="hidden" id="servicefromdate" value="<?php echo $servicedate; ?>" />
							<input type="hidden" id="servicetodate" value="<?php echo $servicedateto; ?>" />
							<input type="hidden" id="servicezoneid" value="<?php echo $zone_id; ?>" />
						</li>

						<li class="mr-0 float-right">
							<!--<div class="topiconnew border-0 green-btn">
                    	<a href="<? php // echo base_url(); 
									?>reports/schedule_report_excel_new/<? php // echo $servicedate; 
																								?>/<? php // echo $servicedateto; 
																															?>/<? php // echo $zone_id; 
																																							?>" title="Download to Excel"> <i class="fa fa-download"></i></a>
                    </div>-->
							<div class="topiconnew border-0 green-btn">
								<a href="javascript:void(0);" id="scheduleReportExcelBtn" title="Download to Excel"> <i class="fa fa-file-excel-o"></i></a>
							</div>

						</li>


				</form>
			</div>

			<div class="widget-content">
				<table id="schedule-report" class="table da-table" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th style="line-height: 18px; width: 3%; text-align: center;">Sl No</th>
							<th style="line-height: 18px; width: 10%; text-align: center;">Date of Service</th>
							<th style="line-height: 18px; width: 12%; text-align: center;">Crews Name</th>
							<th style="line-height: 18px; width: 15%; text-align: center;">Address</th>
							<th style="line-height: 18px; width: 10%; text-align: center;">Booking Time</th>
							<th style="line-height: 18px; width: 5%; text-align: center;">Hours</th>
							<th style="line-height: 18px; width: 11%; text-align: center;">Customer Name</th>
							<th style="line-height: 18px; width: 11%; text-align: center;">Mobile</th>
							<th style="line-height: 18px; width: 11%; text-align: center;">Whatsapp</th>
							<th style="line-height: 18px; width: 11%; text-align: center;">Whatsapp Notification</th>
							<th style="line-height: 18px; width: 11%; text-align: center;">Cleaning Supplies</th>
							<th style="line-height: 18px; width: 5%; text-align: center;">Discount</th>
							<th style="line-height: 18px; width: 6%; text-align: center;">Service Amount</th>
							<th style="line-height: 18px; width: 5%; text-align: center;">Previous Outstanding</th>
							<th style="line-height: 18px; width: 13%; text-align: center;">Notes</th>
							<th style="line-height: 18px; width: 5%; text-align: center;">Payment Mode</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if (!empty($reports)) {
							$i = 0;
							$zone_id = 0;
							$totdiscount = 0;
							$totbalance = 0;
							$totamount = 0;
							$tothrs = 0;
							// foreach ($reports as $rep) {
								foreach ($reports as $report) {
									$totamount += $report->total_amount;
									$totdiscount += $report->discount;
									$totbalance += $report->balance;
									if ($zone_id != $report->tabletid) {
										$zone_id = $report->tabletid;
						?>
						<tr>
							<td style="line-height: 18px;"></td>
							<td style="line-height: 18px;"><b style="color:#CB3636;"><?php echo $report->tablet_driver_name; ?></b></td>
							<td colspan="11" style="line-height: 18px;"></td>
						</tr>
						<?php
									}
						?>
									<tr>
										<td style="line-height: 18px; text-align: center;"><?php echo ++$i; ?></td>
										<td style="line-height: 18px;">
											<?php
											$date = DateTime::createFromFormat('d/m/Y', $report->sch_date);
											if ($date !== false) {
												echo $date->format('d-m-y');
											} else {
												echo $report->sch_date;
											}
											?>
										</td>

										<td style="line-height: 18px;"><?php echo $report->allmaidss; ?></td>
										<td style="line-height: 18px;">
											<?php
											if ($report->building != "") {
												echo $report->building . ', ';
											}
											if ($report->customer_address != "") {
												echo $report->customer_address . ', ';
											}
											if ($report->area_name != "") {
												echo $report->area_name;
											}
											?>
										</td>
										<td style="line-height: 18px;"><?php echo $report->booking_time_from . " - " . $report->booking_time_to; ?></td>
										<td style="line-height: 18px;">
											<?php
											$hours = (strtotime($report->time_to) - strtotime($report->time_from)) / 3600;
											$tothrs += $hours;
											$hrs = strtotime($report->time_to) - strtotime($report->time_from);
											?>
											<?php echo gmdate("H:i", $hrs); ?>
										</td>
										<td style="line-height: 18px;"><?php echo $report->customer_name; ?></td>
										<td style="line-height: 18px;"><?php echo $report->mobile_number_1; ?></td>
										<td style="line-height: 18px;"><?php echo $report->whatsapp_no_1; ?></td>
										<td style="line-height: 18px;">
											<?php echo ($report->whatsapp_notification == 'Y') ? 'Yes' : 'No'; ?>
										</td>
										<td style="line-height: 18px;">
											<?php echo ($report->cleaning_material == 'Y') ? 'Yes' : 'No'; ?>
										</td>
										<td style="line-height: 18px;"><?php echo $report->discount; ?></td>
										<td style="line-height: 18px;"><?php echo $report->total_amount; ?></td>
										<td style="line-height: 18px;"><?php echo $report->balance; ?></td>
										<td style="line-height: 18px;"><?php echo $report->booking_note; ?></td>
										<td style="line-height: 18px;"><?php echo $report->pay_by; ?></td>
									</tr>
						<?php
								}
						}

						?>
					</tbody>
					<tr style="font-weight:bold;">
						<td style="line-height: 18px;" colspan=""></td>
						<td style="line-height: 18px;" colspan=""></td>
						<td style="line-height: 18px;" colspan=""></td>
						<td style="line-height: 18px;" colspan=""></td>
						<td style="line-height: 18px;" >Total :</td>
						<td style="line-height: 18px;"><?=$tothrs;?></td>
						<td style="line-height: 18px;"></td>
						<td style="line-height: 18px;"></td>
						<td style="line-height: 18px;"></td>
						<td style="line-height: 18px;"></td>
						<td style="line-height: 18px;"></td>
						<td style="line-height: 18px;"> <?=$totdiscount; ?></td>
						<td style="line-height: 18px;"><?=$totamount; ?></td>
						<td style="line-height: 18px;"> <?= $totbalance; ?></td>
						<td style="line-height: 18px;"></td>
						<td style="line-height: 18px;"></td>
					</tr>
				</table>
			</div><!-- /widget-content -->

		</div><!-- /widget -->
	</div><!-- /span12 -->
</div>
<script>

</script>