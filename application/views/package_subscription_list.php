<style type="text/css">
  .widget .widget-header {
    margin-bottom: 0px;
  }

  table.da-table tr td {
    padding: 0px 6px;
  }
</style>
<div id="disable-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Disable ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to disable this subscription ?</h3>
      <input type="hidden" id="disable_id">
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn red-btn mb-0" onclick="confirm_disable()">Disable</button>
    </div>
  </div>
</div>
<div id="enable-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Enable ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to enable this subscription ?</h3>
      <input type="hidden" id="enable_id">
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn mb-0" onclick="confirm_enable()">Enable</button>
    </div>
  </div>
</div>
<div class="row m-0">
  <div class="col-md-12">
    <div class="widget widget-table action-table">
      <div class="widget-header"> <i class="icon-th-list"></i>
        <h3>Packages Subscriptions</h3>
        <!--<a style="float:right ; margin-right:15px; cursor:pointer;" title="New Subscription" href="<?php echo base_url('package_subscription/new'); ?>"><img src="<?php echo base_url('img/add.png'); ?>"/></a>-->
        <select style="width:150px;" id="all-maids" onchange="statusFilter(this.value)">
          <option value="" <?php echo $active == null ? 'selected' : ''; ?>>All</option>
          <option value="1" <?php echo $status === '1' ? 'selected' : ''; ?>>Active</option>
          <option value="0" <?php echo $status === '0' ? 'selected' : ''; ?>>Inactive</option>
        </select>
      </div>
      <!-- /widget-header -->

      <div class="widget-content">
        <table id="subscription-list-table" class="table table-hover da-table" width="100%">
          <thead>
            <tr>
              <th>
                No.
              </th>
              <th>
                Customer
              </th>
              <th>
                Package
              </th>
              <th>
                Ref. No.
              </th>
              <th>
                Service Date
              </th>
              <th>
                Mobile
              </th>
              <th>
                Amount
              </th>
              <th>
                Start Date
              </th>
              <th>
                End Date
              </th>
              <th>
                Payment Mode
              </th>
              <th>
                Payment Status
              </th>
              <th>
                Status
              </th>
              <th class="td-actions">
                Action
              </th>
            </tr>
          </thead>
          <tbody>
            <?php
            if (count($packages) > 0) {
              foreach ($packages as $key => $package) {
            ?>
                <tr>
                  <td style="line-height: 18px; width: 20px">
                    <?php echo $key + 1; ?>
                  </td>
                  <td><?php echo $package['customer_name'] ?></td>
                  <td><?php echo $package['package_name'] ?></td>
                  <td><?php echo $package['reference_id'] ?></td>
                  <td><?php echo date("d-m-Y", strtotime($package['service_date'])); ?></td>
                  <td><?php echo $package['mobile_number_1'] ?></td>
                  <td><?php echo $package['amount'] ?></td>
                  <td><?php echo $package['subscription_start_date'] ?></td>
                  <td><?php echo $package['subscription_end_date'] ?></td>
                  <td class="text-center"><?php echo ucfirst($package['payment_mode']) ?></td>
                  <?php
                    if($package['payment_status'] == 'Initiated' || $package['payment_status'] == 'initiated' || $package['payment_status'] == 'new'){
                      $css_class = 'secondary';
                    }
                    elseif($package['payment_status'] == 'Processing' || $package['payment_status'] == 'processing'){
                      $css_class = 'info';
                    }
                    elseif($package['payment_status'] == 'Pending' || $package['payment_status'] == 'pending'){
                      $css_class = 'warning';
                    }
                    elseif($package['payment_status'] == 'Authorized' || $package['payment_status'] == 'authorized' || $package['payment_status'] == 'approved' || $package['payment_status'] == 'success'){
                      $css_class = 'success';
                    }
                    else{
                      $css_class = 'error';
                    }
                    ?>
                  <td class="text-center">
                    <span class="n-btn badge badge-<?php echo $css_class; ?>"><?php echo ucfirst($package['payment_status']); ?></span>
                  </td>
                  <td class="text-center">
                    <?php
                    if ($package['subscription_status'] == 'Active') {
                      $css_class = 'success';
                    } else if ($package['subscription_status'] == 'Inactive') {
                      $css_class = 'error';
                    } else {
                      $css_class = 'warning';
                    }
                    ?>
                    <span class="n-btn badge badge-<?php echo $css_class; ?>"><?php echo $package['subscription_status']; ?></span>
                  </td>
                  <td style=" padding: 0px; width: 90px" class="td-actions text-center">
                    
                    <a class="n-btn-icon blue-btn" href="<?php echo base_url(); ?>package_subscription/view/<?php echo $package['id'] ?>" title="View Maid"><i class="btn-icon-only fa fa-eye"> </i></a>
                    
                    <?php if ($package['maid_status'] != 2) { ?>
                      <a class="n-btn-icon purple-btn" href="<?php echo base_url(); ?>package_subscription/edit/<?php echo $package['id'] ?>" title="Edit Package"><i class="btn-icon-only icon-pencil"> </i></a>
                    <?php } ?>
                    <?php if ($package['maid_status'] == 2) { ?>
                      <p class="n-btn-icon red-btn" style="cursor:default;" title="Deleted"><i class="fa fa-ban"></i></p>
                    <?php } ?>
                    <?php
                    //if(user_authenticate() == 1)
                    if ($this->session->userdata('user_logged_in')['user_admin'] == 'Y') {
                      if ($package['subscription_status'] == 'Active') {
                    ?>
                        <a href="javascript:void(0)" class="n-btn-icon green-btn" title="Disbale" onclick="confirm_disable_enable_modal(<?php echo $package['id'] ?>,'<?= $package['subscription_status'] ?>');"><i class="btn-icon-only fa fa-toggle-on "> </i></a>
                      <?php
                      } else if ($package['subscription_status'] == 'Inactive') {
                      ?>
                        <a href="javascript:void(0)" class="n-btn-icon red-btn" title="Enable" onclick="confirm_disable_enable_modal(<?php echo $package['id'] ?>,'<?= $package['subscription_status'] ?>');"><i class="btn-icon-only fa fa-toggle-off"> </i></a>
                      <?php
                      }
                      //                                         if ($package['maid_status']!=2) {
                      ?>

                      <!--                                        <a href="javascript:void(0)" class="btn btn-danger btn-small" onclick="change_status(<?php echo $package['maid_id'] ?>,'2');" title="Delete"><i class="btn-icon-only icon-trash"></i> </a>    -->

                    <?php
                      //                                         }
                    }
                    ?>
                  </td>
                </tr>
              <?php
              }
            } else {
              ?>
              <tr>
                <td class="bg-info" colspan="13">
                  <p class="text-center">No Data Found !</p>
                </td>
              </tr>
            <?php
            }
            ?>
          </tbody>
        </table>
      </div>
      <!-- /widget-content -->
    </div>
    <!-- /widget -->
  </div>
  <!-- /span6 -->
</div>
<!-- Modal -->
<script>
  function confirm_disable_enable_modal(id, status) {
	if (status == 'Active') {
		$('#disable_id').val(id);
		$.fancybox.open({
			autoCenter: true,
			fitToView: false,
			scrolling: false,
			openEffect: 'none',
			openSpeed: 1,
			autoSize: false,
			width: 450,
			height: 'auto',
			helpers: {
				overlay: {
					css: {
						'background': 'rgba(0, 0, 0, 0.3)'
					},
					closeClick: false
				}
			},
			padding: 0,
			closeBtn: false,
			content: $('#disable-popup'),
		});
	} else {
		$('#enable_id').val(id);
		$.fancybox.open({
			autoCenter: true,
			fitToView: false,
			scrolling: false,
			openEffect: 'none',
			openSpeed: 1,
			autoSize: false,
			width: 450,
			height: 'auto',
			helpers: {
				overlay: {
					css: {
						'background': 'rgba(0, 0, 0, 0.3)'
					},
					closeClick: false
				}
			},
			padding: 0,
			closeBtn: false,
			content: $('#enable-popup'),
		});
	}
}

function confirm_disable() {
	const params = new Proxy(new URLSearchParams(window.location.search), {
        get: (searchParams, prop) => searchParams.get(prop),
      });
      status = params.status ? params.status : ''; // "some_value"
      $('body').append($('<form/>')
        .attr({
          'action': _base_url +"package_subscription/disable?status=" + status,
          'method': 'post',
          'id': 'form_jgf'
        })
        .append($('<input/>')
          .attr({
            'type': 'hidden',
            'name': 'id',
            'value': $('#disable_id').val()
          })
        )
      ).find('#form_jgf').submit();
}

function confirm_enable() {
	const params = new Proxy(new URLSearchParams(window.location.search), {
        get: (searchParams, prop) => searchParams.get(prop),
      });
      status = params.status ? params.status : ''; // "some_value"
      $('body').append($('<form/>')
        .attr({
          'action': _base_url +"package_subscription/enable?status=" + status,
          'method': 'post',
          'id': 'form_tys'
        })
        .append($('<input/>')
          .attr({
            'type': 'hidden',
            'name': 'id',
            'value': $('#enable_id').val()
          })
        )
      ).find('#form_tys').submit();
}
function closeFancy() {
	$.fancybox.close();
}
  function statusFilter(status) {
    if (status)
      window.open("package_subscription?status=" + status, "_self")
    else
      window.open("package_subscription", "_self")
  }
  (function (a) {
    a(document).ready(function (b) {
      if (a('#subscription-list-table').length > 0) {
        a("table#subscription-list-table").dataTable({
          'sPaginationType': "full_numbers", "bSort": true, "iDisplayLength": 100, "scrollY": true,"orderMulti": false,
          "scrollX": true,
          'columnDefs': [
            {
              'targets': [4,-1],
              'orderable': false
            },
          ],
          initComplete: function (settings) {

        },
        });
      }
    });
  })(jQuery);
</script>