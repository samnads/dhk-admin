<style type="text/css">
    .select2-arrow{visibility : hidden;}
	.select2-container .select2-choice{
	-moz-appearance: none;
    background: #fff url("../images/ash-arrow.png") no-repeat scroll right 7px top 11px;
   border: 1px solid #ccc;
    border-radius: 3px;
    cursor: pointer;
    font-size: 12px;
    height: 30px;
    line-height: 24px;
   padding: 3px 0 3px 10px;

    text-indent: 0.01px;
	}
        
    #user-repeat-days label{
		display:inline;
	}
	.in-bookingform-field-droop-main label{
		display:inline;
	}
	#user-repeat-end-date {
		width: 73%;
	}
   
</style>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC_hBALevP_xk8OIkxyGQjt_oInuAW3Ivk&amp;libraries=places"></script>
<section>
    <div class="row dash-top-wrapper no-left-right-margin">

            <div class="col-md-12 col-sm-12 no-left-right-padding">
                <!--<div class="widget widget-table action-table" style="margin-bottom:30px">-->
                <div class="widget-header" style="margin-bottom: 0;"> 
                        <form class="form-horizontal" method="POST" action="#">
                            <i class="icon-th-list"></i>
                            <h3>New Booking</h3> 
                            
                        </form> 
                    </div>
                
                    <div class="col-md-12 col-sm-12 confirm-det-cont-box no-left-right-padding borderbox">
                        <?php echo form_open('', array('id' => 'add-user-booking-form')); ?>
                        
                        <div class="col-md-6 col-sm-6 confi-det-cont-det new-booking-box-main no-left-padding">
                            <div class="col-md-12 col-sm-12 text-field-main no-top-padding">
                                <div id="u-error" style="color:red;"></div>
                            </div>
                            <div class="col-md-12 col-sm-12 text-field-main no-top-padding">
                                <div class="col-md-4 col-sm-4 text-field-name no-left-right-padding">
                                    <?php echo form_label('Customer <span>:</span>', 'u_customer_id',  array( 'class' => '')); ?>
                                </div>
                                <div class="col-md-8 col-sm-8 text-field-det no-left-right-padding">
                                    
                                    <div class="in-bookingform-field-droop-main">
                                        <select name="u_customer_id" id="u_customer_id" data-placeholder="Select customer" class="">
                                            <option></option>
                                            <?php
                                            // foreach($customers as $customer)
                                            // {  
                                                // if($customer->payment_type == "D")
                                                // {
                                                    // $paytype = "(Daily)";
                                                // } else if($customer->payment_type == "W")
                                                // {
                                                    // $paytype = "(Weekly)";
                                                // } else if($customer->payment_type == "M")
                                                // {
                                                    // $paytype = "(Monthly)";
                                                // } else
                                                // {
                                                    // $paytype = "";
                                                // } 
                                                // $p_number = array();
                                                // if($customer->phone_number != NULL)
                                                    // array_push ($p_number, $customer->phone_number);
                                                // if($customer->mobile_number_1 != NULL)
                                                    // array_push ($p_number, $customer->mobile_number_1);
                                                // if($customer->mobile_number_2 != NULL)
                                                    // array_push ($p_number, $customer->mobile_number_2);
                                                // if(@$customer->mobile_number_3 != NULL)
                                                    // array_push ($p_number, @$customer->mobile_number_3);

                                                // $phone_number = !empty($p_number) ? ' - ' . join(', ', $p_number) : '';

                                                // echo '<option value="' . $customer->customer_id . '">' . html_escape($customer->customer_nick_name). $phone_number ." ".$paytype.'</option>';
                                            // }

                                            ?>
                                        </select>
                                        <div id="customer-picked-address-u"></div>
                                    </div>
                                </div>
                            </div>
                            <div id="customer-address-panel-user">
                                <div class="head">Pick One Address <span class="close">Close</span></div>

                                <div class="inner">
                                        Loading<span class="dots_loader"></span>
                                </div>			
                            </div>
                            <div class="col-md-12 col-sm-12 text-field-main no-top-padding">
                                <div class="col-md-4 col-sm-4 text-field-name no-left-right-padding">
                                    <?php echo form_label('Service Type <span>:</span>', 'user_service_type_id',  array( 'class' => '')); ?>
                                </div>
                                <div class="col-md-8 col-sm-8 text-field-det no-left-right-padding">
                                    <div class="in-bookingform-field-droop-main">
                                        <select name="user_service_type_id" id="user_service_type_id" data-placeholder="Select service type" class="sel3">
                                            <option></option>
                                            <?php
                                            foreach($service_types as $service_type)
                                            {
                                                echo '<option value="' . $service_type->service_type_id . '">' . html_escape($service_type->service_type_name) . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-12 col-sm-12 text-field-main no-top-padding">
                                <div class="col-md-4 col-sm-4 text-field-name no-left-right-padding">
                                    <?php echo form_label('Cleaning Type <span>:</span>', 'user_cleaning_materials',  array( 'class' => '')); ?>
                                </div>
                                <div class="col-md-8 col-sm-8 text-field-det no-left-right-padding">
                               
                                        <div class="col-md-6 col-sm-6 col-xs-6 log-remb no-left-right-padding">
                                            <input name="user_cleaning_materials" value="Y" id="user_cleaning_materials" class="" type="checkbox">
                                            <label for="user_cleaning_materials"> <span></span> &nbsp; Yes</label>
                                        </div>
                         
                                </div>
                            </div>
                            
                            <div class="col-md-12 col-sm-12 text-field-main no-top-padding">
                                <div class="col-md-4 col-sm-4 text-field-name no-left-right-padding">
                                    <?php echo form_label('Service Start Date <span>:</span>', 'user_date',  array( 'class' => '')); ?>
                                </div>
                                <div class="col-md-8 col-sm-8 text-field-det no-left-right-padding">
                                    <div class="in-bookingform-field-droop-main">
                                         <?php echo form_input(array('name' => 'user_date', 'id' => 'user_date', 'maxlength' => 10, 'tabindex' => 7, 'class' => 'in-bookingform-field', 'value' => '', 'autocomplete' => 'off')); ?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-12 col-sm-12 text-field-main no-top-padding">
                                <div class="col-md-4 col-sm-4 text-field-name no-left-right-padding">
                                    <?php echo form_label('Time <span>:</span>', 'user_time',  array( 'class' => '')); ?>
                                </div>
                                <div class="col-md-8 col-sm-8 text-field-det no-left-right-padding">
                                    <div class="col-md-12 col-sm-12 no-left-right-padding in-bookingform-field-droop-main">
                                        <div class="col-md-6 col-sm-6 no-left-padding">
                                         From:   <select name="time_from" id="b-from-time" data-placeholder="Select" class="sel3 small mr15 pull-right" style="width:75%;">
                                                        <option></option>
                                                        <?php
                                                        foreach($times as $time_index=>$time)
                                                        {
                                                            echo '<option value="' . $time->stamp . '">' . $time->display . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                        </div>
                                        <div class="col-md-6 col-sm-6 no-right-padding">
                                            To:     <select name="time_to" id="b-to-time" data-placeholder="Select" class="sel3 small pull-right"  style="width:75%;">
                                                        <option></option>
                                                        <?php
                                                        foreach($times as $time_index=>$time)
                                                        {
                                                            if($time_index == 't-0')
                                                            {                                                                                
                                                                    continue;
                                                            }
                                                            echo '<option value="' . $time->stamp . '">' . $time->display . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-12 col-sm-12 text-field-main no-top-padding">
                                <div class="col-md-4 col-sm-4 text-field-name no-left-right-padding">
                                    <?php echo form_label('Repeats <span>:</span>', 'user_booking_type',  array( 'class' => '')); ?>
                                </div>
                                <div class="col-md-8 col-sm-8 text-field-det no-left-right-padding">
                                    <div class="in-bookingform-field-droop-main">
                                        <select name="user_booking_type" id="user_booking_type" data-placeholder="Select repeat type" class="sel3">
                                            <option></option>
                                            <option value="OD">Never Repeat - One Day Only</option>
                                            <option value="WE">Every Week</option>
                                            <option value="BW">Every 2 Week</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-12 col-sm-12 text-field-main no-top-padding" id="user-repeat-days">
                                <div class="col-md-4 col-sm-4 text-field-name no-left-right-padding">
                                    <?php echo form_label('Repeat On <span>:</span>', 'user_w_day',  array( 'class' => '')); ?>
                                </div>
                                <div class="col-md-8 col-sm-8 text-field-det no-left-right-padding">
                                    <div class="in-bookingform-field-droop-main">
                                        <label><input type="checkbox" class="w_day" name="user_w_day[]" id="user-repeat-on-0" value="0" /> Sun</label>
                                        <label><input type="checkbox"  class="w_day" name="user_w_day[]" id="user-repeat-on-1" value="1" /> Mon</label>
                                        <label><input type="checkbox" class="w_day"  name="user_w_day[]" id="user-repeat-on-2" value="2" /> Tue</label>
                                        <label><input type="checkbox"  class="w_day" name="user_w_day[]" id="user-repeat-on-3" value="3" /> Wed</label>
                                        <label><input type="checkbox"  class="w_day" name="user_w_day[]" id="user-repeat-on-4" value="4" /> Thu</label>
                                        <label><input type="checkbox" class="w_day"  name="user_w_day[]" id="user-repeat-on-5" value="5" /> Fri</label>
                                        <label><input type="checkbox" class="w_day"  name="user_w_day[]" id="user-repeat-on-6" value="6" /> Sat</label>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-12 col-sm-12 text-field-main no-top-padding" id="user-repeat-ends">
                                <div class="col-md-4 col-sm-4 text-field-name no-left-right-padding">
                                    <?php echo form_label('Ends <span>:</span>', 'user_repeat_end',  array( 'class' => '')); ?>
                                </div>
                                <div class="col-md-8 col-sm-8 text-field-det no-left-right-padding">
                                    <div class="in-bookingform-field-droop-main">
                                        <label class="mr15"><input type="radio" name="user_repeat_end" id="user-repeat-end-never" value="never" checked="checked" /> Never</label>
                                        <label><input type="radio" name="user_repeat_end" id="user-repeat-end-ondate" value="ondate" /> On</label> 
                                        <input type="text" class="user_end_datepicker" id="user-repeat-end-date" data-date-format="dd/mm/yyyy" readonly="readonly" disabled="disabled" />
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-12 col-sm-12 text-field-main no-top-padding">
                                <div class="col-md-4 col-sm-4 text-field-name no-left-right-padding">
                                    <?php echo form_label('Total Amount <span>:</span>', 'user_total_amt',  array( 'class' => '')); ?>
                                </div>
                                <div class="col-md-8 col-sm-8 text-field-det no-left-right-padding">
                                    <div class="in-bookingform-field-droop-main">
                                        <input name="user_total_amt" id="user_total_amt" value="" type="number" class="popup-disc-fld" autocomplete="off"/>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-12 col-sm-12 text-field-main no-top-padding">
                                <div class="col-md-4 col-sm-4 text-field-name no-left-right-padding">
                                    <?php echo form_label('Notes <span>:</span>', 'user_notes',  array( 'class' => '')); ?>
                                </div>
                                <div class="col-md-8 col-sm-8 text-field-det no-left-right-padding">
                                    <div class="in-bookingform-field-droop-main">
                                        <textarea name="user_notes" id="user_notes" class="" placeholder="Note to Driver"></textarea>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-12 col-sm-12 text-field-main no-top-padding">
                                <div class="col-md-4 col-sm-4 text-field-name no-left-right-padding">
                                    <input type="hidden" id="customer-address-id-user" name="customer-address-id-user" />
                                    <input type="hidden" id="booking-id-user" />
                                </div>
                                <div class="col-md-8 col-sm-8 text-field-det no-left-right-padding">
                                    <div class="in-bookingform-field-droop-main">
                                        <input type="button" class="save-but" id="add-user-booking-nform" value="Search For Maids" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                        <div class="col-md-6 col-sm-6 confi-det-cont-det no-right-padding">
                            <div id="customer-add-popup" class="col-md-12 col-sm-12" style="display:none;">
                                <div class="booking_form">
                                    <div class="col-md-12 col-sm-12 text-field-main no-top-padding">
                                        
                                    </div>
                                    <form name="customer-popup" id="customer-popup" method="POST">
                                        <div class="col-md-12 col-sm-12 text-field-main no-top-padding">
                                            <div class="col-md-4 col-sm-4 text-field-name no-left-right-padding">
                                                <?php echo form_label('Customer <span>:</span>', 'customer_name',  array( 'class' => '')); ?>
                                            </div>
                                            <div class="col-md-8 col-sm-8 text-field-det no-left-right-padding">
                                                <div class="in-bookingform-field-droop-main">
                                                    <input type="text" name="customer_name" id="customer_name" />
                                                </div>
                                            </div>
                                        </div> 
                                        
                                        <div class="col-md-12 col-sm-12 text-field-main no-top-padding">
                                            <div class="col-md-4 col-sm-4 text-field-name no-left-right-padding">
                                                <?php echo form_label('Email <span>:</span>', 'customer_email',  array( 'class' => '')); ?>
                                            </div>
                                            <div class="col-md-8 col-sm-8 text-field-det no-left-right-padding">
                                                <div class="in-bookingform-field-droop-main">
                                                    <input type="text" name="customer_email" id="customer_email" />
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-12 col-sm-12 text-field-main no-top-padding">
                                            <div class="col-md-4 col-sm-4 text-field-name no-left-right-padding">
                                                <?php echo form_label('Area <span>:</span>', 'area',  array( 'class' => '')); ?>
                                            </div>
                                            <div class="col-md-8 col-sm-8 text-field-det no-left-right-padding">
                                                <div class="in-bookingform-field-droop-main">
                                                    <select name="area" id="area" class="sel3">
                                                        <option value="" selected>Select Area</option>
                                                         <?php
                                                        if (count($areas) > 0) {
                                                            foreach ($areas as $areasVal) {
                                                                ?>
                                                                <option value="<?php echo $areasVal['area_id'] ?>"><?php echo $areasVal['area_name'] ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-12 col-sm-12 text-field-main no-top-padding">
                                            <div class="col-md-4 col-sm-4 text-field-name no-left-right-padding">
                                                <?php echo form_label('Apartment No <span>:</span>', 'apartmentnos',  array( 'class' => '')); ?>
                                            </div>
                                            <div class="col-md-8 col-sm-8 text-field-det no-left-right-padding">
                                                <div class="in-bookingform-field-droop-main">
                                                    <input type="text" name="apartmentnos" id="apartmentnos" />
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-12 col-sm-12 text-field-main no-top-padding">
                                            <div class="col-md-4 col-sm-4 text-field-name no-left-right-padding">
                                                <?php echo form_label('Address <span>:</span>', 'address',  array( 'class' => '')); ?>
                                            </div>
                                            <div class="col-md-8 col-sm-8 text-field-det no-left-right-padding">
                                                <div class="in-bookingform-field-droop-main">
                                                    <input type="text" name="address" id="address" onkeyup="loadLocationField('address');" />
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-12 col-sm-12 text-field-main no-top-padding">
                                            <div class="col-md-4 col-sm-4 text-field-name no-left-right-padding">
                                                <?php echo form_label('Mobile Number <span>:</span>', 'mobile_number1',  array( 'class' => '')); ?>
                                            </div>
                                            <div class="col-md-8 col-sm-8 text-field-det no-left-right-padding">
                                                <div class="in-bookingform-field-droop-main">
                                                    <input type="text" name="mobile_number1" id="mobile_number1" />
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-12 col-sm-12 text-field-main no-top-padding">
                                            <div class="col-md-4 col-sm-4 text-field-name no-left-right-padding">
                                                
                                            </div>
                                            <div class="col-md-8 col-sm-8 text-field-det no-left-right-padding">
                                                <div class="in-bookingform-field-droop-main">
                                                    <input type="button"  id="popup-add-customer" value="Add" class="save-but"> 
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div id="customer-detail-popup" class="col-md-12 col-sm-12" style="float:right;display:none;">
                                <div class="booking_form">
                                    <div class="col-md-12 col-sm-12 text-field-main no-top-padding">
                                        
                                    </div>
                                    <div class="col-md-12 col-sm-12 text-field-main no-top-padding">
                                        <div class="col-md-4 col-sm-4 text-field-name no-left-right-padding">
                                            <?php echo form_label('Name <span>:</span>', '',  array( 'class' => '')); ?>
                                        </div>
                                        <div class="col-md-8 col-sm-8 text-field-det no-left-right-padding">
                                            <div class="in-bookingform-field-droop-main" id="b-customer-ids-cell">
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-12 col-sm-12 text-field-main no-top-padding">
                                        <div class="col-md-4 col-sm-4 text-field-name no-left-right-padding">
                                            <?php echo form_label('Mobile <span>:</span>', '',  array( 'class' => '')); ?>
                                        </div>
                                        <div class="col-md-8 col-sm-8 text-field-det no-left-right-padding">
                                            <div class="in-bookingform-field-droop-main" id="b-customer-mobile-cell">
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-12 col-sm-12 text-field-main no-top-padding">
                                        <div class="col-md-4 col-sm-4 text-field-name no-left-right-padding">
                                            <?php echo form_label('Email <span>:</span>', '',  array( 'class' => '')); ?>
                                        </div>
                                        <div class="col-md-8 col-sm-8 text-field-det no-left-right-padding">
                                            <div class="in-bookingform-field-droop-main" id="b-customer-email-cell">
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-12 col-sm-12 text-field-main no-top-padding">
                                        <div class="col-md-4 col-sm-4 text-field-name no-left-right-padding">
                                            <?php echo form_label('Area <span>:</span>', '',  array( 'class' => '')); ?>
                                        </div>
                                        <div class="col-md-8 col-sm-8 text-field-det no-left-right-padding">
                                            <div class="in-bookingform-field-droop-main" id="b-customer-area-cell">
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-12 col-sm-12 text-field-main no-top-padding">
                                        <div class="col-md-4 col-sm-4 text-field-name no-left-right-padding">
                                            <?php echo form_label('Zone <span>:</span>', '',  array( 'class' => '')); ?>
                                        </div>
                                        <div class="col-md-8 col-sm-8 text-field-det no-left-right-padding">
                                            <div class="in-bookingform-field-droop-main" id="b-customer-zone-cell">
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-12 col-sm-12 text-field-main no-top-padding">
                                        <div class="col-md-4 col-sm-4 text-field-name no-left-right-padding">
                                            <?php echo form_label('Address <span>:</span>', '',  array( 'class' => '')); ?>
                                        </div>
                                        <div class="col-md-8 col-sm-8 text-field-det no-left-right-padding">
                                            <div class="in-bookingform-field-droop-main" id="b-customer-address-cell">
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-12 col-sm-12 text-field-main no-top-padding">
                                        <div class="col-md-4 col-sm-4 text-field-name no-left-right-padding">
                                            <?php echo form_label('Customer Book Type <span>:</span>', '',  array( 'class' => '')); ?>
                                        </div>
                                        <div class="col-md-8 col-sm-8 text-field-det no-left-right-padding">
                                            <div class="in-bookingform-field-droop-main" id="b-customer-booktype-cell">
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-12 col-sm-12 text-field-main no-top-padding">
                                        <div class="col-md-4 col-sm-4 text-field-name no-left-right-padding">
                                            <?php echo form_label('Payment Mode <span>:</span>', '',  array( 'class' => '')); ?>
                                        </div>
                                        <div class="col-md-8 col-sm-8 text-field-det no-left-right-padding">
                                            <div class="in-bookingform-field-droop-main" id="b-customer-paymode-cell">
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="maid_search" style="border: medium none !important;"></div>
                    </div>
            </div><!--welcome-text-main end-->
     
    </div><!--row content-wrapper end--> 
</section><!--welcome-text end-->