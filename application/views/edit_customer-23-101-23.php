<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAzKGuULLF6eqMALYjRS7FncopnXnJI4ws&amp;libraries=places"></script> -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC_hBALevP_xk8OIkxyGQjt_oInuAW3Ivk&amp;libraries=places"></script>
<!-- this is replaced from PM -->

<style>
    #edit-profile label, input, button, select, textarea{font-size: 12px !important;}
    .widget-content{min-height : 375px;}

</style>  
<script>
 var latlng=[];
</script>  

<div class="row m-0">
    <div class="col-sm-12">      		
        <div class="widget ">
            <div class="widget-header">
                <i class="icon-user"></i>
                <h3>Edit Customer</h3>
                <a style="float:right ; margin-right:20px; cursor:pointer;" href="<?php echo base_url(); ?>customers"><img src="<?php echo base_url();?>img/male-list.png" title="Customer List"/></a>
            </div> <!-- /widget-header -->
                    <div class="widget-content">
                        <div class="tabbable">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#personal" data-toggle="tab">Personal Details</a></li>
                                 <li><a href="#address-details" data-toggle="tab" onclick="resize_map();">Address Details</a></li>
                                <li><a href="#account" data-toggle="tab">Account and Other Details</a></li>
                                
                                <!-- <li><a href="#more" data-toggle="tab">More Details</a></li>-->
                            </ul>
                            <br>
                            <form id="edit-profile" class="form-horizontal" method="post">      
                                <div class="tab-content">
                                    <div class="tab-pane active" id="personal">
                                        <!--   <form id="edit-profile" class="form-horizontal">                                                                                                    -->
                                        
                                        <?php 
                                if($message == "success")
                                {?>
                                    <div class="control-group">
                                        <label class="control-label"></label>
                                            <div class="controls">                                                            
                                                <div class="alert alert-success">
                                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                                    <strong>Success!</strong> Customer Details Updated Successfully.
                                                </div>
                                            </div> <!-- /controls -->	
                                    </div> <!-- /control-group -->
                               <?php
                               }
                               ?>
                                        
                                        
                                        <fieldset>            
                                            <div class="span5" style="margin-left:0px;">        
                                                <div class="widget">
                                                    <div class="widget-content" style="border: 0px">
                                                        <fieldset>
                                                        
                                                        
<!--<div class="row n-field-main m-0">
	<p>Customer</p>
	<div class="n-field-box">
    </div>
</div>-->
          



          
                                                            <!--<div class="control-group">											
                                                                <label class="control-label" for="customer_name">Customer Name &nbsp;<font style="color: #C00">*</font></label>
                                                                <div class="controls">
                                                                    <input type="text" name="customer_name" class="span3" id="customer_name" value="<?php //echo $customer_details[0]['customer_name'] ?>" required="required"  onchang="document.getElementById('customer_nick').value = this.value">
                                                                    <input type="hidden" class="span3" id="customer_id" name="customer_id" value="<?php //echo $customer_details[0]['customer_id'] ?>">
                                                                </div>				
                                                            </div> -->
                                                            
                                                            
                                                            <div class="row n-field-main m-0">
                                                                <p>Customer</p>
                                                                <div class="n-field-box">
                                                                     <input type="text" name="customer_name" class="span3" id="customer_name" value="<?php echo $customer_details[0]['customer_name'] ?>" required="required"  onchang="document.getElementById('customer_nick').value = this.value">
                                                                    <input type="hidden" class="span3" id="customer_id" name="customer_id" value="<?php echo $customer_details[0]['customer_id'] ?>">
                                                                </div>
                                                            </div>

			

                                                            <div class="control-group">											
                                                                <label class="control-label" for="firstname">Customer Nick Name &nbsp;<font style="color: #C00">*</font></label>
                                                                <div class="controls">
                                                                    <input type="text" class="span3" id="customer_nick" name="customer_nick" value="<?php echo $customer_details[0]['customer_nick_name'] ?>" required="required">
                                                                </div> <!-- /controls -->				
                                                            </div> <!-- /control-group -->
                                                            
                                                            
                                                            <div class="row n-field-main m-0">
                                                                <p>Customer</p>
                                                                <div class="n-field-box">
                                                                </div>
                                                            </div>
															
                                                            
															<div class="control-group">											
                                                        <label class="control-label" for="firstname">Is Flagged&nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
															<label class="radio inline"><input type="radio"  name="flag" value="Y" <?php echo $customer_details[0]['is_flag'] == 'Y' ?  'checked="checked"' : ''; ?>> Yes</label>
															<label class="radio inline"><input type="radio" name="flag" value="N" <?php echo $customer_details[0]['is_flag'] == 'N' ?  'checked="checked"' : ''; ?>> No</label>
                                                         </div>			
                                                    </div>
                                                    
                                                    
                                                        <div class="row n-field-main m-0">
                                                            <p>Customer</p>
                                                            <div class="n-field-box">
                                                            </div>
                                                        </div>
                                                    
                                                    
                                                    
													
													<div class="control-group">											
                                                        <label class="control-label" for="reasonflag">Reason For Flagging</label>
                                                        <div class="controls">
                                                            <textarea class="span3" rows="1" id="reasonflag" name="reasonflag"><?php echo $customer_details[0]['flag_reason'] ?></textarea>	
                                                        </div> <!-- /controls -->				
                                                </div> <!-- /control-group -->
                                                
                                                
                                                
                                                         <div class="row n-field-main m-0">
                                                              <p>Customer</p>
                                                              <div class="n-field-box">
                                                              </div>
                                                         </div>
                                                            
                                                            
                                                            
                                                            <div class="control-group">											
                                                            <label class="control-label" for="basicinput">Mobile Number 1 &nbsp;<font style="color: #C00">*</font></label>
                                                            <div class="controls">
                                                                <input type="text" class="span3" id="mobile_number1" name="mobile_number1" value="<?php echo $customer_details[0]['mobile_number_1'] ?>" required="required">
                                                            </div> <!-- /controls -->				
                                                        </div> <!-- /control-group -->
                                                        
                                                        
                                                        <div class="row n-field-main m-0">
                                                            <p>Customer</p>
                                                            <div class="n-field-box">
                                                            </div>
                                                        </div>
                                                            
                                                            
                                                            

                                                        <div class="control-group">											
                                                            <label class="control-label" for="basicinput">Mobile Number 2</label>
                                                            <div class="controls">
                                                                <input type="text" class="span3" id="mobile_number2" name="mobile_number2" value="<?php echo $customer_details[0]['mobile_number_2'] ?>">
                                                            </div> <!-- /controls -->				
                                                        </div> <!-- /control-group -->
                                                        
                                                        <div class="row n-field-main m-0">
                                                            <p>Customer</p>
                                                            <div class="n-field-box">
                                                            </div>
                                                        </div>
                                                            
                                                            
                                                            
                                                            

                                                        <div class="control-group">											
                                                            <label class="control-label" for="basicinput">Mobile Number 3</label>
                                                            <div class="controls">
                                                                <input type="text" class="span3" id="mobile_number3" name="mobile_number3" value="<?php echo $customer_details[0]['mobile_number_3'] ?>">
                                                            </div> <!-- /controls -->				
                                                        </div> <!-- /control-group -->
                                                        
                                                        <div class="row n-field-main m-0">
                                                            <p>Customer</p>
                                                            <div class="n-field-box">
                                                            </div>
                                                        </div>
                                                            
                                                            
                                                            
                                                            

                                                        <div class="control-group">											
                                                            <label class="control-label" for="basicinput">Phone</label>
                                                            <div class="controls">
                                                                <input type="text" class="span3" id="phone" name="phone" value="<?php echo $customer_details[0]['phone_number'] ?>">
                                                            </div> <!-- /controls -->				
                                                        </div> <!-- /control-group -->
                                                        
                                                        <div class="row n-field-main m-0">
                                                            <p>Customer</p>
                                                            <div class="n-field-box">
                                                            </div>
                                                        </div>
                                                            
                                                            
                                                            

                                                        <div class="control-group">											
                                                            <label class="control-label" for="basicinput">Fax</label>
                                                            <div class="controls">
                                                                <input type="text" class="span3" id="fax" name="fax" value="<?php echo $customer_details[0]['fax_number'] ?>">
                                                            </div> <!-- /controls -->				
                                                        </div> <!-- /control-group -->
                                                        
                                                        <div class="row n-field-main m-0">
                                                            <p>Customer</p>
                                                            <div class="n-field-box">
                                                            </div>
                                                        </div>
                                                            
                                                            
                                                            

														<div class="control-group">											
                                                            <label class="control-label" for="basicinput">TRN#</label>
                                                            <div class="controls">
                                                                <input type="text" class="span3" id="trnnumber" name="trnnumber" value="<?php echo $customer_details[0]['trnnumber'] ?>">
                                                            </div> <!-- /controls -->				
                                                        </div>
                                                        
                                                        <div class="row n-field-main m-0">
                                                                <p>Customer</p>
                                                                <div class="n-field-box">
                                                                </div>
                                                            </div>
                                                            
                                                            
                                                            
														
													    	<div class="control-group">											
                                                            <label class="control-label" for="basicinput">VAT#</label>
                                                            <div class="controls">
                                                                <input type="text" class="span3" id="vatnumber" name="vatnumber" value="<?php echo $customer_details[0]['vatnumber'] ?>">
                                                            </div> <!-- /controls -->				
                                                        </div>
                                                        
                                                        
                                                            <div class="row n-field-main m-0">
                                                                <p>Customer</p>
                                                                <div class="n-field-box">
                                                                </div>
                                                            </div>
                                                            
                                                            
                                                            
                                                            
                                                            <div class="control-group" style="display: none">
                                                                <label class="control-label" for="basicinput">Area</label>
                                                                <div class="controls">
                                                                    <select name="area_name" id="area" class="span3" required >
                                                                        <?php
                                                                        if (count($areas) > 0) {
                                                                            foreach ($areas as $areasVal) {
                                                                                ?>
                                                                                <option value="<?php echo $areasVal['area_id'] ?>"><?php echo $areasVal['area_name'] ?></option>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>

                                                                    </select>
                                                                </div>
                                                            </div> 
                                                            
                                                            
                                                            <div class="row n-field-main m-0">
                                                                <p>Customer</p>
                                                                <div class="n-field-box">
                                                                </div>
                                                            </div>
                                                            
                                                            
                                                            
                                                            <div class="control-group">
                                                                <label class="control-label" for="basicinput">Source&nbsp;<font style="color: #C00">*</font></label>
                                                                <div class="controls">
                                                                    <select name="customer_source" id="customer_source" class="span3" required >
																		 <option value="">-Select-</option>
																		<option value="Live In" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Live In" ? 'selected="selected"' : '') : '' ?>>Live In</option>
																		<option value="Live Out" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Live Out" ? 'selected="selected"' : '') : '' ?>>Live Out</option>
																		<option value="Rizek" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Rizek" ? 'selected="selected"' : '') : '' ?>>Rizek</option>
																		<option value="Justmop" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Justmop" ? 'selected="selected"' : '') : '' ?>>Justmop</option>
																		<option value="Matic" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Matic" ? 'selected="selected"' : '') : '' ?>>Matic</option>
																		<option value="ServiceMarket" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "ServiceMarket" ? 'selected="selected"' : '') : '' ?>>ServiceMarket</option>
																		<option value="Helpling" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Helpling" ? 'selected="selected"' : '') : '' ?>>Helpling</option>
																		<option value="Urban Clap" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Urban Clap" ? 'selected="selected"' : '') : '' ?>>Urban Clap</option>
																		<option value="Emaar" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Emaar" ? 'selected="selected"' : '') : '' ?>>Emaar</option>
																		<option value="MyHome" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "MyHome" ? 'selected="selected"' : '') : '' ?>>MyHome</option>
                                                                        <option value="Facebook" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Facebook" ? 'selected="selected"' : '') : '' ?>>Facebook</option>
                                                                        <option value="Google" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Google" ? 'selected="selected"' : '') : '' ?>>Google</option>
                                                                        <option value="Yahoo" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Yahoo" ? 'selected="selected"' : '') : '' ?>>Yahoo</option>
                                                                        <option value="Bing" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Bing" ? 'selected="selected"' : '') : '' ?>>Bing</option>
																		<option value="Direct Call" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Direct Call" ? 'selected="selected"' : '') : '' ?>>Direct Call</option>
																		<option value="Flyers" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Flyers" ? 'selected="selected"' : '') : '' ?>>Flyers</option>
																		<option value="Referral" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Referral" ? 'selected="selected"' : '') : '' ?>>Referral</option>
																		<option value="Watchman/Security Guard" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Watchman/Security Guard" ? 'selected="selected"' : '') : '' ?>>Watchman/Security Guard</option>
																		<option value="Maid" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Maid" ? 'selected="selected"' : '') : '' ?>>Maid</option>
																		<option value="Driver" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Driver" ? 'selected="selected"' : '') : '' ?>>Driver</option>
																		<option value="By email" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "By email" ? 'selected="selected"' : '') : '' ?>>By email</option>
																		<option value="Schedule visit" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Schedule visit" ? 'selected="selected"' : '') : '' ?>>Schedule visit</option>
																		<option value="Website" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Website" ? 'selected="selected"' : '') : '' ?>>Website</option>
																		<option value="Referred by staff" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Referred by staff" ? 'selected="selected"' : '') : '' ?>>Referred by staff</option>
																		<option value="Referred by customer" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Referred by customer" ? 'selected="selected"' : '') : '' ?>>Referred by customer</option>
																		<option value="Tekram" <?php echo isset($customer_details[0]['customer_status']) ? ($customer_details[0]['customer_source'] == "Tekram" ? 'selected="selected"' : '') : '' ?>>Tekram</option>
                                                                      </select>
                                                                </div>
                                                            </div>
                                                            
                                                            
                                                            <div class="row n-field-main m-0">
                                                                <p>Customer</p>
                                                                <div class="n-field-box">
                                                                </div>
                                                            </div>
                                                            
                                                            
                                                            
                                                            
                                                            <?php $style = $customer_details[0]['customer_source'] == "Maid" ? 'style="display:block;"' : 'style="display:none;"'?>
                                                            <div class="control-group" id="customer_source_reference" <?php echo $style; ?> >
                                                                <label class="control-label" for="basicinput">Source Reference&nbsp;<font style="color: #C00">*</font></label>
                                                                <div class="controls">
                                                                    <select name="customer_source_id" id="customer_source_id" class="span3" >
                                                                        <option value="">-- Select Maids --</option>
                                                                        <?php
                                                                        if (!empty($maids) > 0) {
                                                                            foreach ($maids as $maid) { $selected = $maid['maid_id'] == $customer_details[0]['customer_source_id'] ? 'selected="selected"' : '';
                                                                            ?>
                                                                            <option value="<?php echo $maid['maid_id']; ?>" <?php echo $selected ?>><?php echo $maid['maid_name']; ?></option>

                                                                            <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            
                                                            
                                                            <div class="row n-field-main m-0">
                                                                <p>Customer</p>
                                                                <div class="n-field-box">
                                                                </div>
                                                            </div>
                                                            
                                                            
                                                            
															
															<div class="control-group" id="customer_source_others" <?php echo $customer_details[0]['customer_source'] != 'Others' ? 'style="display: none;"' : ''; ?>>
																<label class="control-label" for="basicinput">Source Reference&nbsp;<font style="color: #C00">*</font></label>
																<div class="controls">
																	<input type="text" class="span3" id="customer_source_others_val" name="customer_source_others_val" value="<?php echo $customer_details[0]['customer_source_val'] ?>">
																</div>
															</div>
                                                            
                                                            
                                                            <div class="row n-field-main m-0">
                                                                <p>Customer</p>
                                                                <div class="n-field-box">
                                                                </div>
                                                            </div>
                                                            
                                                            
                                                            
                                                        </fieldset>

                                                    </div> <!-- /widget-content -->
                                                </div> <!-- /widget -->
                                            </div> <!-- /span6 -->

                                            <div class="span5">
                                                <div id="target-2" class="widget">
                                                    <div class="widget-content" style="border: 0px">
                                                        <div class="control-group">
                                                                <label class="control-label" for="basicinput">Customer Type &nbsp;<font style="color: #C00">*</font></label>
                                                                <div class="controls">
                                                                    <select name="customer_type" class="span3" required >
                                                                        <option value="HO" <?php echo isset($customer_details[0]['customer_type']) ? ($customer_details[0]['customer_type'] == "HO" ? 'selected="selected"' : '') : '' ?>>Home</option>
                                                                        <option value="OF" <?php echo isset($customer_details[0]['customer_type']) ? ($customer_details[0]['customer_type'] == "OF" ? 'selected="selected"' : '') : '' ?>>Office</option>
                                                                        <option value="WH" <?php echo isset($customer_details[0]['customer_type']) ? ($customer_details[0]['customer_type'] == "WH" ? 'selected="selected"' : '') : '' ?>>Warehouse</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        <!--Edited by vishnu-->
                                                        <div class="control-group">											
                                                            <label class="control-label" for="basicinput">Is Company&nbsp;<font style="color: #C00">*</font></label>
                                                            <div class="controls">
                                                                <label class="radio inline"><input type="radio"  name="company" value="Y" <?php echo $customer_details[0]['is_company'] == 'Y' ?  'checked="checked"' : ''; ?>> Yes</label>
                                                                <label class="radio inline"><input type="radio" name="company" value="N" <?php echo $customer_details[0]['is_company'] == 'N' ?  'checked="checked"' : ''; ?>> No</label>
                                                                <!--<input type="text" class="span3" id="contact_person" name="contact_person">-->
                                                            </div> <!-- /controls -->				
                                                        </div> <!-- /control-group -->

                                                        <div class="control-group" id="company_source" <?php echo $customer_details[0]['is_company'] == 'N' ? 'style="display: none;"' : ''; ?>>											
                                                            <label class="control-label" for="basicinput">Select Company&nbsp;<font style="color: #C00">*</font></label>
                                                            <div class="controls">
                                                                <select name="company_name" id="company_name" class="span3">
                                                                    <option value="">-Select-</option>
                                                                    <option value="Live In" <?php echo $customer_details[0]['company_name'] == "Live In" ? 'selected="selected"' : ''; ?>>Live In</option>
                                                                    <option value="Live Out" <?php echo $customer_details[0]['company_name'] == "Live Out" ? 'selected="selected"' : ''; ?>>Live Out</option>
                                                                    <option value="Rizek" <?php echo $customer_details[0]['company_name'] == "Rizek" ? 'selected="selected"' : ''; ?>>Rizek</option>
                                                                    <option value="Justmop" <?php echo $customer_details[0]['company_name'] == "Justmop" ? 'selected="selected"' : ''; ?>>Justmop</option>
																	<option value="Matic" <?php echo $customer_details[0]['company_name'] == "Matic" ? 'selected="selected"' : ''; ?>>Matic</option>
																	<option value="ServiceMarket" <?php echo $customer_details[0]['company_name'] == "ServiceMarket" ? 'selected="selected"' : ''; ?>>ServiceMarket</option>
																	<option value="Helpling" <?php echo $customer_details[0]['company_name'] == "Helpling" ? 'selected="selected"' : ''; ?>>Helpling</option>
																	<option value="Urban Clap" <?php echo $customer_details[0]['company_name'] == "Urban Clap" ? 'selected="selected"' : ''; ?>>Urban Clap</option>
																	<option value="Emaar" <?php echo $customer_details[0]['company_name'] == "Emaar" ? 'selected="selected"' : ''; ?>>Emaar</option>
																	<option value="MyHome" <?php echo $customer_details[0]['company_name'] == "MyHome" ? 'selected="selected"' : ''; ?>>MyHome</option>
																</select>
                                                            </div> <!-- /controls -->				
                                                        </div> <!-- /control-group -->
                                                
                                                        <!-- Ends -->
                                                        
                                                        <div class="control-group">											
                                                                <label class="control-label" for="basicinput">Contact Person &nbsp;<font style="color: #C00">*</font></label>
                                                                <div class="controls">
                                                                    <input type="text" class="span3" id="contact_person" name="contact_person" value="<?php echo $customer_details[0]['contact_person'] ?>" required="required">
                                                                </div> <!-- /controls -->				
                                                            </div> <!-- /control-group -->

                                                        <div class="control-group">											
                                                            <label class="control-label" for="basicinput">Website URL</label>
                                                            <div class="controls">
                                                                <input type="text" class="span3" id="website" name="website" value="<?php echo $customer_details[0]['website_url'] ?>">
                                                            </div> <!-- /controls -->				
                                                        </div> <!-- /control-group -->

                                                        <div class="control-group">											
                                                            <label class="control-label" for="basicinput">User Name</label>
                                                            <div class="controls">
                                                                <input type="text" class="span3" id="user_name" name="user_name" value="<?php echo $customer_details[0]['customer_username'] ?>">
                                                            </div><!-- /controls -->			
                                                        </div> <!-- /control-group -->

                                                        <div class="control-group">											
                                                            <label class="control-label" for="email">Email Address</label>
                                                            <div class="controls">
                                                                <input type="text" class="span3" id="email" name="email" value="<?php echo $customer_details[0]['email_address'] ?>">
                                                            </div> <!-- /controls -->				
                                                        </div> <!-- /control-group -->

                                                        <div class="control-group">											
                                                            <label class="control-label" for="password1">Password</label>
                                                            <div class="controls">
                                                                <input type="text" class="span3" id="password1" name="password" value="<?php echo $customer_details[0]['customer_password'] ?>">
                                                            </div> <!-- /controls -->				
                                                        </div> <!-- /control-group --> 
                                                        
                                                        <div class="control-group">											
                                                                <label class="control-label" for="basicinput">Notes</label>
                                                                <div class="controls">
                                                                    <textarea class="span3" rows="5" id="notes" name="notes"><?php echo $customer_details[0]['customer_notes'] ?></textarea>	
                                                                </div> <!-- /controls -->				
                                                            </div> <!-- /control-group -->

                                                    </div> <!-- /widget-content -->
                                                </div> <!-- /widget -->
                                            </div> <!-- /span6 -->

                                        </fieldset>            
                                    </div>
                                    
                                    <div class="tab-pane" id="address-details">
                                <fieldset>            
                                    <div class="span7" style="margin-left:0px;">
                                        <div class="widget">
                                            <div class="widget-content" style="border: 0px">
                                                <?php
                                                            if (count($customer_address) > 0) {
                                                                $i=1;
                                                                foreach ($customer_address as $customer) {
                                                                    ?>
                                                              <div style="margin-top:20px;">
                                                                    <div class="control-group">
                                                                        <label class="control-label" for="basicinput">Area &nbsp;<font style="color: #C00">*</font> &nbsp;<a onclick="remove_address(<?php echo $customer['customer_address_id']?>,<?php echo $customer['customer_id']?>)">[Remove]</a></label>
                                                                        <div class="controls">
                                                                            <select name="area[]" class="span3" style="min-width: 270px;" required >
                                                                                <option value="" selected>Select Area</option>
                                                                                <?php
                                                                                if (count($areas) > 0) {
                                                                                   
                                                                                    foreach ($areas as $areasVal) {
                                                                                        if ($areasVal['area_id'] == $customer['area_id'])
                                                                                            $selected = "selected";
                                                                                        else
                                                                                            $selected = "";
                                                                                        ?>

                                                                                        <option value="<?php echo $areasVal['area_id'] ?>" <?php echo $selected; ?>><?php echo $areasVal['area_name'] ?></option>
                                                                                        <?php
                                                                                    }
                                                                                }
                                                                                ?>

                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                  <div class="control-group">
                                                        <label class="control-label" for="basicinput">Apartment No&nbsp;<font style="color: #C00">*</font></label>
                                                        <div class="controls">
                                                            <input id="" type="text" name="apartment_no[]" value="<?php echo $customer['building'] ?>" class="span3" style="min-width: 270px;"/>
                                                        </div>
                                                    </div>
                                                                    <div class="control-group">											
                                                                        <label class="control-label" for="basicinput">Address &nbsp;<font style="color: #C00">*</font></label>
                                                                        <div class="controls">
                                                                                <input id="address_<?php echo $customer['customer_address_id']?>" type="text" name="address[]" value="<?php echo $customer['customer_address'] ?>" onkeyup="loadLocationField('<?php echo "address_".$customer['customer_address_id']?>','<?php echo "m".$customer['customer_address_id'] ?>');" class="span3" style="min-width: 270px;"/>
<!--                                                                            <textarea class="span3" rows="5" id="address" name="address[]" required="required"></textarea>	-->
                                                                            <input type="hidden" name="address_id[]" value="<?php echo $customer['customer_address_id'] ?>"/>
                                                                            <input type="hidden" name="lat[]" id="latitude_m<?php echo $customer['customer_address_id'] ?>" value="<?php echo $customer['latitude'] ?>"/>
                                                                            <input type="hidden" name="lng[]" id="longitude_m<?php echo $customer['customer_address_id'] ?>" value="<?php echo $customer['longitude'] ?>"/>
                                                                        </div> <!-- /controls -->				
                                                                    </div> <!-- /control-group -->
                                                                    <div id="map_canvas_<?php echo "m".$customer['customer_address_id'] ?>" style="height: 200px;" ></div>
                                                                    <script>
                                                                            var map<?php echo $i ?>;
                                                                            
                                                                            var geocoder = new google.maps.Geocoder();

                                                                            var address = "<?php echo str_replace(' ', '', $customer['customer_address']); ?>";
                                                                            <?php if($customer['latitude']!="" && $customer['longitude']!="" ) {?>
                                                                              locationRio =  {lat:<?php echo $customer['latitude'] ?>, lng:<?php echo $customer['longitude'] ?>};
                                                                              latlng['<?php echo $i ?>']=locationRio;   
                                                                                map<?php echo $i ?> = new google.maps.Map(document.getElementById("map_canvas_m<?php echo $customer['customer_address_id']?>"), {
                                                                                zoom: 14,
                                                                                center:locationRio,


                                                                              });
                                                                                       
                                                                            var marker = new google.maps.Marker({
                                                                            map: "map_canvas_m<?php echo $customer['customer_address_id']?>",
                                                                            draggable: true,
                                                                            animation: google.maps.Animation.DROP,
                                                                            position: locationRio,


                                                                       });
                                                                        marker.setMap(map<?php echo $i ?>);
                                                                                
                                                                            <?php } else {?>    
                                                                            geocoder.geocode( { 'address': address}, function(results, status) {
                                                                                
                                                                                if (status == google.maps.GeocoderStatus.OK) {
                                                                                  locationRio =  results[0].geometry.location
                                                                                  latlng['<?php echo $i ?>']=locationRio;
                                                                                  //var locationRio = {lat:results[0].geometry.location.lat(), lng:results[0].geometry.location.lng()};
                                                                                    //var locationRio={lat:8.5581,lng:76.8816};
                                                                            map<?php echo $i ?> = new google.maps.Map(document.getElementById("map_canvas_m<?php echo $customer['customer_address_id']?>"), {
                                                                            zoom: 14,
                                                                            center:locationRio,
                                                                           

                                                                          });
                                                                            
                                                                          var marker = new google.maps.Marker({
                                                                            map: "map_canvas_m<?php echo $customer['customer_address_id']?>",
                                                                            draggable: true,
                                                                            animation: google.maps.Animation.DROP,
                                                                            position: locationRio,


                                                                       });
                                                                        marker.setMap(map<?php echo $i ?>);
                                                                         //map.setCenter(new google.maps.LatLng(results[0].geometry.location.lat(),results[0].geometry.location.lng()))
                                                                           } 
                                                                                }); 
                                                                               
                                                                            
                                                                       <?php } ?>
                                                                            
                                                                        </script>   
                                                                    
                                                                    
                                                              </div>  
                                                                    <?php
                                                                    $i++;
                                                                }
                                                                
                                                             }
                                                             ?>

                                                            
                                                            
                                              
                                                            <div class="input_fields_wrap"></div><br>				

                                                            <div class="control-group">											
                                                                <div class="controls">
                                                                    <a style="float:right ; margin-right:80px; cursor:pointer;" class="add_field_button">Add More Address</a>	
                                                                </div> <!-- /controls -->	
                                                            </div> <!-- /control-group -->

                                             
                                            </div>
                                        </div> 
                                     </div> 
                                  
                                 </fieldset>
                              </div>

                                    <div class="tab-pane" id="account">

                                        <fieldset>            
                                            <div class="span5" style="margin-left:0px;">
                                                <div class="widget">
                                                    <div class="widget-content" style="border: 0px">
                                                        <fieldset>
                                                            <div class="control-group">
                                                                <label class="control-label" for="basicinput">Customer Book Type &nbsp;<font style="color: #C00">*</font></label>
                                                                <div class="controls">
                                                                    <select name="customers_type" id="customers_type" class="span3" required >
                                                                        <option value="0" <?php echo isset($customer_details[0]['customer_booktype']) ? ($customer_details[0]['customer_booktype'] == "0" ? 'selected="selected"' : '') : '' ?>>Non Regular</option>
                                                                        <option value="1" <?php echo isset($customer_details[0]['customer_booktype']) ? ($customer_details[0]['customer_booktype'] == "1" ? 'selected="selected"' : '') : '' ?>>Regular</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            
                                                            
                                                            <div class="control-group">
                                                                <label class="control-label" for="basicinput">Payment Type &nbsp;<font style="color: #C00">*</font></label>
                                                                <div class="controls">
                                                                    <select name="payment_type" id="payment_type" class="span3" required >
                                                                        <option value="D" <?php echo isset($customer_details[0]['payment_type']) ? ($customer_details[0]['payment_type'] == "D" ? 'selected="selected"' : '') : '' ?>>Daily Paying</option>
                                                                        <option value="W" <?php echo isset($customer_details[0]['payment_type']) ? ($customer_details[0]['payment_type'] == "W" ? 'selected="selected"' : '') : '' ?>>Weekly Paying</option>
                                                                        <option value="M" <?php echo isset($customer_details[0]['payment_type']) ? ($customer_details[0]['payment_type'] == "M" ? 'selected="selected"' : '') : '' ?>>Monthly Paying</option>
                                                                    </select>
                                                                </div>
                                                            </div>


                                                            <div class="control-group">
                                                                <label class="control-label" for="basicinput">Payment Mode &nbsp;<font style="color: #C00">*</font></label>
                                                                <div class="controls">
                                                                    <select name="payment_mode" id="payment_mode" class="span3" required >
																		<option value="">-Select-</option>
                                                                        <option value="Cash" <?php echo isset($customer_details[0]['payment_mode']) ? ($customer_details[0]['payment_mode'] == "Cash" ? 'selected="selected"' : '') : '' ?>>Cash</option>
                                                                        <option value="Cheque" <?php echo isset($customer_details[0]['payment_mode']) ? ($customer_details[0]['payment_mode'] == "Cheque" ? 'selected="selected"' : '') : '' ?>>Cheque</option>
                                                                        <option value="Online" <?php echo isset($customer_details[0]['payment_mode']) ? ($customer_details[0]['payment_mode'] == "Online" ? 'selected="selected"' : '') : '' ?>>Online</option>
                                                                        <option value="Credit Card" <?php echo isset($customer_details[0]['payment_mode']) ? ($customer_details[0]['payment_mode'] == "Credit Card" ? 'selected="selected"' : '') : '' ?>>Credit Card</option>
                                                                    </select>
                                                                </div>
                                                            </div>


                                                            <div class="control-group">											
                                                                <label class="control-label" for="basicinput">Hourly</label>
                                                                <div class="controls">
                                                                    <input type="text" class="span3" id="hourly" name="hourly" value="<?php echo $customer_details[0]['price_hourly'] ?>">

                                                                </div> <!-- /controls -->				
                                                            </div> <!-- /control-group -->


                                                            <div class="control-group">											
                                                                <label class="control-label" for="basicinput">Extra</label>
                                                                <div class="controls">
                                                                    <input type="text" class="span3" id="extra" name="extra" value="<?php echo $customer_details[0]['price_extra'] ?>">
                                                                </div> <!-- /controls -->				
                                                            </div> <!-- /control-group -->


                                                            <div class="control-group">											
                                                                <label class="control-label" for="basicinput">Weekend</label>
                                                                <div class="controls">
                                                                    <input type="text" class="span3" id="weekend" name="weekend" value="<?php echo $customer_details[0]['price_weekend'] ?>">
                                                                </div> <!-- /controls -->				
                                                            </div> <!-- /control-group -->
                                                            
                                                            <div class="control-group">											
                                                                <label class="control-label" for="basicinput">Latitude</label>
                                                                <div class="controls">
                                                                    <input type="text" class="span3" id="latitude" name="latitude" value="<?php echo $customer_details[0]['latitude'] ?>">
                                                                </div> <!-- /controls -->				
                                                            </div> <!-- /control-group -->

                                                        </fieldset>

                                                    </div> <!-- /widget-content -->
                                                </div> <!-- /widget -->
                                            </div> <!-- /span6 -->


                                            <div class="span5">
                                                <div id="target-2" class="widget">
                                                    <div class="widget-content" style="border: 0px">
                                                        <fieldset>
                                                            
                                                            <div class="control-group">											
                                                                <label class="control-label" for="basicinput">Longitude</label>
                                                                <div class="controls">
                                                                    <input type="text" class="span3" id="longitude" name="longitude" value="<?php echo $customer_details[0]['longitude'] ?>">
                                                                </div> <!-- /controls -->				
                                                            </div> <!-- /control-group -->


                                                            <div class="control-group">											
                                                                <label class="control-label">Key</label>
                                                                <div class="controls">
                                                                    <label class="radio inline"><input type="radio"  name="key" value="Y" <?php echo $customer_details[0]['key_given'] == 'Y' ?  'checked="checked"' : ''; ?>> Yes</label>
                                                                    <label class="radio inline"><input type="radio" name="key"  <?php echo $customer_details[0]['key_given'] == 'N' ?  'checked="checked"' : ''; ?>  value="N"> No</label>
                                                                </div>	<!-- /controls -->			
                                                            </div> <!-- /control-group -->

                                                            <div class="control-group">                                         
                                                                <label class="control-label">Rating Mail</label>
                                                                <div class="controls">
                                                                    <label class="radio inline"><input type="radio"  name="rating_mail_stat" <?php echo $customer_details[0]['rating_mail'] == 'Y' ?  'checked="checked"' : ''; ?> value="Y"> Yes</label>
                                                                    <label class="radio inline"><input type="radio" name="rating_mail_stat" <?php echo $customer_details[0]['rating_mail'] == 'N' ?  'checked="checked"' : ''; ?> value="N"> No</label>
                                                                </div>  <!-- /controls -->          
                                                            </div> <!-- /control-group -->


                                                            <div class="control-group">
                                                                <label class="control-label" for="basicinput">Current Photo</label>
                                                                <div class="controls">
                                                                    <!--                                                                    <div id="me" class="styleall" style=" cursor:pointer;"> -->
                                                                    <span style=" cursor:pointer;"> 
                                                                        <?php
                                                                        if ($customer_details[0]['customer_photo_file'] == "") {
                                                                            $image = base_url() . "img/no_image.jpg";
                                                                        } else {
                                                                            $image = base_url() . "customer_img/" . $customer_details[0]['customer_photo_file'];
                                                                        }
                                                                        ?>

                                                                        <img src="<?php echo $image; ?>" style="height: 100px; width: 100px"/> 
                                                                        <input type="hidden" name="old_image" id="old_image" value="<?php echo $customer_details[0]['customer_photo_file'] ?>"/>
                                                                    </span> 
                                                                    <!--                                                                    </div> -->
                                                                </div>
                                                            </div>

                                                            <div class="control-group">
                                                                <label class="control-label" for="basicinput">Change Photo</label>
                                                                <div class="controls">
                                                                    <div id="me" class="styleall" style=" cursor:pointer;"> 
                                                                        <span style=" cursor:pointer;">                             
                                                                            <img src="<?php echo base_url(); ?>img/profile_pic.jpg" style="height: 100px; width: 100px"/> 
                                                                        </span> 
                                                                    </div> 
                                                                </div>
                                                            </div> 
                                                            <br />
                                                        </fieldset>
                                                    </div> <!-- /widget-content -->
                                                </div> <!-- /widget -->
                                            </div> <!-- /span6 -->
                                        </fieldset>            

                                        <div class="form-actions" style="padding-left: 211px;">
                                            <input type="hidden" name="call_method" id="call_method" value="customer/editcustomerimgupload"/>
                                            <input type="hidden" name="img_fold" id="img_fold" value="customer_img"/>
                                            <input type="hidden" name="img_name_resp" id="img_name_resp"/>
                                            <input type="submit" class="btn mm-btn pull-right" value="Submit" name="customer_edit" onclick="return validate_customer();">
                                        </div>   
                                    </div>
                                </div>	
                            </form>
                        </div>
                    </div> <!-- /widget-content -->	
       
        </div> <!-- /widget -->	      		
    </div> <!-- /span8 -->	      		      		      		      	
</div> <!-- /row -->
<script>
function resize_map(){
//alert(locationRio);
var i;
setTimeout(function(){window.dispatchEvent(new Event('resize'));

    if("1" in latlng){map1.setCenter(latlng[1]);}
    if("2" in latlng){map2.setCenter(latlng[2]);}
    if("3" in latlng){map3.setCenter(latlng[3]);}
    if("4" in latlng){map4.setCenter(latlng[4]);}
    if("5" in latlng){map5.setCenter(latlng[5]);}
    if("6" in latlng){map6.setCenter(latlng[6]);}
    if("7" in latlng){map7.setCenter(latlng[7]);}
    if("8" in latlng){map8.setCenter(latlng[8]);}
    if("9" in latlng){map9.setCenter(latlng[9]);}
    if("10" in latlng){map10.setCenter(latlng[10]);}
    //}
}, 1000);

}
 
</script>    