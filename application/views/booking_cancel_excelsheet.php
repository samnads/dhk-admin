<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=BookingCancelReport.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table border="1">
    <thead>
        <tr>
            <th style="line-height: 18px; padding: 10px;">Sl No</th>
			<th style="line-height: 18px; padding: 10px;">Customer Name</th>
			<th style="line-height: 18px; padding: 10px;">Maid Name</th>
			<th style="line-height: 18px; padding: 10px;">Shift</th>
			<th style="line-height: 18px; padding: 10px;">Booking Type</th>
			<th style="line-height: 18px; padding: 10px;">Canceled User</th>
			<th style="line-height: 18px; padding: 10px;">Canceled Date</th>
			<th style="line-height: 18px; padding: 10px;">Remarks</th>
			<th style="line-height: 18px">Booking Date</th>
			<th style="line-height: 18px; padding: 10px;">Status</th>
        </tr>
    </thead>
    <tbody>
	<?php
	if (!empty($reports)) 
	{
		$i = 0;
		foreach ($reports as $report) 
		{
			//Payment Type
			if($report->payment_type == "D")
			{
				$paytype = "(D)";
			} else if($report->payment_type == "W")
			{
				$paytype = "(W)";
			} else if($report->payment_type == "M")
			{
				$paytype = "(M)";
			} else
			{
				$paytype = "";
			}
			$i++;
			?>
			<tr>
				<td style="line-height: 18px; padding: 5px 10px;">
					<?php echo $i; ?>
				</td>
				<td style="line-height: 18px; padding: 5px 10px;">
					<?php echo $report->customer_name; ?> <?php echo $paytype; ?>
				</td>
				<td style="line-height: 18px; padding: 5px 10px;">
					<?php echo $report->maid_name; ?>
				</td>
				<td style="line-height: 18px; padding: 5px 10px;">
					<?php echo $report->shift; ?>
				</td>
				<td style="line-height: 18px; padding: 5px 10px;">
					<?php
					if($report->booking_type == "OD")
					{
						$type = "One Day";
					} else if($report->booking_type == "WE")
					{
						$type = "Weekly";
					} else if($report->booking_type == "BW")
					{
						$type = "Bi-Weekly";
					}
					echo $type;
					?>
				</td>
				<td style="line-height: 18px; padding: 5px 10px;">
					<?php echo $report->user_fullname; ?>
				</td>
				<td style="line-height: 18px; padding: 5px 10px;">
					<?php echo date('d/m/Y h:i A',strtotime($report->added)); ?>
				</td>
				<td style="line-height: 18px; padding: 5px 10px;">
					<?php echo $report->remarks; ?>
				</td>
				<td style="line-height: 18px">
                                        <?php echo date('d/m/Y',strtotime($report->booked_datetime)); ?>
                                    </td>
				<td style="line-height: 18px; padding: 5px 10px;">
					<?php
					$getstatus = $this->reports_model->getservicestatus($report->booking_id,$report->service_date);
					if(count($getstatus) == 0)
					{
						echo "Not Started";
					} else {
						if($getstatus->service_status == 1)
						{
							echo "Started";
						} else if($getstatus->service_status == 2){
							echo "Finished";
						} else if($getstatus->service_status == 3){
							echo "Cancelled";
						}
					}
					?>
				</td>
			</tr>
			<?php
		}
	}
	?>
	</tbody>
</table>