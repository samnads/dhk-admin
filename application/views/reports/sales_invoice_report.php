<script src="https://unpkg.com/xlsx/dist/shim.min.js"></script>
<script src="https://unpkg.com/xlsx/dist/xlsx.full.min.js"></script>
<script src="https://unpkg.com/blob.js@1.0.1/Blob.js"></script>
<script src="https://unpkg.com/file-saver@1.3.3/FileSaver.js"></script>
<div class="main-inner">
	<div class="ml-3 mr-3">
		<div class="widget widget-table action-table" style="margin-bottom:30px">
			<div class="widget-header">
				<ul>
					<li>
						<i class="icon-th-list"></i>
						<h3>Daily Sales Invoice Report</h3>
					</li>
					<li>
						<div class="input-group input-daterange">
							<input type="text" class="form-control" name="filter[from_date]"
								value="<?= date('d/m/Y'); ?>">
							<div class="input-group-addon">to</div>
							<input type="text" class="form-control" name="filter[to_date]"
								value="<?= date('d/m/Y'); ?>">
						</div>
					</li>
					<li class="mr-0 float-right">
						<div class="topiconnew border-0 green-btn"> <a href="javascript:void(0);" id="customerexcelbtn"
								data-action="excel-export"> <i class="fa fa-file-excel-o"></i></a> </div>
					</li>
				</ul>
			</div>
			<!--<div id="LoadingImage" style="text-align:center;display:none;position:absolute;top:133px;right:0px;width:100%;height:100%;background-position:center;"><img src="<?php echo base_url() ?>img/loader.gif"></div>-->
			<div class="widget-content" style="margin-bottom:30px">
				<table id="sales-invoice-report" class="table da-table" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th style="width:50px;">
								<center>Sl. No.</center>
							</th>
							<th>Service Date</th>
							<th class="text-right">Booking Hours</th>
							<th class="text-right">Amount</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td></td>
							<td>
								<span>Total <span class="small">(Current Page)</span></span>
								<hr><span>Grand Total <span class="small">(All Pages)</span></span>
							</td>
							<td class="total_booking_hours">
								<span class="page"></span>
								<hr>
								<span class="full"></span>
							</td>
							<td>
								<span class="total"></span>
								<hr>
								<span class="grand-total"></span>
							</td>
						</tr>
					</tfoot>
					<tbody>
					</tbody>
				</table>
				<table id="sales-invoice-report-excel" style="display:none">
					<thead>
						<tr>
							<th style="width:50px;">
								<center>Sl. No.</center>
							</th>
							<th>Service Date</th>
							<th>Booking Hours</th>
							<th>Amount</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
		<!-- /widget -->
	</div>
	<!-- /span6 -->
</div>