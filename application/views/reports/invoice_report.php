<script src="https://unpkg.com/xlsx/dist/shim.min.js"></script>
<script src="https://unpkg.com/xlsx/dist/xlsx.full.min.js"></script>
<script src="https://unpkg.com/blob.js@1.0.1/Blob.js"></script>
<script src="https://unpkg.com/file-saver@1.3.3/FileSaver.js"></script>
<style type="text/css">
  .widget .widget-header {
    margin-bottom: 0px;
  }


  table.da-table tr td {
    padding: 0px 6px;
  }

  .datepicker-days table .disabled-date.day {
  background: #ffe1e1;
    color: #9b9b9b;
}

.datepicker table tr td.disabled,
.datepicker table tr td.disabled:hover {
  background: #ffe1e1;
    color: #9b9b9b;
}
</style>
<div class="row m-0">
  <div class="col-md-12">
    <div class="widget widget-table action-table">
      <div class="widget-header">
        <form class="form-horizontal" method="POST" action="<?=base_url('reports/invoice_report');?>">
      <ul>
      <li><i class="icon-th-list"></i>
        <h3>Invoice Report</h3></li>
        <li>
          <input type="text" name="filter_from_date" id="filter_from_date" autocomplete="off" placeholder="dd/mm/yyyy" value="<?=$filter_from_date;?>" readonly>
        </li>
        <li>
          <input type="text" name="filter_to_date" id="filter_to_date" autocomplete="off" placeholder="dd/mm/yyyy" value="<?=$filter_to_date;?>" readonly>
        </li>
        <li>
          <input type="submit" class="n-btn" id="customer-statement-search" value="Search">
        </li>
        <li class="mr-0 float-right">
        <div class="topiconnew border-0 green-btn">
        	<a data-action="excel-export" title=""><i class="fa fa-file-excel-o" aria-hidden="true"></i></a>
        </div>
        </li>
        </ul>
        </form>
      </div>
      <!-- /widget-header -->
      <div class="widget-content">
        <table id="holiday-list-table" class="table table-hover da-table" width="100%">
          <thead>
            <tr>
              <th>
                Date
              </th>
              <th>
                Invoice No.
              </th>
              <th>
                Customer
              </th>
              <th>
                Base Amount
              </th>
              <th>
                Vat Amount
              </th>
              <th>
               Total Amount
              </th>
              <th>
                Status
              </th>
            </tr>
          </thead>
          <tbody>
          <?php 
            $tot_base_amt=0;
            $tot_vat_amt=0;
            $tot_amt=0;
            if (count($invoices) > 0) :
            foreach ($invoices as $key => $invoice): 
              $tot_base_amt+=$invoice['invoice_total_amount'];
              $tot_vat_amt+=$invoice['invoice_tax_amount'];
              $tot_amt+=$invoice['invoice_net_amount'];
          ?>
                <tr>
                  <td><?=DateTime::createFromFormat('Y-m-d', $invoice['invoice_date'])->format('d-m-Y') ?></td>
                  <td><?=$invoice['invoice_num']?></td>
                  <td><?=$invoice['customer_name']?></td>
                  <td><?=$invoice['invoice_total_amount']?></td>
                  <td><?=$invoice['invoice_tax_amount']?></td>
                  <td><?=$invoice['invoice_net_amount']?></td>
                  <td>
                  <?=$invoice['invoice_status'] == 3 ? 'Paid' : 'Unknown'?>
                  </td>
                </tr>
                
                <?php
                endforeach;
                ?>
                <tr>
				<td></td>
				<td></td>
                <td><strong>Total</strong></td>
                <td><?= $tot_base_amt; ?></td>
                <td><?= $tot_vat_amt; ?></td>
                <td><?php echo $tot_amt; ?></td>
				<td></td>
            </tr>
            <?php  
              endif;
                ?>
        </table>
        <table id="excel-table" style="display:none" width="100%">
          <thead>
            <tr>
              <th style="width:150px;">
              Invoice Date
              </th>
              <th>
                Invoice No.
              </th>
              <th>
                Customer Name
              </th>
              <th>
                Base Amount
              </th>
              <th>
                Vat Amount
              </th>
              <th>
               Total Amount (AED)
              </th>
              <th>
                Status
              </th>
            </tr>
          </thead>
          <tbody>
          <?php 
          $tot_base_amt=0;
          $tot_vat_amt=0;
          $tot_amt=0;
          if (count($invoices) > 0) :
          foreach ($invoices as $key => $invoice): 
            $tot_base_amt+=$invoice['invoice_total_amount'];
            $tot_vat_amt+=$invoice['invoice_tax_amount'];
            $tot_amt+=$invoice['invoice_net_amount'];
          ?>
                <tr>
                  <td><?=DateTime::createFromFormat('Y-m-d', $invoice['invoice_date'])->format('d-m-Y') ?></td>
                  <td><?=$invoice['invoice_num']?></td>
                  <td><?=$invoice['customer_name']?></td>
                  <td><?=$invoice['invoice_total_amount']?></td>
                  <td><?=$invoice['invoice_tax_amount']?></td>
                  <td><?=$invoice['invoice_net_amount']?></td>
                  <td>
                  <?=$invoice['invoice_status'] == 3 ? 'Paid' : 'Unknown'?>
                  </td>
                      </tr>
                <?php
                endforeach;
                ?>
                <tr>
                <td colspan="2"></td>
                <td><strong>Total</strong></td>
                <td><?= $tot_base_amt; ?></td>
                <td><?= $tot_vat_amt; ?></td>
                <td><?php echo $tot_amt; ?></td>
				<td></td>
            </tr>
            <?php  
              endif;
                ?>
          </tbody>
        </table>
      </div>
      <!-- /widget-content -->
    </div>
    <!-- /widget -->
  </div>
  <!-- /span6 -->
</div>
<!-- Modal -->
<script>
(function(a) {
	a(document).ready(function(b) {
		if (a('#holiday-list-table').length > 0) {
			a("table#holiday-list-table").dataTable({
				'sPaginationType': "full_numbers",
				"bSort": true,
				"iDisplayLength": 100,
				"scrollY": true,
				"orderMulti": false,
				"scrollX": true,
				"aaSorting": []
			});
		}
	});
})(jQuery);
$(function() {
    $('input[name="filter_from_date"]').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
    });
    $('input[name="filter_to_date"]').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
    });
});
$('[data-action="excel-export"]').click(function(){
  var fileName = "Invoice Report "+ "<?= str_replace("/", "_", $filter_from_date) ?>" + " - <?= str_replace("/", "_", $filter_to_date) ?>";
  var fileType = "xlsx";
  var table = document.getElementById("excel-table");
  var wb = XLSX.utils.table_to_book(table, {sheet: "Report",dateNF:'dd/mm/yyyy;@',cellDates:true, raw: true, rawNumbers: false,cellNF: true});
  const ws = wb.Sheets['Report'];
  var wscols = [
    {wch:15},
    {wch:25},
    {wch:30},
    {wch:25},
    {wch:20},
    {wch:20},
    {wch:20},
    {wch:20},
  ];
  ws['!cols'] = wscols;

  var range = XLSX.utils.decode_range(ws['!ref']);
  for(var R = range.s.r + 1; R <= range.e.r; ++R) { 
    [3, 4, 5].forEach(function(C) {
      var cell_address = {c:C, r:R};
      var cell_ref = XLSX.utils.encode_cell(cell_address);
      if(ws[cell_ref] !== undefined) {
        ws[cell_ref].t = 'n'; // change the cell format to number
        ws[cell_ref].v = parseFloat(ws[cell_ref].v); // change the cell value to number
      }
    });
  }

    return XLSX.writeFile(wb, null || fileName + "." + (fileType || "xlsx"));
});

$('[data-action="excel-export-"]').click(function(){
	$('[data-action="excel-export"]').html('<i class="fa fa-spinner fa-spin"></i>');
	var fromdate = $('#filter_from_date').val();
	var todate = $('#filter_to_date').val();

	$.ajax({
		type: "POST",
		url: _base_url + 'customerexcel/invoice_report_excel',
		data: { filter_from_date: fromdate, filter_to_date: todate },
		cache: false,
		success: function (response) {
			// $('[data-action="excel-export"]').html('<i class="fa fa-file-excel-o"></i>');
			// window.location = response;
			alert(response);
		}
	});
});
	
</script>