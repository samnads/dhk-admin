<style>
	.btn-mm-success {
		background-color: #7eb216;
		background-image: linear-gradient(rgba(126, 178, 22, 0), rgba(0, 0, 0, 0.1));
		border-color: rgba(0, 0, 0, 0.05) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.15);
		color: white;
		text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.15);
	}

	.btn-mm-success:hover {
		background-color: #7eb216;
		background-image: linear-gradient(rgba(126, 178, 22, 0), rgba(0, 0, 0, 0.1));
		border-color: rgba(0, 0, 0, 0.05) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.15);
		color: white;
		text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.15);
	}
</style>
<div class="main-inner">
	<div class="ml-3 mr-3">
		<div class="widget widget-table action-table" style="margin-bottom:30px">
			<div class="widget-header">
				<ul>
					<li>
						<i class="icon-th-list"></i>
						<h3>Customer Report</h3>
					</li>
					<li>
						<span style="margin-left:5px;"></span>
						<div class="mm-drop">
							<select style="width: 100px;" id="all-customers">
								<option value="">All</option>
								<option value="1" selected="selected">Active</option>
								<option value="0">Inactive</option>
							</select>
						</div>
					</li>
					<li>
						<span style="color: #fff;">From :</span>
						<input type="text" style="width: 100px;" id="cust_from_date" />
					</li>
					<li>
						<span style="color: #fff;">To :</span>
						<input type="text" style="width: 100px;" id="cust_to_date" />
					</li>
					<li class="mr-0 float-right">
						<div class="topiconnew border-0 green-btn"> <a href="javascript:void(0);" id="customerexcelbtn"> <i class="fa fa-file-excel-o"></i></a></div>
					</li>
                </ul>
			</div>
			<div class="widget-content" style="margin-bottom:30px">
				<table id="customerreporttable" class="table da-table" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th><center>Sl. No.</center></th>
							<th>Name</th>
							<th style="">Primary Phone</th>
							<th style="">Primary Address</th>
							<th style="width:80px;">Source</th>
							<th style="">Added Date</th>
							<th style="">Booking Type</th>
							<th style="">Served by</th>
							<th style="width:70px;">Flagged?</th>
							<th style="">Active or Not</th>
							<th style="text-align: center !important;">Last Booking Date</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
		<!-- /widget -->
	</div>
	<!-- /span6 -->
</div>