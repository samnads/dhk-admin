<script src="https://unpkg.com/xlsx/dist/shim.min.js"></script>
<script src="https://unpkg.com/xlsx/dist/xlsx.full.min.js"></script>
<script src="https://unpkg.com/blob.js@1.0.1/Blob.js"></script>
<script src="https://unpkg.com/file-saver@1.3.3/FileSaver.js"></script>
<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
	.container.booking-fl-box{width: 100% !important;}
</style>
<div class="row m-0">
    <div class="col-md-12">
        <div class="widget widget-table action-table">
            <div class="widget-header">
                <form class="form-horizontal" method="post" action="<?php echo base_url('reports/schedule_report_service_wise') ?>">
                   <ul>
                   <li>
                    <i class="icon-th-list"></i>
                    <h3>Service Wise Report</h3>
                    </li>
                    <li>
					<input type="text" readonly="readonly" style="width: 160px;" id="OneDayDate" name="search_date" value="<?php echo $payment_date ?>">
                    </li>
                    <li>
					<input type="text" readonly="readonly" style="width: 160px;" id="search_date_to" name="search_date_to" value="<?php echo $payment_date_to ?>">
                    </li>
					<li>
                            <select name="filter_service_type_id" class="sel2">
                                <option value="">-- All Servces --</option>
                                <?php foreach ($service_types as $service_type): ?>
                                        <option value="<?=$service_type['service_type_id'];?>" <?= $filter_service_type_id == $service_type['service_type_id'] ? 'selected' : '' ?>> <?=$service_type['service_type_name'];?></option>
								<?php endforeach;?>
                            </select>
                        </li>
                    <li>
                    <li>
                            <select name="filter_maid_id" class="sel2">
                                <option value="">-- All Maids --</option>
                                <?php foreach ($maids as $maid): ?>
                                        <option value="<?=$maid['maid_id'];?>" <?= $filter_maid_id == $maid['maid_id'] ? 'selected' : '' ?>> <?=$maid['maid_name'];?></option>
								<?php endforeach;?>
                            </select>
                        </li>
                    <li>
					<input type="submit" class="n-btn" value="Go" name="add_payment">
					<input type="hidden" id="servicefromdate" value="<?php echo $servicedate; ?>" />
					<input type="hidden" id="servicetodate" value="<?php echo $servicedateto; ?>" />
					<input type="hidden" id="servicezoneid" value="<?php echo $zone_id; ?>" />
                    </li>
                    <li class="mr-0 float-right">
                    <div class="topiconnew border-0 green-btn">
                        <a data-action="excel-export" title=""><i class="fa fa-file-excel-o" aria-hidden="true"></i></a>
                    </div>
                    </li>
                </form>
            </div>
            <div class="widget-content">
                <table id="schedule-report" class="table da-table table-hover" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Sl. No.</th>
							<th>Service</th>
							<th>Service Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $slno = 1; $total = 0; foreach($rows as $key => $row): ?>
                            <?php $total+=$row['total_amount']; ?> 
                            <tr>
								<td><?php echo $slno++; ?></td>
								<td ><?php echo $row['service_type_name']; ?></td>
								<td ><?php echo number_format($row['total_amount'],2,".",","); ?></td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th class="text-right" colspan="2">Total</th>
                            <th><?= number_format($total,2,".",","); ?></th>
                        </tr>
                    </tfoot>
                </table>
                <table id="excel-table" style="display:none">
                    <thead>
                        <tr>
                            <th>Sl. No.</th>
							<th>Service</th>
							<th>Service Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $slno = 1; foreach($rows as $key => $row): ?>
                            <tr>
								<td><?php echo $slno++; ?></td>
								<td ><?php echo $row['service_type_name']; ?></td>
								<td ><?php echo number_format($row['total_amount'],2,".",","); ?></td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th class="text-right" colspan="2">Total</th>
                            <th><?= number_format($total,2,".",","); ?></th>
                        </tr>
                    </tfoot>
                </table>
            </div><!-- /widget-content -->
        </div><!-- /widget -->
    </div><!-- /span12 -->
</div>
<script>
	(function (a) {
    a(document).ready(function (b) {
        if (a('#schedule-report').length > 0) {
            a("table#schedule-report").dataTable({
				'serverSide': false,
                sPaginationType: "full_numbers", "bSort": false, "iDisplayLength": 100, "scrollY": true,
                "scrollX": true,
                footerCallback: function (row, data, start, end, display) {

                },
            });
            a("table#da-ex-datatable-default").dataTable({ "iDisplayLength": 100 });
        }
    });
})(jQuery);
$(function() {
    $('input[name="search_date"]').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
    });
    $('input[name="search_date_to"]').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
    });
});
$('[data-action="excel-export"]').click(function(){
  var fileName = "Service Wise Report "+ "<?= str_replace("/", "_", $payment_date) ?>" + " - <?= str_replace("/", "_", $payment_date_to) ?>";
  var fileType = "xlsx";
  var table = document.getElementById("excel-table");
  var wb = XLSX.utils.table_to_book(table, {sheet: "Report"});
  const ws = wb.Sheets['Report'];
  var wscols = [
    {wch:15},
    {wch:25},
    {wch:30},
    {wch:25},
    {wch:20}
  ];
  ws['!cols'] = wscols;
    return XLSX.writeFile(wb, null || fileName + "." + (fileType || "xlsx"));
});
</script>