<script src="https://unpkg.com/xlsx/dist/shim.min.js"></script>
<script src="https://unpkg.com/xlsx/dist/xlsx.full.min.js"></script>
<script src="https://unpkg.com/blob.js@1.0.1/Blob.js"></script>
<script src="https://unpkg.com/file-saver@1.3.3/FileSaver.js"></script>
<style>
	.btn-mm-success {
		background-color: #7eb216;
		background-image: linear-gradient(rgba(126, 178, 22, 0), rgba(0, 0, 0, 0.1));
		border-color: rgba(0, 0, 0, 0.05) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.15);
		color: white;
		text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.15);
	}

	.btn-mm-success:hover {
		background-color: #7eb216;
		background-image: linear-gradient(rgba(126, 178, 22, 0), rgba(0, 0, 0, 0.1));
		border-color: rgba(0, 0, 0, 0.05) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.15);
		color: white;
		text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.15);
	}
	hr {
		margin: 3px 0 !important;
	}
</style>
<!-- Details of Outstanding popup div --->



<div class="common-popup-wrapper" id="common-popup-wrapper-id">
	<div class="common-popup-section d-flex">
		<div class="common-popup-main p-0 col-md-6">
			<div class="close-btn" onclick="closeFancy()"><img src="<?php echo base_url() ?>images/close-big-w.webp" alt=""></div>
			<div class="row common-popup-title m-0">
				<h5>Outstanding details</h5>
			</div>
			<div class="cm-field-content-main m-0">
			    <table id="customeroutstandingdetailstable" class="table da-table" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th style="width:50px;"><center>Sl. No.</center></th>
							<th style="">Number</th>
							<th style="">Invoice Date</th>
							<th style="">Due Date</th>
							<th style="">Total</th>
							<th style="">Paid Amount</th>
							<th style="">Due Amount</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
				<button type="reset" class="n-btn red-btn mb-0" onclick="closeFancy()">Close</button>
			</div>
		</div>
	</div>
</div>


<div id="details-div" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Outstanding details</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div id="popup-booking" class="col-12 p-0">
    		<table id="customeroutstandingdetailstable--" class="table da-table" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th style="width:50px;"><center>Sl. No.</center></th>
							<th style="">Number</th>
							<th style="">Invoice Date</th>
							<th style="">Due Date</th>
							<th style="">Total</th>
							<th style="">Paid Amount</th>
							<th style="">Due Amount</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
        <div class="modal-footer">
          <button type="reset" class="n-btn red-btn mb-0" onclick="closeFancy()">Close</button>
        </div>
    </div>
  </div>
</div>
<!--------------END----------------------->

<div id="disable-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Disable ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to disable this customer ?</h3>
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn red-btn mb-0" onclick="confirm_enable_disable()">Disable</button>
    </div>
  </div>
</div>
<div id="enable-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Enable ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to enable this customer ?</h3>
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn mb-0" onclick="confirm_enable_disable()">Enable</button>
    </div>
  </div>
</div>
<div id="avatar-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name" class="avatar-popup-name"></span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div id="" class="col-12 p-0">
      <div class="modal-body">
        <div class="img-container">
          <img id="avatar-popup-image" src="#">
        </div>
      </div>
    </div>
  </div>
</div>
<div class="main-inner">
	<div class="ml-3 mr-3">
		<div class="widget widget-table action-table" style="margin-bottom:30px">
			<div class="widget-header">

            <ul>
            <li>
            <i class="icon-th-list"></i>
            <h3>Customer Outstanding</h3>
            </li>

            <li>
				<span style="margin-left:5px;"></span>
				<div class="mm-drop">
					<select style="width: 100px;" id="all-customers">
						<option value="">All</option>
						<option value="1" selected="selected">Active</option>
						<option value="0">Inactive</option>
					</select>
				</div>

               </li>

            <li>

				<span style="color: #fff;">From :</span>
				<input type="text" style="width: 100px;" id="cust_from_date" />

			</li>

            <li>
				<span style="color: #fff;">To :</span>
				<input type="text" style="width: 100px;" id="cust_to_date" />

            </li>
			
			<li>
			<span style="color: #fff;">Customer</span>
			<select class="sel2" style="margin-top: 5px; width:160px; margin-bottom: 9px;" id="customers_vh_rep_new" name="customers_vh_rep">
						<option value="">-- All Customers --</option>
						<?php
						$url_custid='nil';
						foreach($customerlist as $c_val)
						{
							if($c_val->customer_id == $customer_id)
							{
								$selected = 'selected="selected"';
								$url_custid=$customer_id;
							} else {
								$selected = '';
							}
						?>
						<option value="<?php echo $c_val->customer_id; ?>" <?php echo $selected; ?>><?php echo $c_val->customer_name; ?></option>
						<?php
						}
						?>  
                    </select>
			</li>

            <li class="mr-0 float-right">
				<div class="topiconnew border-0 green-btn"> <a href="javascript:void(0);" id="customerexcelbtn" data-action="excel-export"> <i class="fa fa-file-excel-o"></i></a> </div>
                </li>
                </ul>
			</div>
			<!--<div id="LoadingImage" style="text-align:center;display:none;position:absolute;top:133px;right:0px;width:100%;height:100%;background-position:center;"><img src="<?php echo base_url() ?>img/loader.gif"></div>-->
			<div class="widget-content" style="margin-bottom:30px">
				<table id="customeroutstandinglisttablenew" class="table da-table" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th style="width:50px;"><center>Sl. No.</center></th>
							<th style="">Client Name</th>
							<th style="">Mobile</th>
							<th style="">Area</th>
							<!-- <th style="">Total</th>
							<th style="">Paid</th> -->
							<th style="">Balance</th>
							<th style="">Last Invoice Date</th>
							<th style="">Type Of Client</th>
							<th style="">Action</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th style="width:50px;"><center></center></th>
							<th style=""></th>
							<th style=""></th>
							<th style="">Debit<hr>Credit<hr>Total</th>
							<!-- <th style=""></th>
							<th style=""></th> -->
							<th style="">
								<span id="debit-total"></span><hr>
								<span id="credit-total"></span><hr>
								<span id="balance-total"></span>
							</th>
							<th style=""></th>
							<th style=""></th>
							<th style=""></th>
						</tr>
					</tfoot>
					<tbody>
					</tbody>
				</table>
			</div>

		</div>
		<!-- /widget -->
	</div>
	<!-- /span6 -->
</div>
<table id="table-for-excel" class="table" width="100%" style="display:none">
          <thead>
            <tr>
              <th>
                Sl. No.
              </th>
              <th>
                Customer
              </th>
			  <th>
                Mobile
              </th>
			  <th>
                Area
              </th>
			  <!-- <th>
                Total
              </th>
			  <th>
                Paid
              </th> -->
              <th>
                Balance
              </th>
              <th>
                Last Invoice Date
              </th>
			  <th>
                Type of Client
              </th>
            </tr>
          </thead>
          <tbody id="tablebody">
            
	</tbody>
</table>
<script> 
/******************************************************** */
// $('[data-action="excel-export"]').click(function(){
  // var fileName = "Customer Outstanding Report";
  // var fileType = "xlsx";
  // var table = document.getElementById("table-for-excel");
  // var wb = XLSX.utils.table_to_book(table, {sheet: "Report"});
  // const ws = wb.Sheets['Report'];
  // var wscols = [
	// {wch:5},
	// {wch:20},
	// {wch:10},
	// {wch:15},
	// {wch:10},
	// {wch:10},
	// {wch:10},
	// {wch:10},
	// {wch:10}
  // ];
  // ws['!cols'] = wscols;
	// return XLSX.writeFile(wb, null || fileName + "." + (fileType || "xlsx"));
// });
/******************************************************** */
</script> 