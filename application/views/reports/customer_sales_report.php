<script src="https://unpkg.com/xlsx/dist/shim.min.js"></script>
<script src="https://unpkg.com/xlsx/dist/xlsx.full.min.js"></script>
<script src="https://unpkg.com/blob.js@1.0.1/Blob.js"></script>
<script src="https://unpkg.com/file-saver@1.3.3/FileSaver.js"></script>
<style type="text/css">
  .widget .widget-header {
    margin-bottom: 0px;
  }


  table.da-table tr td {
    padding: 0px 6px;
  }

  .datepicker-days table .disabled-date.day {
  background: #ffe1e1;
    color: #9b9b9b;
}

.datepicker table tr td.disabled,
.datepicker table tr td.disabled:hover {
  background: #ffe1e1;
    color: #9b9b9b;
}
</style>
<div class="row m-0">
  <div class="col-md-12">
    <div class="widget widget-table action-table">
      <div class="widget-header">
        <form class="form-horizontal" method="POST" action="<?=base_url('reports/customer_sales_report');?>">
      <ul>
      <li><i class="icon-th-list"></i>
        <h3>Customer Sales Report</h3></li>
        <li>
          <input type="text" name="filter_from_date" autocomplete="off" placeholder="dd/mm/yyyy" value="<?=$filter_from_date;?>" readonly>
        </li>
        <li>
          <input type="text" name="filter_to_date" autocomplete="off" placeholder="dd/mm/yyyy" value="<?=$filter_to_date;?>" readonly>
        </li>
        <li>
          <input type="submit" class="n-btn" id="customer-statement-search" value="Search">
        </li>
        <li class="mr-0 float-right">
        <div class="topiconnew border-0 green-btn">
        	<a data-action="excel-export" title=""><i class="fa fa-file-excel-o" aria-hidden="true"></i></a>
        </div>
        </li>
        </ul>
        </form>
      </div>
      <!-- /widget-header -->
      <div class="widget-content">
        <table id="holiday-list-table" class="table table-hover da-table" width="100%">
          <thead>
            <tr>
              <th>
                Customer
              </th>
              <th>
                Net Amount
              </th>
              <th>
                VAT Amount
              </th>
              <th>
                Total Amount (AED)
              </th>
            </tr>
          </thead>
          <tbody>
          <?php 
          $total = 0;
          if (count($rows) > 0) :
          foreach ($rows as $key => $row):
          $invoice_net_amount+=$row['invoice_net_amount'];
          // for correct vat values
          $row['invoice_tax_amount'] = $row['invoice_net_amount']*5/(100+5);
          $row['invoice_total_amount'] = $row['invoice_net_amount'] - $row['invoice_tax_amount'];
          ?>
                <tr>
                  <td><?=$row['customer_name']?></td>
                  <td><?=number_format($row['invoice_total_amount'],2,".",",")?></td>
                  <td><?=number_format($row['invoice_tax_amount'],2,".",",")?></td>
                  <td><?=$row['invoice_net_amount']?></td>
                </tr>
          <?php
          endforeach;
          endif;
          ?>
          </tbody>
          <tfoot>
            <?php
            $invoice_tax_amount = $invoice_net_amount*5/(100+5);
            $invoice_total_amount = $invoice_net_amount - $invoice_tax_amount;
            ?>
            <tr>
              <th class="text-right" colspan="3">Net Amount</th>
              <th><?= number_format($invoice_total_amount,2,".",","); ?></th>
            </tr>
            <tr>
              <th class="text-right" colspan="3">VAT Amount</th>
              <th><?= number_format($invoice_tax_amount,2,".",","); ?></th>
            </tr>
            <tr>
              <th class="text-right" colspan="3">Total Amount</th>
              <th><?= number_format($invoice_net_amount,2,".",","); ?></th>
            </tr>
          </tfoot>
        </table>
        <table id="excel-table" style="display:none" width="100%">
          <thead>
            <tr>
              <th>
                Customer
              </th>
              <th>
                Net Amount
              </th>
              <th>
                VAT Amount
              </th>
              <th>
                Total Amount (AED)
              </th>
            </tr>
          </thead>
          <tbody>
          <?php 
          if (count($rows) > 0) :
          foreach ($rows as $key => $row): 
            // for correct vat values
          $row['invoice_tax_amount'] = $row['invoice_net_amount']*5/(100+5);
          $row['invoice_total_amount'] = $row['invoice_net_amount'] - $row['invoice_tax_amount'];
          ?>
                <tr>
                  <td><?=$row['customer_name']?></td>
                  <td><?=number_format($row['invoice_total_amount'],2,".",",")?></td>
                  <td><?=number_format($row['invoice_tax_amount'],2,".",",")?></td>
                  <td><?=$row['invoice_net_amount']?></td>
                </tr>
          <?php
          endforeach;
          endif;
          ?>
          </tbody>
          <tfoot>
            <tr>
              <th class="text-right" colspan="3">Net Amount</th>
              <th><?= number_format($invoice_total_amount,2,".",","); ?></th>
            </tr>
            <tr>
              <th class="text-right" colspan="3">VAT Amount</th>
              <th><?= number_format($invoice_tax_amount,2,".",","); ?></th>
            </tr>
            <tr>
              <th class="text-right" colspan="3">Total Amount</th>
              <th><?= number_format($invoice_net_amount,2,".",","); ?></th>
            </tr>
          </tfoot>
        </table>
      </div>
      <!-- /widget-content -->
    </div>
    <!-- /widget -->
  </div>
  <!-- /span6 -->
</div>
<!-- Modal -->
<script>
(function(a) {
	a(document).ready(function(b) {
		if (a('#holiday-list-table').length > 0) {
			a("table#holiday-list-table").dataTable({
				'sPaginationType': "full_numbers",
				"bSort": true,
				"iDisplayLength": 100,
				"scrollY": true,
				"orderMulti": false,
				"scrollX": true,
        "aaSorting": []
			});
		}
	});
})(jQuery);
$(function() {
    $('input[name="filter_from_date"]').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
    });
    $('input[name="filter_to_date"]').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
    });
});
$('[data-action="excel-export"]').click(function(){
  var fileName = "Customer Sales Report "+ "<?= str_replace("/", "_", $filter_from_date) ?>" + " - <?= str_replace("/", "_", $filter_to_date) ?>";
  var fileType = "xlsx";
  var table = document.getElementById("excel-table");
  var wb = XLSX.utils.table_to_book(table, {sheet: "Report"});
  const ws = wb.Sheets['Report'];
  var wscols = [
    {wch:15},
    {wch:25},
    {wch:30},
    {wch:25},
    {wch:20}
  ];
  ws['!cols'] = wscols;
    return XLSX.writeFile(wb, null || fileName + "." + (fileType || "xlsx"));
});
</script>