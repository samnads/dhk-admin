<script src="https://unpkg.com/xlsx/dist/shim.min.js"></script>
<script src="https://unpkg.com/xlsx/dist/xlsx.full.min.js"></script>
<script src="https://unpkg.com/blob.js@1.0.1/Blob.js"></script>
<script src="https://unpkg.com/file-saver@1.3.3/FileSaver.js"></script>
<style>
	.btn-mm-success {
		background-color: #7eb216;
		background-image: linear-gradient(rgba(126, 178, 22, 0), rgba(0, 0, 0, 0.1));
		border-color: rgba(0, 0, 0, 0.05) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.15);
		color: white;
		text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.15);
	}

	.btn-mm-success:hover {
		background-color: #7eb216;
		background-image: linear-gradient(rgba(126, 178, 22, 0), rgba(0, 0, 0, 0.1));
		border-color: rgba(0, 0, 0, 0.05) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.15);
		color: white;
		text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.15);
	}
</style>

<div class="main-inner">
	<div class="ml-3 mr-3">
		<div class="widget widget-table action-table" style="margin-bottom:30px">
			<div class="widget-header">

            <ul>
            <li>
            <i class="icon-th-list"></i>
            <h3>Staff Report</h3>
            </li>

            <li>

				<span style="color: #fff;">From :</span>
				<input type="text" style="width: 100px;" id="staff_from_date" value="<?php echo $currentdate; ?>" />

			</li>

            <li>
				<span style="color: #fff;">To :</span>
				<input type="text" style="width: 100px;" id="staff_to_date" value="<?php echo $currentdate; ?>" />

            </li>
			
			<li>
			<span style="color: #fff;">Staff</span>
			<select class="sel2" style="margin-top: 5px; width:160px; margin-bottom: 9px;" id="staff_type_id" name="staff_type_id">
						<option value="">-- All Staff --</option>
						<?php
						foreach($maidlist as $c_val)
						{
						?>
						<option value="<?php echo $c_val->maid_id; ?>"><?php echo $c_val->maid_name; ?></option>
						<?php
						}
						?>  
                    </select>
			</li>

            <li class="mr-0 float-right">
				<div class="topiconnew border-0 green-btn"> <a href="javascript:void(0);" id="staffexcelbtn" data-action="excel-export"> <i class="fa fa-file-excel-o"></i></a> </div>
                </li>
                </ul>
			</div>
			<!--<div id="LoadingImage" style="text-align:center;display:none;position:absolute;top:133px;right:0px;width:100%;height:100%;background-position:center;"><img src="<?php echo base_url() ?>img/loader.gif"></div>-->
			<div class="widget-content" style="margin-bottom:30px">
				<table id="staffreportlist" class="table da-table" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th style="width:50px;"><center>Sl. No.</center></th>
							<th style="">Staff Name</th>
							<th style="">Hours Booked</th>
							<th style="">Hours Free</th>
							<th style="">Hours Absent</th>
							<th style="">Total Amount</th>
							<!--<th style="">Amount Paid</th>
							<th style="">Pending Amount</th>-->
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th style="width:50px;"><center></center></th>
							<th style="">Total</th>
							<th style=""><span id="hours-booked"></span></th>
							<th style=""><span id="hours-free"></span></th>
							<th style=""><span id="hours-absent"></span></th>
							<th style=""><span id="total-amt"></span></th>
							<!--<th style=""><span id="paid-total"></span></th>
							<th style=""><span id="pending-total"></span></th>-->
						</tr>
					</tfoot>
					<tbody>
					</tbody>
				</table>
			</div>

		</div>
		<!-- /widget -->
	</div>
	<!-- /span6 -->
</div>
<table id="table-for-excel-staff" class="table" width="100%" style="display:none">
          <thead>
            <tr>
				<th>Sl. No.</th>
				<th>Staff Name</th>
				<th>Hours Booked</th>
				<th>Hours Free</th>
				<th>Hours Absent</th>
				<th>Total Amount</th>
				<!--<th>Amount Paid</th>
				<th>Pending Amount</th>-->
            </tr>
          </thead>
          <tbody id="stafftablebody">
            
	</tbody>
</table>
<script> 
/******************************************************** */
// $('[data-action="excel-export"]').click(function(){
  // var fileName = "Customer Outstanding Report";
  // var fileType = "xlsx";
  // var table = document.getElementById("table-for-excel");
  // var wb = XLSX.utils.table_to_book(table, {sheet: "Report"});
  // const ws = wb.Sheets['Report'];
  // var wscols = [
	// {wch:5},
	// {wch:20},
	// {wch:10},
	// {wch:15},
	// {wch:10},
	// {wch:10},
	// {wch:10},
	// {wch:10},
	// {wch:10}
  // ];
  // ws['!cols'] = wscols;
	// return XLSX.writeFile(wb, null || fileName + "." + (fileType || "xlsx"));
// });
/******************************************************** */
</script> 