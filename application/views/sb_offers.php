<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
</style>
<div id="delete-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Delete ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to delete this offer ?</h3>
      <input type="hidden" id="delete_id">
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn mb-0" data-dismiss="modal" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn red-btn mb-0" data-dismiss="modal" onclick="confirm_delete()">Delete</button>
    </div>
  </div>
</div>
<div id="disableConfirmModal" class="modal hide" role="dialog">
    <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header disable">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3>Confirm Disable Offer ?</h3>
            </div>
            <div class="modal-body">
                <h3>Are you sure you want to disable this offer ?</h3>
            </div>
            <div class="modal-footer">
                <button type="button" class="n-btn red-btn mb-0" data-dismiss="modal">Cancel</button>
                <button type="button" class="n-btn red-btn mb-0" data-dismiss="modal"
                    onclick="confirm_disable()">Disable</button>
            </div>
        </div>
    </div>
</div>
<div id="enableConfirmModal" class="modal hide" role="dialog">
    <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header enable">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3>Confirm Enable Offer ?</h3>
            </div>
            <div class="modal-body">
                <h3>Are you sure you want to enable this offer ?</h3>
            </div>
            <div class="modal-footer">
                <button type="button" class="n-btn red-btn mb-0" data-dismiss="modal">Cancel</button>
                <button type="button" class="n-btn red-btn mb-0" data-dismiss="modal"
                    onclick="confirm_enable()">Enable</button>
            </div>
        </div>
    </div>
</div>
<div id="new-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">New Offer</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="col-12 p-0">
      <form method="post" id="offer_form" enctype="multipart/form-data">
        <div class="modal-body">
          <div class="row m-0 n-field-main">
            <p>Offer Title</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" id="offer_heading" name="offer_heading" required>
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Offer Type</p>
            <div class="col-sm-12 p-0 n-field-box">
                <select class="sel2" name="offer_type" id="offer_type" onchange="check_hide_status()" style="width:100%;" required>
                <option value="Text Type">Text</option>
                <option value="Image Type">Image</option>
                </select>
            </div>
          </div>
          <div class="row m-0 n-field-main add_offer_text">
            <p>Offer Sub-Title</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" id="offer_subheading" name="offer_subheading">
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Offer Content</p>
            <div class="col-sm-12 p-0 n-field-box">
              <textarea name="offer_content" id="offer_content" style="width:100%;"></textarea>
            </div>
          </div>
          <div class="row m-0 n-field-main add_offer_image">
            <div class="col-sm-6 p-0 n-field-box">
               <p>Offer Image</p>
              <input type="file" accept="image/*" name="offer_image" id="offer_image" style="opacity:1" >
            </div>
          </div>
        </div>
    </div>
    <div class="modal-footer">
      <button type="reset" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="submit" class="n-btn mb-0" value="Submit" name="offer_sub">Save</button>
    </div>
    </form>
  </div>
</div>
<div id="edit-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Edit Offer</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="col-12 p-0">
      <form method="post" id="offer_form" enctype="multipart/form-data">
        <input type="hidden" id="edit_id_offers_sb" name="edit_id_offers_sb">
        <div class="modal-body">
          <div class="row m-0 n-field-main">
            <p>Offer Title</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" id="edit_offer_heading" name="edit_offer_heading" required>
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Offer Type</p>
            <div class="col-sm-12 p-0 n-field-box">
                <select class="sel2" name="edit_offer_type" id="edit_offer_type" onchange="check_hide_status()" style="width:100%;" required>
                <option value="Text Type">Text</option>
                <option value="Image Type">Image</option>
                </select>
            </div>
          </div>
          <div class="row m-0 n-field-main edit_offer_text">
            <p>Offer Sub-Title</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" id="edit_offer_subheading" name="edit_offer_subheading">
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Offer Content</p>
            <div class="col-sm-12 p-0 n-field-box">
              <textarea name="edit_offer_content" id="edit_offer_content" style="width:100%;"></textarea>
            </div>
          </div>
          <div class="row m-0 n-field-main edit_offer_image">
            <div class="col-sm-6 p-0 n-field-box">
              <p>Change Offer Image</p>
              <input type="file" accept="image/*" name="offer_image" id="offer_image" style="opacity:1" >
            </div>
             <div class="col-sm-6 p-0 n-field-box">
               <p>Current Offer Image</p>
              <img id="current_offer_image" src="#" style="height:50px;width:auto;" />
            </div>
          </div>
        </div>
    </div>
    <div class="modal-footer">
      <button type="reset" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="submit" class="n-btn mb-0" value="Submit" name="offer_edit">Update</button>
    </div>
    </form>
  </div>
</div>
<!--<div id="newModal" class="modal hide" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" method="post">
                <div class="modal-header create">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3>New Offer</h3>
                </div>
                <div class="modal-body">
                    <div class="tabbable">
                    <div class="tab-content">
                        <form id="offer_form" class="form-horizontal" method="post" enctype="multipart/form-data">
                            <fieldset>
                                <div class="control-group">
                                    <label class="control-label" for="offer_heading">Offer Tittle</label>
                                    <div class="controls">
                                        <input type="text" class="span10" name="offer_heading" id="offer_heading" required="required">
                                    </div> 
                                </div> 
                                <div class="control-group">
                                    <label class="control-label" for="offer_type">Offer Type</label>
                                    <div class="controls">
                                        <select class="span10" name="offer_type_" id="offer_type_"   required="required" onchange="check_hide_status()">
                                            <option value="Text Type">Text</option>
                                            <option value="Image Type">Image</option>
                                        </select>
                                    </div> 
                                </div>
                                <section class="add_offer_text">
                                    <div class="control-group">
                                        <label class="control-label" for="offer_subheading">Offer Sub-Tittle</label>
                                        <div class="controls">
                                            <input type="text" class="span10" name="offer_subheading" id="offer_subheading" >
                                        </div> 
                                    </div> 
                                    <div class="control-group">
                                        <label class="control-label" for="offer_content">Offer Content</label>
                                        <div class="controls">
                                            <textarea class="span10" name="offer_content" id="offer_content" ></textarea>
                                        </div> 
                                    </div> 
                                </section>
                                <section class="add_offer_image">
                                    <div class="control-group">
                                        <label class="control-label" for="offer_image">Offer Image</label>
                                        <div class="controls">
                                            <input type="file" accept="image/*" class="span10" name="offer_image" id="offer_image" style="opacity:1" >
                                        </div> 
                                    </div>
                                </section>
                            </fieldset>

                    </div>
                </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn mm-btn pull-right" value="Submit" name="offer_sub">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="editModal" class="modal hide" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" method="post">
                <div class="modal-header update">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3>Edit Offer</h3>
                </div>
                <div class="modal-body">
                    <div class="widget-content">
                        <div class="tabbable">
                            <div class="tab-content">
                        <form id="area" class="form-horizontal" method="post" enctype="multipart/form-data">
                            <fieldset>
                                <div class="control-group">
                                    <label class="control-label" for="edit_offer_heading">Offer Tittle</label>
                                    <div class="controls">
                                        <input type="text" class="span10" name="edit_offer_heading" id="edit_offer_heading" required="required">
                                        <input type="hidden" class="span3" id="edit_id_offers_sb" name="edit_id_offers_sb">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="edit_offer_type">Offer Type</label>
                                    <div class="controls">
                                        <select class="span10" name="edit_offer_type" id="edit_offer_type"   required="required" onchange="check_hide_status()">
                                            <option value="Text Type">Text</option>
                                            <option value="Image Type">Image</option>
                                        </select>
                                    </div> 
                                </div>
                            <section class="edit_offer_text">
                                <div class="control-group">
                                    <label class="control-label" for="edit_offer_subheading">Offer Sub-Tittle</label>
                                    <div class="controls">
                                        <input type="text" class="span10" name="edit_offer_subheading" id="edit_offer_subheading" >
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="edit_offer_content">Offer Content</label>
                                    <div class="controls">
                                        <textarea class="span10" name="edit_offer_content" id="edit_offer_content" ></textarea>
                                    </div> 
                                </div>
                            </section>
                             <section class="edit_offer_image">
                                <div class="control-group">
                                    <label class="control-label" for="offer_image">Offer Image</label>
                                    <div class="controls">
                                        <input type="file" accept="image/*" class="span10" name="offer_image" id="offer_image" style="opacity:1" >
                                    </div> 
                                </div>
                            </section>
                            </fieldset>
                    </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn mm-btn pull-right" value="Submit" name="offer_edit">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>-->
<div id="enable-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Activate ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to activate this offer ?</h3>
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn mb-0" onclick="confirm_enable()">Activate</button>
    </div>
  </div>
</div>
<div id="disable-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Deactivate ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to deactivate this offer ?</h3>
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn mb-0" onclick="confirm_disable()">Deactivate</button>
    </div>
  </div>
</div>
<div class="row m-0">
    <div class="col-md-12">
        <div class="widget widget-table action-table">
            <div class="widget-header">
            <ul>
            <li>
            <i class="icon-th-list"></i>
                <h3>Offer List</h3>
                </li>
                <li class="mr-0 float-right"> 
                
                 
                
                <div class="topiconnew border-0 green-btn">
              	   <a onclick="newPopup()" title="Add"> <i class="fa fa-plus"></i></a>
              </div>
              </li>
              </ul>
              
            
              
              
              
              </div>
                
            </div>
            <style>
			.dataTables_wrapper table tr.even td.sorting_1 { background-color: none; }
			</style>
            <!-- /widget-header -->
            <div class="widget-content pt-0">
                <table id="offer-list-table" class="table da-table" cellspacing="0" width="100%">
<!--                <table class="table table-striped table-bordered">-->
                    <thead>
                        <tr>
                            <th style="line-height: 18px;width: 20px;"><center>No.</center></th>
                            <th style="line-height: 18px;width: 50px;">Status </th>
                            <th style="line-height: 18px;width: 100px;">Offer Heading </th>
                            <th style="line-height: 18px;width: 90px;">Offer Type</th>
                            <th style="line-height: 18px">Offer Subheading</th>
                            <th style="line-height: 18px">Offer Content</th>
                            <th style="line-height: 18px;width: 90px;">Offer Image</th>
                            <th style="line-height: 18px; width: 80px;" class="td-actions">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
if (count($offers) > 0) {
    $i = 1;
    foreach ($offers as $offers_val) {
        ?>
                                <tr>
                                    <td style="line-height: 18px;"><center><?php echo $i; ?></center></td>
                                    <td style="line-height: 18px;">
                                        <?php if ($offers_val['offer_status'] == 0) {?>
                                        <button onclick="confirm_disable_enable_modal(<?php echo $offers_val['id_offers_sb'] ?>,<?php echo $offers_val['offer_status'] ?>)"  class="n-btn  mb-0">Active</button><?php } else {?>
                                     <button onclick="confirm_disable_enable_modal(<?php echo $offers_val['id_offers_sb'] ?>,<?php echo $offers_val['offer_status'] ?>)"  class="n-btn red-btn mb-0">Disabled</button> <?php }?>
                                    </td>
                                    <td style="line-height: 18px"><?php echo $offers_val['offer_heading'] ?></td>
                                    <td style="line-height: 18px"><?php echo $offers_val['offer_type'] ?></td>
                                    <td style="line-height: 18px"><?php echo $offers_val['offer_subheading'] ?: '-' ?></td>
                                    <td style="line-height: 18px"><?php echo $offers_val['offer_content'] ?: '-' ?></td>
                                    <td style="line-height: 18px">
                                        <?php echo $offers_val['offer_type'] == 'Image Type' ? '<img src="' . base_url() . 'offer_img/uploads/' . $offers_val['offer_image'] . '" style="height:50px; width:auto; margin: 0 auto; border-radius: 5px;">' : '-'; ?>
                                    </td>
                                    <td style="line-height: 18px" class="td-actions">
										<a href="javascript:;" class="n-btn-icon purple-btn" onclick="edit_get(<?php echo $offers_val['id_offers_sb'] ?>);"><i class="btn-icon-only icon-pencil"> </i></a>
										<?php if (user_authenticate() == 1) {?>
											<a href="javascript:;" class="n-btn-icon red-btn" onclick="confirm_delete_modal(<?php echo $offers_val['id_offers_sb'] ?>);"><i class="btn-icon-only icon-remove"> </i></a>
										<?php }?>
									</td>
                                </tr>
                                <?php
$i++;
    }
}
?>

                    </tbody>
                </table>
            </div>
            <!-- /widget-content -->
        </div>
        <!-- /widget -->
    </div><!-- /span5 -->


</div>

<script>
function edit_offer(id_offers_sb) {
	//alert(zone_id);
	$.ajax({
		type: "POST",
		url: _base_url + "offers/edit_offers",
		data: {
			id_offers_sb: id_offers_sb
		},
		dataType: "text",
		cache: false,
		success: function(result) {
			//alert(result);
			var obj = jQuery.parseJSON(result);
			$.each($.parseJSON(result), function(edit, value) {
				//alert(JSON.stringify(value));
				$('#edit_offer_heading').val(value.offer_heading);
				$('#edit_id_offers_sb').val(value.id_offers_sb);
				$('#edit_offer_type').val(value.offer_type);
				$('#edit_offer_subheading').val(value.offer_subheading);
				$('#edit_offer_content').val(value.offer_content);
			});
			$('#add_area').hide();
			$('#edit_area').show();
			check_hide_status();
		}
	});
}
check_hide_status();

function check_hide_status() {
	if ($('#offer_type').val() == 'Text Type') {
		$('.add_offer_text').show();
		$('.add_offer_image').hide();
    $('#offer_content').parent().parent().show();
	} else {
		$('.add_offer_text').hide();
		$('.add_offer_image').show();
    $('#offer_content').parent().parent().hide();
	}
	if ($('#edit_offer_type').val() == 'Text Type') {
		$('.edit_offer_text').show();
		$('.edit_offer_image').hide();
    $('#edit_offer_content').parent().parent().show();
	} else {
		$('.edit_offer_text').hide();
		$('.edit_offer_image').show();
    $('#edit_offer_content').parent().parent().hide();
	}
}

function change_stat(id_offers_sb) {
	if (confirm('Are you sure you want to change ststus demo?')) {
		$.ajax({
			type: "POST",
			url: _base_url + "offers/change_statu",
			data: {
				id_offers_sb: id_offers_sb
			},
			dataType: "text",
			cache: false,
			success: function(result) {
				alert(result);
				window.location.assign(_base_url + 'offers');
			}
		});
	}
}
(function(a) {
	a(document).ready(function(b) {
		if (a('#offer-list-table').length > 0) {
			a("table#offer-list-table").dataTable({
				'sPaginationType': "full_numbers",
				"bSort": true,
				"scrollY": true,
				"orderMulti": false,
				'bFilter': true,
				"lengthChange": true,
				"pageLength": 100,
				'columnDefs': [{
					'targets': [6,-1],
					'orderable': false
				}, ]
			});
		}
	});
})(jQuery);

function confirm_delete_modal(id) {
	$('#delete_id').val(id);
	$.fancybox.open({
		autoCenter : true,
		fitToView : false,
		scrolling : false,
		openEffect : 'none',
		openSpeed : 1,
    autoSize: false,
     width:450,
     height:'auto',
		helpers : {
			overlay : {
				css : {
					'background' : 'rgba(0, 0, 0, 0.3)'
				},
				closeClick: false
			}
		},
		padding : 0,
		closeBtn : false,
		content: $('#delete-popup'),
  });
}

function confirm_delete() {
	$.ajax({
		type: "POST",
		url: _base_url + "offers/remove_offers",
		data: {
			id_offers_sb: $('#delete_id').val()
		},
		dataType: "text",
		cache: false,
		success: function(result) {
			location.reload();
		},
		error: function(data) {
			alert("Error !");
		}
	});
}

function edit_get(id) {
	$.ajax({
		type: "POST",
		url: _base_url + "offers/edit_offers",
		data: {
			id_offers_sb: id
		},
		dataType: "text",
		cache: false,
		success: function(result) {
			var obj = jQuery.parseJSON(result);
			$.each($.parseJSON(result), function(edit, value) {
				//alert(JSON.stringify(value));
				$('#edit_offer_heading').val(value.offer_heading);
				$('#edit_id_offers_sb').val(value.id_offers_sb);
				$('#edit_offer_type').val(value.offer_type).trigger('change');
				$('#edit_offer_subheading').val(value.offer_subheading);
				$('#edit_offer_content').val(value.offer_content);
        if(value.offer_image){
          $('#current_offer_image').attr('src', '<?= OFFER_IMG_ANDROID_ABSOLUTE_URL ?>'+value.offer_image).parent().show();
        }
        else{
          $('#current_offer_image').attr('src', '<?= OFFER_IMG_ANDROID_ABSOLUTE_URL ?>'+value.offer_image).parent().hide();
        }
			});
			$.fancybox.open({
				autoCenter: true,
				fitToView: false,
				scrolling: false,
				openEffect: 'none',
				openSpeed: 1,
				autoSize: false,
				width: 450,
				height: 'auto',
				helpers: {
					overlay: {
						css: {
							'background': 'rgba(0, 0, 0, 0.3)'
						},
						closeClick: false
					}
				},
				padding: 0,
				closeBtn: false,
				content: $('#edit-popup'),
			});
			check_hide_status();
		},
		error: function(data) {
			alert(data.statusText);
			console.log(data);
		}
	});
}

function confirm_disable() {
	$.ajax({
		type: "POST",
		url: _base_url + "offers/change_status",
		data: {
			id_offers_sb: disable_offerid
		},
		dataType: "text",
		cache: false,
		success: function(result) {
			alert(result);
			window.location.assign(_base_url + 'offers');
		},
		error: function(data) {
			alert(data.statusText);
			console.log(data);
		}
	});
}

function confirm_enable() {
	$.ajax({
		type: "POST",
		url: _base_url + "offers/change_status",
		data: {
			id_offers_sb: disable_offerid
		},
		dataType: "text",
		cache: false,
		success: function(result) {
			alert(result);
			window.location.assign(_base_url + 'offers');
		},
		error: function(data) {
			alert(data.statusText);
			console.log(data);
		}
	});
}

function confirm_disable_enable_modal(id, status) {
    disable_offerid = id;
	offer_status = status;
	if (status == 0) {
		$.fancybox.open({
		autoCenter : true,
		fitToView : false,
		scrolling : false,
		openEffect : 'none',
		openSpeed : 1,
    autoSize: false,
     width:450,
     height:'auto',
		helpers : {
			overlay : {
				css : {
					'background' : 'rgba(0, 0, 0, 0.3)'
				},
				closeClick: false
			}
		},
		padding : 0,
		closeBtn : false,
		content: $('#disable-popup'),
  });
	} else {
		$.fancybox.open({
		autoCenter : true,
		fitToView : false,
		scrolling : false,
		openEffect : 'none',
		openSpeed : 1,
    autoSize: false,
     width:450,
     height:'auto',
		helpers : {
			overlay : {
				css : {
					'background' : 'rgba(0, 0, 0, 0.3)'
				},
				closeClick: false
			}
		},
		padding : 0,
		closeBtn : false,
		content: $('#enable-popup'),
  });
	}
}
function closeFancy(){
  $.fancybox.close();
}
function newPopup(){
  $.fancybox.open({
		autoCenter : true,
		fitToView : false,
		scrolling : false,
		openEffect : 'none',
		openSpeed : 1,
    autoSize: false,
     width:450,
     height:'auto',
		helpers : {
			overlay : {
				css : {
					'background' : 'rgba(0, 0, 0, 0.3)'
				},
				closeClick: false
			}
		},
		padding : 0,
		closeBtn : false,
		content: $('#new-popup'),
  });
}
</script>
