<div id="delete-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Confirm Delete ?</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to delete this flat ?</h3>
      <input type="hidden" id="delete_flatid">
    </div>
    <div class="modal-footer">
      <button type="button" class="n-btn mb-0" data-dismiss="modal" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn red-btn mb-0" data-dismiss="modal" onclick="confirm_delete()">Delete</button>
    </div>
  </div>
</div>
<div id="new-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">New Flat</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div id="popup-booking" class="col-12 p-0">
      <form class="form-horizontal" method="post" id="add-new-zone-form">
        <div class="modal-body">
          <div class="row m-0 n-field-main">
            <p>Flat Name</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" id="flatname" name="flatname" autocomplete="off" required>
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Tablet IMEI</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" id="tablet_imei" name="tablet_imei" autocomplete="off" required>
            </div>
          </div>
        </div>
    </div>
    <div class="modal-footer">
      <button type="reset" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="submit" class="n-btn mb-0" value="Submit" name="flat_sub">Save</button>
    </div>
    </form>
  </div>
</div>
<div id="edit-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Edit Flat</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div id="popup-booking" class="col-12 p-0">
      <form class="form-horizontal" method="post" id="add-new-zone-form">
        <div class="modal-body">
          <div class="row m-0 n-field-main">
            <p>Flat Name</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text"  id="edit_flatname" name="edit_flatname" autocomplete="off" required>
              <input type="hidden" class="span3" id="edit_flatid" name="edit_flatid">
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Tablet IMEI</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" id="edit_tablet_imei" name="edit_tablet_imei" autocomplete="off" required>
            </div>
          </div>
        </div>
    </div>
    <div class="modal-footer">
      <button type="reset" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="submit" class="n-btn mb-0" value="Submit" name="flat_edit">Update</button>
    </div>
    </form>
  </div>
</div>
<div class="row m-0">  
    <div class="col-md-12">
    
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Flat</h3>
              
              <div class="topiconnew border-0 green-btn">
              	   <a onclick="newPopup()" title="Add"> <i class="fa fa-plus"></i></a>
              </div>
                    
            </div>
            <!-- /widget-header -->
             <div class="widget-content">
                <table id="flat-list-table" class="table table-striped table-bordered da-table">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; width: 20px;  text-align:center;"> Sl.No. </th>
                            <th style="line-height: 18px"> Flat </th>
                            <th style="line-height: 18px"> Tablet imei</th>
                            <th style="line-height: 18px" class="td-actions">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($flats) > 0) {
                            $i = 1;
                            foreach ($flats as $flats_val) {
                                ?>  
                        <tr>
                            <td style="line-height: 18px; width: 20px; text-align:center;"><?php echo $i; ?></td>
                            <td style="line-height: 18px"><?php echo $flats_val['flat_name'] ?></td>
                            <td style="line-height: 18px"><?php echo $flats_val['tablet_imei'] ?></td>
                            <td style="line-height: 18px" class="td-actions">
								<a href="javascript:;" class="n-btn-icon purple-btn" onclick="edit_get(<?php echo $flats_val['flat_id'] ?>);"><i class="btn-icon-only icon-pencil"> </i></a>
								<?php if(user_authenticate() == 1) {?>
								<a href="javascript:;" class="n-btn-icon red-btn" onclick="confirm_delete_modal(<?php echo $flats_val['flat_id'] ?>);"><i class="btn-icon-only icon-remove"> </i></a>
								<?php } ?>
							</td>
                        </tr>
                        <?php
                                $i++;
                            }
                        }
                        ?>   
                        
                    </tbody>
                </table>
            </div>
            <!-- /widget-content --> 
        </div>
        <!-- /widget --> 
    </div>
    <!-- /span6 --> 
</div>
<script>
/*********************************************************************************** */
(function (a) {
    a(document).ready(function (b) {
        if (a('#flat-list-table').length > 0) {
            a("table#flat-list-table").dataTable({
                'sPaginationType': "full_numbers",
                "bSort": true,
                "scrollY": true,
                "orderMulti": false,
                'bFilter':false,
                "lengthChange": false,
                "pageLength": 100,
                'columnDefs': [
                    {
                        'targets': [-1],
                        'orderable': false
                    },
                ]
            });
        }
    });
})(jQuery);
function confirm_delete_modal(id){
    $('#delete_flatid').val(id);
    $.fancybox.open({
		autoCenter : true,
		fitToView : false,
		scrolling : false,
		openEffect : 'none',
		openSpeed : 1,
    autoSize: false,
     width:450,
     height:'auto',
		helpers : {
			overlay : {
				css : {
					'background' : 'rgba(0, 0, 0, 0.3)'
				},
				closeClick: false
			}
		},
		padding : 0,
		closeBtn : false,
		content: $('#delete-popup'),
  });
}
function confirm_delete(){
        $.ajax({
            type: "POST",
            url: _base_url + "settings/remove_flat",
            data: { flat_id: $('#delete_flatid').val() },
            dataType: "text",
            cache: false,
            success: function (result) {
                location.reload();
            },
            error:function(data){
                alert(data.statusText);
                console.log(data);
            }
        });
}
function edit_get(id){
  $('.mm-loader').show();
    $.ajax({
        type: "POST",
        url: _base_url + "settings/edit_flat",
        data: { flat_id: id },
        dataType: "text",
        cache: false,
        success: function (result) {
            var obj = jQuery.parseJSON(result);
            $.each($.parseJSON(result), function (edit, value) {
                $('#edit_flatid').val(value.flat_id);
                $('#edit_flatname').val(value.flat_name);
                $('#edit_tablet_imei').val(value.tablet_imei);
            });
             $.fancybox.open({
		autoCenter : true,
		fitToView : false,
		scrolling : false,
		openEffect : 'none',
		openSpeed : 1,
    autoSize: false,
     width:450,
     height:'auto',
		helpers : {
			overlay : {
				css : {
					'background' : 'rgba(0, 0, 0, 0.3)'
				},
				closeClick: false
			}
		},
		padding : 0,
		closeBtn : false,
		content: $('#edit-popup'),
  });
  $('.mm-loader').hide();
        },
        error:function(data){
          $('.mm-loader').hide();
            alert(data.statusText);
            console.log(data);
        }
    });
}
function closeFancy(){
  $.fancybox.close();
}
function newPopup(){
  $.fancybox.open({
		autoCenter : true,
		fitToView : false,
		scrolling : false,
		openEffect : 'none',
		openSpeed : 1,
    autoSize: false,
     width:450,
     height:'auto',
		helpers : {
			overlay : {
				css : {
					'background' : 'rgba(0, 0, 0, 0.3)'
				},
				closeClick: false
			}
		},
		padding : 0,
		closeBtn : false,
		content: $('#new-popup'),
  });
}
/*********************************************************************************** */
</script>