<style type="text/css">
    .widget .widget-header{margin-bottom: 0px;}
	.container.booking-fl-box{width: 100% !important;}
</style>
<div class="row m-0">   
    <div class="col-md-12">
    
        <div class="widget widget-table action-table" style="margin-bottom:30px">
            <div class="widget-header"> 
                <form class="form-horizontal" method="post" action="<?php echo base_url() . 'reports/schedule-report' ?>">
                    <ul>
                    <li>
                    <i class="icon-th-list"></i>
                    <h3>Schedule Report</h3> 
                    </li>
                    <li>
					<input type="text" readonly="readonly" style="width: 160px;" id="OneDayDate" name="search_date" value="<?php echo $payment_date ?>">
                    </li>
                    <li>
					<input type="submit" class="n-btn" value="Go" name="add_payment">
                    </li>
					<li>
                    <div class="topiconnew border-0 green-btn">
                    	<a href="<?php echo base_url(); ?>reports/schedule_report_excel/<?php echo $servicedate; ?>" title="Download to Excel"> <i class="fa fa-download"></i></a>
                    </div>
                    </li>
                    </ul>
                </form>   
                
            </div>

            <div class="widget-content" style="margin-bottom:30px">
                <table id="da-ex-datatable-numberpaging" class="table da-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th style="line-height: 18px;"><center>Sl No</center></th>
                            <th style="line-height: 18px">Name</th>
                            <th style="line-height: 18px">Phone</th>
                            <th style="line-height: 18px">Email</th>
                            <th style="line-height: 18px">Date of Service</th>
                            <th style="line-height: 18px">Booking Type</th>
                            <th style="line-height: 18px">Booking Source</th>
                            <th style="line-height: 18px">Cleaner</th>
                            <th style="line-height: 18px">Amount</th>
                            <th style="line-height: 18px">Status</th>
                            <th style="line-height: 18px">Star Rating</th>
                            <th style="line-height: 18px">Comments</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        <?php
                        if(!empty($reports))
                        {
                            $i = 0;
							$totamount = 0;
                            foreach ($reports as $report)
                            {
								$totamount += $report->total_amount;
						?>
							<tr>
								<td style="line-height: 18px; text-align:center;"><center><?php echo ++$i; ?></center></td>
								<td style="line-height: 18px;"><?php echo $report->customer_name; ?></td>
								<td style="line-height: 18px;"><?php echo $report->mobile_number_1; ?></td>
								<td style="line-height: 18px;"><?php echo $report->email_address; ?></td>
								<td style="line-height: 18px;"><?php echo $payment_date ?></td>
								<td style="line-height: 18px;">
									<?php
									if($report->booking_type == "OD")
									{
										echo "One Day";
									} else if($report->booking_type == "WE"){
										echo "Weekly";
									} else if($report->booking_type == "BW"){
										echo "Bi-Weekly";
									}
									?>
								</td>
								<td style="line-height: 18px;"><?php echo $report->customer_source; ?></td>
								<td style="line-height: 18px;"><?php echo $report->maid_name; ?></td>
								<td style="line-height: 18px;"><?php echo $report->total_amount; ?></td>
								<td style="line-height: 18px;">
								<?php
								if($report->service_status == 1)
								{
									echo "In Progress";
								} else if($report->service_status == 2){
									echo "Finished";
								} else if($report->service_status == 3){
									echo "Cancelled";
								} else {
									echo "Not Started";
								}
								?>
								</td>
								<td style="line-height: 18px; text-align: center;">
									<?php
									if($report->rating > 0)
									{
										echo $report->rating;
									}
									?>
								</td>
								<td style="line-height: 18px;"><?php echo $report->comments; ?></td>
                            </tr>
						<?php
                            }
                        }
                        
                        ?>
                    </tbody>
					<tfoot>
						<tr>
							<td style="line-height: 18px;"></td>
							<td style="line-height: 18px;"></td>
							<td style="line-height: 18px;"></td>
							<td style="line-height: 18px;"></td>
							<td style="line-height: 18px;"></td>
							<td style="line-height: 18px;"></td>
							<td style="line-height: 18px;"></td>
							<td style="line-height: 18px;">Total</td>
							<td style="line-height: 18px;"><?php echo $totamount; ?></td>
							<td style="line-height: 18px;"></td>
							<td style="line-height: 18px;"></td>
							<td style="line-height: 18px;"></td>
						</tr>
					</tfoot>
                </table>
            </div><!-- /widget-content --> 

        </div><!-- /widget --> 
    </div><!-- /span12 --> 
</div>

