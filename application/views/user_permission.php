<div class="row m-0">
    <div class="col-sm-12">
        <div class="widget ">
            <div class="widget-header mb-0">
            <ul>
            <li>
                <i class="icon-globe"></i>
                <h3>Edit User - <?php echo $user->user_fullname ?></h3>
                </li>
                <li class="mr-0 float-right">
                
                <div class="topiconnew border-0 green-btn"> <a href="<?php echo base_url() . 'users/'; ?>" title="Users"> <i class="fa fa-user-plus"></i></a> </div></li>
                </ul>
            </div>

            <div class="widget-content">
                <div class="tabbable">
                    <div class="tab-content">
                        <form class="permission-list" method="post" action="<?php echo base_url() . 'users/permissions/' . $user_id ?>">
                            <fieldset>
                            <div class="col-sm-12">
                                         <?php if ($this->session->flashdata('success_msg') || $this->session->flashdata('error_msg')) {?>
                                <div class="control-group">
                                    <div class="controls">
                                        <div class="alert <?php echo $this->session->flashdata('success_msg') ? 'alert-success' : ''; ?>">
                                            <button type="button" class="close" data-dismiss="alert">×</button>
                                            <?php echo $this->session->flashdata('success_msg') ? $this->session->flashdata('success_msg') : $this->session->flashdata('error_msg'); ?>
                                        </div>
                                    </div>
                                </div>


                                <?php }?>
                                        <div class="control-group no-bottom-margin">
                                    <div class="controls n-radi-check-main">
                                        <input type="checkbox" id="select-all" name="selectall">
                                        <label class="radio inline permission-select-all"  style="background: #8793D6; padding-left: 5px !important; color: #FFF; border-radius: 3px; overflow: hidden;" for="select-all"><span class="border-radius-3"></span> <p style="color: #FFF;">Select All</p></label>
                                    </div>
                                </div>
                                <hr>

                                          <?php
foreach ($modules as $module) {
    //print_r($e_user);exit;
    echo '<div class="col-md-4 n-radi-check-main">';
    echo form_checkbox(array('name' => 'permissions[]', 'value' => $module['module_id'], 'id' => 'parent-' . $module['module_id'], 'tabindex' => 3, 'class' => 'parent', 'checked' => set_checkbox('permissions', 1, !empty($user_modules) ? (in_array($module['module_id'], $user_modules) ? 1 : 0) : 0)));
    echo '<label for="parent-' . $module['module_id'] . '" class="radio inline "><span class="border-radius-3"></span> <p> ' . ucwords($module['module_name']) . '</p></label>';
    echo '</div>';
    if (isset($module['sub_modules'])) {
        echo '<div class="">';
        foreach ($module['sub_modules'] as $sub_module) {
            echo '<div class="col-md-4 n-radi-check-main">';
            echo form_checkbox(array('name' => 'permissions[]', 'value' => $sub_module['module_id'], 'id' => 'child-' . $sub_module['module_id'], 'tabindex' => 3, 'class' => 'child-' . $sub_module['parent_id'], 'onclick' => 'checkChildModules(this)', 'checked' => set_checkbox('permissions', 1, !empty($user_modules) ? (in_array($sub_module['module_id'], $user_modules) ? 1 : 0) : 0)));
            echo '<label  for="child-' . $sub_module['module_id'] . '" class="radio inline"><span class="border-radius-3"></span> <p>' . ucwords($sub_module['sub_module']) . '</p></label>';
            echo '</div>';
        }
        echo '</div>';
    }
}
?>

                                    <div class="clear"></div>


                              </div>


                                <div class="col-sm-12 mt-4">
                                    <input type="submit" class="n-btn" value="Submit" name="update_user">

                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>