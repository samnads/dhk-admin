<div class="row">
        
    <div class="span7" style="float: left; margin-left: 15px;">
        <div class="widget widget-table action-table">
            <div class="widget-header">
            <ul>
            <li><i class="icon-th-list"></i>
              <h3>Price List</h3>
              </li>
              <li>
             
             
              <div class="topiconnew border-0 green-btn">
                       <a onclick="add_souq_price();" title="Add Services"> <i class="fa fa-user-plus"></i></a>
                    </div> 
                    
              </li>
              </ul>
                    
            </div>
            <!-- /widget-header -->
             <div class="widget-content">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; width: 20px"> Sl.No. </th>
                            <th style="line-height: 18px"> Date </th>
                            <th style="line-height: 18px"> Rate</th>
                            <th style="line-height: 18px" class="td-actions">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($prices) > 0) {
                            $i = 1;
                            foreach ($prices as $prices_val) {
                                ?>  
                        <tr>
                            <td style="line-height: 18px; width: 20px"><?php echo $i; ?></td>
                            <td style="line-height: 18px"><?php echo $prices_val['offer_date'] ?></td>
                            <td style="line-height: 18px"><?php echo $prices_val['offer_price'] ?></td>
                            <td style="line-height: 18px" class="td-actions">
								<a href="javascript:;" class="btn btn-small btn-warning" onclick="edit_souq_price(<?php echo $prices_val['id'] ?>);"><i class="btn-icon-only icon-pencil"> </i></a>
							</td>
                        </tr>
                        <?php
                                $i++;
                            }
                        }
                        ?>   
                        
                    </tbody>
                </table>
            </div>
            <!-- /widget-content --> 
        </div>
        <!-- /widget --> 
    </div>
    <!-- /span6 --> 
    <div class="span5" id="add_souqprice" style="display: none; float: left; margin-left: 15px;">      		
	<div class="widget ">
	    <div class="widget-header">
	      	<i class="icon-cogs"></i>
                    <h3>Add SouqMaid Price</h3>
                    <a style="float:right ; margin-right:15px; cursor:pointer;" onclick="hideadd_souq_price();"><img src="<?php echo base_url();?>img/minus.png" title="Hide"/></a>
            </div> <!-- /widget-header -->
	
            <div class="widget-content">
		<div class="tabbable">
                    <div class="tab-content">
<!--                        <form id="zone" class="form-horizontal" method="post" action="<?php //echo base_url();?>zones/add_zone">-->
                        <form id="area" class="form-horizontal" method="post">
                            <fieldset>
                                    <div class="control-group">											
					<label class="control-label" for="flatname">Service Date</label>
                                            <div class="controls">
                                                <input type="text" class="span3" id="service_date_val" name="service_date_val" readonly="readonly" required="required">
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
										
                                    <div class="control-group">											
					<label class="control-label" for="tablet_imei">Service Rate</label>
                                            <div class="controls">
                                                <input type="text" class="span3" id="service_rate" name="service_rate" required="required">
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
                                    
                                    
                                   
										
                                    <br />
										
                                    <div class="form-actions">
                                        <input type="submit" class="btn mm-btn pull-right" value="Submit" name="services_rate_sub">
<!--					<button class="btn">Cancel</button>-->
                                    </div> <!-- /form-actions -->
                            </fieldset>
                        </form>
			
                    </div>
		</div>
            </div> <!-- /widget-content -->
	</div> <!-- /widget -->
    </div> <!-- /span6 -->
    <div class="span5" id="edit_add_souqprice" style="display: none;">      		
	<div class="widget ">
	    <div class="widget-header">
	      	<i class="icon-cogs"></i>
                    <h3>Edit SouqMaid Price</h3>
                    <a style="float:right ; margin-right:15px; cursor:pointer;" onclick="hideedit_souq_price();"><img src="<?php echo base_url();?>img/minus.png" title="Hide"/></a>
            </div> <!-- /widget-header -->
	
            <div class="widget-content">
		<div class="tabbable">
                    <div class="tab-content">
<!--                        <form id="zone" class="form-horizontal" method="post" action="<?php //echo base_url();?>zones/add_zone">-->
                        <form id="area" class="form-horizontal" method="post">
                            <fieldset>
                                    <div class="control-group">											
					<label class="control-label" for="flatname">Service Date</label>
                                            <div class="controls">
                                                <input type="text" class="span3" id="edit_service_date_val" name="edit_service_date_val" readonly="readonly" required="required">
                                                <input type="hidden" class="span3" id="edit_service_offer_id" name="edit_service_offer_id">
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
										
                                    <div class="control-group">											
					<label class="control-label" for="tablet_imei">Service Rate</label>
                                            <div class="controls">
                                                <input type="text" class="span3" id="edit_service_rate" name="edit_service_rate" required="required">
                                            </div> <!-- /controls -->				
                                    </div> <!-- /control-group -->
                                    
                                    
                                   
										
                                    <br />
										
                                    <div class="form-actions">
                                        <input type="submit" class="btn mm-btn pull-right" value="Submit" name="services_rate_edit">
<!--					<button class="btn">Cancel</button>-->
                                    </div> <!-- /form-actions -->
                            </fieldset>
                        </form>
			
                    </div>
		</div>
            </div> <!-- /widget-content -->
	</div> <!-- /widget -->
    </div> <!-- /span6 -->
</div>