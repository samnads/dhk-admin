<div id="new-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">New Zone</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div id="popup-booking" class="col-12 p-0">
      <form class="form-horizontal" method="post" id="add-new-zone-form">
        <div class="modal-body">
          <div class="row m-0 n-field-main">
            <p>Zone Name</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" name="zonename" autocomplete="off" required>
            </div>
          </div>
          <div class="row m-0 n-field-main">
            <p>Driver Name</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" name="drivername" autocomplete="off" required>
            </div>
          </div>
          <div class="row m-0">
            <div class="col-12">
              <p>Is Spare Zone</p>
            </div>
            <div class="col-sm-6 n-radi-check-main p-0">
              <input type="radio" value="Y" id="spare-yes" name="spare" class="" required>
              <label for="spare-yes"><span class="border-radius-3"></span> <p>Yes</p> </label>
            </div>
            <div class="col-sm-6 n-radi-check-main p-0">
              <input type="radio" value="N" id="spare-no" name="spare" class="" checked>
              <label for="spare-no"><span class="border-radius-3"></span> <p>No</p> </label>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="reset" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
          <button type="submit" class="n-btn mb-0" value="Submit" name="zone_sub">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>
<div id="edit-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Edit Zone</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div id="popup-booking" class="col-12 p-0">
      <form class="form-horizontal" method="post" id="add-new-zone-form">
        <div class="modal-body">
          <div class="row m-0 n-field-main">
            <p>Zone Name</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" name="edit_zonename" id="edit_zonename" autocomplete="off" required>
              <input type="hidden" name="edit_zoneid" id="edit_zoneid">
            </div>
          </div>

          <div class="row m-0 n-field-main">
            <p>Driver Name</p>
            <div class="col-sm-12 p-0 n-field-box">
              <input type="text" name="edit_drivername" id="edit_drivername" autocomplete="off" required>
            </div>
          </div>
          <div class="row m-0">
            <div class="col-12">
              <p>Is Spare Zone</p>
            </div>
            <div class="col-sm-6 n-radi-check-main p-0">
              <input type="radio" value="Y" id="R_NAME_yes" name="edit_spare" class="" required>
              <label for="R_NAME_yes">
                <span class="border-radius-3"></span> <p>Yes</p> </label>
            </div>
            <div class="col-sm-6 n-radi-check-main p-0">
              <input type="radio" value="N" id="R_NAME_no" name="edit_spare" class="">
              <label for="R_NAME_no">
                <span class="border-radius-3"></span> <p>No</p> </label>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="reset" class="n-btn red-btn mb-0" onclick="closeFancy()">Cancel</button>
          <button type="submit" class="n-btn mb-0" value="Submit" name="zone_edit">Update</button>
        </div>
      </form>
    </div>
  </div>
</div>
<div id="delete-popup" style="display:none;">
  <div class="popup-main-box">
    <div class="col-md-12 col-sm-12 green-popup-head">
      <span id="b-maid-name">Delete Zone</span>
      <span id="b-time-slot"></span>
      <span class="pop_close n-close-btn">&nbsp;</span>
    </div>
    <div class="modal-body">
      <h3>Are you sure you want to delete this zone ?</h3>
      <input type="hidden" id="delete_zoneid">
    </div>
    <div class="modal-footer">
      <input type="hidden" name="delete_areaid" id="delete_areaid">
      <button type="button" class="n-btn mb-0" onclick="closeFancy()">Cancel</button>
      <button type="button" class="n-btn red-btn mb-0" onclick="confirm_delete()">Delete</button>
    </div>
  </div>
</div>
<div class="row m-0">
    <div class="col-md-12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Zones List</h3>

              <div class="topiconnew border-0 green-btn">
              	   <a onclick="newModal()" title="Add"> <i class="fa fa-plus"></i></a>
              </div>

            </div>
            <!-- /widget-header -->
             <div class="widget-content">

                <table id="zone-list-table" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th style="line-height: 18px; width: 20px;  text-align:center;"> Sl.No. </th>
                            <th style="line-height: 18px"> Zone Name </th>
                            <th style="line-height: 18px"> Driver Name</th>
                            <th style="line-height: 18px" class="td-actions">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
if (count($zones) > 0) {
    $i = 1;
    foreach ($zones as $zones_val) {
        ?>
                        <tr>
                            <td style="line-height: 18px; width: 20px;  text-align:center;"><?php echo $i; ?></td>
                            <td style="line-height: 18px"><?php echo $zones_val['zone_name'] ?></td>
                            <td style="line-height: 18px"><?php echo $zones_val['driver_name'] ?></td>
                            <td style="line-height: 18px" class="td-actions">
								<a href="javascript:;" class="n-btn-icon purple-btn" onclick="edit_zone_get(<?php echo $zones_val['zone_id'] ?>);"><i class="btn-icon-only icon-pencil"> </i></a>
								<?php if (user_authenticate() == 1) {?>
								<a href="javascript:;" class="n-btn-icon red-btn" onclick="confirm_delete_modal(<?php echo $zones_val['zone_id'] ?>);"><i class="btn-icon-only icon-remove"> </i></a>
								<?php }?>
							</td>
                        </tr>
                        <?php
$i++;
    }
}
?>

                    </tbody>
                </table>

            </div>
            <!-- /widget-content -->
        </div>
        <!-- /widget -->
    </div>
    <!-- /span6 -->
    <div class="span6" id="edit_zone" style="display: none;">

    </div> <!-- /span6 -->

</div>
<script>
/*********************************************************************************** */
(function (a) {
    a(document).ready(function (b) {
        if (a('#zone-list-table').length > 0) {
            a("table#zone-list-table").dataTable({
                'sPaginationType': "full_numbers",
                "bSort": true,  "scrollY": true,
                "orderMulti": false,
                'bFilter':true,
                "lengthChange": false,
                'columnDefs': [
                    {
                        'targets': [-1],
                        'orderable': false
                    },
                ]
            });
        }
    });
})(jQuery);
function edit_zone_get(zone_id){
  $('.mm-loader').show();
    $.ajax({
        type: "POST",
        url: _base_url + "settings/edit_zone",
        data: { zone_id: zone_id },
        dataType: "text",
        cache: false,
        success: function (result) {
            //alert(result);
            var obj = jQuery.parseJSON(result);
            $.each($.parseJSON(result), function (edit, value) {
                $('#edit_zoneid').val(value.zone_id)
                $('#edit_zonename').val(value.zone_name)
                $('#edit_drivername').val(value.driver_name)
                $('input[value="' + value.spare_zone + '"]').prop('checked', true);
            });
            $.fancybox.open({
                autoCenter : true,
                fitToView : false,
                scrolling : false,
                openEffect : 'none',
                openSpeed : 1,
                autoSize: false,
                width:450,
                height:'auto',
                helpers : {
                  overlay : {
                    css : {
                      'background' : 'rgba(0, 0, 0, 0.3)'
                    },
                    closeClick: false
                  }
                },
                padding : 0,
                closeBtn : false,
                content: $('#edit-popup'),
              });
              $('.mm-loader').hide();
        },
        error:function(data){
          $('.mm-loader').hide();
            alert(data.statusText);
            console.log(data);
        }
    });
}
function closeFancy(){
  $.fancybox.close();
}
function confirm_delete_modal(zone_id){
    $('#delete_zoneid').val(zone_id);
  $.fancybox.open({
		autoCenter : true,
		fitToView : false,
		scrolling : false,
		openEffect : 'none',
		openSpeed : 1,
    autoSize: false,
     width:450,
     height:'auto',
		helpers : {
			overlay : {
				css : {
					'background' : 'rgba(0, 0, 0, 0.3)'
				},
				closeClick: false
			}
		},
		padding : 0,
		closeBtn : false,
		content: $('#delete-popup'),
  });
}
function confirm_delete(){
     $.ajax({
            type: "POST",
            url: _base_url + "settings/remove_zone",
            data: { zone_id: $('#delete_zoneid').val() },
            dataType: "text",
            cache: false,
            success: function (result) {
                if (result == 1) {
                    alert("Cannot Delete! This Zones has Areas");
                }
                else {
                    alert("Zone Deleted Successfully");
                    setTimeout(location.reload.bind(location), 1);
                }
            },
            error:function(data){
                alert(data.statusText);
                console.log(data);
            }
        });
}
function newModal(){
  $.fancybox.open({
		autoCenter : true,
		fitToView : false,
		scrolling : false,
		openEffect : 'none',
		openSpeed : 1,
    autoSize: false,
     width:450,
     height:'auto',
		helpers : {
			overlay : {
				css : {
					'background' : 'rgba(0, 0, 0, 0.3)'
				},
				closeClick: false
			}
		},
		padding : 0,
		closeBtn : false,
		content: $('#new-popup'),
  });
}
/*********************************************************************************** */
</script>
