<style type="text/css">
    .select2-arrow{visibility : hidden;}
	.select2-container .select2-choice{
	-moz-appearance: none;
    background: #fff url("../images/ash-arrow.png") no-repeat scroll right 7px top 11px;
   border: 1px solid #ccc;
    border-radius: 3px;
    cursor: pointer;
    font-size: 12px;
    height: 32px;
    line-height: 24px;
   padding: 3px 0 3px 10px;

    text-indent: 0.01px;
	}
        
    #user-repeat-days label{
		display:inline;
	}
	.in-bookingform-field-droop-main label{
		display:inline;
	}
	#user-repeat-end-date {
		width: 100%;
	}
	
	.in-bookingform-field-droop-main {
  width: 100%;}
  
  .select2-container {width: 100% !important;}
  label { margin-bottom: 0px;}
  
  
  #customer-detail-popup .n-field-box .in-bookingform-field-droop-main {
  font-weight: 600;
  font-family: Arial, Helvetica, sans-serif;
}

#user-repeat-days label {
  margin-right: 0;
}
   
</style>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC_hBALevP_xk8OIkxyGQjt_oInuAW3Ivk&amp;libraries=places"></script>
<section>
    <div class="row dash-top-wrapper m-0">

            <div class="col-sm-12 p-0">
               
               
                <div class="widget-header" style="margin-bottom: 0;"> 
                        <form class="form-horizontal" method="POST" action="#">
                        <ul>
                        <li>
                            <i class="icon-th-list"></i>
                            <h3>New Booking</h3> 
                            </li>
                            </ul>
                        </form> 
                </div>
                
                
                
                    <div class="col-md-12 col-sm-12 confirm-det-cont-box  borderbox pl-3 pb-3 pr-3">
                        <?php echo form_open('', array('id' => 'add-user-booking-form')); ?>
                        <div class="row m-0 n-field-main">
                                <div id="u-error" style="color:red;"></div>
                            </div>
                            
                        <div class="col-sm-4">
                        
                        
                            
                            
                            
                            <div class="row m-0 n-field-main">
                                <p>
                                    <?php echo form_label('Customer ', 'u_customer_id',  array( 'class' => '')); ?>
                                </p>
                                <div class="col-sm-12 p-0 n-field-box">
                                    
                                    <div class="in-bookingform-field-droop-main">
                                        <select name="u_customer_id" id="u_customer_id" data-placeholder="Select customer" class="">
                                            <option></option>
                                            <?php
                                            // foreach($customers as $customer)
                                            // {  
                                                // if($customer->payment_type == "D")
                                                // {
                                                    // $paytype = "(Daily)";
                                                // } else if($customer->payment_type == "W")
                                                // {
                                                    // $paytype = "(Weekly)";
                                                // } else if($customer->payment_type == "M")
                                                // {
                                                    // $paytype = "(Monthly)";
                                                // } else
                                                // {
                                                    // $paytype = "";
                                                // } 
                                                // $p_number = array();
                                                // if($customer->phone_number != NULL)
                                                    // array_push ($p_number, $customer->phone_number);
                                                // if($customer->mobile_number_1 != NULL)
                                                    // array_push ($p_number, $customer->mobile_number_1);
                                                // if($customer->mobile_number_2 != NULL)
                                                    // array_push ($p_number, $customer->mobile_number_2);
                                                // if(@$customer->mobile_number_3 != NULL)
                                                    // array_push ($p_number, @$customer->mobile_number_3);

                                                // $phone_number = !empty($p_number) ? ' - ' . join(', ', $p_number) : '';

                                                // echo '<option value="' . $customer->customer_id . '">' . html_escape($customer->customer_nick_name). $phone_number ." ".$paytype.'</option>';
                                            // }

                                            ?>
                                        </select>
                                        <div id="customer-picked-address-u"></div>
                                    </div>
                                </div>
                            </div>
                            
                            
                            <div id="customer-address-panel-user">
                                <div class="head">Pick One Address <span class="close">Close</span></div>

                                <div class="inner">
                                        Loading<span class="dots_loader"></span>
                                </div>			
                            </div>
                            
                            
                            
                            <div class="row m-0 n-field-main">
                                <p>
                                    <?php echo form_label('Service Type ', 'user_service_type_id',  array( 'class' => '')); ?>
                                </p>
                                <div class="col-sm-12 p-0 n-field-box">
                                    <div class="in-bookingform-field-droop-main">
                                        <select name="user_service_type_id" id="user_service_type_id" data-placeholder="Select service type" class="sel3">
                                            <option></option>
                                            <?php
                                            foreach($service_types as $service_type)
                                            {
                                                echo '<option value="' . $service_type->service_type_id . '">' . html_escape($service_type->service_type_name) . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            <div class="row m-0 n-field-main">
                                <p>
                                    <?php echo form_label('Service Start Date ', 'user_date',  array( 'class' => '')); ?>
                                </p>
                                <div class="col-sm-12 p-0 n-field-box">
                                    <div class="in-bookingform-field-droop-main">
                                         <?php echo form_input(array('name' => 'user_date', 'id' => 'user_date', 'maxlength' => 10, 'tabindex' => 7, 'class' => 'in-bookingform-field', 'value' => '', 'autocomplete' => 'off')); ?>
                                    </div>
                                </div>
                            </div>
                            
                            
                            
                            
                            
                            <div class="row m-0 n-field-main">
                                
                                    <!--<?php //echo form_label('Time ', 'user_time',  array( 'class' => '')); ?>-->
                               
                                <div class="col-sm-12 p-0">
                                    
                                        <div class="col-md-6 col-sm-6 no-left-padding">
                                             <p>From Time</p>
                                             <select name="time_from" id="b-from-time" data-placeholder="Select" class="sel3 small mr15 pull-right">
                                                        <option></option>
                                                        <?php
                                                        foreach($times as $time_index=>$time)
                                                        {
                                                            echo '<option value="' . $time->stamp . '">' . $time->display . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                        </div>
                                        <div class="col-md-6 col-sm-6 no-right-padding">
                                             <p>To Time</p>
                                             <select name="time_to" id="b-to-time" data-placeholder="Select" class="sel3 small pull-right"  >
                                                        <option></option>
                                                        <?php
                                                        foreach($times as $time_index=>$time)
                                                        {
                                                            if($time_index == 't-0')
                                                            {                                                                                
                                                                    continue;
                                                            }
                                                            echo '<option value="' . $time->stamp . '">' . $time->display . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                        </div>
                                </div>
                            </div>
                            
                            
                            
                            
                            
                            <div class="row m-0 n-field-main">
                                <p>
                                    <?php echo form_label('Repeats ', 'user_booking_type',  array( 'class' => '')); ?>
                                </p>
                                <div class="col-sm-12 p-0 n-field-box">
                                    <div class="in-bookingform-field-droop-main">
                                        <select name="user_booking_type" id="user_booking_type" data-placeholder="Select repeat type" class="sel3">
                                            <option></option>
                                            <option value="OD"><?= $settings->od_booking_text; ?></option>
                                            <option value="WE"><?= $settings->we_booking_text; ?></option>
                                            <option value="BW"><?= $settings->bw_booking_text; ?></option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            
                            
                            
                            
                            
                            
                           
                            
                            
                            
       <div class="row m-0 n-field-main" id="user-repeat-days">
          <p><?php echo form_label('Repeat On ', 'user_w_day',  array( 'class' => '')); ?></p>
          <div class="col-sm-12 pl-0 pr-0 n-field-box n-field-day">
               
               <div class="n-days">
                	<input id="user-repeat-on-0" type="checkbox" value="0" name="user-w_day[]" class="w_day">
                	<label for="user-repeat-on-0"> <span class="border-radius-3"></span><p>Sun</p></label>
               </div>
               
               <div class="n-days">
                	<input id="user-repeat-on-1" type="checkbox" value="1" name="user-w_day[]" class="w_day">
                	<label for="user-repeat-on-1"> <span class="border-radius-3"></span><p>Mon</p></label>
               </div>
               
               <div class="n-days">
                	<input id="user-repeat-on-2" type="checkbox" value="2" name="user-w_day[]" class="w_day">
                	<label for="user-repeat-on-2"> <span class="border-radius-3"></span><p>Tue</p></label>
               </div>
               
               <div class="n-days">
                	<input id="user-repeat-on-3" type="checkbox" value="3" name="user-w_day[]" class="w_day">
                	<label for="user-repeat-on-3"> <span class="border-radius-3"></span><p>Wed</p></label>
               </div>
               
               <div class="n-days">
                	<input id="user-repeat-on-4" type="checkbox" value="4" name="user-w_day[]" class="w_day">
                	<label for="user-repeat-on-4"> <span class="border-radius-3"></span><p>Thu</p></label>
               </div>
               
               <div class="n-days">
               	  <input id="user-repeat-on-5" type="checkbox" value="5" name="user-w_day[]" class="w_day">
                	<label for="user-repeat-on-5"> <span class="border-radius-3"></span><p>Fri</p></label>
               </div>
               
               <div class="n-days pr-0">
               	  <input id="user-repeat-on-6" type="checkbox" value="6" name="user-w_day[]" class="w_day">
               	  <label for="user-repeat-on-6"> <span class="border-radius-3"></span><p>Sat</p></label>
               </div>
          
              <div class="clear"></div>
          </div>
        </div>
                            
                            
                            
                            
                            
                               
                            
       <div class="row m-0 n-field-main" id="user-repeat-ends">
          
          <div class="col-sm-6 pl-0 n-field-box n-end-main">
          <p>Never Ends</p>
          
          	   <div class="n-end">
                	<input id="user-repeat-end-never" type="radio" value="never" name="user_repeat_end" class="" checked>
                    <label for="user-repeat-end-never"> <span class="border-radius-3"></span></label>
               </div>
          </div>
          
          <div class="col-sm-6 pr-0 n-field-box"> 
          <p>Ends</p> 
          <div class="n-end">
               <input id="user-repeat-end-ondate" type="radio" value="ondate" name="user_repeat_end" class="">
               <label for="user-repeat-end-ondate"> <span class="border-radius-3"></span></label>
          </div>  
          <div class="n-end-field">
               <input type="text" class="user_end_datepicker" id="user-repeat-end-date" data-date-format="dd/mm/yyyy" readonly="readonly" disabled="disabled" />
          </div>
          </div>
        </div>
        
        
        
        
                            
                            
                         </div>
                         
                         <div class="col-sm-4">   
                            
                            
                            
                            
                            <div class="row m-0 n-field-main">
                                <p>
                                    <?php echo form_label('Cleaning Type ', 'user_cleaning_materials',  array( 'class' => '')); ?>
                                </p>
                                <div class="col-sm-12 p-0 n-field-box">
                               
                                        
                                  <!--<input name="user_cleaning_materials" value="Y" id="user_cleaning_materials" class="" type="checkbox">
                                  <label for="user_cleaning_materials"> <span></span> &nbsp; Yes</label>-->
                                  
                                  
                                  <div class="switch-main">         
                  <label class="switch">
                     <input type="checkbox">
                     <span class="slider round"></span>
                  </label>
              </div>
                         
                                </div>
                            </div>
                            
                            
                            
                            <div class="row m-0 n-field-main">
                                <p>
                                    <?php echo form_label('Total Amount ', 'user_total_amt',  array( 'class' => '')); ?>
                                </p>
                                <div class="col-sm-12 p-0 n-field-box">
                                    <div class="in-bookingform-field-droop-main">
                                        <input name="user_total_amt" id="user_total_amt" value="" type="number" class="popup-disc-fld" autocomplete="off"/>
                                    </div>
                                </div>
                            </div>
                            
                            
                            
                            
                            
                            
                            
                            <div class="row m-0 n-field-main">
                                <p>
                                    <?php echo form_label('Notes ', 'user_notes',  array( 'class' => '')); ?>
                                </p>
                                <div class="col-sm-12 p-0 n-field-box">
                                    <div class="in-bookingform-field-droop-main">
                                        <textarea name="user_notes" id="user_notes" class="" placeholder="Note to Driver"></textarea>
                                    </div>
                                </div>
                            </div>

                            
                            
                            <div class="row m-0 n-field-main">
                                <p>
                                    <input type="hidden" id="customer-address-id-user" name="customer-address-id-user" />
                                    <input type="hidden" id="booking-id-user" />
                                </div>
                                
                                
                            <div class="col-sm-12 p-0">
                                    
                                 <input type="button" class="n-btn" id="add-user-booking-nform" value="Search For Maids" />
                                   
                                </div>
                                
                                
                                
                         </div>
                         
                         
                         <div class="col-sm-4">
                              <div id="customer-detail-popup" class="col-md-12 col-sm-12" style="float:right;display:none;">
                                <div class="booking_form">
                                    <div class="row m-0 n-field-main">
                                        
                                    </div>
                                    
                                    
                                    
                                    <div class="row m-0 n-field-main">
                                        <p>
                                            <?php echo form_label('Name ', '',  array( 'class' => '')); ?>
                                </p>
                                        <div class="col-sm-12 p-0 n-field-box">
                                            <div class="in-bookingform-field-droop-main" id="b-customer-ids-cell">
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                    
                                    <div class="row m-0 n-field-main">
                                        <p>
                                            <?php echo form_label('Mobile ', '',  array( 'class' => '')); ?>
                                </p>
                                        <div class="col-sm-12 p-0 n-field-box">
                                            <div class="in-bookingform-field-droop-main" id="b-customer-mobile-cell">
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                    
                                    <div class="row m-0 n-field-main">
                                        <p>
                                            <?php echo form_label('Email ', '',  array( 'class' => '')); ?>
                                </p>
                                        <div class="col-sm-12 p-0 n-field-box">
                                            <div class="in-bookingform-field-droop-main" id="b-customer-email-cell">
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                    
                                    <div class="row m-0 n-field-main">
                                        <p>
                                            <?php echo form_label('Area ', '',  array( 'class' => '')); ?>
                                </p>
                                        <div class="col-sm-12 p-0 n-field-box">
                                            <div class="in-bookingform-field-droop-main" id="b-customer-area-cell">
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                    
                                    <div class="row m-0 n-field-main">
                                        <p>
                                            <?php echo form_label('Zone ', '',  array( 'class' => '')); ?>
                                </p>
                                        <div class="col-sm-12 p-0 n-field-box">
                                            <div class="in-bookingform-field-droop-main" id="b-customer-zone-cell">
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                    
                                    <div class="row m-0 n-field-main">
                                        <p>
                                            <?php echo form_label('Address ', '',  array( 'class' => '')); ?>
                                </p>
                                        <div class="col-sm-12 p-0 n-field-box">
                                            <div class="in-bookingform-field-droop-main" id="b-customer-address-cell">
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                    
                                    <div class="row m-0 n-field-main">
                                        <p>
                                            <?php echo form_label('Customer Book Type ', '',  array( 'class' => '')); ?>
                                </p>
                                        <div class="col-sm-12 p-0 n-field-box">
                                            <div class="in-bookingform-field-droop-main" id="b-customer-booktype-cell">
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                    
                                    <div class="row m-0 n-field-main">
                                        <p>
                                            <?php echo form_label('Payment Mode ', '',  array( 'class' => '')); ?>
                                </p>
                                        <div class="col-sm-12 p-0 n-field-box">
                                            <div class="in-bookingform-field-droop-main" id="b-customer-paymode-cell">
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                         </div>
                            
                            
                        </div>
                        <?php echo form_close(); ?>
                        
                        <div class="col-md-6 col-sm-6 confi-det-cont-det no-right-padding">
                            <div id="customer-add-popup" class="col-md-12 col-sm-12" style="display:none;">
                                <div class="booking_form">
                           
                           
                                    <form name="customer-popup" id="customer-popup" method="POST">
                                        <div class="row m-0 n-field-main">
                                            <p>
                                                <?php echo form_label('Customer ', 'customer_name',  array( 'class' => '')); ?>
                                </p>
                                            <div class="col-sm-12 p-0 n-field-box">
                                                <div class="in-bookingform-field-droop-main">
                                                    <input type="text" name="customer_name" id="customer_name" />
                                                </div>
                                            </div>
                                        </div> 
                                        
                                        
                                        
                                        
                                        <div class="row m-0 n-field-main">
                                            <p>
                                                <?php echo form_label('Email ', 'customer_email',  array( 'class' => '')); ?>
                                </p>
                                            <div class="col-sm-12 p-0 n-field-box">
                                                <div class="in-bookingform-field-droop-main">
                                                    <input type="text" name="customer_email" id="customer_email" />
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                        
                                        
                                        <div class="row m-0 n-field-main">
                                            <p>
                                                <?php echo form_label('Area ', 'area',  array( 'class' => '')); ?>
                                </p>
                                            <div class="col-sm-12 p-0 n-field-box">
                                                <div class="in-bookingform-field-droop-main">
                                                    <select name="area" id="area" class="sel3">
                                                        <option value="" selected>Select Area</option>
                                                         <?php
                                                        if (count($areas) > 0) {
                                                            foreach ($areas as $areasVal) {
                                                                ?>
                                                                <option value="<?php echo $areasVal['area_id'] ?>"><?php echo $areasVal['area_name'] ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                        
                                        
                                        <div class="row m-0 n-field-main">
                                            <p>
                                                <?php echo form_label('Apartment No ', 'apartmentnos',  array( 'class' => '')); ?>
                                </p>
                                            <div class="col-sm-12 p-0 n-field-box">
                                                <div class="in-bookingform-field-droop-main">
                                                    <input type="text" name="apartmentnos" id="apartmentnos" />
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                        
                                        
                                        <div class="row m-0 n-field-main">
                                            <p>
                                                <?php echo form_label('Address ', 'address',  array( 'class' => '')); ?>
                                </p>
                                            <div class="col-sm-12 p-0 n-field-box">
                                                <div class="in-bookingform-field-droop-main">
                                                    <input type="text" name="address" id="address" onkeyup="loadLocationField('address');" />
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                        
                                        
                                        <div class="row m-0 n-field-main">
                                            <p>
                                                <?php echo form_label('Mobile Number ', 'mobile_number1',  array( 'class' => '')); ?>
                                </p>
                                            <div class="col-sm-12 p-0 n-field-box">
                                                <div class="in-bookingform-field-droop-main">
                                                    <input type="text" name="mobile_number1" id="mobile_number1" />
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                        
                                        
                                        <div class="row m-0 n-field-main">
                                            <p>
                                                
                                            </div>
                                            
                                            
                                        <div class="col-sm-12 p-0 n-field-box">
                                                <div class="in-bookingform-field-droop-main">
                                                    <input type="button"  id="popup-add-customer" value="Add" class="save-but"> 
                                                </div>
                                            </div>
                                            
                                            
                                   </form>   
                                            
                                </div>
                                   
                             </div>
                       </div>
                            
                            
                            
            </div>
            
            <div id="maid_search" style="border: medium none !important;"></div>
     
     
    </div><!--row content-wrapper end--> 
</section><!--welcome-text end-->