<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class File_upload
{
    public function save_from_xhr($target_file_name)
    {
		$input = fopen("php://input", "r");
		$temp = tmpfile();
		$real_size = stream_copy_to_stream($input, $temp);
		fclose($input);
		
		if ($real_size != $this->get_size())
		{            
			return FALSE;
		}
		
		$target = fopen($target_file_name, 'w');        
		fseek($temp, 0, SEEK_SET);
		stream_copy_to_stream($temp, $target);
		fclose($target);
		
		return TRUE;
    }
	
	public function save_from_form($target_file_name, $file_input = 'qqfile')
	{
		if(!move_uploaded_file($_FILES[$file_input]['tmp_name'], $target_file_name))
		{
			return false;
		}
		
		return true;
	}
	
	public function get_size()
	{
		if(isset($_SERVER['CONTENT_LENGTH']))
		{
			return (int)$_SERVER['CONTENT_LENGTH'];            
		} 
		else 
		{
			throw new Exception('Getting content length is not supported');
		}  
	}
}
